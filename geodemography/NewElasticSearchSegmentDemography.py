import logging
import json
import datetime
import dateutil.relativedelta
import math
from ElasticSearchWrapperDemography import ElasticSearchWrapperDemography

class ElasticSearchSegmentDemography(object):
    def __init__(self, aggregatedApi = "33"):
        self.logger = logging.getLogger("segmentsSearch_log")
        self.aggregatedApi = aggregatedApi
        self.elasticSearchClient = ElasticSearchWrapperDemography(indexName = "geodemography")

    def getBasicQuery(self):
        return {"query":{"bool":{"must":[]}},"size":0}

    def getAllQuery(self):
        return {"query":{"match_all":{}},"size":0}

    def buildCityQuery(self, attributeJson):
        cities = []

        for city in attributeJson["city"]:
            cities.append({"term":{"cityCounter.city":{"value":str(city)}}})
                
        return {"nested": {"path": "cityCounter", "query": {"bool": {"should": cities}}}}

    def buildCityCounterQuery(self, attributeJson): 
        cities = []

        for city in attributeJson["cityCounter"]:
            cities.append({"range":{"cityCounter.counter":{"gte":int(city["min"]),"lte":int(city["max"])}}}) 
                
        return {"nested": {"path": "cityCounter", "query": {"bool": {"should": cities}}}}

    def buildCategoryQuery(self, attributeJson):
        categories = []

        for category in attributeJson["category"]:
            categories.append({"nested": {"path": "cats", "query": {"term":{"cats.category":{"value":str(category)}}}}})
                
        return {"bool": {"should": categories}}

    def buildSubCategoryQuery(self, attributeJson):
        subcategories = []

        for subcategory in attributeJson["subcategory"]:
            subcategories.append({"nested": {"path": "subcats", "query": {"term":{"subcats.subcat":{"value":str(subcategory)}}}}})

        return {"bool": {"should": subcategories}}

    def buildDateRangeQuery(self, attributeJson):
        return {"range":{"createdAtTimestamp":{"format":"epoch_millis","gte":int(attributeJson["dateRange"]["max"]),"lte":int(attributeJson["dateRange"]["min"])}}}

    def buildDayOfWeekQuery(self, attributeJson):
        dayOfWeeks = []    

        for day in attributeJson["day"]:
            dayOfWeeks.append({"script":{"script":{"source":"doc['createdAtTimestamp'].value.getDayOfWeekEnum().getValue() == params.day","lang":"painless","params":{"day":int(day)}}}})
  
        return {"bool": {"should": dayOfWeeks}}

    def buildTimeSlotQuery(self, attributeJson):
        timeSlots = []

        for slot in attributeJson["slot"]:
            timeSlots.append({"script":{"script":{"source":"doc['createdAtTimestamp'].value.getHour() >= params.min && doc['createdAtTimestamp'].value.getHour() < params.max","lang":"painless","params":{"min":int(slot["min"]),"max":int(slot["max"])}}}})

        return {"bool": {"should": timeSlots}}

    def buildGenderQuery(self, attributeJson):
        genders = []

        for gender in attributeJson["gender"]:
            genders.append({"nested":{"path":"genderCounter","query":{"term":{"genderCounter.gender":{"value":gender}}}}})

        return {"bool":{"should": genders}}

    def buildGenderCounterQuery(self, attributeJson): 
        genders = []

        for gender in attributeJson["genderCounter"]:
            genders.append({"range":{"genderCounter.counter":{"gte":int(gender["min"]),"lte":int(gender["max"])}}}) 
                
        return {"nested": {"path": "genderCounter", "query": {"bool": {"should": genders}}}}

    def buildLanguageQuery(self, attributeJson):
        languages = []

        for lang in attributeJson["language"]:
            languages.append({"term":{"languageCounter.language":{"value":lang}}})

        return {"nested":{"path":"languageCounter","query":{"bool":{"should": languages}}}}

    def buildLanguageCounterQuery(self, attributeJson): 
        languages = []

        for lang in attributeJson["languageCounter"]:
            genders.append({"range":{"languageCounter.counter":{"gte":int(lang["min"]),"lte":int(lang["max"])}}}) 
                
        return {"nested": {"path": "languageCounter", "query": {"bool": {"should": languages}}}}

    def buildAgeQuery(self, attributeJson):
        ages = []

        for age in attributeJson["age"]:
            ages.append({"bool":{"must":[{"range":{"ageGroupCounter.max_age":{"gte":age["min"],"lte":age["max"]}}},{"range":{"ageGroupCounter.min_age":{"gte":age["min"],"lte":age["max"]}}}]}})

        return {"nested":{"path":"ageGroupCounter","query":{"bool":{"should":ages}}}}

    def buildAgeCounterQuery(self, attributeJson): 
        ages = []

        for age in attributeJson["ageGroupCounter"]:
            ages.append({"range":{"ageGroupCounter.counter":{"gte":int(age["min"]),"lte":int(age["max"])}}}) 
                
        return {"nested": {"path": "ageGroupCounter", "query": {"bool": {"should": ages}}}}

    def buildModelQuery(self, attributeJson):
        models = []

        for model in attributeJson["model"]:
            models.append({"nested":{"path":"brandModelCounter","query":{"match":{"brandModelCounter.model":model}}}})

        return {"bool":{"must": models}}

    def buildModelCounterQuery(self, attributeJson): 
        models = []

        for model in attributeJson["brandModelCounter"]:
            models.append({"range":{"brandModelCounter.counter":{"gte":int(model["min"]),"lte":int(model["max"])}}}) 
                
        return {"nested": {"path": "brandModelCounter", "query": {"bool": {"should": models}}}}

    def buildBrandQuery(self, attributeJson):
        brands = []

        for brand in attributeJson["brand"]:
            brands.append({"nested":{"path":"brandModelCounter","query":{"term":{"brandModelCounter.brand":{"value":brand}}}}})

        return {"bool":{"must": brands}} 

    def buildBrandCounterQuery(self, attributeJson): 
        brands = []

        for brand in attributeJson["brandModelCounter"]:
            brands.append({"range":{"brandModelCounter.counter":{"gte":int(brand["min"]),"lte":int(brand["max"])}}}) 
                
        return {"nested": {"path": "brandModelCounter", "query": {"bool": {"should": brands}}}}

    def searchForSegment(self, attributeJson):
        if len(attributeJson):
            esQuery = self.getBasicQuery() 
    
            if "city" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildCityQuery(attributeJson))

            if "category" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildCategoryQuery(attributeJson))

            if "subcategory" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildSubCategoryQuery(attributeJson))

            if "dateRange" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildDateRangeQuery(attributeJson))

            if "day" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildDayOfWeekQuery(attributeJson))

            if "slot" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildTimeSlotQuery(attributeJson))

            if "brand" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildBrandQuery(attributeJson))

            if "model" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildModelQuery(attributeJson))

            if "language" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildLanguageQuery(attributeJson))

            if "age" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildAgeQuery(attributeJson))

            if "gender" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildGenderQuery(attributeJson))

            if "cityCounter" in attributeJson:
                esQuery["query"]["bool"]["must"].append(self.buildCityCounterQuery(attributeJson))
        else:
            esQuery = self.getAllQuery() 

        return esQuery

    def getCountForSegment(self, segment):
        response = self.elasticSearchClient.getDataFromElasticSearchDemography(self.searchForSegment(segment))
        responseJson = json.loads(response.decode('utf8').replace("'", '"'))
        self.logger.info(responseJson)
        return self.getSegmentCountJson(segment["name"], responseJson, segment)

    def getSegmentCountJson(self, segmentName, responseJson, segment):
        apiKey = segment['apiKey']
        segmentCount = responseJson['hits']['total']
        segmentCountJson = {} 
        segmentCountJson[segmentName] = {}
        segmentCountJson[segmentName]["value"] = segmentCount
        segmentCountJson[segmentName]["type"] = "demography"
        return segmentCountJson


    

# attributeJson = {}

# attributeJson = {"city":["pune", "kolkata"], "dateRange":{"max": 1552147857200,"min": 1552147857200}, "day":["1","2"]}

# attributeJson = {"city":["pune", "kolkata"], "cityCounter": {"value": 2, "condition": "gte"}}

# attributeJson = {"day":["1"]}

# attributeJson = {"slot": [{"min": 8, "max": 12}, {"min": 5, "max": 8}]}

# attributeJson = {"cityCounter": [{"min": 2, "max": 2}, {"min": 3, "max": 3}], "city":["alappuzha"]}

# attributeJson = {"category":["14","12"], "subcategory":["14","12"]}

# attributeJson = {"category":[8, 9], "subcategory":["38","39"], "gender":["female"]}

attributeJson = {"city":["mumbai","bengaluru"],"category":[14,8,3],"subCategory":[],"gender":["Male"],"age":[{"min":17,"max":24},{"min":24,"max":35},{"min":35,"max":45},{"min":45,"max":55},{"min":55,"max":65}],"language":["hi","Hindi","en","English"]}

# attributeJson = {"city":["Kolkata", "Bangalore"], "category":["14","12"], "subcategory":["14","12"], "dateRange":{"max": 1552147857200,"min": 1552147857200}, "day":["1","2"]}

# cityQuery = { "should"/"must" : [ city1, city2 ] }

# attributeJson = {"age": [{"max":25, "min":20 },{"max":30, "min":40}]}

# attributeJson = {"brand":["none","samsung"]}

# attributeJson = {"gender":["Male","Female"]}

# --------------------------------------------------- React query to convert queryList: ---------------------------------------------------

# [...(new Set(window.instanceOfThis.state.queryList.map(q => q.city)))]

# [...(new Set(window.instanceOfThis.state.queryList.map(q => q.day.value)))]

# [...(new Set(window.instanceOfThis.state.queryList.filter(q => q.dimension.name == "subcat").map(q1 => q1.dimension.value)))]

# [...(new Set(window.instanceOfThis.state.queryList.filter(q => q.dimension.name == "cat").map(q1 => q1.dimension.value)))]

# [...(new Set(window.instanceOfThis.state.queryList.map(q => q.slot.min)))]

# [...(new Set(window.instanceOfThis.state.queryList.map(q => q.slot.max)))]

# window.instanceOfThis.state.queryList[0].duration.startDate

# window.instanceOfThis.state.queryList[0].duration.endDate

obj = ElasticSearchSegmentDemography()

responseQuery = obj.getCountForSegment(attributeJson)

import json

print(json.dumps(responseQuery))
