from ElasticSearchWrapper import ElasticSearchWrapper
import argparse
import time
import re

parser = argparse.ArgumentParser(description='Script to map newly added demography data with geodata on daily basis.')
parser.add_argument('--dateString', type=str, help='Pass dateString in the format of DD/MM/YY')
args = parser.parse_args()

if args.dateString:
    string = args.dateString
    pattern = '[0-9]{2,2}/[0-9]{2,2}/[0-9]{2,2}'
    match = re.match(pattern, string)

    if match and len(string) == 8:
        d = time.strptime(string,'%d/%m/%y')
        currDate = int(time.mktime(d)*1000)
        nextDate = currDate + 86400000

        demographyQuery = {"query":{"bool":{"should":[{"range":{"createdAtTimestamp":{"gte":currDate,"lt":nextDate}}},{"range":{"lastUpdatedTimestamp":{"gte":currDate,"lt":nextDate}}}]}}}
        response = demographyMapper.getDataFromElasticSearch(geodataQuery, "demography")
        responseJson = json.loads(response)
        print(responseJson["hits"]["total"])
        for demo in responseJson["hits"]["hits"]:
            gid = demo["_id"]
            demographyJson = json.loads(demographyMapper.getDataFromElasticSearchTest(gid, "geodemography"))
            gidHash = demo
            gidHash['cats'] = []
            gidHash['subcats'] = []

            if demographyJson["found"]:
                gidHash['cats'] = demographyJson['_source']['cats']
                gidHash['subcats'] = demographyJson['_source']['subcats']
            
            # demographyMapper.storeDataInElasticSearch(gid, gidHash, "geodemography")

        geodataQuery = {"aggs":{"gids":{"terms":{"field":"gid"},"aggs":{"category":{"terms":{"field":"categoryId","size":40}},"subcategory":{"terms":{"field":"subcategoryId","size":210}}}}},"size":0,"query":{"bool":{"must":[{"nested":{"path":"info","query":{"range":{"info.createdAt":{"gte":currDate,"lt":nextDate}}}}}]}}}
        response = demographyMapper.getDataFromElasticSearch(geodataQuery, "geodataprocessedinsights")
        responseJson = json.loads(response)

        for geoData in responseJson["aggregations"]["gids"]["buckets"]:
            gid = geoData["key"]
            currentTime = int(round(time.time() * 1000))
            gidHash = {"createdAtTimestamp":str(currentTime),"languageCounter":[],"brandModelCounter":[],"cats":[],"subcats":[],"gid":gid,"ageGroupCounter":[],"lastUpdatedTimestamp":currentTime}
            demographyJson = json.loads(demographyMapper.getDataFromElasticSearchTest(gid, "geodemography"))

            if demographyJson["found"]:
                gidHash = demographyJson['_source']
                gidHash['cats'] = []
                gidHash['subcats'] = []

            # gidHash['cats'] = geoData["category"]["buckets"]

            for category in geoData["category"]["buckets"]:
                gidHash['cats'].append({"category":category["key"],"counter":category["doc_count"]})

            # gidHash['subcats'] = geoData["subcategory"]["buckets"]
            for subcategory in geoData["subcategory"]["buckets"]:
                gidHash['subcats'].append({"subCategory":subcategory["key"],"counter":subcategory["doc_count"]})

            # demographyMapper.storeDataInElasticSearch(gid, gidHash, "geodemography")

        # print(currDate)
        # print(nextDate)
    else:
        parser.error('Invalid dateString. Valid format DD/MM/YY')
else:
    parser.error('Provide dateString. Valid format DD/MM/YY')
