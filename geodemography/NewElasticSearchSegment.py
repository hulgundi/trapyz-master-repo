import logging
import json
import datetime
import dateutil.relativedelta
import math

def getBasicQuery():
    return {"query":{"bool":{"must":[]}},"size":0}

def getAllQuery():
    return {"query":{"match_all":{}},"size":0}

def buildCityQuery(attributeJson):
    cities = []

    for city in attributeJson["city"]:
        cities.append({"term":{"cityCounter.city":{"value":str(city)}}})
                
    return {"nested": {"path": "cityCounter", "query": {"bool": {"should": cities}}}}

def buildCityCounterQuery(attributeJson): 
    cities = []

    for city in attributeJson["cityCounter"]:
        cities.append({"range":{"cityCounter.counter":{"gte":int(city["min"]),"lte":int(city["max"])}}}) 
                
    return {"nested": {"path": "cityCounter", "query": {"bool": {"should": cities}}}}

def buildCategoryQuery(attributeJson):
    categories = []

    for category in attributeJson["category"]:
        categories.append({"nested": {"path": "category", "query": {"term":{"category.key":{"value":str(category)}}}}})
                
    return {"bool": {"should": categories}}

# buildCategoryCounterQuery(attributeJson) :: Need to check whether cats.doc_count should be nested or not

def buildSubCategoryQuery(attributeJson):
    subcategories = []

    for subcategory in attributeJson["subcategory"]:
        # subcategories.append({"term":{"subcategory.key":{"value":str(subcategory)}}})
        subcategories.append({"nested": {"path": "subcategory", "term":{"subcategory.key":{"value":str(subcategory)}}}})

    # return {"nested": {"path": "subcategory", "query": {"bool": {"should": subcategories}}}}
    return {"bool": {"should": subcategories}}

# buildSubCategoryCounterQuery(attributeJson) :: Need to check whether subcats.doc_count should be nested or not

def buildDateRangeQuery(attributeJson):
    return {"range":{"createdAtTimestamp":{"format":"epoch_millis","gte":int(attributeJson["dateRange"]["max"]),"lte":int(attributeJson["dateRange"]["min"])}}}

def buildDayOfWeekQuery(attributeJson):
    dayOfWeeks = []    

    for day in attributeJson["day"]:
        dayOfWeeks.append({"script":{"script":{"source":"doc['createdAtTimestamp'].value.getDayOfWeekEnum().getValue() == params.day","lang":"painless","params":{"day":int(day)}}}})
  
    return {"bool": {"should": dayOfWeeks}}

def buildTimeSlotQuery(attributeJson):
    timeSlots = []

    for slot in attributeJson["slot"]:
        timeSlots.append({"script":{"script":{"source":"doc['createdAtTimestamp'].value.getHour() >= params.min && doc['createdAtTimestamp'].value.getHour() < params.max","lang":"painless","params":{"min":int(slot["min"]),"max":int(slot["max"])}}}})

    return {"bool": {"should": timeSlots}}

def buildGenderQuery(attributeJson):
    genders = []

    for gender in attributeJson["gender"]:
        genders.append({"nested":{"path":"genderCounter","query":{"term":{"genderCounter.gender":{"value":gender}}}}})

    return {"bool":{"should": genders}} # Need to modify according to AND/OR

def buildGenderCounterQuery(attributeJson): 
    genders = []

    for gender in attributeJson["genderCounter"]:
        genders.append({"range":{"genderCounter.counter":{"gte":int(gender["min"]),"lte":int(gender["max"])}}}) 
                
    return {"nested": {"path": "genderCounter", "query": {"bool": {"should": genders}}}}

def buildLanguageQuery(attributeJson):
    languages = []

    for lang in attributeJson["language"]:
        languages.append({"term":{"languageCounter.language":{"value":lang}}})

    return {"nested":{"path":"languageCounter","query":{"bool":{"should": languages}}}}

def buildLanguageCounterQuery(attributeJson): 
    languages = []

    for lang in attributeJson["languageCounter"]:
        genders.append({"range":{"languageCounter.counter":{"gte":int(lang["min"]),"lte":int(lang["max"])}}}) 
                
    return {"nested": {"path": "languageCounter", "query": {"bool": {"should": languages}}}}

def buildAgeQuery(attributeJson):
    ages = []

    for age in attributeJson["age"]:
        ages.append({"bool":{"must":[{"range":{"ageGroupCounter.max_age":{"gte":age["min"],"lte":age["max"]}}},{"range":{"ageGroupCounter.min_age":{"gte":age["min"],"lte":age["max"]}}}]}})

    return {"nested":{"path":"ageGroupCounter","query":{"bool":{"should":ages}}}}

def buildAgeCounterQuery(attributeJson): 
    ages = []

    for age in attributeJson["ageGroupCounter"]:
        ages.append({"range":{"ageGroupCounter.counter":{"gte":int(age["min"]),"lte":int(age["max"])}}}) 
                
    return {"nested": {"path": "ageGroupCounter", "query": {"bool": {"should": ages}}}}

def buildModelQuery(attributeJson): # For model use match query
    models = []

    for model in attributeJson["model"]:
        # models.append({"nested":{"path":"brandModelCounter","query":{"term":{"brandModelCounter.model":{"value":model}}}}})
        models.append({"nested":{"path":"brandModelCounter","query":{"match":{"brandModelCounter.model":model}}}})

    return {"bool":{"must": models}}

def buildModelCounterQuery(attributeJson): 
    models = []

    for model in attributeJson["brandModelCounter"]:
        models.append({"range":{"brandModelCounter.counter":{"gte":int(model["min"]),"lte":int(model["max"])}}}) 
                
    return {"nested": {"path": "brandModelCounter", "query": {"bool": {"should": models}}}}

def buildBrandQuery(attributeJson):
    brands = []

    for brand in attributeJson["brand"]:
        brands.append({"nested":{"path":"brandModelCounter","query":{"term":{"brandModelCounter.brand":{"value":brand}}}}})

    return {"bool":{"must": brands}} # Need to modify according to AND/OR

def buildBrandCounterQuery(attributeJson): 
    brands = []

    for brand in attributeJson["brandModelCounter"]:
        brands.append({"range":{"brandModelCounter.counter":{"gte":int(brand["min"]),"lte":int(brand["max"])}}}) 
                
    return {"nested": {"path": "brandModelCounter", "query": {"bool": {"should": brands}}}}

def searchForSegment(attributeJson):
    # if "city" in attributeJson or  "subcategory" in attributeJson or "category" in attributeJson or "day" in attributeJson or "dateRange" in attributeJson:
    if len(attributeJson):
        esQuery = getBasicQuery() 
    
        if "city" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildCityQuery(attributeJson))

        if "category" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildCategoryQuery(attributeJson))

        if "subcategory" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildSubCategoryQuery(attributeJson))

        if "dateRange" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildDateRangeQuery(attributeJson))

        if "day" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildDayOfWeekQuery(attributeJson))

        if "slot" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildTimeSlotQuery(attributeJson))

        if "brand" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildBrandQuery(attributeJson))

        if "model" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildModelQuery(attributeJson))

        if "language" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildLanguageQuery(attributeJson))

        if "age" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildAgeQuery(attributeJson))

        if "gender" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildGenderQuery(attributeJson))

        if "cityCounter" in attributeJson:
            esQuery["query"]["bool"]["must"].append(buildCityCounterQuery(attributeJson))
    else:
        esQuery = getAllQuery() 

    return esQuery

# attributeJson = {}

# attributeJson = {"city":["pune", "kolkata"], "dateRange":{"max": 1552147857200,"min": 1552147857200}, "day":["1","2"]}

attributeJson = {"city":["pune", "kolkata"], "cityCounter": {"value": 2, "condition": "gte"}}

# attributeJson = {"day":["1"]}

attributeJson = {"slot": [{"min": 8, "max": 12}, {"min": 5, "max": 8}]}

attributeJson = {"cityCounter": [{"min": 2, "max": 2}, {"min": 3, "max": 3}], "city":["alappuzha"]}

attributeJson = {"category":["14","12"], "subcategory":["14","12"]}

attributeJson = {"category":[8, 9]}

# attributeJson = {"city":["Kolkata", "Bangalore"], "category":["14","12"], "subcategory":["14","12"], "dateRange":{"max": 1552147857200,"min": 1552147857200}, "day":["1","2"]}

# cityQuery = { "should"/"must" : [ city1, city2 ] }

# attributeJson = {"age": [{"max":25, "min":20 },{"max":30, "min":40}]}

# attributeJson = {"brand":["none","samsung"]}

# attributeJson = {"gender":["Male","Female"]}

# --------------------------------------------------- React query to convert queryList: ---------------------------------------------------

# [...(new Set(window.instanceOfThis.state.queryList.map(q => q.city)))]

# [...(new Set(window.instanceOfThis.state.queryList.map(q => q.day.value)))]

# [...(new Set(window.instanceOfThis.state.queryList.filter(q => q.dimension.name == "subcat").map(q1 => q1.dimension.value)))]

# [...(new Set(window.instanceOfThis.state.queryList.filter(q => q.dimension.name == "cat").map(q1 => q1.dimension.value)))]

# [...(new Set(window.instanceOfThis.state.queryList.map(q => q.slot.min)))]

# [...(new Set(window.instanceOfThis.state.queryList.map(q => q.slot.max)))]

# window.instanceOfThis.state.queryList[0].duration.startDate

# window.instanceOfThis.state.queryList[0].duration.endDate

responseQuery = searchForSegment(attributeJson)

import json

print(json.dumps(responseQuery))
