import boto3
from datetime import datetime, timedelta
import logging
import random
import requests
import json
import itertools
from requests_aws4auth import AWS4Auth
import os

class ElasticSearchWrapperDemography(object):
    def __init__(self, indexName = "geodemography"):
        self.elasticSearchEndpoint = "http://juno-es.trapyz.com:9200/"
        self.indexName = indexName
        self.maxDistanceSupported = 300
        self.logger = logging.getLogger("elasticsearch_log")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler("elasticsearch.log")
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        # logger.debug("Instantiated ProcessedUserDataHelper")
    
    def send_signed(self, method, url, service="es", region="us-east-1", body=None):
        credentials = boto3.Session().get_credentials()
        auth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)
        fn = getattr(requests, method)
        response = fn(url, auth=auth, data=body, headers={"Content-Type":"application/json"})
        self.logger.debug ("response: %s", response)
        if response.status_code == 200 or response.status_code == 201:
            htmlBody = response.content
            self.logger.debug ("html body: %s", htmlBody) 
            return htmlBody

        return None

    def getDataFromElasticSearchDemography(self, queryDict):
        url = self.elasticSearchEndpoint + "geodemography/_search"
        self.logger.debug ("url: %s data : %s", url, json.dumps(queryDict))
        return self.send_signed("get", url, body=json.dumps(queryDict))
 

