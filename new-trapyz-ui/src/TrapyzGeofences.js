import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import trapyzLogo from './trapyz-white-icon.png';
import dailyHuntLogo from './dailyhunt-logo.png';
import trapyzLogo2 from './trapyz-logo.png';
import trapyzMapLogo from './google-map-icon.png';
import trapyzAdvanceLogo from './advance_settings.png';
import chevronDown from './logout-image.png';
import segmentIcon from './segment-icon.png';
import loaderImage from './loader.gif';
import TrapyzLoginPage from './TrapyzLoginPage';
import TrapyzMapPage from './TrapyzMapPage';
import TrapyzAdvancedPage from './TrapyzAdvancedPage';
import './TrapyzHomePage.css';
import TrapyzHomePage from './TrapyzHomePage';
 
function makeAjaxGetRequest(requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();
  
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };
  if (synchType === undefined) {
    synchType = true;
  }
  xmlHttp.open("GET", requestUrl, synchType);
  xmlHttp.send(null);
}

function makeAjaxPostRequest(str, requestUrl, callbackFunction, misc, synchType) {
    var xmlHttp = new XMLHttpRequest();
    var formData = new FormData();

  formData.append('segment', str);
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };
  if (synchType === undefined) {
    synchType = true;
  }
  xmlHttp.open("POST", requestUrl, synchType);
  xmlHttp.send(formData);
}

function preventDefaultAction(event) {
  event.stopPropagation();
}

function logout() {
  var items = ["uname", "name", "key"], item;

  for(item in items) {
    sessionStorage.removeItem(items[item]);  
  }
  ReactDOM.render(<TrapyzLoginPage />, document.getElementById('root'));  
}

function showMapPage() {
    ReactDOM.render(<TrapyzMapPage google={window.google} data={window.instanceOfThis.state.latLngList}/>, document.getElementById('root'));  
}

function showAdvancedPage() {
    ReactDOM.render(<TrapyzAdvancedPage/>, document.getElementById('root'));  
}

function showHomePage() {
    ReactDOM.render(<TrapyzHomePage />, document.getElementById('root'));  
}

function toggleLoader(flag) {
  if (flag) {
    document.getElementById("trapyz-main-tint-div").style.display = "block";
  } else {
    document.getElementById("trapyz-main-tint-div").style.display = "none";
  }
}

function loadCategories() {
    makeAjaxGetRequest("https://canvas.trapyz.com/getCategory.php", function(response) {
        var responseJson, keys = [], index, categoryList = [], categories = [];
	window.instanceOfThis.setState({categoriesList: []});

	if (response.responseText === "\n") {
	  responseJson = "";
	} else {
          responseJson = JSON.parse(response.responseText);
	}
	  
        keys = Object.keys(responseJson);

	for (index = 0; index < keys.length; index++) {
	    //categoryList.push(responseJson[keys[index]]);
	    categories.push(
		<option value={responseJson[keys[index]]}>
		    {responseJson[keys[index]]}
		</option>
            );
        }
	
        window.instanceOfThis.setState({categoriesList: categories});
      });  
}

function loadLatLng() {
    if(!window.instanceOfThis.props.data) {
	makeAjaxGetRequest("https://canvas.trapyz.com/getLatLngNew.php", function(response) {
            var responseJson, keys = [], latLng = [], index, latLngParts = [];
	    window.instanceOfThis.setState({latLngList: []});

	    if (response.responseText === "\n") {
		responseJson = "";
	    } else {
		responseJson = JSON.parse(response.responseText);
	    }
	    
            keys = Object.keys(responseJson);
            for (index = 0; index < keys.length; index++) {
		latLngParts = responseJson[keys[index]].split("_");
		latLng.push({ "latitude":parseFloat(latLngParts[0]), "longitude":parseFloat(latLngParts[1]),"sname":latLngParts[2]});

            }
            window.instanceOfThis.setState({latLngList: latLng});
	});  
    } else {
	window.instanceOfThis.setState({latLngList: window.instanceOfThis.props.data});
    }
}

function fetchLatLng() {
    var selectedCity, selectedCategory;
    selectedCity = document.getElementById('select-city').value;
    selectedCategory = document.getElementById('select-category').value;
    
    var query = "SELECT MasterRecordSet.* FROM MasterRecordSet where ", params = '', storeParameter, request = false;
    var cityParameter = "city LIKE '" + selectedCity + "'";
    
    var categoryParameter = " AND cat LIKE '" + selectedCategory + "'";

    params = query + cityParameter + categoryParameter; 
    console.log(params);
    toggleLoader(true);

    makeAjaxGetRequest(("https://canvas.trapyz.com/fetchLatLng.php?params="+encodeURIComponent(params)), function(response) {
        var responseJson, storeDetails, pin, keys = [], storeList = [], index, storeHash = {}, areasList = window.instanceOfThis.state.areaItems, areaMapList = {}, hash = {}, areaArray = [], noPinCode = [], absentPins = [], latLngList = [], geofence = {};
	console.log('stores loaded');
	window.instanceOfThis.setState({latLngList: JSON.parse(response.responseText)});
	//latLngList1 = JSON.parse(JSON.stringify(latLngList));
	var params1 = "SELECT geofences FROM geofences where ID = 9"
	makeAjaxGetRequest(("https://canvas.trapyz.com/fetchGeoFences.php?params="+encodeURIComponent(params1)), function(response) {
	    console.log(response.responseText);
	    var currentGeoFence = JSON.parse(response.responseText);
	    window.instanceOfThis.setState({geoFence: JSON.parse(response.responseText)});
	    var sampleGeoFence = window.instanceOfThis.state.geoFence[0];
	    window.instanceOfThis.state.latLngList.forEach(function(latLng) {
		geofence = {};
		geofence.latitude = latLng[0]; geofence.longitude = latLng[1];
		geofence.distance = document.getElementById('distance').value;
		geofence.dwell = document.getElementById('dwell').value;
		geofence.responsiveness = document.getElementById('responsiveness').value;
		geofence.message = document.getElementById('message').value;
		geofence.status = true;
		currentGeoFence.push(JSON.parse(JSON.stringify(geofence)));
	    });
	    window.instanceOfThis.setState({geoFence: currentGeoFence});
	    window.instanceOfThis.state.geoFence = currentGeoFence;
	    toggleLoader(false);
	});	    
    });
}

class TrapyzGeofences extends Component {
  constructor (props) {
    super(props);
    this.api = sessionStorage.getItem("key");
    this.topCategoriesDone = false;
    this.city = "";
    this.username = sessionStorage.getItem("name");
    this.state = {
      totalActive: 0,
      weeklyActive: 0,
      monthlyActive: 0,
      uniqueCount: 0,
      cityItems: [],
      top5Categories: [],
	latLngList: [],
	categoriesList: [],
	geoFence: []
    }
  }
    
  render() {

    window.instanceOfThis = this;

    if (!this.citiesDone) {
      this.citiesDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap?mapType=city", function(response) {
        var responseJson, keys = [], citiesList = [], index;

	if (response.responseText === "\n") {
	  responseJson = "";
	} else {
          responseJson = JSON.parse(response.responseText);
	}
	  
        keys = Object.keys(responseJson);
        for (index = 0; index < keys.length; index++) {
          citiesList.push(
            <option value={responseJson[keys[index]]}>
              {responseJson[keys[index]]}
            </option>
          );
        }
        window.instanceOfThis.setState({cityItems: citiesList});
      });
	loadCategories();
    }

      if (!this.topCategoriesDone) {
      var responseJson, sortedCategoriesResult = [], top5CategoriesList = [];
      
      this.topCategoriesDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=catgid&api=" + this.api, function(response) {
        var index, key;
        
        responseJson = JSON.parse(response.responseText);
        for (key in responseJson) {
          sortedCategoriesResult.push([key, responseJson[key]]);
        }
        sortedCategoriesResult.sort(function(a, b) {
          return b[1] - a[1];
        });
        for (index = 0; index < 5 && index < sortedCategoriesResult.length; index++) {
          var progressWidth = 0, styleString = "", sortedCategoryNumber = "";

          progressWidth = (sortedCategoriesResult[index][1] / sortedCategoriesResult[0][1]) * 100;

	  if(sortedCategoriesResult[index][1] >= 10000 && sortedCategoriesResult[index][1] < 1000000) {
	      sortedCategoryNumber = (Math.floor(sortedCategoriesResult[index][1]/1000)) + 'K';
	  } else if(sortedCategoriesResult[index][1] >= 1000000) {
	      sortedCategoryNumber = (Math.floor(sortedCategoriesResult[index][1]/1000000)) + 'M';
	  } else {
	    sortedCategoryNumber = sortedCategoriesResult[index][1];
	  }

          styleString = {
            width: progressWidth + "px"
          }
	    
          top5CategoriesList.push(
            <div className="trapyz-top-5-progress-content">
              <div className="trapyz-top-5-normal-heading">
                {sortedCategoriesResult[index][0]}
              </div>
              <div id="top-5-store-progress-0" className="trapyz-top-5-progress-bar" style={styleString}>
              </div>
              <div id="top-5-store-count-0" className="trapyz-top-5-normal-text">
                {sortedCategoryNumber}
              </div>
            </div>
          );
        }
        window.instanceOfThis.setState({top5Categories: top5CategoriesList});
      }); 
    }

 
    if (this.state.totalActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=total&api=" + this.api, function(response) {
        window.instanceOfThis.setState({totalActive: response.responseText});
      });  
    } 

    if (this.state.weeklyActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=week&api=" + this.api, function(response) {
        window.instanceOfThis.setState({weeklyActive: response.responseText});
      });  
    }

    if (this.state.monthlyActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=month&api=" + this.api, function(response) {
        window.instanceOfThis.setState({monthlyActive: response.responseText});
      });  
    } 
 
    return (
      <div className="trapyz-main-container">
        <div id="trapyz-main-tint-div" className="trapyz-main-tint">
          <img src={loaderImage} className="trapyz-loader-image" alt=""/>
        </div>
        <div className="trapyz-menu-panel">
          <div className="trapyz-main-logo">
            <img className="trapyz-logo-img" alt="" src={trapyzLogo}/>
          </div>
          <div className="trapyz-client-logo">
            <img className="trapyz-client-logo-img" alt="" src={dailyHuntLogo}/>
          </div>
          <div className="trapyz-unique-users-container">
            <div className="trapyz-total-users">
              <div id="total-active" className="trapyz-user-number">{this.state.totalActive}</div>
              Total<br/>Installed
            </div>
            <div className="trapyz-weekly-users">
              <div id="weekly-active" className="trapyz-user-number">{this.state.weeklyActive}</div>
              Weekly<br/>Active
            </div>
            <div className="trapyz-monthly-users">
              <div id="monthly-active" className="trapyz-user-number">{this.state.monthlyActive}</div>
              Monthly<br/>Active
            </div>
          </div>
          <div className="trapyz-segments-header">
            <img className="trapyz-segments-header-icon" alt="" src={segmentIcon}/>
            <div className="trapyz-segments-heading-container">
              <div className="trapyz-bold-heading">
                Segments
              </div>
              <div className="trapyz-normal-heading">
                Curated In-market segments
              </div>
            </div>
          </div>
          <div id="top-5-categories" className="trapyz-top-5-box">
            <div className="trapyz-top-5-bold-heading">
              Top 5 Visited Categories
            </div>
            {this.state.top5Categories}
          </div>
        </div>
        <div className="trapyz-canvas-panel">    
          <div className="trapyz-user-panel">
            <div className="trapyz-user-detail-container">
              <div className="trapyz-map-logo">
                <img className="trapyz-map-logo-image" src={trapyzMapLogo} alt="" onClick={showMapPage} title="Store Map"/>
              </div>
	      <div className="trapyz-user-image">
                <img className="trapyz-user-profile-image" src={trapyzLogo2} alt="" onClick={showHomePage} title="Home"/>
              </div>
	      <div className="trapyz-advance-image">
                <img className="trapyz-advance-logo-image" src={trapyzAdvanceLogo} alt="" onClick={showAdvancedPage} title="Advanced Settings"/>
              </div>
              <div className="trapyz-user-text">
                {this.username}
              </div>
              <div className="trapyz-logout-image-container" onClick={logout}>
                <img className="trapyz-logout-image" src={chevronDown} alt="" title="Log Out"/>
              </div>
            </div>
          </div>

	<div className="trapyz-filter-panel">
            <div className="trapyz-filter-heading">
              Geofences
            </div>
            
          </div>
          <hr className="trapyz-hor-rule"></hr>
            </div>
            <section className="form-geofence">
	      <div className="form-inline">
	        <div>
                  <select id="select-city" className="trapyz-segment-city-select">
                    <option value="">Select City</option>
                    {this.state.cityItems}
                  </select>
                </div>
	        <div>
                  <select id="select-category" className="trapyz-segment-city-select">
                    <option value="">Select Category</option>
                    {this.state.categoriesList}
                  </select>
                </div>
	      </div>

	      <div className="form-inline">
	        <div>
	          <label className="trapyz-segment-filter-heading">Distance</label>
                  <input type="text" id="distance"/>
                </div>
	        <div>
	          <label className="trapyz-segment-filter-heading">Dwell</label>
                  <input type="text" id="dwell"/>
                </div>
              </div>

              <div className="form-inline">
	        <div>
	          <label className="trapyz-segment-filter-heading">Responsiveness</label>
                  <input type="text" id="responsiveness"/>
                </div>
	        <div>
	          <label className="trapyz-segment-filter-heading">Message</label>
                  <input type="text" id="message"/>
                </div>
	      </div>
	    <button className="trapyz-segment-create-button" onClick={() => fetchLatLng()}>
              Fetch Lat/Lng
            </button>
	    
	</section>
	    <section className="hidden">
	    {JSON.stringify(this.state.geoFence)}
	    </section>
      </div>
    );
  }
}

export default TrapyzGeofences;
