// WARNING: DO NOT EDIT. This file is Auto-Generated by AWS Mobile Hub. It will be overwritten.

// Copyright 2017-2018 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to
// copy, distribute and modify it.

// AWS Mobile Hub Project Constants
const awsmobile = {
    'aws_app_analytics': 'enable',
    'aws_cognito_identity_pool_id': 'us-east-1:738ad915-f782-445c-8f74-53367f1ecf84',
    'aws_cognito_region': 'us-east-1',
    'aws_mobile_analytics_app_id': '3bc2f25cbe384889b7d8fdc9c90b1bdb',
    'aws_mobile_analytics_app_region': 'us-east-1',
    'aws_project_id': '1d3f66c5-dd7e-41d4-9178-99b3ebc0f30d',
    'aws_project_name': 'TrapyzInsightsReact',
    'aws_project_region': 'us-east-1',
    'aws_resource_name_prefix': 'trapyzinsightsreact-mobilehub-636890682',
}

export default awsmobile;
