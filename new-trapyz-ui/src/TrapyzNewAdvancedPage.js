import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import trapyzLogo from './trapyz-white-icon.png';
import dailyHuntLogo from './dailyhunt-logo.png';
import deleteButtonImg from './Button-Delete.png';
import trapyzLogo2 from './trapyz-logo.png';
import trapyzMapLogo from './google-map-icon.png';
import trapyzAdvanceLogo from './advance_settings.png';
import chevronDown from './logout-image.png';
import segmentIcon from './segment-icon.png';
import loaderImage from './loader.gif';
import TrapyzHomePage from './TrapyzHomePage';
import './TrapyzNewAdvancedPage.css';
import PieChart from 'react-minimal-pie-chart';
import TrapyzLoginPage from './TrapyzLoginPage';
import DonutChart from "react-svg-donut-chart";

function makeAjaxGetRequest(requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };

  if (synchType === undefined) {
    synchType = true;
  }

  xmlHttp.open("GET", requestUrl, synchType);
  xmlHttp.send(null);
}

function makeAjaxPostRequest(str, requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();
  var formData = new FormData();
  formData.append('segment', str);

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };

  if (synchType === undefined) {
    synchType = true;
  }

  xmlHttp.open("POST", requestUrl, synchType);
  xmlHttp.send(formData);
}

function preventDefaultAction(event) {
  event.stopPropagation();
}

/*function showHomePage() {
    ReactDOM.render(<TrapyzHomePage />, document.getElementById('root'));
}

function escapeEvent(event){
    event.preventDefault();
    if(event.keyCode === 27){
	showHomePage();
    }
}*/

function loadCities() {
  makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap?mapType=city", function(response) {
    var responseJson, keys = [], citiesList = [], index, hash = {'cityId':'', 'value':''};
  	window.instanceOfThis.setState({cityItems: []});

	  if (response.responseText === "\n") {
	    responseJson = "";
	  } else {
      responseJson = JSON.parse(response.responseText);
	  }

    keys = Object.keys(responseJson);

    for (index = 0; index < keys.length; index++) {
	    hash = {'cityId':'', 'value':''};
	    hash['cityId'] = keys[index];
	    hash['value'] = responseJson[keys[index]];
      citiesList.push(hash);
    }

    window.instanceOfThis.setState({cityItems: citiesList});
	  loadCategories();
  });
}

function loadCategories() {
  makeAjaxGetRequest("https://canvas.trapyz.com/getCategory.php", function(response) {
    var responseJson, keys = [], index, categoryList = [], categories = [], hash = {'categoryId':'', 'value':''};
	  window.instanceOfThis.setState({categoryItems: []});

	  if (response.responseText === "\n") {
	    responseJson = "";
	  } else {
      responseJson = JSON.parse(response.responseText);
	  }

    keys = Object.keys(responseJson);

	  for (index = 0; index < keys.length; index++) {
	    hash = {'categoryId':'', 'value':''};
	    hash['categoryId'] = keys[index];
	    hash['value'] = responseJson[keys[index]];
	    categories.push(hash);
    }

    window.instanceOfThis.setState({categoryItems: categories});
	  loadSubCategories();
  });
}

function loadSubCategories() {
  makeAjaxGetRequest("https://canvas.trapyz.com/getSubCategories.php", function(response) {
    var responseJson, keys = [], subCategoryList = [], index, hash = {'categoryId':'', 'name':'', 'subCategoryId':''};

	  if (response.responseText === "\n") {
	    responseJson = "";
	  } else {
      responseJson = JSON.parse(response.responseText);
	  }

	  keys = Object.keys(responseJson);

    for (index = 0; index < keys.length; index++) {
	    hash = {'categoryId':'', 'name':'', 'subCategoryId':''};
	    hash['subCategoryId'] = keys[index];
	    hash['name'] = responseJson[keys[index]].split('_')[0];
	    hash['categoryId'] = responseJson[keys[index]].split('_')[1];
	    subCategoryList.push(hash);
    }

    loader(false);
    window.instanceOfThis.setState({subCategoryItems: subCategoryList});

	  if(window.instanceOfThis.props.data) {
	    loadEdit();
	  }
  });
}

function loadAreas() {
  makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap-city.php", function(response) {
    var responseJson, keys = [], areasList = [], index, hash = {'cityId':'', 'value':'', 'areaId':''};
	  window.instanceOfThis.setState({areaItems: []});

	  if (response.responseText === "\n") {
	    responseJson = "";
	  } else {
      responseJson = JSON.parse(response.responseText);
	  }

    keys = Object.keys(responseJson);

    for (index = 0; index < keys.length; index++) {
	    hash = {'cityId':'', 'name':'', 'areaId':'', 'pin':''};
	    hash['areaId'] = keys[index];
	    hash['name'] = responseJson[keys[index]].split('_')[0];
	    hash['cityId'] = responseJson[keys[index]].split('_')[1];
	    hash['pin'] = responseJson[keys[index]].split('_')[2];
	    areasList.push(hash);
    }

    window.instanceOfThis.setState({areaItems: areasList});
	  loader(false);

	  if(window.instanceOfThis.props.data) {
	      editStateInitiate();
	  }
  });
}

function loadCitySpecificAreas() {
  if(document.getElementById('enter-seher').value != '' && window.instanceOfThis.state.currentCity != '') {
	  loader(true);

    makeAjaxGetRequest("https://canvas.trapyz.com/load-city-specific-areas.php?cityId=" + window.instanceOfThis.state.currentCity, function(response) {
      var responseJson, keys = [], areasList = [], index, hash = {'cityId':'', 'value':'', 'areaId':''};
	    window.instanceOfThis.setState({areaItems: []});

	    if (response.responseText === "\n") {
		    responseJson = "";
	    } else {
		    responseJson = JSON.parse(response.responseText);
	    }

      keys = Object.keys(responseJson);

      for (index = 0; index < keys.length; index++) {
		    hash = {'cityId':'', 'name':'', 'areaId':'', 'pin':''};
		    hash['areaId'] = keys[index];
		    hash['name'] = responseJson[keys[index]].split('_')[0];
		    hash['cityId'] = responseJson[keys[index]].split('_')[1];
		    hash['pin'] = responseJson[keys[index]].split('_')[2];
		    areasList.push(hash);
      }

	    window.instanceOfThis.setState({areaItems: areasList});
	    loader(false);
	    showAreaSelectionBox(true);
	  });
  }
}

function loadAreasEdit(pins) {
  loader(true);
  makeAjaxGetRequest("https://canvas.trapyz.com/load-edit-areas.php?pins=" + encodeURIComponent(pins), function(response) {
    var responseJson, keys = [], areasList = [], index, hash = {'cityId':'', 'value':'', 'areaId':''};
	  window.instanceOfThis.setState({areaItems: []});

	  if (response.responseText === "\n") {
	    responseJson = "";
	  } else {
      responseJson = JSON.parse(response.responseText);
	  }

    keys = Object.keys(responseJson);

    for (index = 0; index < keys.length; index++) {
	    hash = {'cityId':'', 'name':'', 'areaId':'', 'pin':''};
	    hash['areaId'] = keys[index];
	    hash['name'] = responseJson[keys[index]].split('_')[0];
	    hash['cityId'] = responseJson[keys[index]].split('_')[1];
	    hash['pin'] = responseJson[keys[index]].split('_')[2];
	    areasList.push(hash);
    }

	  window.instanceOfThis.setState({areaItems: areasList});
	  loader(false);
	  editStateInitiate();
  });
}

function loader(loading) {
  if(loading) {
	  document.getElementById('loader').classList.remove('hidden');
  } else {
	  document.getElementById('loader').classList.add('hidden');
  }
}

function selectAreaFromList(target) {
  var areaId = target.currentTarget.value;
  document.getElementById('settings-area-list').classList.add('hidden');
  window.instanceOfThis.setState({currentArea: areaId});
  window.instanceOfThis.state.currentArea = areaId;
  window.instanceOfThis.state.selectedArea = [];
  document.getElementById('enter-area').value = target.currentTarget.innerText;
}

function selectArea() {
  var areaList = window.instanceOfThis.state.temporaryAreaList, filteredAreaList = [], areaText = document.getElementById('enter-area').value, index, showFilteredAreaList = [];

  if(areaText !== '') {
	  window.instanceOfThis.setState({selectedArea: []});
	  window.instanceOfThis.state.selectedArea = [];
	  filteredAreaList = areaList.filter((h) => (h.name.toLowerCase().indexOf(areaText.toLowerCase()) > -1));

	  for(index=0; index < filteredAreaList.length; index++) {
	    showFilteredAreaList.push(
		    <li value={filteredAreaList[index].areaId} onClick={selectAreaFromList.bind(this)}>
		      {filteredAreaList[index].name}
		    </li>
	    );
    }

	  showFilteredAreaList.sort((a,b) => (a.props.children.toLowerCase().indexOf(areaText) - b.props.children.toLowerCase().indexOf(areaText)));
	  window.instanceOfThis.setState({selectedArea: showFilteredAreaList});
	  window.instanceOfThis.state.selectedArea = showFilteredAreaList;
	  document.getElementById('settings-area-list').classList.remove('hidden');
  } else {
	  document.getElementById('settings-area-list').classList.add('hidden');
	  window.instanceOfThis.setState({selectedArea: []});
	  window.instanceOfThis.state.selectedArea = [];
	  window.instanceOfThis.setState({currentArea: ''});
	  window.instanceOfThis.state.currentArea = '';
  }
}

function addArea() {
  var areaId = window.instanceOfThis.state.currentArea, areaList = window.instanceOfThis.state.areaList, selectedArea;
  selectedArea = window.instanceOfThis.state.temporaryAreaList.find(area => area.areaId == areaId);

  if(!areaList.find(area => area.props.value == areaId)) {
	   areaList.push(
		   <li value={areaId}>
		     <span>{selectedArea.name}</span>
		     <img src={deleteButtonImg} value={areaId} onClick={removeArea.bind(this)}/>
	     </li>
	   );
	   window.instanceOfThis.setState({areaList: areaList});
	   window.instanceOfThis.state.areaList = areaList;
  }

  window.instanceOfThis.state.currentArea = '';
  document.getElementById('enter-area').value = '';
}

function removeArea(target) {
  var areaId = target.target.attributes.value.value, areaList = window.instanceOfThis.state.areaList, filteredAreaList = [];
  filteredAreaList = areaList.filter(area => area.props.value != areaId);
  window.instanceOfThis.setState({areaList: filteredAreaList});
  window.instanceOfThis.state.areaList = filteredAreaList;
}

function removeCity(target) {
  var cityId = target.target.attributes.value.value, cityList = window.instanceOfThis.state.cityList, filteredCityList = [], filteredQueryState = [];
  filteredCityList = cityList.filter(city => city.props.value != cityId);
  window.instanceOfThis.setState({cityList: filteredCityList});
  window.instanceOfThis.state.cityList = filteredCityList;
  filteredQueryState = window.instanceOfThis.state.currentQueryState.filter(query => query.cityId != cityId);
  window.instanceOfThis.state.currentQueryState = filteredQueryState;
  dynamicQueryBuild();
}

function addCity() {
  if(checkCurrentQueryLength()) {
	  showErrorMessage("Query Length Exceeds Please reduce some attributes then try");
	  return false;
  }

  if(window.instanceOfThis.state.currentCity) {
	  var cityId = window.instanceOfThis.state.currentCity, selectedCity, currentQueryState;
	  selectedCity = window.instanceOfThis.state.cityItems.find(city => city.cityId == cityId), currentQueryState = window.instanceOfThis.state.currentQueryState;

	  if(!currentQueryState.find(city => city.cityId == cityId)) {
	    currentQueryState.push({'cityId':cityId, 'cityName': selectedCity.value, 'areas':[]});
	    rebuildCityArea();
	  }

	  window.instanceOfThis.state.currentCity = '';
	  document.getElementById('enter-seher').value = '';
	  dynamicQueryBuild();
  }
}

function selectCityFromList(target) {
  var cityId = target.currentTarget.value;
  document.getElementById('settings-city-list').classList.add('hidden');
  window.instanceOfThis.setState({currentCity: cityId});
  window.instanceOfThis.state.currentCity = cityId;
  window.instanceOfThis.state.selectedCity = [];
  document.getElementById('enter-seher').value = target.currentTarget.innerText;
}

function selectCity() {
  var cityList = window.instanceOfThis.state.cityItems, filteredCityList = [], cityText = document.getElementById('enter-seher').value, index, showFilteredCityList = [];

  if(cityText !== '') {
	  window.instanceOfThis.setState({selectedCity: []});
	  window.instanceOfThis.state.selectedCity = [];
	  filteredCityList = cityList.filter((h) => (h.value.toLowerCase().indexOf(cityText.toLowerCase()) > -1));

	  for(index=0; index < filteredCityList.length; index++) {
	    showFilteredCityList.push(
		    <li value={filteredCityList[index].cityId} onClick={selectCityFromList.bind(this)}>
		      {filteredCityList[index].value}
		    </li>
	    );
    }

	  showFilteredCityList.sort((a,b) => (a.props.children.toLowerCase().indexOf(cityText) - b.props.children.toLowerCase().indexOf(cityText)));
	  window.instanceOfThis.setState({selectedCity: showFilteredCityList});
	  window.instanceOfThis.state.selectedCity = showFilteredCityList;
	  document.getElementById('settings-city-list').classList.remove('hidden');
  } else {
	  document.getElementById('settings-city-list').classList.add('hidden');
	  window.instanceOfThis.setState({selectedCity: []});
	  window.instanceOfThis.state.selectedCity = [];
	  window.instanceOfThis.setState({currentCity: ''});
	  window.instanceOfThis.state.currentCity = '';
  }
    //window.addEventListener("keyup",escapeEvent);
}

// -------------------------------------------------------Category Operations

function removeCategory(target) {
  var categoryId = target.target.attributes.value.value, categoryList = window.instanceOfThis.state.categoryList, filteredCategoryList = [], filteredQueryStateCategory = [];
  filteredCategoryList = categoryList.filter(category => category.props.value != categoryId);
  window.instanceOfThis.setState({categoryList: filteredCategoryList});
  window.instanceOfThis.state.categoryList = filteredCategoryList;
  filteredQueryStateCategory = window.instanceOfThis.state.currentQueryStateCategory.filter(query => query.categoryId != categoryId);
  window.instanceOfThis.state.currentQueryStateCategory = filteredQueryStateCategory;
  dynamicQueryBuild();
}

function addCategory() {
  if(checkCurrentQueryLength()) {
    showErrorMessage("Query Length Exceeds Please reduce some attributes then try...");
    return false;
  }

  if(window.instanceOfThis.state.currentCategory) {
    var categoryId = window.instanceOfThis.state.currentCategory, selectedCategory, currentQueryStateCategory;
    selectedCategory = window.instanceOfThis.state.categoryItems.find(category => category.categoryId == categoryId), currentQueryStateCategory = window.instanceOfThis.state.currentQueryStateCategory;

    if(!currentQueryStateCategory.find(category => category.categoryId == categoryId)) {
      currentQueryStateCategory.push({'categoryId':categoryId, 'categoryName': selectedCategory.value, 'subCategories':[]});
      rebuildCategorySubCategory();
    }

    window.instanceOfThis.state.currentCategory = '';
    document.getElementById('enter-category').value = '';
    dynamicQueryBuild();
  }
}

function selectCategoryFromList(target) {
  var categoryId = target.currentTarget.value;
  document.getElementById('settings-category-list').classList.add('hidden');
  window.instanceOfThis.setState({currentCategory: categoryId});
  window.instanceOfThis.state.currentCategory = categoryId;
  document.getElementById('enter-category').value = target.currentTarget.innerText;
  window.instanceOfThis.state.selectedCategory = [];
}

function selectCategory() {
  var categoryList = window.instanceOfThis.state.categoryItems, filteredCategoryList = [], categoryText = document.getElementById('enter-category').value, index, showFilteredCategoryList = [];

  if(categoryText !== '') {
    window.instanceOfThis.setState({selectedCategory: []});
    window.instanceOfThis.state.selectedCategory = [];
    filteredCategoryList = categoryList.filter((h) => (h.value.toLowerCase().indexOf(categoryText.toLowerCase()) > -1));

    for(index=0; index < filteredCategoryList.length; index++) {
      showFilteredCategoryList.push(
        <li value={filteredCategoryList[index].categoryId} onClick={selectCategoryFromList.bind(this)}>
          {filteredCategoryList[index].value}
        </li>
      );
    }

    showFilteredCategoryList.sort((a,b) => (a.props.children.toLowerCase().indexOf(categoryText) - b.props.children.toLowerCase().indexOf(categoryText)));
    window.instanceOfThis.setState({selectedCategory: showFilteredCategoryList});
    window.instanceOfThis.state.selectedCategory = showFilteredCategoryList;
    document.getElementById('settings-category-list').classList.remove('hidden');
  } else {
    document.getElementById('settings-category-list').classList.add('hidden');
    window.instanceOfThis.setState({selectedCategory: []});
    window.instanceOfThis.state.selectedCategory = [];
  }
  //window.addEventListener("keyup",escapeEvent);
}

function showAreaSelectionBox(flag) {
  if(flag && window.instanceOfThis.state.currentCity) {
    document.getElementById('area-selection').classList.remove('hidden');
    var temporaryAreaList = window.instanceOfThis.state.areaItems.filter(area => area.cityId == window.instanceOfThis.state.currentCity);
    window.instanceOfThis.setState({temporaryAreaList: temporaryAreaList});
    window.instanceOfThis.state.temporaryAreaList = temporaryAreaList;

    if(window.instanceOfThis.state.currentQueryState.find(query => query.cityId == window.instanceOfThis.state.currentCity)) {
      window.instanceOfThis.state.areaList = [];
      var areaList = [], areas = [], areaItems = window.instanceOfThis.state.areaItems, k;
      areas = window.instanceOfThis.state.currentQueryState.find(query => query.cityId == window.instanceOfThis.state.currentCity).areas;

      for(k=0; k < areas.length; k++) {
        areaList.push(
          <li value={areas[k].areaId}>
            <span>{areas[k].areaName}</span>
            <img src={deleteButtonImg} value={areas[k].areaId} onClick={removeArea.bind(this)}/>
          </li>
        );
      }

      window.instanceOfThis.setState({areaList: areaList});
      window.instanceOfThis.state.areaList = areaList;
    }
  } else {
    document.getElementById('area-selection').classList.add('hidden');
    window.instanceOfThis.setState({temporaryAreaList: []});
    window.instanceOfThis.state.temporaryAreaList = [];
    window.instanceOfThis.state.areaList = [];
    window.instanceOfThis.state.currentCity = '';
    document.getElementById('enter-seher').value = '';
    document.getElementById('enter-area').value = '';
  }
}

function removeAreaFromQuery(target) {
  var cityId = target.currentTarget.attributes.city.value, areaId = target.currentTarget.attributes.value.value;
  var areas = window.instanceOfThis.state.currentQueryState.find(query => query.cityId == cityId).areas;
  var newAreas = areas.filter(area => area['areaId'] != areaId); //var newAreas = areas.filter(area => area != areaId);
  window.instanceOfThis.state.currentQueryState.find(query => query.cityId == cityId).areas = newAreas;
  rebuildCityArea();
  dynamicQueryBuild();
}

function addAreaToQuery() {
  if(checkCurrentQueryLength()) {
    showErrorMessage("Query Length Exceeds Please reduce some attributes then try...");
    return false;
  }

  var cityId = window.instanceOfThis.state.currentCity, areas = [], newAreas = [], newAreas1 = [];
  addCity();
  newAreas = window.instanceOfThis.state.areaList.map(area => area.props.value);
  newAreas1 = window.instanceOfThis.state.areaList.map(area => ({areaId: area.props.value, areaName: area.props.children[0].props.children}));
  window.instanceOfThis.state.currentQueryState.find(query => query.cityId == cityId).areas = newAreas1;
  window.instanceOfThis.state.currentCity = cityId;
  rebuildCityArea();
  showAreaSelectionBox(false);
  dynamicQueryBuild();
}

function rebuildCityArea() {
  var areaItems = window.instanceOfThis.state.areaItems;
  var currentQueryState = window.instanceOfThis.state.currentQueryState;
  var cityList = [], selectedCity, areaList = [], areas = [], index, k;

  for(index = 0; index < currentQueryState.length; index++) {
    areas = currentQueryState[index].areas;
    areaList = [];

    for(k=0;k<areas.length;k++) {
      areaList.push(
        <div value={areas[k].areaId}>
          <span>{areas[k].areaName}</span>
          <img src={deleteButtonImg} value={areas[k].areaId} city={currentQueryState[index].cityId} onClick={removeAreaFromQuery.bind(this)}/>
        </div>
      );
    }

    cityList.push(
      <li value={currentQueryState[index].cityId}>
        <div className="header">
          <span value={currentQueryState[index].cityId} onClick={showCustomAreas.bind(this)}>{currentQueryState[index].cityName}</span>
          <img src={deleteButtonImg} value={currentQueryState[index].cityId} onClick={removeCity.bind(this)}/>
        </div>
        <section>
          {areaList}
        </section>
      </li>
    );
  }

  window.instanceOfThis.setState({cityList: cityList});
  window.instanceOfThis.state.cityList = cityList;
}

//...............................................................................Sub Category

function selectSubCategoryFromList(target) {
  var subCategoryId = target.currentTarget.value;
  document.getElementById('settings-sub-category-list').classList.add('hidden');
  window.instanceOfThis.setState({currentSubCategory: subCategoryId});
  window.instanceOfThis.state.currentSubCategory = subCategoryId;
  window.instanceOfThis.state.selectedSubCategory = [];
  document.getElementById('enter-sub-category').value = target.currentTarget.innerText;
}

function selectSubCategory() {
  var subCategoryList = window.instanceOfThis.state.temporarySubCategoryList, filteredSubCategoryList = [], subCategoryText = document.getElementById('enter-sub-category').value, index, showFilteredSubCategoryList = [];

  if(subCategoryText !== '') {
    window.instanceOfThis.setState({selectedSubCategory: []});
    window.instanceOfThis.state.selectedSubCategory = [];
    filteredSubCategoryList = subCategoryList.filter((h) => (h.name.toLowerCase().indexOf(subCategoryText.toLowerCase()) > -1));

    for(index=0; index < filteredSubCategoryList.length; index++) {
      showFilteredSubCategoryList.push(
        <li value={filteredSubCategoryList[index].subCategoryId} onClick={selectSubCategoryFromList.bind(this)}>
          {filteredSubCategoryList[index].name}
        </li>
      );
    }

    showFilteredSubCategoryList.sort((a,b) => (a.props.children.toLowerCase().indexOf(subCategoryText) - b.props.children.toLowerCase().indexOf(subCategoryText)));
    window.instanceOfThis.setState({selectedSubCategory: showFilteredSubCategoryList});
    window.instanceOfThis.state.selectedSubCategory = showFilteredSubCategoryList;
    document.getElementById('settings-sub-category-list').classList.remove('hidden');
  } else {
    document.getElementById('settings-sub-category-list').classList.add('hidden');
    window.instanceOfThis.setState({selectedSubCategory: []});
    window.instanceOfThis.state.selectedSubCategory = [];
    window.instanceOfThis.setState({currentSubCategory: ''});
    window.instanceOfThis.state.currentSubCategory = '';
  }
  //window.addEventListener("keyup",escapeEvent);
}

function addSubCategory() {
  var subCategoryId = window.instanceOfThis.state.currentSubCategory, subCategoryList = window.instanceOfThis.state.subCategoryList, selectedSubCategory;
  selectedSubCategory = window.instanceOfThis.state.temporarySubCategoryList.find(subCategory => subCategory.subCategoryId == subCategoryId);

  if(!subCategoryList.find(subCategory => subCategory.props.value == subCategoryId)) {
    subCategoryList.push(
      <li value={subCategoryId}>
        <span>{selectedSubCategory.name}</span>
        <img src={deleteButtonImg} value={subCategoryId} onClick={removeSubCategory.bind(this)}/>
      </li>
    );
    window.instanceOfThis.setState({subCategoryList: subCategoryList});
    window.instanceOfThis.state.subCategoryList = subCategoryList;
  }

  window.instanceOfThis.state.currentSubCategory = '';
  document.getElementById('enter-sub-category').value = '';
}

//-------------------------------------------------------------------------check
function removeSubCategory(target) {
  var subCategoryId = target.target.attributes.value.value, subCategoryList = window.instanceOfThis.state.subCategoryList, filteredSubCategoryList = [];
  filteredSubCategoryList = subCategoryList.filter(subCategory => subCategory.props.value != subCategoryId);
  window.instanceOfThis.setState({subCategoryList: filteredSubCategoryList});
  window.instanceOfThis.state.subCategoryList = filteredSubCategoryList;
}

function showCustomSubCategories(target) {
  window.instanceOfThis.state.currentCategory = target.currentTarget.attributes.value.value;
  showSubCategorySelectionBox(true);
}

function showCustomAreas(target) {
  window.instanceOfThis.state.currentCity = target.currentTarget.attributes.value.value;
  showAreaSelectionBox(true);
}

function showSubCategorySelectionBox(flag) {
  if(flag && window.instanceOfThis.state.currentCategory) {
    document.getElementById('sub-category-selection').classList.remove('hidden');
    var temporarySubCategoryList = window.instanceOfThis.state.subCategoryItems.filter(subCategory => subCategory.categoryId == window.instanceOfThis.state.currentCategory);
    window.instanceOfThis.setState({temporarySubCategoryList: temporarySubCategoryList});
    window.instanceOfThis.state.temporarySubCategoryList = temporarySubCategoryList;

    if(window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == window.instanceOfThis.state.currentCategory)) {
      window.instanceOfThis.state.subCategoryList = [];
      var subCategoryList = [], subCategories = [], subCategoryItems = window.instanceOfThis.state.subCategoryItems, k;
      subCategories = window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == window.instanceOfThis.state.currentCategory).subCategories;

      for(k=0; k < subCategories.length; k++) {
        subCategoryList.push(
          <li value={subCategories[k]}>
            <span>{subCategoryItems.find(subCategory => subCategory.subCategoryId == subCategories[k]).name}</span>
            <img src={deleteButtonImg} value={subCategories[k]} onClick={removeSubCategory.bind(this)}/>
          </li>
        );
      }

      window.instanceOfThis.setState({subCategoryList: subCategoryList});
      window.instanceOfThis.state.subCategoryList = subCategoryList;
    }
  } else {
    document.getElementById('sub-category-selection').classList.add('hidden');
    window.instanceOfThis.setState({temporarySubCategoryList: []});
    window.instanceOfThis.state.temporarySubCategoryList = [];
    window.instanceOfThis.state.subCategoryList = [];
    window.instanceOfThis.state.currentCategory = '';
    document.getElementById('enter-category').value = '';
    document.getElementById('enter-sub-category').value = '';
  }
}

function removeSubCategoryFromQuery(target) {
  var categoryId = target.currentTarget.attributes.category.value, subCategoryId = target.currentTarget.attributes.value.value;
  var subCategories = window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == categoryId).subCategories;
  var newSubCategories = subCategories.filter(subCategory => subCategory != subCategoryId);
  window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == categoryId).subCategories = newSubCategories;
  rebuildCategorySubCategory();
  dynamicQueryBuild();
}

function addSubCategoryToQuery() {
  if(checkCurrentQueryLength()) {
    showErrorMessage("Query Length Exceeds Please reduce some attributes then try...");
    return false;
  }

  var categoryId = window.instanceOfThis.state.currentCategory, subCategories = [], newSubCategories = [];
  addCategory();
  newSubCategories = window.instanceOfThis.state.subCategoryList.map(subCategory => subCategory.props.value);
  window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == categoryId).subCategories = newSubCategories;
  window.instanceOfThis.state.currentCategory = categoryId;
  rebuildCategorySubCategory();
  showSubCategorySelectionBox(false);
  dynamicQueryBuild();
}

function rebuildCategorySubCategory() {
  var subCategoryItems = window.instanceOfThis.state.subCategoryItems;
  var currentQueryStateCategory = window.instanceOfThis.state.currentQueryStateCategory;
  var categoryList = [], selectedCity, subCategoryList = [], subCategories = [], index, k;

  for(index = 0; index < currentQueryStateCategory.length; index++) {
    subCategories = currentQueryStateCategory[index].subCategories;
    subCategoryList = [];

    for(k=0;k<subCategories.length;k++) {
      subCategoryList.push(
        <div value={subCategories[k]}>
          <span>{subCategoryItems.find(subCategory => subCategory.subCategoryId == subCategories[k]).name}</span>
          <img src={deleteButtonImg} value={subCategories[k]} category={currentQueryStateCategory[index].categoryId} onClick={removeSubCategoryFromQuery.bind(this)}/>
        </div>
      );
    }

    categoryList.push(
      <li value={currentQueryStateCategory[index].categoryId}>
        <div className="header">
          <span value={currentQueryStateCategory[index].categoryId} onClick={showCustomSubCategories.bind(this)}>{currentQueryStateCategory[index].categoryName}</span>
          <img src={deleteButtonImg} value={currentQueryStateCategory[index].categoryId} onClick={removeCategory.bind(this)}/>
        </div>
        <section>
          {subCategoryList}
        </section>
      </li>
    );
  }

  window.instanceOfThis.setState({categoryList: categoryList});
  window.instanceOfThis.state.categoryList = categoryList;
}

function parseDateOption(target) {
  var dateRange = {
    "month": {
      "start": Date.now() - 2678400000,
      "end": Date.now()
    },

    "2month": {
      "start": Date.now() - 5356800000,
      "end": Date.now()
    },

    "3month": {
      "start": Date.now() - 8035200000,
      "end": Date.now()
    }
  };

  window.instanceOfThis.setState({dateRange: dateRange[target.currentTarget.value]});
  window.instanceOfThis.state.dateRange = dateRange[target.currentTarget.value];
  selectRadio(target.currentTarget.value);
  dynamicQueryBuild();
}

function selectRadio(value) {
  var radios = document.getElementsByClassName("date-range-radio"), i;

  for(i=0;i<radios.length;i++) {
    if(radios[i].attributes.value.value == value) {
      radios[i].classList.add('selected');
    } else {
      radios[i].classList.remove('selected');
    }
  }
}

function deSelectRadios() {
  var radios = document.getElementsByClassName("date-range-radio"), i;

  for(i=0;i<radios.length;i++) {
    radios[i].classList.remove('selected');
  }
}

function clearCheckboxColors(className) {
  var radios = document.getElementsByClassName(className), i;

  for(i=0;i<radios.length;i++) {
	  radios[i].classList.remove('checked');
  }
}

function dynamicQueryBuild() {
  extractDimensionFromCategoryStateQuery();
}

function finalQueryBuild() {
  extractDimensionFromCategoryStateQuery();
  showCount();
}

function checkCurrentQueryLength() {
  var queryLength = window.instanceOfThis.state.queryList.length;
  return (queryLength > 800);
}

function extractDimensionFromCategoryStateQuery() {
  var currentQueryStateCategory = window.instanceOfThis.state.currentQueryStateCategory, index, hash = {'type':'', 'value':''}, dimensionList = [];

  for(index = 0; index < currentQueryStateCategory.length; index++) {
	  if(currentQueryStateCategory[index].subCategories.length > 0) {
	    currentQueryStateCategory[index].subCategories.forEach(function(subCategory) {
		    hash = {'type':'subcat', 'value':subCategory};
		    dimensionList.push(hash);
	    });
	  } else {
	    hash = {'type':'cat', 'value':currentQueryStateCategory[index].categoryId};
	    dimensionList.push(hash);
	  }
  }
  window.instanceOfThis.setState({dimensionList: dimensionList});
  window.instanceOfThis.state.dimensionList = dimensionList;
  buildQuery();
}

function buildQuery() {
  var currentQueryState = window.instanceOfThis.state.currentQueryState, dimensionList = window.instanceOfThis.state.dimensionList, queryList = [], finalQuery = [], singleQuery = [];
  var startDate = (window.instanceOfThis.state.dateRange.start ? window.instanceOfThis.state.dateRange.start : null), endDate = (window.instanceOfThis.state.dateRange.end ? window.instanceOfThis.state.dateRange.end : null), dateType = ((startDate && endDate) ? "custom" : "");
  var visit = document.getElementById("settings-visit").value;

  if(currentQueryState.length > 0) {
	  currentQueryState.forEach(function(query) {
      if(query.areas.length > 0) {
		    query.areas.forEach(function(area) {
		      queryList.push({"city":query.cityId,"dimension":{"name":"","value":""},"distance":{"condition":"","value":""},"duration":{"type":dateType,"startDate":startDate,"endDate":endDate},"pin":area.areaId,"visit":{"condition":"eq","value":visit}});
		    });
	    } else {
		    queryList.push({"city":query.cityId,"dimension":{"name":"","value":""},"distance":{"condition":"","value":""},"duration":{"type":dateType,"startDate":startDate,"endDate":endDate},"pin":"","visit":{"condition":"eq","value":visit}});
	    }
	  });
  } else {
	  queryList.push({"city":"","dimension":{"name":"","value":""},"distance":{"condition":"","value":""},"duration":{"type":dateType,"startDate":startDate,"endDate":endDate},"pin":"","visit":{"condition":"eq","value":visit}});
  }

  if(dimensionList.length > 0) {
	  dimensionList.forEach(function(dimension) {
	    singleQuery = JSON.parse(JSON.stringify(queryList));
	    singleQuery.forEach(function(query) {
		    query.dimension.name = dimension.type;
		    query.dimension.value = dimension.value;
	    });

      finalQuery = finalQuery.concat(singleQuery);
	  });
  } else {
	  finalQuery = queryList;
  }

  var daysOfWeek = window.instanceOfThis.state.daysOfWeek, tempQuery = [], newQuery = [];

  if(daysOfWeek.length === 1) {
	  finalQuery.forEach(function(q) {
	    q.day = {'value': daysOfWeek[0]};
	  });
  } else if(daysOfWeek.length > 1) {
    newQuery = [];
	  daysOfWeek.forEach(function(dayOfWeek) {
	    tempQuery = JSON.parse(JSON.stringify(finalQuery));
	    tempQuery.forEach(function(q) {
		    q.day = {'value': dayOfWeek};
	    });

      newQuery = newQuery.concat(tempQuery);
	  });

    finalQuery = newQuery;
  }

  var timesOfDay = window.instanceOfThis.state.timesOfDay;

  if(timesOfDay.length === 1) {
	  finalQuery.forEach(function(q) {
	    q.slot = timesOfDay[0];
	  });
  } else if(timesOfDay.length > 1) {
    newQuery = [];

    timesOfDay.forEach(function(timeOfDay) {
	    tempQuery = JSON.parse(JSON.stringify(finalQuery));
	    tempQuery.forEach(function(q) {
		    q.slot = timeOfDay;
	    });

      newQuery = newQuery.concat(tempQuery);
	  });

    finalQuery = newQuery;
  }

  window.instanceOfThis.setState({queryList: finalQuery}, function afterQueryChange(){ this.state.queryModified = true });
  window.instanceOfThis.state.queryList = finalQuery;
  console.log(finalQuery);
  //showCount();
}

function uncheckBoxes(name) {
  var i, allCheckBoxes = document.getElementsByName(name);

  for(i=0; i<allCheckBoxes.length; i++) {
	  allCheckBoxes[i].checked = false;
  }

  dynamicQueryBuild();
}

function clearStates() {
  uncheckBoxes('day-of-week');
  uncheckBoxes('time-of-day');
  uncheckBoxes('date-range');
  deSelectRadios();
  clearCheckboxColors("day-checkbox");
  clearCheckboxColors("time-checkbox");
  document.getElementById('settings-visit').value = '';

  window.instanceOfThis.setState({
	  selectedCity: [],
	  currentCity: '',
	  cityList: [],
	  categoryList: [],
	  currentCategory: '',
	  selectedCategory: [],
	  temporaryAreaList: [],
	  areaList: [],
	  selectedArea: [],
	  currentArea: [],
	  currentQueryState: [],
  	temporarySubCategoryList: [],
  	subCategoryList: [],
  	selectedSubCategory: [],
  	currentSubCategory: [],
  	currentQueryStateCategory: [],
  	dimensionList: [],
  	queryList: [],
  	dateRange: {},
  	daysOfWeek: [],
  	timesOfDay: [],
  	outputQueryCount: 0,
  	paddingAngle: 0
  });

  dataPieOutput();
}

function showCount() {
  loader(true);
  window.instanceOfThis.setState({outputQueryCount: 0});
  window.instanceOfThis.state.outputQueryCount = 0;
  dataPieOutput();
  var queryList = window.instanceOfThis.state.queryList, baseRequestUrlTest;
  var new_hash = {'description': 'new' ,'name': 'new test', 'attributeJson': JSON.stringify({ 'queryList': queryList }), 'apiKey': window.instanceOfThis.api};
  var str = JSON.stringify(new_hash);
  var baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat-advanced";
  window.instanceOfThis.state.paddingAngle = 0;
  //baseRequestUrlTest = 'http://127.0.0.1:8000/showCountForCustomSegment?segment=';
  //baseRequestUrlTest = 'http://127.0.0.1:8000/showCountForCustomSegment';
  //var encoded_uri = encodeURIComponent(str);
  //makeAjaxPostRequest(str, baseRequestUrlTest, function(response) {
  //makeAjaxGetRequest(baseRequestUrlTest + encoded_uri, function(response) {

  makeAjaxPostRequest(str, baseRequestUrl, function(response) {
    console.log(response.responseText);
    loader(false);
    var countQueryJson = JSON.parse(response.responseText), countQuery = 0;
    countQuery = countQueryJson["new test"]["value"];
  	window.instanceOfThis.setState({outputQueryCount: countQuery});
  	window.instanceOfThis.state.outputQueryCount = countQuery;
    dataPieOutput();
  	document.getElementById("save-segment-advanced").disabled = false;
  	window.instanceOfThis.state.queryModified = false;
  	return;
  });
}

function showCustomDateSelectionBox() {
  document.getElementById('custom-date-selection-box').classList.remove('hidden');
  document.getElementById('date-error').classList.add('hidden');
}

function hideCustomDateSelectionBox() {
  document.getElementById('custom-date-selection-box').classList.add('hidden');
  document.getElementById('date-error').classList.add('hidden');
  document.getElementById('custom-date-range').checked = false;
  document.getElementById('settings-custom-start-date').value = "";
  document.getElementById('settings-custom-end-date').value = "";
  window.instanceOfThis.setState({dateRange: []});
  window.instanceOfThis.state.dateRange = [];
  deSelectRadios();
}

function addCustomDate() {
  var customDateStart = new Date(document.getElementById('settings-custom-start-date').value.split('-')), customDateEnd = new Date(document.getElementById('settings-custom-end-date').value.split('-'));

  if(document.getElementById('settings-custom-start-date').value != "" && document.getElementById('settings-custom-end-date').value != "") {
    if(customDateStart.getTime() > customDateEnd.getTime() || customDateStart.getTime() > Date.now() || customDateEnd.getTime() > Date.now()) {
      document.getElementById('date-error').classList.remove('hidden');
    } else if(customDateStart.getTime() <= customDateEnd.getTime()) {
      var dateRange = {
        "start": customDateStart.getTime(),
        "end": customDateEnd.getTime() + 86399000
      };

      window.instanceOfThis.setState({dateRange: dateRange});
      window.instanceOfThis.state.dateRange = dateRange;
      document.getElementById('date-error').classList.add('hidden');
      document.getElementById('custom-date-selection-box').classList.add('hidden');
      dynamicQueryBuild();
      selectRadio("custom");
    }
  } else {
    document.getElementById('date-error').classList.remove('hidden');
  }
}

function addDayOfWeek(target) {
  var daysOfWeek =  window.instanceOfThis.state.daysOfWeek;

  if(target.currentTarget.checked) {
    if(!checkCurrentQueryLength()) {
      daysOfWeek.push(target.currentTarget.value);
      checkDayOfWeek(target.currentTarget.value);
    } else {
      target.currentTarget.checked = false;
      showErrorMessage("Query Length Exceeds Please reduce some attributes then try...");
    }

  } else {
    daysOfWeek = daysOfWeek.filter(dayOfWeek => dayOfWeek != target.currentTarget.value);
    unCheckDayOfWeek(target.currentTarget.value);
  }

  daysOfWeek = [...new Set(daysOfWeek)];
  window.instanceOfThis.setState({daysOfWeek: daysOfWeek});
  window.instanceOfThis.state.daysOfWeek = daysOfWeek;
  dynamicQueryBuild();
}

function checkDayOfWeek(value) {
  var checkboxes = document.getElementsByClassName("day-checkbox"), i;
  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.add('checked');
      break;
    }
  }
}

function unCheckDayOfWeek(value) {
  var checkboxes = document.getElementsByClassName("day-checkbox"), i;
  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.remove('checked');
      break;
    }
  }
}

function addTimeOfDay(target) {
  var timesOfDay =  window.instanceOfThis.state.timesOfDay;
  var slots = {1:{min: 5, max: 8}, 2:{min: 8, max: 12}, 3:{min: 12, max: 18}, 4:{min: 18, max: 21}, 5:{min:21, max:24}, 6:{min:0, max:5}};

  if(target.currentTarget.checked) {
    if(!checkCurrentQueryLength()) {
      timesOfDay.push(slots[target.currentTarget.value]);
      checkTimeOfDay(target.currentTarget.value);
    } else {
      target.currentTarget.checked = false;
      showErrorMessage("Query Length Exceeds Please reduce some attributes then try...");
    }

  } else {
    timesOfDay = timesOfDay.filter(timeOfDay => JSON.stringify(timeOfDay) != JSON.stringify(slots[target.currentTarget.value]));
    unCheckTimeOfDay(target.currentTarget.value);
  }

  window.instanceOfThis.setState({timesOfDay: timesOfDay});
  window.instanceOfThis.state.timesOfDay = timesOfDay;
  dynamicQueryBuild();
}

function checkTimeOfDay(value) {
  var checkboxes = document.getElementsByClassName("time-checkbox"), i;

  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.add('checked');
      break;
    }
  }
}

function unCheckTimeOfDay(value) {
  var checkboxes = document.getElementsByClassName("time-checkbox"), i;

  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.remove('checked');
      break;
    }
  }
}

function showSaveSegmentBox(flag) {
  if(flag && (window.instanceOfThis.state.currentQueryState.length > 0 || window.instanceOfThis.state.currentQueryStateCategory.length > 0)) {
    if(window.instanceOfThis.state.queryList.length > 0 && !window.instanceOfThis.state.queryModified) {
      document.getElementById('save-segment-box').classList.remove('hidden');

      if(window.instanceOfThis.props.data) {
        document.getElementById('segment-name-advanced').value = window.instanceOfThis.props.data.segmentName;
        document.getElementById('segment-description-advanced').value = window.instanceOfThis.props.data.segmentDesc;
      }
    } else {
      showErrorMessage('Build Query');
    }
  } else {
    document.getElementById('save-segment-box').classList.add('hidden');
    document.getElementById('segment-name-advanced').value = '';
    document.getElementById('segment-description-advanced').value ='';
  }
}

// function saveSegment() {
//   if(window.instanceOfThis.state.queryList.length > 0) {
//     var queryList = window.instanceOfThis.state.queryList, name, desc, url;
//     name = document.getElementById('segment-name-advanced').value;
//     desc = document.getElementById('segment-description-advanced').value;
//
//     if(name != '' && desc != '') {
//       if(window.instanceOfThis.props.data) {
//         url = "https://canvas.trapyz.com/edit-segment-advanced";
//         var new_hash = {'segmentDesc': desc ,'segmentName': name, 'queryList': queryList, 'api': window.instanceOfThis.api, 'segmentCount': window.instanceOfThis.state.outputQueryCount, 'existingSegmentName':window.instanceOfThis.props.data.segmentName,'segmentVisit':''};
//       } else {
//         url = "https://canvas.trapyz.com/save-segment-advanced";
//         var new_hash = {'segmentDesc': desc ,'segmentName': name, 'queryList': queryList, 'api': window.instanceOfThis.api, 'segmentCount': window.instanceOfThis.state.outputQueryCount, 'existingSegmentName':name,'segmentVisit':''};
//       }
//
//       console.log(new_hash);
//       var segmentJson = JSON.stringify(new_hash);
//
//       makeAjaxPostRequest(segmentJson, url, function(response) {
//         console.log(response.responseText);
//         showSaveSegmentBox(false);
//         window.location.reload();
//         return;
//       });
//     } else {
//       showErrorMessage('Please Provide Name and Description');
//     }
//   } else {
//     showErrorMessage('Build Query');
//     showSaveSegmentBox(false);
//   }
// }

function saveSegment() {
  if(window.instanceOfThis.state.queryList.length > 0) {
    var queryList = window.instanceOfThis.state.queryList, name, desc, url;
    name = document.getElementById('segment-name-advanced').value;
    desc = document.getElementById('segment-description-advanced').value;

    if(name != '' && desc != '') {
      if(window.instanceOfThis.props.data) {
		    url = "https://canvas.trapyz.com/edit-segment-demography";
		    var new_hash = {
          'segmentDesc': desc ,'segmentName': name, 'query': {"queryList":queryList}, 'api': window.instanceOfThis.api,
          'segmentCount': window.instanceOfThis.state.outputQueryCount,
          'id': window.instanceOfThis.props.data.id,
        };
	    } else {
		    url = "https://canvas.trapyz.com/save-segment-demography";
		    var new_hash = {
          'segmentDesc': desc ,'segmentName': name, 'query': {"queryList":queryList}, 'type': 'custom',
          'api': window.instanceOfThis.api, 'segmentCount': window.instanceOfThis.state.outputQueryCount
        };
	    }

	    var segmentJson = JSON.stringify(new_hash);

	    makeAjaxPostRequest(segmentJson, url, function(response) {
        if(response.responseText.trim() == "1") {
          showSaveSegmentBox(false);
          showHomePage();
        } else {
          showErrorMessage(response.responseText.trim());
        }

		    return;
	    });
    } else {
      showErrorMessage('Please Provide Name and Description');
    }
  } else {
    showErrorMessage('Build Query');
    showSaveSegmentBox(false);
  }
}

function loadEdit() {
  var pins = window.instanceOfThis.props.data.query.queryList.map(query => query.pin);
  pins = pins.filter(pin => pin != '');
  pins = [...new Set(pins)];

  if(pins.length > 0) {
    loadAreasEdit(pins);
  } else {
    editStateInitiate();
  }
}

function editStateInitiate() {
  loader(true);
  var queryList = window.instanceOfThis.props.data.query.queryList, selectedCity, currentQueryState = [], cityId, currentQueryStateCategory = [], categoryId, selectedCategory, subCategoryId, daysOfWeek = [], timeOfDay = [];

  queryList.forEach(function(query) {
    cityId = query.city;

    if(cityId != '') {
      selectedCity = window.instanceOfThis.state.cityItems.find(city => city.cityId == cityId);

      if(!currentQueryState.find(city => city.cityId == cityId)) {
        currentQueryState.push({'cityId':cityId, 'cityName': selectedCity.value, 'areas':[]});
      }

      if(query.pin != '') {
        var cityRef;

        if(cityRef = currentQueryState.find(query => query.cityId == cityId)) {
          if(!cityRef.areas.find(area => area.areaId == query.pin)) {
            cityRef.areas.push({areaId: query.pin, areaName: window.instanceOfThis.state.areaItems.find(area => area.areaId == query.pin).name});
          }
        }
      }
    }

    if(query.dimension.name == 'cat') {
      categoryId = query.dimension.value;
      selectedCategory = window.instanceOfThis.state.categoryItems.find(category => category.categoryId == categoryId);

      if(!currentQueryStateCategory.find(category => category.categoryId == categoryId)) {
        currentQueryStateCategory.push({'categoryId':categoryId, 'categoryName': selectedCategory.value, 'subCategories':[]});
      }
    } else if(query.dimension.name == 'subcat') {
      subCategoryId = query.dimension.value;
      categoryId = window.instanceOfThis.state.subCategoryItems.find(subCat => subCat.subCategoryId == subCategoryId).categoryId;
      selectedCategory = window.instanceOfThis.state.categoryItems.find(category => category.categoryId == categoryId);

      if(!currentQueryStateCategory.find(category => category.categoryId == categoryId)) {
        currentQueryStateCategory.push({'categoryId':categoryId, 'categoryName': selectedCategory.value, 'subCategories':[]});
      }

      var catRef = currentQueryStateCategory.find(query => query.categoryId == categoryId);

      if(!catRef.subCategories.find(subcat => subcat == subCategoryId)) {
        catRef.subCategories.push(subCategoryId);
      }
    }

    if(query.day) {
      daysOfWeek.push(query.day.value);
    }

    if(query.slot) {
      timeOfDay.push(JSON.stringify(query.slot));
    }
  });

  daysOfWeek = [...new Set(daysOfWeek)];
  timeOfDay = [...new Set(timeOfDay)];
  var timesOfDay = timeOfDay.map(t => JSON.parse(t));
  document.getElementById('settings-visit').value = queryList[0].visit.value;
  var dateRange = {start: queryList[0].duration.startDate, end: queryList[0].duration.endDate};
  window.instanceOfThis.state.dateRange = {};
  deSelectRadios();

  if(queryList[0].duration.endDate && queryList[0].duration.startDate) {
    window.instanceOfThis.setState({dateRange: dateRange});
    window.instanceOfThis.state.dateRange = dateRange;
    document.getElementById('custom-date-range').checked = true;
    selectRadio('custom');
  }

  document.getElementById('date-range-30').checked = false;
  window.instanceOfThis.setState({currentQueryState: currentQueryState});
  window.instanceOfThis.state.currentQueryState = currentQueryState;
  window.instanceOfThis.setState({currentQueryStateCategory: currentQueryStateCategory});
  window.instanceOfThis.state.currentQueryStateCategory = currentQueryStateCategory;
  window.instanceOfThis.setState({daysOfWeek: daysOfWeek});
  window.instanceOfThis.state.daysOfWeek = daysOfWeek;
  window.instanceOfThis.setState({timesOfDay: timesOfDay});
  window.instanceOfThis.state.timesOfDay = timesOfDay;
  window.instanceOfThis.setState({outputQueryCount: window.instanceOfThis.props.data.segmentCount});
  window.instanceOfThis.state.outputQueryCount = window.instanceOfThis.props.data.segmentCount;

  rebuildCityArea();
  rebuildCategorySubCategory();
  editDayOfWeek();
  editTimeOfDay();
  dynamicQueryBuild();
  dataPieOutput();
  loader(false);
}

function editDayOfWeek() {
  var i, allCheckBoxes = document.getElementsByName('day-of-week'), daysOfWeek = window.instanceOfThis.state.daysOfWeek;

  for(i=0; i<allCheckBoxes.length; i++) {
    if(daysOfWeek.find(dayOfWeek => dayOfWeek == allCheckBoxes[i].value)) {
      allCheckBoxes[i].checked = true;
      checkDayOfWeek(allCheckBoxes[i].value);
    }
  }
}

function editTimeOfDay() {
  var i, allCheckBoxes = document.getElementsByName('time-of-day'), timesOfDay = window.instanceOfThis.state.timesOfDay;
  var slots = {'{"min":5,"max":8}':1, '{"min":8,"max":12}':2, '{"min":12,"max":18}':3, '{"min":18,"max":21}':4, '{"min":21,"max":24}':5, '{"min":0,"max":5}':6};

  var timeOfDay = timesOfDay.map(t => slots[JSON.stringify(t)]);

  for(i=0; i<allCheckBoxes.length; i++) {
    if(timeOfDay.find(t => t == allCheckBoxes[i].value)) {
      allCheckBoxes[i].checked = true;
      checkTimeOfDay(allCheckBoxes[i].value);
    }
  }
}

function showErrorMessage(msg) {
  document.getElementById('error-message-box').classList.remove('hidden');
  window.instanceOfThis.setState({errorMessage: msg});
}

function hideErrorMessage() {
  document.getElementById('error-message-box').classList.add('hidden');
  window.instanceOfThis.setState({errorMessage: ''});
}

function logout() {
  var items = ["uname", "name", "key"], item;

  for(item in items) {
    sessionStorage.removeItem(items[item]);
  }

  ReactDOM.render(<TrapyzLoginPage />, document.getElementById('root'));
}

function dataPieOutput() {
  var dataPie = [
    {value: 0, stroke: "#4A90E2", strokeWidth: 8},
    {value: 0, stroke: "#F7B342", strokeWidth: 8}
  ];

  dataPie[1]["value"] = parseInt(window.instanceOfThis.state.totalActive) - parseInt(window.instanceOfThis.state.uniqueCount);
  dataPie[0]["value"] = parseInt(window.instanceOfThis.state.outputQueryCount);
  window.instanceOfThis.setState({dataPie: dataPie});
}

function findTotalActive() {
  var str = JSON.stringify({});
  var baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat-demography";

  makeAjaxPostRequest(str, baseRequestUrl, function(response) {
    console.log(response.responseText);
    var countQueryJson = JSON.parse(response.responseText);
    window.instanceOfThis.setState({totalActive: countQueryJson["value"]});
    dataPieOutput();
	  return;
  });
}

function showHomePage() {
  ReactDOM.render(<TrapyzHomePage />, document.getElementById('root'));
}

class TrapyzNewAdvancedPage extends Component {
  constructor (props) {
    super(props);
    this.api = sessionStorage.getItem("key");
    this.topCategoriesDone = false;
    this.city = "";
    this.username = sessionStorage.getItem("name");
    this.state = {
      totalActive: 0,
    	uniqueCount: 0,
    	cityItems: [],
    	categoryItems: [],
    	selectedCity: [],
    	currentCity: '',
    	cityList: [],
    	categoryList: [],
    	currentCategory: '',
    	selectedCategory: [],
    	areaItems: [],
    	temporaryAreaList: [],
    	areaList: [],
    	selectedArea: [],
    	currentArea: [],
    	currentQueryState: [],
    	subCategoryItems: [],
    	temporarySubCategoryList: [],
    	subCategoryList: [],
    	selectedSubCategory: [],
    	currentSubCategory: [],
    	currentQueryStateCategory: [],
    	dimensionList: [],
    	queryList: [],
    	dateRange: {"start": Date.now() - 2678400000,"end": Date.now()},
    	daysOfWeek: [],
    	timesOfDay: [],
    	outputQueryCount: 0,
    	pieChart: [],
    	paddingAngle: 0,
    	queryModified: false,
    	errorMessage: ""
    }
  }

  componentDidMount() {
	//this.forceUpdate();
    if (!this.citiesDone) {
      loader(true);
      this.citiesDone = true;
      loadCities();
      document.getElementById("save-segment-advanced").disabled = true;
      document.getElementById("date-range-30").checked = true;
      selectRadio("month");
    }

    findTotalActive();
    window.scrollTo(0, 0);

    makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=week&api=" + this.api, function(response) {
      window.instanceOfThis.setState({weeklyActive: response.responseText.trim()});
    });

    makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=month&api=" + this.api, function(response) {
      window.instanceOfThis.setState({monthlyActive: response.responseText.trim()});
    });
  }

/*
    componentWillUpdate(nextProps, nextState) {
	if (nextState.changed == true && this.state.changed == false) {
	    this.props.onWillOpen();
	}
    }*/

  render() {
      //this.forceUpdate();
    window.instanceOfThis = this;

    // if (this.state.totalActive === 0) {
	  //   makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=total&api=" + this.api, function(response) {
    //     window.instanceOfThis.setState({totalActive: response.responseText});
    //     dataPieOutput();
	  //   });
    // }

    return (
      <div className="trapyz-advanced-segment-page">
        <header>
          <div className="heading">
            <section className="logo">
              <div>
                <img src="./assets/TrapyzLogo.png" alt="Trapyz Logo" height="24px" width="60px"/>
              </div>
              <span>Audience Insights Platform</span>
            </section>
            <section className="installed">
              <div>
                <label>Total Installed</label>
                <span>{this.state.totalActive}</span>
              </div>
              <div>
                <label>Weekly Active</label>
                <span>{this.state.weeklyActive}</span>
              </div>
              <div>
                <label>Monthly Active</label>
                <span>{this.state.monthlyActive}</span>
              </div>
            </section>
          </div>
          <aside className="session">
            <span>Demo User</span>
            <img src="./assets/Logout.png" onClick={logout} alt="Logout" width="24px" height="24px"/>
          </aside>
        </header>
	      <section className="settings">
	        <article className="operations">
	          <h2>Customize and Select Your Segment</h2>
            <main>
	            <section className="selector">
	              <h3>City & Area</h3>
	              <section className="input-box">
	                <input type="text" id="enter-seher" placeholder="Search City" onKeyUp={() => selectCity()}/>
	                <button className="add" onClick={() => addCity()}>+</button>
	                <span className="show-area" onClick={() => loadCitySpecificAreas()}>SHOW AREA</span>
	              </section>
	              <ul id="settings-city-list" className="settings-city-list hidden">
                  {this.state.selectedCity}
	              </ul>
                <section className="city-list-box">
                  <ul>{this.state.cityList}</ul>
	              </section>
	            </section>
	            <section className="selector">
	              <h3>Categories & Sub Categories</h3>
	              <section className="input-box">
	                <input type="text" id="enter-category" placeholder="Search Category" onKeyUp={() => selectCategory()}/>
	                <button className="add" onClick={() => addCategory()}>+</button>
	                <span className="show-area" onClick={() => showSubCategorySelectionBox(true)}>SHOW SUB-CATEGORY</span>
	              </section>
	              <ul id="settings-category-list" className="settings-category-list hidden">
                  {this.state.selectedCategory}
	              </ul>
                <section className="city-list-box">
                  <ul>{this.state.categoryList}</ul>
	              </section>
	            </section>
	          </main>
	          <section className="settings-date-range">
              <span>Date Range</span>
              <input type="radio" name="date-range" id="date-range-30" value="month" onClick={parseDateOption.bind(this)}/>
	            <label className="date-range-radio" value="month">Last 30 Days</label>
	            <input type="radio" name="date-range" value="2month" onClick={parseDateOption.bind(this)}/>
	            <label className="date-range-radio" value="2month">Last 60 Days</label>
	            <input type="radio" name="date-range" value="3month" onClick={parseDateOption.bind(this)}/>
	            <label className="date-range-radio" value="3month">Last 90 Days</label>
	            <input type="radio" name="date-range" id="custom-date-range" value="custom" onClick={() => showCustomDateSelectionBox()}/>
	            <label className="date-range-radio" value="custom">Custom Date</label>
	            <p>{window.instanceOfThis.state.dateRange.start ? ('(' + ((new Date(window.instanceOfThis.state.dateRange.start)).getDate() + '/' + ((new Date(window.instanceOfThis.state.dateRange.start)).getMonth() + 1) + '/' + (new Date(window.instanceOfThis.state.dateRange.start)).getFullYear()) + ' to ' + ((new Date(window.instanceOfThis.state.dateRange.end)).getDate() + '/' + ((new Date(window.instanceOfThis.state.dateRange.end)).getMonth() + 1) + '/' + (new Date(window.instanceOfThis.state.dateRange.end)).getFullYear()) + ')') : ''}</p>
	          </section>
            <section className="slot-box">
              <section className="time-of-day">
                <span>Time Slot</span>
	              <div>
	                <input type="checkbox" name="time-of-day" value="1" onClick={addTimeOfDay.bind(this)}/>
	                <label className="time-checkbox" value="1">Dawn</label>
	                <input type="checkbox" name="time-of-day" value="2" onClick={addTimeOfDay.bind(this)}/>
	                <label className="time-checkbox" value="2">Morning</label>
	                <input type="checkbox" name="time-of-day" value="3" onClick={addTimeOfDay.bind(this)}/>
	                <label className="time-checkbox" value="3">Afternoon</label>
	              </div>
	              <div>
	                <input type="checkbox" name="time-of-day" value="4" onClick={addTimeOfDay.bind(this)}/>
	                <label className="time-checkbox" value="4">Evening</label>
	                <input type="checkbox" name="time-of-day" value="5" onClick={addTimeOfDay.bind(this)}/>
	                <label className="time-checkbox" value="5">Night</label>
	                <input type="checkbox" name="time-of-day" value="6" onClick={addTimeOfDay.bind(this)}/>
	                <label className="time-checkbox" value="6">Late Night</label>
	              </div>
	            </section>
	            <section className="day-of-week">
                <span>Day of the Week</span>
	              <div>
	                <input type="checkbox" name="day-of-week" value="1" onClick={addDayOfWeek.bind(this)}/>
	                <label className="day-checkbox" value="1">Monday</label>
	                <input type="checkbox" name="day-of-week" value="2" onClick={addDayOfWeek.bind(this)}/>
	                <label className="day-checkbox" value="2">Tuesday</label>
	                <input type="checkbox" name="day-of-week" value="3" onClick={addDayOfWeek.bind(this)}/>
	                <label className="day-checkbox" value="3">Wednesday</label>
	              </div>
	              <div>
	                <input type="checkbox" name="day-of-week" value="4" onClick={addDayOfWeek.bind(this)}/>
	                <label className="day-checkbox" value="4">Thursday</label>
	                <input type="checkbox" name="day-of-week" value="5" onClick={addDayOfWeek.bind(this)}/>
	                <label className="day-checkbox" value="5">Friday</label>
	                <input type="checkbox" name="day-of-week" value="6" onClick={addDayOfWeek.bind(this)}/>
	                <label className="day-checkbox" value="6">Saturday</label>
	                <input type="checkbox" name="day-of-week" value="7" onClick={addDayOfWeek.bind(this)}/>
	                <label className="day-checkbox" value="7">Sunday</label>
	              </div>
	            </section>
	          </section>
	          <section className="visit">
	            <span>No. of Visit</span>
	            <input type="text" id="settings-visit"/>
	          </section>
	          <section className="show-count-box">
	            <button className="show-count" onClick={() => finalQueryBuild()}>SHOW COUNT</button>
	            <button className="other-buttons" onClick={() => showSaveSegmentBox(true)}>SAVE SEGMENT</button>
	            <button className="other-buttons" onClick={() => clearStates()}>CLEAR</button>
              <button className="other-buttons" onClick={() => showHomePage()}>CANCEL</button>
	          </section>
	        </article>

	        <aside className="sidebar">
	          <section>
	            <h2>Segment Size</h2>
	            <div className="total-count">
	              <span className="color"></span>
	              <div className="content">
	                <span className="count">{window.instanceOfThis.state.totalActive}</span>
	                <span>Total Installed</span>
	              </div>
	            </div>
	            <div className="unique-gids">
	              <span className="color"></span>
	              <div className="content">
	                <span className="count">{window.instanceOfThis.state.outputQueryCount}</span>
	                <span>Segment Count</span>
	              </div>
	            </div>
	          </section>
	          <section className="pie-chart">
              {<DonutChart data={this.state.dataPie} />}
	          </section>
	        </aside>
	      </section>
	      <aside id="loader" className="hidden loader">
	        <img src="./assets/progress-bar.gif" alt=""/>
	      </aside>
	      <section id="area-selection" className="area-selection-box hidden">
	        <div className="header">
	          <span>Select Area</span>
	          <div className="selected-city">
	            <label>City:</label>
	            <span>{this.state.cityItems.find(city => city.cityId == this.state.currentCity) ? this.state.cityItems.find(city => city.cityId == this.state.currentCity).value : ''}</span>
            </div>
	        </div>
	        <section className="area-input-box">
            <label>Select Area</label>
	          <div>
	            <input type="text" id="enter-area" onKeyUp={() => selectArea()}/>
	            <button className="add" onClick={() => addArea()}>+</button>
	          </div>
	        </section>
	        <section className="area-list-box">
	          <ul>{this.state.areaList}</ul>
	        </section>
	        <section className="area-select-box">
	          <button className="select" onClick={() => addAreaToQuery()}>SELECT</button>
	          <button className="clear" onClick={() => showAreaSelectionBox(false)}>CLEAR</button>
	        </section>
	        <ul id="settings-area-list" className="settings-area-list custom hidden">
            {this.state.selectedArea}
	        </ul>
	      </section>

	      <section id="sub-category-selection" className="area-selection-box hidden">
	        <div className="header">
	          <span>Select Sub Category</span>
	          <div className="selected-city">
	            <label>Category:</label>
	            <span>{this.state.categoryItems.find(category => category.categoryId == this.state.currentCategory) ? this.state.categoryItems.find(category => category.categoryId == this.state.currentCategory).value : ''}</span>
            </div>
	        </div>
	        <section className="area-input-box">
            <label>Select Sub Category</label>
	          <div>
	            <input type="text" id="enter-sub-category" onKeyUp={() => selectSubCategory()}/>
	            <button className="add" onClick={() => addSubCategory()}>+</button>
	          </div>
	        </section>
	        <section className="area-list-box">
	          <ul>{this.state.subCategoryList}</ul>
	        </section>
	        <section className="area-select-box">
	          <button className="select" onClick={() => addSubCategoryToQuery()}>SELECT</button>
	          <button className="clear" onClick={() => showSubCategorySelectionBox(false)}>CLEAR</button>
	        </section>
	        <ul id="settings-sub-category-list" className="settings-area-list custom hidden">
            {this.state.selectedSubCategory}
	        </ul>
	      </section>
	      <section id="custom-date-selection-box" className="date-range-selection-box hidden">
	        <div className="header"><span>Custom Date Range</span></div>
	        <section>
	          <div>
	            <label>Start Date</label>
	            <input type="date" id="settings-custom-start-date" max={(new Date()).toJSON().substr(0,10)} onChange={() => document.getElementById('date-error').classList.add('hidden')}/>
	          </div>
	          <div>
	            <label>End Date</label>
	            <input type="date" id="settings-custom-end-date" max={(new Date()).toJSON().substr(0,10)} onChange={() => document.getElementById('date-error').classList.add('hidden')}/>
	          </div>
	        </section>
	        <footer>
	          <span id="date-error" className="invalid-date">Invalid Date</span>
	          <input type="submit" value="SELECT DATE" className="select-date" onClick={() => addCustomDate()}/>
	          <input type="reset" value="CANCEL" className="cancel-date" onClick={() => hideCustomDateSelectionBox()}/>
	        </footer>
	      </section>
	      <section id="save-segment-box" className="save-segment-box hidden">
	        <div className="header">
            <span>Save Segment</span>
          </div>
	        <section>
	          <div>
	            <label>Segment Name</label>
	            <input type="text" id="segment-name-advanced"/>
	          </div>
	          <div>
	            <label>Description</label>
	            <textarea id="segment-description-advanced"/>
	          </div>
	        </section>
	        <section className="save-button-box">
	          <button id="save-segment-advanced" className="select" onClick={() => saveSegment()}>SAVE</button>
	          <button className="clear" onClick={() => showSaveSegmentBox(false)}>CANCEL</button>
	        </section>
	      </section>
	      <aside id="error-message-box" className="error-message-box hidden">
          <p>{window.instanceOfThis.state.errorMessage}</p>
	        <div>
	          <input type="submit" value="OK" className="hide-alert" onClick={() => hideErrorMessage()}/>
          </div>
	      </aside>
      </div>
    );
  }
}

export default TrapyzNewAdvancedPage;
