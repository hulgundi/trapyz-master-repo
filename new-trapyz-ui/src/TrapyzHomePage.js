import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrapyzLoginPage from './TrapyzLoginPage';
import './TrapyzHomePageNew.css';
import TrapyzAdvancedPage from './TrapyzAdvancedPage';
import TrapyzNewAdvancedPage from './TrapyzNewAdvancedPage';
import foodImage from './foodie.jpg';
import geekImage from './geek.jpg';
import fashionImage from './fashion.jpg';
import autoImage from './automobile.jpg';
import homeImage from './homemaker.jpg';
import personalCareImage from './personal-care.jpg';
import sportImage from './sport.jpg';
import affluentImage from './affluent.jpg';
import travellerImage from './traveller.jpg';
import customImage from './custom.jpg';
import loaderImage from './loader.gif';
import DonutChart from "react-svg-donut-chart";

function makeAjaxGetRequest(requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };

  if (synchType === undefined) {
    synchType = true;
  }

  xmlHttp.open("GET", requestUrl, synchType);
  xmlHttp.send(null);
}

function makeAjaxPostRequest(str, requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();
  var formData = new FormData();

  formData.append('segment', str);

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };

  if (synchType === undefined) {
    synchType = true;
  }

  xmlHttp.open("POST", requestUrl, synchType);
  xmlHttp.send(formData);
}

function preventDefaultAction(event) {
  event.stopPropagation();
}

function logout() {
  var items = ["uname", "name", "key"], item;

  for(item in items) {
    sessionStorage.removeItem(items[item]);
  }

  ReactDOM.render(<TrapyzLoginPage />, document.getElementById('root'));
}

// function refreshSegment(event) {
//   var api, segmentName, targetElement, parentElement;
//   api = sessionStorage.getItem("key");
//   segmentName = event.target.getAttribute("val");
//   parentElement = document.getElementById(segmentName);
//   parentElement.parentNode.getElementsByClassName("trapyz-hamburger-menu")[0].style.display = "none";
//   loader(true);
//
//   makeAjaxGetRequest("https://canvas.trapyz.com/new-refresh-segment.php?api=" + api + "&seg=" + segmentName, function(response) {
//     var countQueryJson = JSON.parse(response.responseText), countQuery = 0;
//     countQuery = countQueryJson[segmentName]["value"];
//     parentElement = document.getElementById(segmentName);
//     targetElement = parentElement.parentNode.parentNode.getElementsByClassName("trapyz-card-count")[0];
//     loader(false);
//     targetElement.innerHTML = countQuery;
//   }, segmentName);
// }

function refreshSegment(event) {
  var api, segmentName, targetElement, parentElement;
  api = sessionStorage.getItem("key");
  segmentName = event.target.getAttribute("val");
  parentElement = document.getElementById(segmentName);
  parentElement.parentNode.getElementsByClassName("trapyz-hamburger-menu")[0].style.display = "none";
  loader(true);

  makeAjaxGetRequest("https://canvas.trapyz.com/refresh-segment-demography.php?api=" + api + "&seg=" + segmentName, function(response) {
    var countQueryJson = JSON.parse(response.responseText), countQuery = 0;

    if(parentElement.parentNode.getElementsByClassName("trapyz-hamburger-menu")[0].attributes.val.value === "demography") {
      countQuery = countQueryJson["value"];
    } else {
      countQuery = countQueryJson[segmentName]["value"];
    }

    parentElement = document.getElementById(segmentName);
    targetElement = parentElement.parentNode.parentNode.getElementsByClassName("trapyz-card-count")[0];
    loader(false);
    targetElement.innerHTML = countQuery;
  }, segmentName);
}

function findTotalActive() {
  var str = JSON.stringify({});
  var baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat-demography";

  makeAjaxPostRequest(str, baseRequestUrl, function(response) {
    var countQueryJson = JSON.parse(response.responseText);
    window.instanceOfThis.setState({totalActive: countQueryJson["value"]});
	  return;
  });
}

function showAdvancedPage() {
  ReactDOM.render(<TrapyzAdvancedPage />, document.getElementById('root'));
}

function showCustomPage() {
  ReactDOM.render(<TrapyzNewAdvancedPage />, document.getElementById('root'));
}

function toggleHamburgerMenu(event){
  var menuItems;
  menuItems = event.target.parentNode.getElementsByClassName("trapyz-hamburger-menu");

  if(menuItems[0].style.display === "inline-block") {
  	closeHamburgerMenu();
    menuItems[0].style.display = "none";
  } else {
  	closeHamburgerMenu();
    menuItems[0].style.display = "inline-block";
    window.addEventListener("keyup",escapeHamburgerEvent);
  }
}

function closeHamburgerMenu(){
  var menuItems;
  menuItems = document.getElementsByClassName("trapyz-hamburger-menu");

  for(var i=0; i<menuItems.length; i++) {
    menuItems[i].style.display = "none";
    window.removeEventListener("keyup",escapeHamburgerEvent);
  }
}

function escapeHamburgerEvent(event){
  event.preventDefault();

  if(event.keyCode === 27){
	  closeHamburgerMenu();
  }
}

function loader(loading) {
  if(loading) {
	  document.getElementById('loader').classList.remove('hidden');
  } else {
	  document.getElementById('loader').classList.add('hidden');
  }
}

function showEditSegment() {}
function exportSegment() {}
function showConfirmationExport() {}
function revertSegment() {}

function showConfirmation(event) {
  document.getElementsByClassName("trapyz-confirmation-outer-box")[0].classList.remove('hidden');
  window.instanceOfThis.segmentToDelete = event.target.getAttribute("val");
  // window.instanceOfThis.segmentToDeleteType = event.target.parentNode.getAttribute("val");
  event.target.parentNode.style.display = "none";
}

function hideConfirmation() {
  document.getElementsByClassName("trapyz-confirmation-outer-box")[0].classList.add('hidden');
  window.instanceOfThis.segmentToDelete = "";
}

// function showConfirmationExport() {
//   document.getElementsByClassName("trapyz-confirmation-export-outer-box")[0].style.display = "block";
// }

function deleteSegment() {
  var api, segmentName, url = "https://canvas.trapyz.com/delete-segment-demography.php?api=";

  if (window.instanceOfThis.segmentToDelete !== "") {
    api = sessionStorage.getItem("key");
    segmentName = window.instanceOfThis.segmentToDelete;

    makeAjaxGetRequest(url + api + "&seg=" + segmentName, function() {
      window.location.reload();
    });
  }
}

// function deleteSegment() {
//   var api, segmentName, advanceUrl = "https://canvas.trapyz.com/delete-segment.php?api=",
//   demographyUrl = "https://canvas.trapyz.com/delete-segment-demography.php?api=";
//
//   if (window.instanceOfThis.segmentToDelete !== "") {
//     api = sessionStorage.getItem("key");
//     segmentName = window.instanceOfThis.segmentToDelete;
//
//     if(window.instanceOfThis.segmentToDeleteType == "advance") {
//       makeAjaxGetRequest(advanceUrl + api + "&seg=" + segmentName, function() {
//         window.location.reload();
//       });
//     } else if(window.instanceOfThis.segmentToDeleteType == "demography") {
//       makeAjaxGetRequest(demographyUrl + api + "&seg=" + segmentName, function() {
//         window.location.reload();
//       });
//     }
//   }
// }

function showDemographyEditSegment(event) {
  var trapyz = window.instanceOfThis, segmentElement,
	segmentQueryJson = {
	    "segmentName": "",
	    "segmentDesc": "",
	    "api": "",
	    "segmentCount": "",
	    "query": [],
	    "existingSegmentName": ""
	};

  segmentQueryJson.api = trapyz.api;
  segmentQueryJson.existingSegmentName = event.target.getAttribute("val");
  segmentElement = document.getElementById(segmentQueryJson.existingSegmentName);
  var segmentCardElement = segmentElement.parentNode.parentNode;

  makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentDetailsDemography.php?api=" + segmentQueryJson.api + "&seg=" + encodeURIComponent(segmentQueryJson.existingSegmentName), function(response) {
	  var responseJson, queryJson;
	  responseJson = JSON.parse(response.responseText);
	  queryJson = JSON.parse(responseJson["attributeJson"]);
	  segmentQueryJson.query = queryJson;
	  segmentQueryJson.segmentDesc = responseJson.description;
	  segmentQueryJson.segmentName = responseJson.name;
	  segmentQueryJson.segmentCount = responseJson.segmentCount;
    segmentQueryJson.id = responseJson.id;
	  // closeHamburgerMenu();

    if(responseJson.type == "demography") {
	    ReactDOM.render(<TrapyzAdvancedPage data={segmentQueryJson}/>, document.getElementById('root'));
    } else if(responseJson.type == "custom" || responseJson.type == "interest") {
      ReactDOM.render(<TrapyzNewAdvancedPage data={segmentQueryJson}/>, document.getElementById('root'));
    }

    window.scrollTo(0, 0);
  }, segmentQueryJson.segmentName);
}

// function showAdvanceEditSegment(event) {
//   var trapyz = window.instanceOfThis, segmentElement,
// 	segmentQueryJson = {
// 	    "segmentName": "",
// 	    "segmentDesc": "",
// 	    "api": "",
// 	    "segmentCount": "",
// 	    "queryList": [],
// 	    "existingSegmentName": "",
// 	    "segmentVisit": ""
// 	};
//
//   segmentQueryJson.api = trapyz.api;
//   segmentQueryJson.existingSegmentName = event.target.getAttribute("val");
//   segmentElement = document.getElementById(segmentQueryJson.existingSegmentName);
//   // trapyz.existingSegmentName = segmentQueryJson.existingSegmentName;
//   // trapyz.segmentQueryJson.existingSegmentName = segmentQueryJson.existingSegmentName;
//   var segmentCardElement = segmentElement.parentNode.parentNode;
//
//   makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentDetailsAdvance.php?api=" + segmentQueryJson.api + "&seg=" + encodeURIComponent(segmentQueryJson.existingSegmentName), function(response) {
// 	  var responseJson, queryJson;
// 	  responseJson = JSON.parse(response.responseText);
// 	  queryJson = JSON.parse(responseJson["attributeJson"]);
// 	  segmentQueryJson.queryList = queryJson.queryList;
// 	  segmentQueryJson.segmentDesc = responseJson.description;
// 	  segmentQueryJson.segmentName = responseJson.name;
// 	  segmentQueryJson.segmentVisit = queryJson.segmentVisit;
// 	  segmentQueryJson.segmentCount = segmentCardElement.getElementsByClassName("trapyz-card-count")[0].innerHTML;
// 	  closeHamburgerMenu();
// 	  // window.addEventListener("keyup",escapeEvent);
// 	  // document.getElementById("trapyz-new-advanced-page-container").style.display = "block";
// 	  // ReactDOM.unmountComponentAtNode(document.getElementById('trapyz-new-advanced-page'));
// 	  ReactDOM.render(<TrapyzNewAdvancedPage data={segmentQueryJson}/>, document.getElementById('root'));
// 	  window.scrollTo(0, 0);
//   }, segmentQueryJson.segmentName);
// }

function initialiseLifeStyle() {
  var lifeStylesList = [
    {"lifeStyle":"Upper Class","counter":12300},
    {"lifeStyle":"Middle Class","counter":5000},
    {"lifeStyle":"New-Middle Class","counter":12000},
    {"lifeStyle":"In Between","counter":10000},
    {"lifeStyle":"Below Poverty Line","counter":7000}
  ], dataPie = [
    {value: 0, stroke: "#F5D44F", strokeWidth: 8},
    {value: 0, stroke: "#F6B755", strokeWidth: 8},
    {value: 0, stroke: "#EE7E73", strokeWidth: 8},
    {value: 0, stroke: "#6BB5E5", strokeWidth: 8},
    {value: 0, stroke: "#61C48C", strokeWidth: 8}
  ], lifeStyles = [];

  lifeStylesList.forEach((i,index) => (dataPie[index]['value'] = i['counter']));
  lifeStylesList.forEach(function(lifeStyle) {
    lifeStyles.push(
      <div>
        <label>{lifeStyle['lifeStyle']}</label>
        <span className="color-box"></span>
        <span className="display-counter">{modifyDisplayCount(lifeStyle['counter'])}</span>
      </div>
    );
  });

  window.instanceOfThis.setState({lifeStyle: lifeStyles});
  window.instanceOfThis.setState({dataPie: dataPie});
}

function initialiseIncomeSegment() {
  var incomeSegmentsList = [
    {"income":"< 20K","counter":12000},
    {"income":"20K - 40K","counter":10000},
    {"income":"40K - 80K","counter":13500},
    {"income":"80K - 120K","counter":10000},
    {"income":"120K - 160K","counter":9000},
    {"income":"160K - 200K","counter":8000},
    {"income":"200K - 250K","counter":7000},
    {"income":"250K - 300K","counter":6000},
    {"income":"300K - 400K","counter":5000},
    {"income":"> 400K","counter":4000}
  ], incomeSegments = [];

  var maxCount =  incomeSegmentsList.reduce((i,j) => (i['counter']>=j['counter']? i : j));
  var incomeSegmentsListPx = {}, containerWidth = document.getElementsByClassName('income-segment')[0].offsetWidth;
  containerWidth = ((containerWidth * 0.92)/2) - 38 - 70;

  incomeSegmentsList.forEach((i) => (incomeSegmentsListPx[i['income']] = i['counter']/maxCount['counter']*containerWidth));
  incomeSegmentsList.forEach(function(income) {
    incomeSegments.push(
      <div>
        <label>{income['income']}</label>
        <span className="display-counter">{modifyDisplayCount(income['counter'])}</span>
        <span className="scale" style={{width: incomeSegmentsListPx[income['income']] + 'px'}}></span>
      </div>
    );
  });

  window.instanceOfThis.setState({incomeSegments: incomeSegments});
}

function initialiseTopCities() {
  var topCities = [
    {"city":"Bangalore","counter":25000},
    {"city":"Hyderabad","counter":20000},
    {"city":"Pune","counter":18000},
    {"city":"Mumbai","counter":17000},
    {"city":"Gurgaon","counter":13000}
  ], topCitiesList = [], images = [], topCitiesCounterPx = [];
  var maxtopCities = topCities.reduce((i,j) => (i['counter']>=j['counter']? i : j));
  var topCitiesCounter = {}, containerWidth = document.getElementsByClassName('static-city')[0].offsetWidth;
  containerWidth = ((containerWidth * 0.88) - 120);
  topCities.forEach((i) => (topCitiesCounter[i['city']] = Math.floor(i['counter']/maxtopCities['counter']*containerWidth/16)));
  topCities.forEach((i) => (topCitiesCounterPx[i['city']] = i['counter']/maxtopCities['counter']*containerWidth));

  topCities.forEach(function(city) {
    images = [];

    for(var i=0;i<topCitiesCounter[city['city']];i++) {
      images.push(
        <img src="./assets/Map.png" className="my-2" width="12px" height="18px"/>
      );
    }

    topCitiesList.push(
      <div>
        <label>{city['city']}</label>
        <span className="display-counter">{modifyDisplayCount(city['counter'])}</span>
        <span style={{width: topCitiesCounterPx[city['city']] + 'px'}}>{images}</span>
      </div>
    );
  });

  window.instanceOfThis.setState({topCitiesList: topCitiesList});
}

function initialisePhoneBrands() {
  var phoneBrands = [
    {"phone":"Google Pixel 2 XL","counter":2600},
    {"phone":"Samsung Galaxy Note 8","counter":2400},
    {"phone":"Samsung Galaxy S9 Plus","counter":2350},
    {"phone":"Honor View 10","counter":2000},
    {"phone":"OnePlus 5T","counter":1500}
  ], phoneBrandsList = [], images = [], phoneBrandCounterPx = [];
  var maxPhoneBrand = phoneBrands.reduce((i,j) => (i['counter']>=j['counter']? i : j));
  var phoneBrandCounter = {}, containerWidth = document.getElementsByClassName('phone-brand')[0].offsetWidth;
  containerWidth = containerWidth * 0.88 * 0.46;
  phoneBrands.forEach((i) => (phoneBrandCounter[i['phone']] = Math.floor(i['counter']/maxPhoneBrand['counter']*containerWidth/16)));
  phoneBrands.forEach((i) => (phoneBrandCounterPx[i['phone']] = i['counter']/maxPhoneBrand['counter']*containerWidth));

  phoneBrands.forEach(function(phone) {
    images = [];

    for(var i=0;i<phoneBrandCounter[phone['phone']];i++) {
      images.push(
        <img src="./assets/Mobile.png" className="my-2" width="12px" height="18px"/>
      );
    }

    phoneBrandsList.push(
      <div>
        <label>{phone['phone']}</label>
        <span className="display-counter">{modifyDisplayCount(phone['counter'])}</span>
        <span style={{width: phoneBrandCounterPx[phone['phone']] + 'px'}}>{images}</span>
      </div>
    );
  });

  window.instanceOfThis.setState({phoneBrandsList: phoneBrandsList});
}

function initialiseTop5Categories(top5CategoriesList) {
  var top5Categories = [], maxCat =  top5CategoriesList.reduce((i,j) => (i['val']>=j['val']? i : j));
  var top5CategoriesListPx = {}, containerWidth = document.getElementsByClassName('top-5-categories')[0].offsetWidth;
  containerWidth = (containerWidth * 0.88) - 50 - 124;

  top5CategoriesList.forEach((i) => (top5CategoriesListPx[i['cat']] = i['val']/maxCat['val']*containerWidth));
  top5CategoriesList.forEach(function(category) {
    top5Categories.push(
      <div>
        <label>{category['cat']}</label>
        <span className="display-counter">{category['dval']}</span>
        <span className="scale" style={{width: top5CategoriesListPx[category['cat']] + 'px'}}></span>
      </div>
    );
  });

  window.instanceOfThis.setState({top5Categories: top5Categories});
}

function initialiseAgeGenders() {
  var staticAgeGenders = [],
  ageGenders = [
    {"min":18,"max":24,"M":1200,"F":1400},
    {"min":25,"max":39,"M":1300,"F":1100},
    {"min":40,"max":44,"M":1600,"F":1500},
    {"min":45,"max":54,"M":1300,"F":1350},
    {"min":55,"max":70,"M":900,"F":800}
  ], ageGenderCounter = [];

  ageGenders.forEach((a) => (ageGenderCounter.push(a["M"], a["F"])));
  var maxAgeGenderCounter = ageGenderCounter.reduce((i,j) => (i>=j ? i : j));
  var ageGenderCounterPx = {};
  ageGenderCounter.forEach((i) => (ageGenderCounterPx[i] = i/maxAgeGenderCounter*77));

  ageGenders.forEach(function(ageGender) {
    staticAgeGenders.push(
      <div className="age-gender__child">
        <div className="scaled-value">
          <div className="male">
            <span style={{height: ageGenderCounterPx[ageGender["M"]] + 'px'}}></span>
            <label>{ageGender["M"]}</label>
          </div>
          <div className="female">
            <span style={{height: ageGenderCounterPx[ageGender["F"]] + 'px'}}></span>
            <label>{ageGender["F"]}</label>
          </div>
        </div>
        <div>
          <img src="./assets/Man.png" className="man" alt="Man" width="11px" height="29px"/>
          <img src="./assets/Woman.png" alt="Woman" width="15px" height="29px"/>
        </div>
        <span className="desc">{ageGender["min"] + " - " + ageGender["max"] + " yrs"}</span>
        <span className="">{ageGender["F"] + ageGender["M"]}</span>
      </div>
    );
  });

  window.instanceOfThis.setState({ageGenders: staticAgeGenders});
}

function modifyDisplayCount(val) {
  if(val >= 10000 && val < 1000000) {
    return ((Math.floor(val/1000)) + 'K');
  } else if(val >= 1000000) {
    return ((Math.floor(val/1000000)) + 'M');
  } else {
    return val;
  }
}

class TrapyzHomePage extends Component {
  constructor (props) {
    super(props);
    this.api = sessionStorage.getItem("key");
    this.topCategoriesDone = false;
    this.segmentCardsDone = false;
    this.descDone = false;
    this.export=false;
    this.areasDone = false;
    this.customSegmentsDone = false;
    this.uniqueGids = {};
    this.queryJson = {};
    this.customCardsList = [];
    this.customSegmentJson = {};
    this.username = sessionStorage.getItem("name");
    this.segmentToDelete = "";
    this.state = {
 	    totalActive: 0,
	    weeklyActive: 0,
	    monthlyActive: 0,
	    uniqueCount: 0,
	    cityItems: [],
	    cityEditItems: [],
	    areaItems: {},
	    top5Categories: [],
	    segmentCards: [],
	    interestItems: [],
	    interestEditItems: [],
	    customSegmentCards: [],
      descMap: {},
      presetCardsList: [],
      ageGenders: [],
      phoneBrandsList: [],
      topCitiesList: [],
      incomeSegments: [],
      lifeStyle: [],
      dataPie: []
    }
  }

  componentDidMount () {
    loader(true);
    initialiseAgeGenders();
    initialisePhoneBrands();
    initialiseTopCities();
    initialiseIncomeSegment();
    initialiseLifeStyle();
    findTotalActive();
  }

  render() {

    window.instanceOfThis = this;

    if (!this.descDone) {
      this.descDone = true;

      makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentDesc.php?api=" + this.api, function(response) {
        window.instanceOfThis.setState({descMap: JSON.parse(response.responseText)});
      });
    }

    if (!this.citiesDone) {
      this.citiesDone = true;

      makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap?mapType=city", function(response) {
        var responseJson, keys = [], citiesList = [], index;

	      if (response.responseText === "\n") {
	        responseJson = "";
	      } else {
          responseJson = JSON.parse(response.responseText);
	      }

        keys = Object.keys(responseJson);

        for (index = 0; index < keys.length; index++) {
          citiesList.push(responseJson[keys[index]]);
        }

        window.instanceOfThis.setState({cityItems: citiesList});
      });
    }

    if (this.state.weeklyActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=week&api=" + this.api, function(response) {
        window.instanceOfThis.setState({weeklyActive: response.responseText.trim()});
      });
    }

    if (this.state.monthlyActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=month&api=" + this.api, function(response) {
        window.instanceOfThis.setState({monthlyActive: response.responseText.trim()});
      });
    }

    if (!this.topCategoriesDone) {
      var responseJson, sortedCategoriesResult = [], top5CategoriesList = [], h= {};
      this.topCategoriesDone = true;

      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=catgid&api=" + this.api, function(response) {
        var index, key;
        responseJson = JSON.parse(response.responseText);

        for (key in responseJson) {
          sortedCategoriesResult.push([key, responseJson[key]]);
        }

        sortedCategoriesResult.sort(function(a, b) {
          return b[1] - a[1];
        });

        for (index = 0; index < 5 && index < sortedCategoriesResult.length; index++) {
          var sortedCategoryNumber = modifyDisplayCount(sortedCategoriesResult[index][1]);
          h = {}
          h['cat'] = sortedCategoriesResult[index][0];
          h['val'] = sortedCategoriesResult[index][1];
          h['dval'] = sortedCategoryNumber;
          top5CategoriesList.push(h);
        }

        initialiseTop5Categories(top5CategoriesList);
      });
    }

    if (!this.segmentCardsDone) {
      var sortedSegmentsResult = [],

      segmentNameMap = {
        "Traveller": {
          "image": travellerImage
        },
        "Sports Enthusiast": {
          "image": sportImage
        },
        "Foodie": {
          "image": foodImage
        },
        "Personal Care": {
          "image": personalCareImage
        },
        "Geek": {
          "image": geekImage
        },
        "Auto Enthusiast": {
          "image": autoImage
        },
        "Fashionista": {
          "image": fashionImage
        },
        "Homemaker": {
          "image": homeImage
        },
        "Affluent": {
          "image": affluentImage
        },
	      "Custom": {
	         "image": customImage
	      }
      };

      this.segmentCardsDone = true;
      var apiKey = this.api;

      makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentCardsData.php?api=" + this.api, function(response) {
        var idString = "", presetCardsList = [], tempCardsList = [], customCardsList = [], key, index, imgSrc = "", cardsList = [], hamburgerMenu = [], classText;
        responseJson = JSON.parse(response.responseText);

        for (key in responseJson) {
          sortedSegmentsResult.push([key, responseJson[key]["value"]]);
        }

        sortedSegmentsResult.sort(function(a, b) {
          return b[1] - a[1];
        });

        presetCardsList.push(
          <article className="create-segment">
      	    <a onClick={showAdvancedPage.bind(this)}>
        		  <img src="./assets/Add.png" alt="Add" width="22px" height="22px"/>
        		  <span>Add Demographic Segment</span>
    	  	  </a>
    		    <a onClick={showCustomPage.bind(this)}>
      		    <img src="./assets/Add.png" alt="Add" width="22px" height="22px"/>
      		    <span>Add Custom Segment</span>
    		    </a>
  		    </article>
  		  );

        // for (index = 0; index < sortedSegmentsResult.length; index++) {
        //   idString = "trapyz-card-" + index;
	      //   hamburgerMenu = [];
        //
        //   if (Object.keys(segmentNameMap).some(x => x === sortedSegmentsResult[index][0])) {
	      //     imgSrc = segmentNameMap[sortedSegmentsResult[index][0]]["image"];
        //
        //     hamburgerMenu.push(
	      //       <div className="trapyz-hamburger-menu">
		    //         <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={refreshSegment}>Refresh</div>
        //         <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]}>Edit</div>
        //         <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={revertSegment}>Revert</div>
		    //         <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={exportSegment}>Export to csv</div>
		    //         <div className="trapyz-hamburger-menu-item inactive" val={sortedSegmentsResult[index][0]}>Export to csv</div>
		    //         <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={showConfirmationExport}>Export to csv</div>
		    //       </div>
	      //     );
	      //   }
        //   else if (responseJson[sortedSegmentsResult[index][0]]["isAdvance"] == 1) {
	      //     imgSrc = segmentNameMap["Custom"]["image"];
        //
        //     hamburgerMenu.push(
	      //       <div className="trapyz-hamburger-menu advance" val="advance">
		    //         <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={refreshSegment}>Refresh</div>
        //         <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={showDemographyEditSegment}>Edit</div>
        //         <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={showConfirmation}>Delete</div>
        //         <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={exportSegment}>Export to csv</div>
		    //         <div className="trapyz-hamburger-menu-item inactive" val={sortedSegmentsResult[index][0]}>Export to csv</div>
		    //         <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={showConfirmationExport}>Export to csv</div>
		    //       </div>
	      //     );
	      //   }
        //
        //   else {
	      //     imgSrc = segmentNameMap["Custom"]["image"];
	      //     hamburgerMenu.push(
	      //       <div className="trapyz-hamburger-menu">
		    //         <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={refreshSegment}>Refresh</div>
        //         <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]}>Edit</div>
        //         <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={showConfirmation}>Delete</div>
        //         <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={exportSegment}>Export to csv</div>
		    //         <div className="trapyz-hamburger-menu-item inactive" val={sortedSegmentsResult[index][0]}>Export to csv</div>
		    //         <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={showConfirmationExport}>Export to csv</div>
		    //       </div>
	      //     );
	      //   }
        //
        //   presetCardsList.push(
        //     <article className="segment-card" id={idString}>
   			//       <div>
      	// 	      <img src={imgSrc} alt="Affluent Image" width="175px" height="111px"/>
      	// 	      <summary>
			  //           <label>{sortedSegmentsResult[index][0]}</label>
				//           <span className="trapyz-card-count">{sortedSegmentsResult[index][1]}</span>
	  		// 	      </summary>
	  		// 	      <img id={sortedSegmentsResult[index][0]} className="hamburger" src="./assets/hamburger.png" width="18px" height="18px" onClick={toggleHamburgerMenu}/>
	  		// 	      {hamburgerMenu}
			  //       </div>
			  //       <p>{window.instanceOfThis.state.descMap[sortedSegmentsResult[index][0]]}</p>
  			//     </article>
        //   );
        // }

        makeAjaxGetRequest("https://canvas.trapyz.com/getDemographySegmentCardsData.php?api=" + apiKey, function(response) {
          responseJson = JSON.parse(response.responseText), sortedSegmentsResult = [];

          responseJson.forEach(function(segment) {
            idString = "trapyz-card-demography", imgSrc = segmentNameMap["Custom"]["image"];
  	        hamburgerMenu = [];

            if(segment["type"] == "preset") {
              imgSrc = segmentNameMap[segment["name"]]["image"];

              hamburgerMenu.push(
                <div className="trapyz-hamburger-menu" val={segment["type"]}>
                  <div className="trapyz-hamburger-menu-item" val={segment["name"]} onClick={refreshSegment}>Refresh</div>
                  <div className="trapyz-hamburger-menu-item inactive" val={segment["name"]}>Export to csv</div>
                </div>
              );
            } else {
              hamburgerMenu.push(
                <div className="trapyz-hamburger-menu" val={segment["type"]}>
                  <div className="trapyz-hamburger-menu-item" val={segment["name"]} onClick={refreshSegment}>Refresh</div>
                  <div className="trapyz-hamburger-menu-item" val={segment["name"]} onClick={showDemographyEditSegment}>Edit</div>
                  <div className="trapyz-hamburger-menu-item" val={segment["name"]} onClick={showConfirmation}>Delete</div>
                  <div className="trapyz-hamburger-menu-item inactive" val={segment["name"]}>Export to csv</div>
                </div>
              );
            }

            presetCardsList.push(
              <article className="segment-card" id={idString}>
     			      <div>
        		      <img src={imgSrc} alt="Custom Image" width="175px" height="111px"/>
        		      <summary>
  			            <label>{segment["name"]}</label>
  				          <span className="trapyz-card-count">{segment["count"]}</span>
  	  			      </summary>
  	  			      <img id={segment["name"]} className="hamburger" src="./assets/hamburger.png" width="18px" height="18px" onClick={toggleHamburgerMenu}/>
  	  			      {hamburgerMenu}
  			        </div>
  			        <p>{segment["desc"]}</p>
    			    </article>
            );
          });

          var butns = presetCardsList.shift();

          presetCardsList.sort(function(a,b) {
            return b.props.children[0].props.children[1].props.children[1].props.children - a.props.children[0].props.children[1].props.children[1].props.children;
          });

          tempCardsList.push(butns);
          presetCardsList.forEach((card) => tempCardsList.push(card));
          window.instanceOfThis.setState({segmentCards: tempCardsList});
        });

        loader(false);
      });
    }

    return (
      <div>
      	<header>
          <div className="heading">
	          <section className="logo">
	            <div>
	              <img src="./assets/TrapyzLogo.png" alt="Trapyz Logo" height="24px" width="60px"/>
	            </div>
	            <span>Audience Insights Platform</span>
	          </section>
	          <section className="installed">
	            <div>
	              <label>Total Installed</label>
	              <span>{this.state.totalActive}</span>
	            </div>
	            <div>
	              <label>Weekly Active</label>
	              <span>{this.state.weeklyActive}</span>
	            </div>
	            <div>
	              <label>Monthly Active</label>
	              <span>{this.state.monthlyActive}</span>
	            </div>
	          </section>
          </div>
          <aside className="session">
	          <span>Demo User</span>
	          <img src="./assets/Logout.png" onClick={logout} alt="Logout" width="24px" height="24px"/>
          </aside>
        </header>
        <main>
          <section className="category">
	          <h2>Customers by Category</h2>
	          <section>
	            <article>
	              <figure className="age-gender">
                  {this.state.ageGenders}
                </figure>
	              <figcaption>
	                <span>Customer by</span>
	                <label>AGE & GENDER</label>
	              </figcaption>
	            </article>
	            <article>
	              <figure className="phone-brand">
                  {this.state.phoneBrandsList}
                </figure>
	              <figcaption>
	                <span>Customer by</span>
	                <label>TOP 5 PHONE BRAND & MODEL</label>
	    	        </figcaption>
	            </article>
	            <article>
	              <figure className="top-5-categories">
                  {this.state.top5Categories}
                </figure>
	              <figcaption>
	                <span>Customer by</span>
	                <label>TOP 5 VISITED CATEGORIES</label>
	              </figcaption>
	            </article>
	            <article>
	              <figure className="income-segment">
                  {this.state.incomeSegments}
                </figure>
	              <figcaption>
	                <span>Customer by</span>
	                <label>INCOME SEGMENT</label>
	              </figcaption>
	            </article>
	            <article>
	              <figure className="lifestyle">
                  <div className="caste">
                    {this.state.lifeStyle}
                  </div>
                  <div className="donut-chart">
                    {<DonutChart data={this.state.dataPie}/>}
                  </div>
                </figure>
	              <figcaption>
	                <span>Customer by</span>
	                <label>LIFESTYLE AFFINITY SEGMENT</label>
	              </figcaption>
	            </article>
	            <article>
	              <figure className="static-city">
                  {this.state.topCitiesList}
                </figure>
	              <figcaption>
	                <span>Customer by</span>
	                <label>TOP 5 CITY</label>
	              </figcaption>
	            </article>
	          </section>
          </section>
          <section className="segments">
	          <h2>Your Customised Segments</h2>
	          <section>
	            {this.state.segmentCards}
            </section>
          </section>
        </main>
        <aside id="loader" className="loader hidden">
	        <img src="./assets/progress-bar.gif" alt=""/>
	      </aside>
        <div class="trapyz-confirmation-outer-box hidden">
          <div class="trapyz-confirmation-inner-box">
            <p>Are you sure you want to delete this segment ?</p>

            <div class="trapyz-confirmation-button-holder">
              <div class="trapyz-confirmation-ok-button" onClick={() => deleteSegment()}>Yes</div>
              <div class="trapyz-confirmation-cancel-button" onClick={() => hideConfirmation(false)}>No</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TrapyzHomePage;
