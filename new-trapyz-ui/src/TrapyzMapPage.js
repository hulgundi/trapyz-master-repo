import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import trapyzLogo from './trapyz-white-icon.png';
import dailyHuntLogo from './dailyhunt-logo.png';
import trapyzLogo2 from './trapyz-logo.png';
import trapyzMapLogo from './google-map-icon.png';
import segmentIcon from './segment-icon.png';
import chevronDown from './logout-image.png';
import loaderImage from './loader.gif';
import TrapyzLoginPage from './TrapyzLoginPage';
import TrapyzHomePage from './TrapyzHomePage';
import './TrapyzHomePage.css';
 
function makeAjaxGetRequest(requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();
  
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };
  if (synchType === undefined) {
    synchType = true;
  }
  xmlHttp.open("GET", requestUrl, synchType);
  xmlHttp.send(null);
}

function preventDefaultAction(event) {
  event.stopPropagation();
}

function logout() {
  var items = ["uname", "name", "key"], item;

  for(item in items) {
    sessionStorage.removeItem(items[item]);  
  }
  ReactDOM.render(<TrapyzLoginPage />, document.getElementById('root'));  
}

function toggleLoader(flag) {
  if (flag) {
    document.getElementById("trapyz-main-tint-div").style.display = "block";
  } else {
    document.getElementById("trapyz-main-tint-div").style.display = "none";
  }
}

function showMapPage() {
      ReactDOM.render(<TrapyzMapPage />, document.getElementById('root'));  
}

function showHomePage() {
    var latLngData = window.instanceOfThis.state.latLngList;
    ReactDOM.render(<TrapyzHomePage data={latLngData} />, document.getElementById('root'));  
}

var map, heatmap, markers = [], markerView = false;

function initMap() {
    var google = window.google;

    if(!google) {
	google = window.instanceOfThis.props.google;
    }

    if(window.instanceOfThis.state.latLngList.length == 0){
	window.instanceOfThis.state.latLngList = window.instanceOfThis.props.data;
    }
    
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: {lat: 22.0574, lng: 82.9382}
        //mapTypeId: 'roadmap'
	//mapTypeId: google.maps.MapTypeId.SATELLITE
    });
    
    heatmap = new google.maps.visualization.HeatmapLayer({
        data: getPoints(),
        map: map,
	radius: 30,
	opacity: 0.8
    });

    
    map.addListener('zoom_changed', function() {
	if(!markerView) {
	    if(map.get('zoom') >= 14) {
		heatmap.set('radius', 60);
	    } else {
		heatmap.set('radius', 30);
	    }
	}
    });
    
    setMarkers();
    clearMarkers();
}

function setMarkers() {
    var ind=0, hash;
    var latLngs = window.instanceOfThis.state.latLngList;

    while(ind<latLngs.length) {
	hash = latLngs[ind];
	addMarker({lat: hash.latitude, lng: hash.longitude}, hash.sname);
	ind = ind + 1;
    }
}

function addMarker(location, sname) {
    var google = window.google, titleLatLng = (location['lat'].toString() + ', ' + location['lng'].toString());
    var marker = new google.maps.Marker({
        position: location,
        map: map,
	title: sname
    });
    markers.push(marker);
}

function toggleHeatmap() {
    heatmap.setMap(heatmap.getMap() ? null : map);
}

function toggleMarkers() {
    if(markerView) {
	clearMarkers();
	heatmap.setMap(map);
	markerView = false;
    } else {
	heatmap.setMap(null);
	showMarkers();
	markerView = true;
    }
}

function changeGradient() {
    var gradient = [
        'rgba(0, 255, 255, 0)',
        'rgba(0, 255, 255, 1)',
        'rgba(0, 191, 255, 1)',
        'rgba(0, 127, 255, 1)',
        'rgba(0, 63, 255, 1)',
        'rgba(0, 0, 255, 1)',
        'rgba(0, 0, 223, 1)',
        'rgba(0, 0, 191, 1)',
        'rgba(0, 0, 159, 1)',
        'rgba(0, 0, 127, 1)',
        'rgba(63, 0, 91, 1)',
        'rgba(127, 0, 63, 1)',
        'rgba(191, 0, 31, 1)',
        'rgba(255, 0, 0, 1)'
    ]
    heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

function showMarkers() {
    setMapOnAll(map);
}
/*
function changeRadius() {
    heatmap.set('radius', 30);
}

function changeOpacity() {
    heatmap.set('opacity', heatmap.get('opacity') ? null : 0.8);
}
*/
function getPoints() {
    var ind=0, pointsList=[], hash;
    var latLngs = window.instanceOfThis.state.latLngList;
    const google = window.google;
    while(ind<latLngs.length) {
	hash = latLngs[ind];
	pointsList.push(new google.maps.LatLng(hash.latitude, hash.longitude));
	ind = ind + 1;
    }

    return pointsList;
}

function showMap() {
    initMap();
    document.getElementById('map').style.position = 'absolute';
    document.getElementById('map').style.overflow = 'hidden';
    document.getElementById('map').style.width = '700px';
    document.getElementById('map').style.top = '93px';
    document.getElementById('map').style.height = '698px';
    document.getElementById('map').style.left = '50px';
}

function loadLatLng() {
    if(window.instanceOfThis.props.data.length == 0) {
	makeAjaxGetRequest("https://canvas.trapyz.com/getLatLngNew.php", function(response) {
            var responseJson, keys = [], latLng = [], index, latLngParts = [];
	    window.instanceOfThis.setState({latLngList: []});

	    if (response.responseText === "\n") {
		responseJson = "";
	    } else {
		responseJson = JSON.parse(response.responseText);
	    }
	    
            keys = Object.keys(responseJson);
            for (index = 0; index < keys.length; index++) {
		latLngParts = responseJson[keys[index]].split("_");
		latLng.push({ "latitude":parseFloat(latLngParts[0]), "longitude":parseFloat(latLngParts[1]) ,"sname":latLngParts[2]});
            }
            window.instanceOfThis.setState({latLngList: latLng});
	    showMap();
	    toggleLoader(false);
	});  
    } else {
	window.instanceOfThis.setState({latLngList: window.instanceOfThis.props.data});
	showMap();
	toggleLoader(false);
    }
}

class TrapyzMapPage extends Component {
  constructor (props) {
    super(props);
    this.api = sessionStorage.getItem("key");
    this.topCategoriesDone = false;
    this.segmentCardsDone = false;
    this.defaultDistanceCondition = "lt";
    this.defaultVisitCondition = "ge";
    this.defaultDistance = "100";
    this.defaultDimensionName = "cat";
    this.interestDone = false;
    this.descDone = false;
    this.export=false;
    this.citiesDone = false;
    this.isQueryModified = false;
    this.areasDone = false;
    this.customSegmentsDone = false;
    this.visit = "1";
    this.city = "";
    this.pin = "";
    this.dateType="";
    this.startDate = null;
    this.endDate = null;
    this.segmentName = "";
    this.segmentDesc = "";
    this.existingSegmentName = "";
    this.uniqueGids = {};
    this.ulist=[];
    this.queryJson = {
    "dimension": {
      "name": "",
      "value": ""
    },
    "visit": {
      "condition": "gt",
      "value": "1"  
    },
    "distance": {
      "condition": "",
      "value": ""
    },
    "duration": {
      "type": "",
      "startDate": "",
      "endDate": ""
    },
    "city": "",
    "pin": ""
  };
    this.customCardsList = [];
    this.customSegmentJson = {};
    this.segmentToDelete = "";
    this.segmentQueryJson = {
      "segmentName": "",
      "segmentDesc": "",
      "api": "",
      "segmentCount": "",
      "queryList": [],
      "existingSegmentName": "",
      "segmentVisit":""
    };
    this.username = sessionStorage.getItem("name");
    this.interestCategoryMap = {
      "Automotive":[
      "20",
      "8"
      ],
      "Fashion": [
        "3",
        "1",
        "33",
        "16"
      ],
      "Beauty": [
        "27"
      ],
      "Finance":[
      "9"
      ],
      "Education": [
        "34"
      ],
      "Entertainment": [
        "13"
      ],
      "F&B": [
        "14"
      ],
      "Fitness": [
        "29",
        "21",
        "28"
      ],
      "Home & Lifestyle": [
        "22"
      ],
      "Leisure & Hobbies": [
        "31",
        "26",
        "35"
      ],
      "Groceries": [
        "25",
        "10"
      ],
      "Travel": [
        "32",
        "23"
      ],
      "Electronics":[
      "11"
      ]
    };
    this.state = {
      totalActive: 0,
      weeklyActive: 0,
      monthlyActive: 0,
      uniqueCount: 0,
      selectedCity: 0,
      queryCount: 0,
      currentQueryCount: 0,
      cityItems: [],
      cityEditItems: [],
      areaItems: {},
      selectedAreaItems: [],
      selectedEditAreaItems: [],
      top5Categories: [],
      segmentCards: [],
      interestItems: [],
      interestEditItems: [],
      customSegmentCards: [],
      descMap: {},
      latLngList: []
    }
  }

    componentDidMount () {
	toggleLoader(true);
	loadLatLng();
	clearMarkers();
    }

  render() {

    window.instanceOfThis = this;
      
    if (!this.descDone) {
      this.descDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentDesc.php?api=" + this.api, function(response) {
        window.instanceOfThis.setState({descMap: JSON.parse(response.responseText)});
      });
	toggleLoader(true);
    }

    if (this.state.totalActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=total&api=" + this.api, function(response) {
        window.instanceOfThis.setState({totalActive: response.responseText});
      });  
    } 

    if (this.state.weeklyActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=week&api=" + this.api, function(response) {
        window.instanceOfThis.setState({weeklyActive: response.responseText});
      });  
    }

    if (this.state.monthlyActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=month&api=" + this.api, function(response) {
        window.instanceOfThis.setState({monthlyActive: response.responseText});
      });  
    } 

    if (!this.topCategoriesDone) {
      var responseJson, sortedCategoriesResult = [], top5CategoriesList = [];
      
      this.topCategoriesDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=catgid&api=" + this.api, function(response) {
        var index, key;
        
        responseJson = JSON.parse(response.responseText);
        for (key in responseJson) {
          sortedCategoriesResult.push([key, responseJson[key]]);
        }
        sortedCategoriesResult.sort(function(a, b) {
          return b[1] - a[1];
        });
        for (index = 0; index < 5 && index < sortedCategoriesResult.length; index++) {
          var progressWidth = 0, styleString = "", sortedCategoryNumber = "";

          progressWidth = (sortedCategoriesResult[index][1] / sortedCategoriesResult[0][1]) * 100;

	  if(sortedCategoriesResult[index][1] >= 10000 && sortedCategoriesResult[index][1] < 1000000) {
	      sortedCategoryNumber = (Math.floor(sortedCategoriesResult[index][1]/1000)) + 'K';
	  } else if(sortedCategoriesResult[index][1] >= 1000000) {
	      sortedCategoryNumber = (Math.floor(sortedCategoriesResult[index][1]/1000000)) + 'M';
	  } else {
	    sortedCategoryNumber = sortedCategoriesResult[index][1];
	  }

          styleString = {
            width: progressWidth + "px"
          }
	    
          top5CategoriesList.push(
            <div className="trapyz-top-5-progress-content">
              <div className="trapyz-top-5-normal-heading">
                {sortedCategoriesResult[index][0]}
              </div>
              <div id="top-5-store-progress-0" className="trapyz-top-5-progress-bar" style={styleString}>
              </div>
              <div id="top-5-store-count-0" className="trapyz-top-5-normal-text">
                {sortedCategoryNumber}
              </div>
            </div>
          );
        }
        window.instanceOfThis.setState({top5Categories: top5CategoriesList});
      }); 
    }

    return (
      <div className="trapyz-main-container">
        <div id="trapyz-main-tint-div" className="trapyz-main-tint">
          <img src={loaderImage} className="trapyz-loader-image" alt=""/>
        </div>
        <div className="trapyz-menu-panel">
          <div className="trapyz-main-logo">
            <img className="trapyz-logo-img" alt="" src={trapyzLogo}/>
          </div>
          <div className="trapyz-client-logo">
            <img className="trapyz-client-logo-img" alt="" src={dailyHuntLogo}/>
          </div>
          <div className="trapyz-unique-users-container">
            <div className="trapyz-total-users">
              <div id="total-active" className="trapyz-user-number">{this.state.totalActive}</div>
              Total<br/>Installed
            </div>
            <div className="trapyz-weekly-users">
              <div id="weekly-active" className="trapyz-user-number">{this.state.weeklyActive}</div>
              Weekly<br/>Active
            </div>
            <div className="trapyz-monthly-users">
              <div id="monthly-active" className="trapyz-user-number">{this.state.monthlyActive}</div>
              Monthly<br/>Active
            </div>
          </div>
          <div className="trapyz-segments-header">
            <img className="trapyz-segments-header-icon" alt="" src={segmentIcon}/>
            <div className="trapyz-segments-heading-container">
              <div className="trapyz-bold-heading">
                Segments
              </div>
              <div className="trapyz-normal-heading">
                Curated In-market segments
              </div>
            </div>
          </div>
          
          <div id="top-5-categories" className="trapyz-top-5-box">
            <div className="trapyz-top-5-bold-heading">
              Top 5 Visited Categories
            </div>
            {this.state.top5Categories}
          </div>
        </div>
        <div className="trapyz-canvas-panel">
          <div className="trapyz-user-panel">
            <div className="trapyz-user-detail-container">
	      <div className="trapyz-map-logo">
                <img className="trapyz-map-logo-image" src={trapyzMapLogo} alt="" onClick={showMapPage} title="Store Map"/>
              </div>
	      <div className="trapyz-user-image">
                <img className="trapyz-user-profile-image" src={trapyzLogo2} alt="" onClick={showHomePage} title="Home"/>
              </div>
              <div className="trapyz-user-text">
                {this.username}
              </div>
              <div className="trapyz-logout-image-container" onClick={logout}>
                <img className="trapyz-logout-image" src={chevronDown} alt="" title="Log Out"/>
              </div>
            </div>  
          </div>
	  <section>
	    <div id="floating-panel">
	      <button onClick={toggleHeatmap} style={{display: 'none'}}>Toggle Heatmap</button>
	      <button onClick={changeGradient}>Change gradient</button>
	      <button onClick={toggleMarkers}>Toggle Markers</button>
	    </div>
	    <hr className="trapyz-hor-rule"/>
	    <div id="map"></div>
	  </section>
        </div>
      </div>
    );
  }
}

export default TrapyzMapPage;
