import React, { Component } from 'react';
import trapyzLogo from './trapyz-white-icon.png';
import dailyHuntLogo from './dailyhunt-logo.png';
import trapyzLogo2 from './trapyz-logo.png'
import segmentIcon from './segment-icon.png';
import foodImage from './foodie.jpg';
import geekImage from './geek.jpg';
import fashionImage from './fashion.jpg';
import healthImage from './health.jpg';
import autoImage from './automobile.jpg';
import homeImage from './homemaker.jpg';
import personalCareImage from './personal-care.jpg';
import sportImage from './sport.jpg';
import affluentImage from './affluent.jpg';
import './TrapyzApp.css';

function makeAjaxRequest(requestUrl, instanceOfThis, callbackFunction) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.resultVariable = instanceOfThis;
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this);
    }
  };
  xmlHttp.open("GET", requestUrl, true);
  xmlHttp.send(null);
}

class TrapyzApp extends Component {
  constructor (props) {
    super(props);
    this.multiplier = 97;
    this.api = 9;
    this.topCategoriesDone = false;
    this.segmentCardsDone = false;
    this.username = "Ranga"
    this.state = {
      totalActive: 0,
      weeklyActive: 0,
      monthlyActive: 0,
      top5Categories: [],
      segmentCards: []
    }
  }

  render() {
    if (this.state.totalActive === 0) {
      makeAjaxRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=total&api=" + this.api, this, function(response) {
        response.resultVariable.setState({totalActive: response.responseText});
      });  
    } 

    if (this.state.weeklyActive === 0) {
      makeAjaxRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=week&api=" + this.api, this, function(response) {
        response.resultVariable.setState({weeklyActive: response.responseText});
      });  
    }

    if (this.state.monthlyActive === 0) {
      makeAjaxRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=month&api=" + this.api, this, function(response) {
        response.resultVariable.setState({monthlyActive: response.responseText});
      });  
    } 

    if (!this.topCategoriesDone) {
    	var responseJson, sortedCategoriesResult = [], top5CategoriesList = [];
    	
	  	this.topCategoriesDone = true;
	 		makeAjaxRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=catgid&api=" + this.api, this, function(response) {
	      responseJson = JSON.parse(response.responseText);
	      for (var key in responseJson) {
	      	sortedCategoriesResult.push([key, responseJson[key] * response.resultVariable.multiplier]);
	      }
	      sortedCategoriesResult.sort(function(a, b) {
	        return b[1] - a[1];
	      });
	      for (var index = 0; index < 5 && index < sortedCategoriesResult.length; index++) {
	      	var progressWidth = 0, styleString = "";

	      	progressWidth = (sortedCategoriesResult[index][1] / sortedCategoriesResult[0][1]) * 100;
	      	styleString = {
	      		width: progressWidth + "px"
	      	}
	      	top5CategoriesList.push(
	        	<div className="trapyz-top-5-progress-content">
	            <div className="trapyz-top-5-normal-heading">
	              {sortedCategoriesResult[index][0]}
	            </div>
	            <div id="top-5-store-progress-0" className="trapyz-top-5-progress-bar" style={styleString}>
	            </div>
	            <div id="top-5-store-count-0" className="trapyz-top-5-normal-text">
	              {sortedCategoriesResult[index][1]}
	            </div>
	          </div>);
	      }
      	response.resultVariable.setState({top5Categories: top5CategoriesList});
    	});	
  	}

  	if (!this.segmentCardsDone) {
  		var sortedSegmentsResult = [], segmentCardsList, segmentNameMap = {
  			"health-freak": {
  				"name": "Health Freak", 
  				"image": healthImage,
  				"desc": "Users who visited Health & Fitness stores and gourmet department stores"
  			},
  			"sports-enthusiast": {
  				"name": "Sports Enthusiast", 
  				"image": sportImage,
  				"desc": "Users who frequent sportswear and health & fitness stores"
  			},
  			"foodie": {
  				"name": "Foodie", 
  				"image": foodImage,
  				"desc": "Users who visit F&B outlets frequently"
  			},
  			"personal-care": {
  				"name": "Personal Care", 
  				"image": personalCareImage,
  				"desc": "Users who visited personal care stores like salons and spas and/or cosmetics stores"
  			},
  			"geek": {
  				"name": "Geek", 
  				"image": geekImage,
  				"desc": "Users who frequent electronics, music and photography category stores"
  			}, 
  			"auto-enthusiast": {
  				"name": "Auto Enthusiast", 
  				"image": autoImage,
  				"desc": "Users who frequent auto dealerships and car accessory showrooms"
  			},
  			"fashionista": {
  				"name": "Fashionista", 
  				"image": fashionImage,
  				"desc": "Users who frequent apparel, personal care and watches & jewellery stores"
  			},
  			"home-maker": {
  				"name": "Homemaker",
  				"image": homeImage,
  				"desc": "Users who frequent department and home & lifestyle stores"
  			},
  			"affluent": {
  				"name": "Affluent",
  				"image": affluentImage,
  				"desc": "Users who frequent lifestyle and upmarket stores"
  			}
  		};

  		this.segmentCardsDone = true;
  		makeAjaxRequest("https://canvas.trapyz.com/getSegmentCardsData.php?api=" + this.api, this, function(response) {
  			var idString = "", cardsList = [];

	      responseJson = JSON.parse(response.responseText);
	      for (var key in responseJson) {
	      	sortedSegmentsResult.push([key, responseJson[key] * response.resultVariable.multiplier]);
	      }
	      sortedSegmentsResult.sort(function(a, b) {
	        return b[1] - a[1];
	      });
	      for (var index = 0; index < sortedSegmentsResult.length; index++) {
  				idString = "trapyz-card-" + index;
  				cardsList.push(<a className="trapyz-cards" id={idString} href="https://canvas.trapyz.com/trapyz-insights-dash/dashboard/partials/custom_queries">
                  <div className="trapyz-card-image-container">
                  	<img className="trapyz-card-image" alt="" src={segmentNameMap[sortedSegmentsResult[index][0]]["image"]}/>
                  	<div className="trapyz-card-image-tint"></div>
                  </div>
                  <div className="trapyz-card-title">
                    {segmentNameMap[sortedSegmentsResult[index][0]]["name"]}
                  </div>
                  <div className="trapyz-card-count">
                    {sortedSegmentsResult[index][1]}
                  </div>
                  <div className="trapyz-card-text">
                    {segmentNameMap[sortedSegmentsResult[index][0]]["desc"]}
                  </div>
                </a>)
					}
					response.resultVariable.setState({segmentCards: cardsList});
      });
  	}

    return (
	    <div className="trapyz-main-container">
	    	    <iframe src="https://search-geouserdata-n4qdbepki32rsntb5v3ww6ordu.us-east-1.es.amazonaws.com/_plugin/kibana/app/kibana#/dashboard/9bd0ee10-a9f6-11e8-8682-f19d56dbac5b?embed=true&_g=(refreshInterval%3A(display%3A'1%20day'%2Cpause%3A!f%2Csection%3A3%2Cvalue%3A86400000)%2Ctime%3A(from%3Anow-60d%2Cinterval%3A'1d'%2Cmode%3Aquick%2Ctimezone%3AAsia%2FKolkata%2Cto%3Anow))" height="600" width="800"></iframe>
        <div className="trapyz-menu-panel">
          <div className="trapyz-main-logo">
            <img className="trapyz-logo-img" alt="" src={trapyzLogo}/>
          </div>
          <div className="trapyz-client-logo">
            <img className="trapyz-client-logo-img" alt="" src={dailyHuntLogo}/>
          </div>
          <div className="trapyz-unique-users-container">
            <div className="trapyz-total-users">
              <div id="total-active" className="trapyz-user-number">{this.state.totalActive * this.multiplier}</div>
              Total<br/>Installed
            </div>
            <div className="trapyz-weekly-users">
              <div id="weekly-active" className="trapyz-user-number">{this.state.weeklyActive * this.multiplier}</div>
              Weekly Active
            </div>
            <div className="trapyz-monthly-users">
              <div id="monthly-active" className="trapyz-user-number">{this.state.monthlyActive * this.multiplier}</div>
              Monthly Active
            </div>
          </div>
          <div className="trapyz-segments-header">
            <img className="trapyz-segments-header-icon" alt="" src={segmentIcon}/>
            <div className="trapyz-bold-heading">
              Segments
            </div>
            <div className="trapyz-normal-heading">
              Curated In-market segments
            </div>
          </div>
          
          <div id="top-5-categories" className="trapyz-top-5-box">
            <div className="trapyz-top-5-bold-heading">
              Top 5 Visited Categories
            </div>
            {this.state.top5Categories}
          </div>
        </div>
        <div className="trapyz-canvas-panel">
          <div className="trapyz-user-panel">
            <div className="trapyz-user-detail-container">
              <div className="trapyz-user-image">
                <img className="trapyz-user-profile-image" src={trapyzLogo2} alt=""/>
              </div>
              <div className="trapyz-user-text">
                {this.username}
              </div>
            </div>
          </div>
          <div className="trapyz-filter-panel">
            <div className="trapyz-filter-heading">
              Segments
            </div>
          </div>
          <hr className="trapyz-hor-rule"></hr>
          <div className="trapyz-cards-panel">
            {this.state.segmentCards}
          </div>
        </div>
      </div>
    );
  }
}

export default TrapyzApp;
