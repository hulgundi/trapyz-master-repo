import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import trapyzLogo from './trapyz-white-icon.png';
import dailyHuntLogo from './dailyhunt-logo.png';
import deleteButtonImg from './Button-Delete.png';
import trapyzLogo2 from './trapyz-logo.png';
import trapyzMapLogo from './google-map-icon.png';
import trapyzAdvanceLogo from './advance_settings.png';
import chevronDown from './logout-image.png';
import segmentIcon from './segment-icon.png';
import loaderImage from './loader.gif';
import TrapyzLoginPage from './TrapyzLoginPage';
import DonutChart from "react-svg-donut-chart";
import TrapyzHomePage from './TrapyzHomePage';
import PieChart from 'react-minimal-pie-chart';

function makeAjaxGetRequest(requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };

  if (synchType === undefined) {
    synchType = true;
  }

  xmlHttp.open("GET", requestUrl, synchType);
  xmlHttp.send(null);
}

function makeAjaxPostRequest(str, requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();
  var formData = new FormData();

  formData.append('segment', str);

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };

  if (synchType === undefined) {
    synchType = true;
  }

  xmlHttp.open("POST", requestUrl, synchType);
  xmlHttp.send(formData);
}

function preventDefaultAction(event) {
  event.stopPropagation();
}

function findTotalActive() {
  var str = JSON.stringify(checkBlankQuery());
  var baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat-demography";

  makeAjaxPostRequest(str, baseRequestUrl, function(response) {
    var countQueryJson = JSON.parse(response.responseText);
    window.instanceOfThis.setState({totalActive: countQueryJson["value"]});
    dataPieOutput();
	  return;
  });
}

function checkBlankQuery() {
  var query = window.instanceOfThis.state.query;

  if(!((query["city"].length) || (query["category"].length) || (query["subCategory"].length) ||
       (query["age"].length) || (query["gender"].length) || (query["language"].length))) {
    return {};
  }

  return query;
}

function showCount() {
  loader(true);
  window.instanceOfThis.setState({uniqueCount: 0});
  var str = JSON.stringify(checkBlankQuery());
  var baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat-demography";
  window.scrollTo(0, 0);

  makeAjaxPostRequest(str, baseRequestUrl, function(response) {
    var countQueryJson = JSON.parse(response.responseText);
    window.instanceOfThis.setState({uniqueCount: countQueryJson["value"]});
    dataPieOutput();
	  return;
  });
}

// function escapeEvent(event){
//     event.preventDefault();
//     if(event.keyCode === 27){
// 	showHomePage();
//     }
// }

function showHomePage() {
    ReactDOM.render(<TrapyzHomePage />, document.getElementById('root'));
}

function loadCities() {
  makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap?mapType=city", function(response) {
    var responseJson, keys = [], citiesList = [], index, hash = {'cityId':'', 'value':''};
	  //window.instanceOfThis.setState({cityItems: []});

	  if (response.responseText === "\n") {
	     responseJson = "";
	  } else {
      responseJson = JSON.parse(response.responseText);
	  }

    keys = Object.keys(responseJson);

    for (index = 0; index < keys.length; index++) {
	    hash = {'cityId':'', 'value':''};
	    hash['cityId'] = keys[index];
	    hash['value'] = responseJson[keys[index]];
      citiesList.push(hash);
    }

    window.instanceOfThis.setState({cityItems: citiesList});
	  loadCategories();
  });
}

function loadCategories() {
  makeAjaxGetRequest("https://canvas.trapyz.com/getCategory.php", function(response) {
    var responseJson, keys = [], index, categoryList = [], categories = [], hash = {'categoryId':'', 'value':''};
	  //window.instanceOfThis.setState({categoryItems: []});

	  if (response.responseText === "\n") {
	    responseJson = "";
	  } else {
      responseJson = JSON.parse(response.responseText);
	  }

    keys = Object.keys(responseJson);

	  for (index = 0; index < keys.length; index++) {
	    hash = {'categoryId':'', 'value':''};
	    hash['categoryId'] = keys[index];
	    hash['value'] = responseJson[keys[index]];
	    categories.push(hash);
    }

    window.instanceOfThis.setState({categoryItems: categories});
	  loadSubCategories();
  });
}

function loadSubCategories() {
  makeAjaxGetRequest("https://canvas.trapyz.com/getSubCategories.php", function(response) {
    var responseJson, keys = [], subCategoryList = [], index, hash = {'categoryId':'', 'name':'', 'subCategoryId':''};

	  if (response.responseText === "\n") {
	    responseJson = "";
	  } else {
      responseJson = JSON.parse(response.responseText);
	  }

	  keys = Object.keys(responseJson);

    for (index = 0; index < keys.length; index++) {
	    hash = {'categoryId':'', 'name':'', 'subCategoryId':''};
	    hash['subCategoryId'] = keys[index];
	    hash['name'] = responseJson[keys[index]].split('_')[0];
	    hash['categoryId'] = responseJson[keys[index]].split('_')[1];
	    subCategoryList.push(hash);
    }

    window.instanceOfThis.setState({subCategoryItems: subCategoryList});
    loader(false);

    if(window.instanceOfThis.props.data) {
	    editStateInitiate(window.instanceOfThis.props.data);
	  }
  });
}

function selectCityFromList(target) {
  var city = target.currentTarget.innerText;
  document.getElementById('city-list').classList.add('hidden');
  window.instanceOfThis.setState({currentCity: city});
  window.instanceOfThis.state.currentCity = city;
  window.instanceOfThis.state.selectedCity = [];
  document.getElementById('enter-city').value = city;
}

function selectCity() {
  var cityList = window.instanceOfThis.state.cityItems,
  filteredCityList = [], cityText = document.getElementById('enter-city').value,
  index, showFilteredCityList = [];

  if(cityText !== '') {
	  window.instanceOfThis.setState({selectedCity: []});
	  window.instanceOfThis.state.selectedCity = [];
	  filteredCityList = cityList.filter((h) => (h.value.toLowerCase().indexOf(cityText.toLowerCase()) > -1));

    for(index=0; index < filteredCityList.length; index++) {
	    showFilteredCityList.push(
		    <li value={filteredCityList[index].cityId} onClick={selectCityFromList.bind(this)}>
		      {filteredCityList[index].value}
		    </li>
	    );
    }

	  showFilteredCityList.sort((a,b) => (a.props.children.toLowerCase().indexOf(cityText) - b.props.children.toLowerCase().indexOf(cityText)));
	  window.instanceOfThis.setState({selectedCity: showFilteredCityList});
	  window.instanceOfThis.state.selectedCity = showFilteredCityList;

    if(showFilteredCityList.length <= 0) {
      document.getElementById('city-list').classList.add('hidden');
    } else {
      document.getElementById('city-list').classList.remove('hidden');
    }
  } else {
	  document.getElementById('city-list').classList.add('hidden');
	  window.instanceOfThis.setState({selectedCity: []});
	  window.instanceOfThis.state.selectedCity = [];
	  window.instanceOfThis.setState({currentCity: ''});
	  window.instanceOfThis.state.currentCity = '';
  }
}

function loader(loading) {
  if(loading) {
	  document.getElementById('loader').classList.remove('hidden');
  } else {
	  document.getElementById('loader').classList.add('hidden');
  }
}

function removeCity(target) {
  var city = target.target.attributes.value.value.toLowerCase(), query = window.instanceOfThis.state.query;
  query["city"] = query["city"].filter(c => c != city);
  rebuildCity();
}

function addCity() {
  var city = window.instanceOfThis.state.currentCity.toLowerCase();

  if(city) {
    var query = window.instanceOfThis.state.query;

    if (!query["city"].find((i) => (i == city))) {
      query["city"].push(city);
      rebuildCity();
    }

	  window.instanceOfThis.state.currentCity = '';
	  document.getElementById('enter-city').value = '';
  }
}

function removeCategory(target) {
  var categoryId = target.target.attributes.value.value, categoryList = window.instanceOfThis.state.categoryList,
  query = window.instanceOfThis.state.query, filteredCategoryList = [], filteredQueryStateCategory = [];
  filteredCategoryList = categoryList.filter(category => category.props.value != categoryId);
  query["category"] = query["category"].filter(c => c != categoryId);
  window.instanceOfThis.setState({categoryList: filteredCategoryList});
  window.instanceOfThis.state.categoryList = filteredCategoryList;
  var subCategories = new Set(window.instanceOfThis.state.currentQueryStateCategory.filter(query => query.categoryId == categoryId)[0].subCategories);
  query["subCategory"] = query["subCategory"].filter(x => !subCategories.has(x));
  filteredQueryStateCategory = window.instanceOfThis.state.currentQueryStateCategory.filter(query => query.categoryId != categoryId);
  window.instanceOfThis.state.currentQueryStateCategory = filteredQueryStateCategory;
  rebuildCategorySubCategory();
}

function addCategory() {
  var categoryId = window.instanceOfThis.state.currentCategory, selectedCategory,
  currentQueryStateCategory, query = window.instanceOfThis.state.query;

  if(categoryId) {
	  selectedCategory = window.instanceOfThis.state.categoryItems.find(category => category.categoryId == categoryId),
    currentQueryStateCategory = window.instanceOfThis.state.currentQueryStateCategory;

	  if(!currentQueryStateCategory.find(category => category.categoryId == categoryId)) {
	    currentQueryStateCategory.push({'categoryId':categoryId, 'categoryName': selectedCategory.value, 'subCategories':[]});
      query["category"].push(categoryId);
      rebuildCategorySubCategory();
	  }

	  window.instanceOfThis.state.currentCategory = '';
	  document.getElementById('enter-category').value = '';
  }
}

function selectCategoryFromList(target) {
  var categoryId = target.currentTarget.value;
  document.getElementById('category-list').classList.add('hidden');
  window.instanceOfThis.setState({currentCategory: categoryId});
  window.instanceOfThis.state.currentCategory = categoryId;
  document.getElementById('enter-category').value = target.currentTarget.innerText;
  window.instanceOfThis.state.selectedCategory = [];
}

function selectCategory() {
  var categoryList = window.instanceOfThis.state.categoryItems,
  filteredCategoryList = [], categoryText = document.getElementById('enter-category').value,
  index, showFilteredCategoryList = [];

  if(categoryText !== '') {
	  window.instanceOfThis.setState({selectedCategory: []});
	  window.instanceOfThis.state.selectedCategory = [];
	  filteredCategoryList = categoryList.filter((h) => (h.value.toLowerCase().indexOf(categoryText.toLowerCase()) > -1));

	  for(index=0; index < filteredCategoryList.length; index++) {
	    showFilteredCategoryList.push(
		    <li value={filteredCategoryList[index].categoryId} onClick={selectCategoryFromList.bind(this)}>
		      {filteredCategoryList[index].value}
		    </li>
	    );
    }

	  showFilteredCategoryList.sort((a,b) => (a.props.children.toLowerCase().indexOf(categoryText) - b.props.children.toLowerCase().indexOf(categoryText)));
	  window.instanceOfThis.setState({selectedCategory: showFilteredCategoryList});
	  window.instanceOfThis.state.selectedCategory = showFilteredCategoryList;

    if(showFilteredCategoryList.length <= 0) {
      document.getElementById('category-list').classList.add('hidden');
    } else {
      document.getElementById('category-list').classList.remove('hidden');
    }
  } else {
	  document.getElementById('category-list').classList.add('hidden');
	  window.instanceOfThis.setState({selectedCategory: []});
	  window.instanceOfThis.state.selectedCategory = [];
  }
}

function rebuildCity() {
  var query = window.instanceOfThis.state.query, cityList = [];

  query["city"].forEach(function(city) {
    cityList.push(
      <li value={city}>
        <header>
          <span>{city.charAt(0).toUpperCase() + city.slice(1)}</span>
          <img src="./assets/Button-Delete.png" height="16px" width="16px" value={city} onClick={removeCity.bind(this)}/>
        </header>
      </li>
    );
  });

  window.instanceOfThis.setState({cityList: cityList});
  window.instanceOfThis.state.cityList = cityList;
}

function selectSubCategoryFromList(target) {
  var subCategoryId = target.currentTarget.value;
  document.getElementById('settings-sub-category-list').classList.add('hidden');
  window.instanceOfThis.setState({currentSubCategory: subCategoryId});
  window.instanceOfThis.state.currentSubCategory = subCategoryId;
  window.instanceOfThis.state.selectedSubCategory = [];
  document.getElementById('enter-sub-category').value = target.currentTarget.innerText;
}

function selectSubCategory() {
  var subCategoryList = window.instanceOfThis.state.temporarySubCategoryList, filteredSubCategoryList = [], subCategoryText = document.getElementById('enter-sub-category').value, index, showFilteredSubCategoryList = [];

  if(subCategoryText !== '') {
  	window.instanceOfThis.setState({selectedSubCategory: []});
	  window.instanceOfThis.state.selectedSubCategory = [];
	  filteredSubCategoryList = subCategoryList.filter((h) => (h.name.toLowerCase().indexOf(subCategoryText.toLowerCase()) > -1));

	  for(index=0; index < filteredSubCategoryList.length; index++) {
	    showFilteredSubCategoryList.push(
		    <li value={filteredSubCategoryList[index].subCategoryId} onClick={selectSubCategoryFromList.bind(this)}>
		      {filteredSubCategoryList[index].name}
		    </li>
	    );
    }

	  showFilteredSubCategoryList.sort((a,b) => (a.props.children.toLowerCase().indexOf(subCategoryText) - b.props.children.toLowerCase().indexOf(subCategoryText)));
	  window.instanceOfThis.setState({selectedSubCategory: showFilteredSubCategoryList});
	  window.instanceOfThis.state.selectedSubCategory = showFilteredSubCategoryList;
	  document.getElementById('settings-sub-category-list').classList.remove('hidden');
  } else {
	  document.getElementById('settings-sub-category-list').classList.add('hidden');
	  window.instanceOfThis.setState({selectedSubCategory: []});
	  window.instanceOfThis.state.selectedSubCategory = [];
	  window.instanceOfThis.setState({currentSubCategory: ''});
	  window.instanceOfThis.state.currentSubCategory = '';
  }
}

function addSubCategory() {
  var subCategoryId = window.instanceOfThis.state.currentSubCategory, subCategoryList = window.instanceOfThis.state.subCategoryList, selectedSubCategory;
  selectedSubCategory = window.instanceOfThis.state.temporarySubCategoryList.find(subCategory => subCategory.subCategoryId == subCategoryId);

  if(!subCategoryList.find(subCategory => subCategory.props.value == subCategoryId)) {
	  subCategoryList.push(
		  <li value={subCategoryId}>
		    <span>{selectedSubCategory.name}</span>
		    <img src={deleteButtonImg} value={subCategoryId} onClick={removeSubCategory.bind(this)}/>
	    </li>
	  );

    window.instanceOfThis.setState({subCategoryList: subCategoryList});
	  window.instanceOfThis.state.subCategoryList = subCategoryList;
  }

  window.instanceOfThis.state.currentSubCategory = '';
  document.getElementById('enter-sub-category').value = '';
}

function removeSubCategory(target) {
  var subCategoryId = target.target.attributes.value.value, subCategoryList = window.instanceOfThis.state.subCategoryList, filteredSubCategoryList = [];
  filteredSubCategoryList = subCategoryList.filter(subCategory => subCategory.props.value != subCategoryId);
  window.instanceOfThis.setState({subCategoryList: filteredSubCategoryList});
  window.instanceOfThis.state.subCategoryList = filteredSubCategoryList;
}

function showSubCategorySelectionBox(flag) {
  if(flag && window.instanceOfThis.state.currentCategory) {
	  document.getElementById('sub-category-selection').classList.remove('hidden');
	  var temporarySubCategoryList = window.instanceOfThis.state.subCategoryItems.filter(subCategory => subCategory.categoryId == window.instanceOfThis.state.currentCategory);
	  window.instanceOfThis.setState({temporarySubCategoryList: temporarySubCategoryList});
	  window.instanceOfThis.state.temporarySubCategoryList = temporarySubCategoryList;

	  if(window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == window.instanceOfThis.state.currentCategory)) {
	    window.instanceOfThis.state.subCategoryList = [];
	    var subCategoryList = [], subCategories = [], subCategoryItems = window.instanceOfThis.state.subCategoryItems, k;
	    subCategories = window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == window.instanceOfThis.state.currentCategory).subCategories;

	    for(k=0; k < subCategories.length; k++) {
		    subCategoryList.push(
			    <li value={subCategories[k]}>
			      <span>{subCategoryItems.find(subCategory => subCategory.subCategoryId == subCategories[k]).name}</span>
			      <img src={deleteButtonImg} value={subCategories[k]} onClick={removeSubCategory.bind(this)}/>
			    </li>
		    );
	    }

	    window.instanceOfThis.setState({subCategoryList: subCategoryList});
	    window.instanceOfThis.state.subCategoryList = subCategoryList;
	  }
  } else {
	  document.getElementById('sub-category-selection').classList.add('hidden');
	  window.instanceOfThis.setState({temporarySubCategoryList: []});
	  window.instanceOfThis.state.temporarySubCategoryList = [];
	  window.instanceOfThis.state.subCategoryList = [];
	  window.instanceOfThis.state.currentCategory = '';
	  document.getElementById('enter-category').value = '';
	  document.getElementById('enter-sub-category').value = '';
  }
}

function removeSubCategoryFromQuery(target) {
  var categoryId = target.currentTarget.attributes.category.value, subCategoryId = target.currentTarget.attributes.value.value;
  var subCategories = window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == categoryId).subCategories;
  var newSubCategories = subCategories.filter(subCategory => subCategory != subCategoryId),
  query = window.instanceOfThis.state.query;
  query["subCategory"] = query["subCategory"].filter(subCategory => subCategory != subCategoryId);
  window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == categoryId).subCategories = newSubCategories;
  rebuildCategorySubCategory();
}

function addSubCategoryToQuery() {
  var categoryId = window.instanceOfThis.state.currentCategory, newSubCategories = [],
  query = window.instanceOfThis.state.query;
  addCategory();
  newSubCategories = window.instanceOfThis.state.subCategoryList.map(subCategory => subCategory.props.value);
  window.instanceOfThis.state.currentQueryStateCategory.find(query => query.categoryId == categoryId).subCategories = newSubCategories;
  window.instanceOfThis.state.currentCategory = categoryId;
  newSubCategories.forEach((subcat) => (query["subCategory"].push(subcat)));
  query["subCategory"] = [...new Set(query["subCategory"])];
  rebuildCategorySubCategory();
  showSubCategorySelectionBox(false);
}

function showSubCategories(target) {
  window.instanceOfThis.state.currentCategory = target.currentTarget.attributes.value.value;
  showSubCategorySelectionBox(true);
}

function rebuildCategorySubCategory() {
  var subCategoryItems = window.instanceOfThis.state.subCategoryItems;
  var currentQueryStateCategory = window.instanceOfThis.state.currentQueryStateCategory;
  var categoryList = [], selectedCity, subCategoryList = [], subCategories = [], index, k;

  for(index = 0; index < currentQueryStateCategory.length; index++) {
	  subCategories = currentQueryStateCategory[index].subCategories;
	  subCategoryList = [];

	  for(k=0;k<subCategories.length;k++) {
	    subCategoryList.push(
		    <div value={subCategories[k]}>
		      <span>{subCategoryItems.find(subCategory => subCategory.subCategoryId == subCategories[k]).name}</span>
		      <img src={deleteButtonImg} value={subCategories[k]} category={currentQueryStateCategory[index].categoryId} onClick={removeSubCategoryFromQuery.bind(this)} height="16px" width="16px"/>
		    </div>
	    );
	  }

	  categoryList.push(
		  <li value={currentQueryStateCategory[index].categoryId}>
		    <header>
		      <span value={currentQueryStateCategory[index].categoryId} onClick={showSubCategories.bind(this)}>
            {currentQueryStateCategory[index].categoryName}
          </span>
		      <img src={deleteButtonImg} value={currentQueryStateCategory[index].categoryId} height="16px" width="16px" onClick={removeCategory.bind(this)}/>
        </header>
		    <section>
		      {subCategoryList}
	      </section>
	    </li>
	  );
  }

  window.instanceOfThis.setState({categoryList: categoryList});
  window.instanceOfThis.state.categoryList = categoryList;
}

function clearStates() {
  var state = window.instanceOfThis.state;
  state.dataPie = [];
  dataPieOutput();
  window.scrollTo(0, 0);

  window.instanceOfThis.setState({
    uniqueCount: 0,
    selectedCity: [],
    currentCity: '',
    cityList: [],
    categoryList: [],
    currentCategory: '',
    selectedCategory: [],
    subCategoryList: [],
    selectedSubCategory: [],
    currentSubCategory: [],
    currentQueryStateCategory: [],
    query: {
      "city":[],
      "category":[],
      "subCategory":[],
      "gender":[],
      "age":[],
      "language":[]
    },
    dataPie: []
  });

  document.getElementsByName("age").forEach(inp => inp.checked = 0);
  document.getElementsByName("gender").forEach(inp => inp.checked = 0);
  document.getElementsByName("language").forEach(inp => inp.checked = 0);
}

function showSaveSegmentBox(flag) {
  var query = window.instanceOfThis.state.query;

  if(flag && (query["city"].length > 0 || query["category"].length > 0 || query["subCategory"].length > 0
              || query["age"].length > 0|| query["gender"].length > 0|| query["language"].length > 0)) {

    if(window.instanceOfThis.state.uniqueCount) {
	    document.getElementById('save-segment-box').classList.remove('hidden');

	    if(window.instanceOfThis.props.data) {
		    document.getElementById('segment-name-advanced').value = window.instanceOfThis.props.data.segmentName;
		    document.getElementById('segment-description-advanced').value = window.instanceOfThis.props.data.segmentDesc;
	    }
	  } else {
	    showErrorMessage('Build Query');
	  }
  } else {
	  document.getElementById('save-segment-box').classList.add('hidden');
	  document.getElementById('segment-name-advanced').value = '';
	  document.getElementById('segment-description-advanced').value ='';
  }
}

function saveSegment() {
  var query = window.instanceOfThis.state.query, name, desc, url;

  if(query["city"].length > 0 || query["category"].length > 0 || query["subCategory"].length > 0
     || query["age"].length > 0|| query["gender"].length > 0|| query["language"].length > 0) {

	  name = document.getElementById('segment-name-advanced').value;
	  desc = document.getElementById('segment-description-advanced').value;

	  if(name != '' && desc != '') {
	    if(window.instanceOfThis.props.data) {
		    url = "https://canvas.trapyz.com/edit-segment-demography";
		    var new_hash = {
          'segmentDesc': desc ,'segmentName': name, 'query': query, 'api': window.instanceOfThis.api,
          'segmentCount': window.instanceOfThis.state.uniqueCount,
          'id': window.instanceOfThis.props.data.id,
        };
	    } else {
		    url = "https://canvas.trapyz.com/save-segment-demography";
		    var new_hash = {
          'segmentDesc': desc ,'segmentName': name, 'query': query, 'type': 'demography',
          'api': window.instanceOfThis.api, 'segmentCount': window.instanceOfThis.state.uniqueCount
        };
	    }

	    var segmentJson = JSON.stringify(new_hash);

	    makeAjaxPostRequest(segmentJson, url, function(response) {
        if(response.responseText.trim() == "1") {
          showSaveSegmentBox(false);
          showHomePage();
        } else {
          showErrorMessage(response.responseText.trim());
        }
        
		    return;
	    });
	  } else {
	    showErrorMessage('Please Provide Name and Description');
	  }
  } else {
	  showErrorMessage('Build Query');
	  showSaveSegmentBox(false);
  }
}

function showErrorMessage(msg) {
  document.getElementById('error-message-box').classList.remove('hidden');
  window.instanceOfThis.setState({errorMessage: msg});
}

function hideErrorMessage() {
  document.getElementById('error-message-box').classList.add('hidden');
  window.instanceOfThis.setState({errorMessage: ''});
}

function editStateInitiate(data) {
  loader(true);
  var query = data["query"], categoryItems = window.instanceOfThis.state.categoryItems,
  subCategoryItems = window.instanceOfThis.state.subCategoryItems,
  currentQueryStateCategory = window.instanceOfThis.state.currentQueryStateCategory;

  var ageRanges = {
    '{"min":17,"max":24}':1, '{"min":24,"max":35}':2, '{"min":35,"max":45}':3,
    '{"min":45,"max":55}':4, '{"min":55,"max":65}':5, '{"min":65,"max":75}':6
  };

  query["category"].forEach(function(categoryId) {
    var selectedCategory = categoryItems.find(category => category.categoryId == categoryId);
    currentQueryStateCategory.push({'categoryId':categoryId, 'categoryName': selectedCategory.value, 'subCategories':[]});
  });

  query["subCategory"].forEach(function(subCategoryId) {
    var categoryId = subCategoryItems.find(subCat => subCat.subCategoryId == subCategoryId).categoryId;
    currentQueryStateCategory.find(cat => cat.categoryId == categoryId).subCategories.push(subCategoryId);
  });

  query["age"].forEach(function(age) {
    checkAge(ageRanges[JSON.stringify(age)]);
    document.getElementsByName("age").forEach(g => ((ageRanges[JSON.stringify(age)] == g.value) ? g.checked = 1 : g.checked));
  });

  window.instanceOfThis.state.query = query;
  window.instanceOfThis.setState({uniqueCount: data["segmentCount"]});
  dataPieOutput();
  rebuildCity();
  rebuildCategorySubCategory();

  query["language"].forEach(function(l) {
    checkLanguage(l);
    document.getElementsByName("language").forEach(g => ((g.value == l) ? g.checked = 1 : g.checked));
  });

  query["gender"].forEach(function(g1) {
    checkGender(g1);
    document.getElementsByName("gender").forEach(g => ((g.value == g1) ? g.checked = 1 : g.checked));
  });

  document.getElementById('segment-name-advanced').value = data["segmentName"];
  document.getElementById('segment-description-advanced').value = data["segmentDesc"];
  loader(false);
  // "UPDATE DemographySegments SET name = '".$dataJson->segmentName."', description = '".$dataJson->segmentDesc.'", attributeJson = '.json_encode($dataJson->query).', "segmentCount" = '.$dataJson->segmentCount.' WHERE DemographySegments.id = '.$dataJson->id;
  // 'DELETE FROM DemographySegments WHERE DemographySegments.id = '.$dataJson->id
}

function addAgeGroup(target) {
  var query =  window.instanceOfThis.state.query;
  var ageRanges = {
    1:{min: 17, max: 24}, 2:{min: 24, max: 35}, 3:{min: 35, max: 45},
    4:{min: 45, max: 55}, 5:{min: 55, max: 65}, 6:{min: 65, max: 75}
  };

  if(target.currentTarget.checked) {
    query["age"].push(ageRanges[target.currentTarget.value]);
    checkAge(target.currentTarget.value);
  } else {
    query["age"] = query["age"].filter(age => JSON.stringify(age) != JSON.stringify(ageRanges[target.currentTarget.value]));
    unCheckAge(target.currentTarget.value);
  }
}

function addGender(target) {
  var query =  window.instanceOfThis.state.query;

  if(target.currentTarget.checked) {
    query["gender"].push(target.currentTarget.value);
    checkGender(target.currentTarget.value);
  } else {
    query["gender"] = query["gender"].filter(gender => gender != target.currentTarget.value);
    unCheckGender(target.currentTarget.value);
  }
}

function addLanguage(target) {
  var query = window.instanceOfThis.state.query;
  var languages = {
    "Hindi": ["hi","Hindi"],"English":["en","English"],"Tamil":["ta"],"Telugu":["te"],
    "Malayalam": ["ml"],"Kannada": ["kn"],"Bengali": ["bn"],"Gujarati": ["gu"],"Marathi": ["mr"],
    "Oriya": ["or"],"Punjabi": ["pa"],"Bihari": ["bh"],"Urdu": ["ur"],"Nepali": ["ne"]
  };

  if(target.currentTarget.checked) {
    languages[target.currentTarget.value].forEach(lang => query["language"].push(lang));
    checkLanguage(target.currentTarget.value);
  } else {
    query["language"] = query["language"].filter(lang => (!languages[target.currentTarget.value].includes(lang)))
    unCheckLanguage(target.currentTarget.value);
  }
}

function initialiseLanguageCheckboxes() {
  var languages = ["Hindi","English","Tamil","Telugu","Malayalam","Kannada",
                   "Bengali","Gujarati","Marathi","Oriya","Punjabi","Bihari","Urdu","Nepali"], langs = [];

  languages.forEach(function(lang) {
    langs.push(
      <li>
        <div className="checkbox">
          <input type="checkbox" name="language" value={lang} onClick={addLanguage.bind(this)}/>
        </div>
        <span className="check-language" value={lang}>{lang}</span>
      </li>
    );
  });

  window.instanceOfThis.setState({languages: langs});
}

function dataPieOutput() {
  var dataPie = [
    {value: 0, stroke: "#4A90E2", strokeWidth: 8},
    {value: 0, stroke: "#F7B342", strokeWidth: 8}
  ];

  dataPie[1]["value"] = parseInt(window.instanceOfThis.state.totalActive) - parseInt(window.instanceOfThis.state.uniqueCount);
  dataPie[0]["value"] = parseInt(window.instanceOfThis.state.uniqueCount);
  window.instanceOfThis.setState({dataPie: dataPie});
  loader(false);
}

function checkGender(value) {
  var checkboxes = document.getElementsByClassName("check-gender"), i;

  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.add('checked');
      break;
    }
  }
}

function unCheckGender(value) {
  var checkboxes = document.getElementsByClassName("check-gender"), i;

  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.remove('checked');
      break;
    }
  }
}

function checkAge(value) {
  var checkboxes = document.getElementsByClassName("check-age"), i;

  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.add('checked');
      break;
    }
  }
}

function unCheckAge(value) {
  var checkboxes = document.getElementsByClassName("check-age"), i;

  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.remove('checked');
      break;
    }
  }
}

function checkLanguage(value) {
  var checkboxes = document.getElementsByClassName("check-language"), i;

  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.add('checked');
      break;
    }
  }
}

function unCheckLanguage(value) {
  var checkboxes = document.getElementsByClassName("check-language"), i;

  for(i=0;i<checkboxes.length;i++) {
    if(checkboxes[i].attributes.value.value == value) {
      checkboxes[i].classList.remove('checked');
      break;
    }
  }
}

class TrapyzNewAdvancedPage extends Component {
  constructor (props) {
    super(props);
    this.api = sessionStorage.getItem("key");
    this.username = sessionStorage.getItem("name");
    this.state = {
	    totalActive: 0,
	    uniqueCount: 0,
      weeklyActive: 0,
      monthlyActive: 0,
	    cityItems: [],
	    categoryItems: [],
	    selectedCity: [],
	    currentCity: '',
	    cityList: [],
	    categoryList: [],
	    currentCategory: '',
	    selectedCategory: [],
	    subCategoryItems: [],
	    temporarySubCategoryList: [],
	    subCategoryList: [],
	    selectedSubCategory: [],
	    currentSubCategory: [],
	    currentQueryStateCategory: [],
      languages: [],
      query: {
        "city":[],
        "category":[],
        "subCategory":[],
        "gender":[],
        "age":[],
        "language":[]
      },
      dataPie: [],
      errorMessage: ""
    }
  }

  componentDidMount() {
    loader(true);
    initialiseLanguageCheckboxes();
    loadCities();
    findTotalActive();
    window.scrollTo(0, 0);

    makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=week&api=" + this.api, function(response) {
      window.instanceOfThis.setState({weeklyActive: response.responseText.trim()});
    });

    makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=month&api=" + this.api, function(response) {
      window.instanceOfThis.setState({monthlyActive: response.responseText.trim()});
    });
  }

  render() {
    window.instanceOfThis = this;

    return (
      <div>
        <header>
          <div className="heading">
            <section className="logo">
              <div>
                <img src="./assets/TrapyzLogo.png" alt="Trapyz Logo" height="24px" width="60px"/>
              </div>
              <span>Audience Insights Platform</span>
            </section>
            <section className="installed">
              <div>
                <label>Total Installed</label>
                <span>{this.state.totalActive}</span>
              </div>
              <div>
                <label>Weekly Active</label>
                <span>{this.state.weeklyActive}</span>
              </div>
              <div>
                <label>Monthly Active</label>
                <span>{this.state.monthlyActive}</span>
              </div>
            </section>
          </div>
          <aside className="session">
            <span>Demo User</span>
            <img src="./assets/Logout.png" alt="Logout" width="24px" height="24px"/>
          </aside>
        </header>
        <div className="demographic-segmentation">
          <main>
            <section className="query">
              <h2>Demographic Segment</h2>
              <p>
                Segment users based on a combination of attributes like age-range, gender, category of store visited, city of interest, income level and lifestyle affinity.
              </p>
              <div className="query__block">
                <h3>Customize and Select Your Segment</h3>
                <div className="query__block__age-gender">
                  <section>
                    <h4>Select Age Range</h4>
                    <div className="query__block__age-gender__age-block">
                      <div>
                        <input type="checkbox" name="age" value="1" onClick={addAgeGroup.bind(this)}/>
                        <label className="check-age" value="1">17-24 yrs</label>
                      </div>
                      <div>
                        <input type="checkbox" name="age" value="2" onClick={addAgeGroup.bind(this)}/>
                        <label className="check-age" value="2">24-35 yrs</label>
                      </div>
                      <div>
                        <input type="checkbox" name="age" value="3" onClick={addAgeGroup.bind(this)}/>
                        <label className="check-age" value="3">35-45 yrs</label>
                      </div>
                      <div>
                        <input type="checkbox" name="age" value="4" onClick={addAgeGroup.bind(this)}/>
                        <label className="check-age" value="4">45-55 yrs</label>
                      </div>
                      <div>
                        <input type="checkbox" name="age" value="5" onClick={addAgeGroup.bind(this)}/>
                        <label className="check-age" value="5">55-65 yrs</label>
                      </div>
                      <div>
                        <input type="checkbox" name="age" value="6" onClick={addAgeGroup.bind(this)}/>
                        <label className="check-age" value="6">65-75 yrs</label>
                      </div>
                    </div>
                  </section>
                  <section>
                    <h4>Select Gender</h4>
                    <div className="query__block__age-gender__gender-block">
                      <div>
                        <input type="checkbox" name="gender" value="Male" onClick={addGender.bind(this)}/>
                        <label className="check-gender" value="Male">Male</label>
                      </div>
                      <div>
                        <input type="checkbox" name="gender" value="Female" onClick={addGender.bind(this)}/>
                        <label className="check-gender" value="Female">Female</label>
                      </div>
                    </div>
                  </section>
                </div>
                <div className="query__block__category-city">
                  <section className="query__block__category-city__block">
                    <h4>Categories & Sub Categories</h4>
                    <div className="input-box">
                      <input type="text" id="enter-category" placeholder="Search Category" onKeyUp={() => selectCategory()}/>
                      <button className="add" onClick={() => addCategory()}>+</button>
                      <span className="show" onClick={() => showSubCategorySelectionBox(true)}>SHOW SUB-CATEGORY</span>
                      <ul id="category-list" className="category-list hidden">
                        {this.state.selectedCategory}
                      </ul>
                    </div>
                    <ul>
                      {this.state.categoryList}
                    </ul>
                  </section>
                  <section className="query__block__category-city__block">
                    <h4>City</h4>
                    <div className="input-box">
                      <input type="text" id="enter-city" placeholder="Search City" onKeyUp={() => selectCity()}/>
                      <button className="add" onClick={() => addCity()}>+</button>
                      <ul id="city-list" className="city-list hidden">
                        {this.state.selectedCity}
                      </ul>
                    </div>
                    <ul>
                      {this.state.cityList}
                    </ul>
                  </section>
                </div>
                <div className="query__block__income-affinity hidden">
                  <section className="query__block__income-affinity__block">
                    <h4>Customer Income Segmentation</h4>
                    <ul>
                      <li>
                        <div className="checkbox">
                          <input type="checkbox" name="" value=""/>
                        </div>
                        <span>{"< 20000"}</span>
                      </li>
                      <li>
                        <div className="checkbox">
                          <input type="checkbox" name="" value=""/>
                        </div>
                        <span>20000 - 40000</span>
                      </li>
                      <li>
                        <div className="checkbox">
                          <input type="checkbox" name="" value=""/>
                        </div>
                        <span>40000 - 80000</span>
                      </li>
                    </ul>
                  </section>
                  <section className="query__block__income-affinity__block">
                    <h4>Customer Lifestyle Affinity Segmentation</h4>
                    <ul>
                      <li>
                        <div className="checkbox">
                          <input type="checkbox" name="" value=""/>
                        </div>
                        <span>{"< 20000"}</span>
                      </li>
                      <li>
                        <div className="checkbox">
                          <input type="checkbox" name="" value=""/>
                        </div>
                        <span>20000 - 40000</span>
                      </li>
                      <li>
                        <div className="checkbox">
                          <input type="checkbox" name="" value=""/>
                        </div>
                        <span>40000 - 80000</span>
                      </li>
                    </ul>
                  </section>
                </div>
                <div className="query__block__phone-language">
                  <section className="query__block__phone-language__block hidden">
                    <h4>Phone Brand & Model</h4>
                    <div className="input-box">
                      <input type="text" id="enter-phone" placeholder="Search Phone"/>
                      <button className="add">+</button>
                      <span className="show">SHOW MODEL</span>
                    </div>
                    <ul className="phone">
                      <li>
                        <header>
                          <span>Clothing</span>
                          <img src="./assets/Button-Delete.png" height="16px" width="16px" value="5"/>
                        </header>
                        <section>
                          <div value="134">
                            <span>Baby Wear</span>
                            <img src="./assets/Button-Delete.png" height="16px" width="16px" value="134" city="5"/>
                          </div>
                          <div value="80">
                            <span>Children’s Wear</span>
                            <img src="./assets/Button-Delete.png" height="16px" width="16px" value="80" city="5"/>
                          </div>
                          <div value="80">
                            <span>Ladies Wear</span>
                            <img src="./assets/Button-Delete.png" height="16px" width="16px" value="80" city="5"/>
                          </div>
                        </section>
                      </li>
                      <li>
                        <header>
                          <span>Shoes</span>
                          <img src="./assets/Button-Delete.png" height="16px" width="16px" value="5"/>
                        </header>
                      </li>
                    </ul>
                  </section>
                  <section className="query__block__phone-language__block">
                    <h4>Language</h4>
                    <ul className="language">
                      {this.state.languages}
                    </ul>
                  </section>
                </div>
              </div>
              <footer className="action">
                <button className="show-count" onClick={() => showCount()}>SHOW COUNT</button>
                <a onClick={() => showSaveSegmentBox(true)}>SAVE SEGMENT</a>
                <a onClick={() => clearStates()}>CLEAR</a>
                <a onClick={() => showHomePage()}>CANCEL</a>
              </footer>
            </section>
            <section className="result">
              <h2>Unique Gids</h2>
              <div className="total-count">
                <span className="color"></span>
                <div className="content">
                  <span className="count">{this.state.totalActive}</span>
                  <span>Total Installed</span>
                </div>
              </div>
              <div className="unique-count">
                <span className="color"></span>
                <div className="content">
                  <span className="count">{this.state.uniqueCount}</span>
                  <span>Segment Count</span>
                </div>
              </div>
              <div>
                {<DonutChart data={this.state.dataPie} />}
              </div>
            </section>
          </main>
        </div>
        <section id="sub-category-selection" className="area-selection-box hidden">
  	      <div className="header">
  	        <span>Select Sub Category</span>
  	        <div className="selected-city">
  	          <label>Category:</label>
  	          <span>{this.state.categoryItems.find(category => category.categoryId == this.state.currentCategory) ? this.state.categoryItems.find(category => category.categoryId == this.state.currentCategory).value : ''}</span>
            </div>
  	      </div>
  	      <section className="area-input-box">
            <label>Select Sub Category</label>
  	        <div>
  	          <input type="text" id="enter-sub-category" placeholder="Search Sub Category" onKeyUp={() => selectSubCategory()}/>
  	          <button className="add" onClick={() => addSubCategory()}>+</button>
              <ul id="settings-sub-category-list" className="settings-area-list hidden">
                {this.state.selectedSubCategory}
      	      </ul>
            </div>
  	      </section>
  	      <section className="area-list-box">
  	        <ul>{this.state.subCategoryList}</ul>
  	      </section>
  	      <section className="area-select-box">
  	        <button className="select" onClick={() => addSubCategoryToQuery()}>SELECT</button>
  	        <button className="clear" onClick={() => showSubCategorySelectionBox(false)}>CLEAR</button>
  	      </section>
  	    </section>
        <section id="save-segment-box" className="save-segment-box hidden">
	        <div className="header">
            <span>Save Segment</span>
          </div>
	        <section>
	          <div>
	            <label>Segment Name</label>
	            <input type="text" id="segment-name-advanced"/>
	          </div>
	          <div>
	            <label>Description</label>
	            <textarea id="segment-description-advanced"/>
	          </div>
	        </section>
	        <section className="save-button-box">
	          <button id="save-segment-advanced" onClick={() => saveSegment()} className="select">SAVE</button>
	          <button className="clear" onClick={() => showSaveSegmentBox(false)}>CANCEL</button>
	        </section>
	      </section>
        <aside id="loader" className="loader hidden">
	        <img src="./assets/progress-bar.gif" alt=""/>
	      </aside>
        <aside id="error-message-box" className="error-message-box hidden">
          <p>{window.instanceOfThis.state.errorMessage}</p>
	        <div>
	          <input type="submit" value="OK" className="hide-alert" onClick={() => hideErrorMessage()}/>
          </div>
	      </aside>
      </div>
    );
  }
}

export default TrapyzNewAdvancedPage;
