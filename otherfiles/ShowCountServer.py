import cherrypy
import random
import string
from ElasticSearchSegment import ElasticSearchSegment
from cherrypy._cpserver import Server
from cherrypy.process.plugins import Daemonizer
import logging
import json
import time

class ShowCountServer(object):
    def __init__(self):
        self.aggregateApi = "33"
        self.elasticSearchSegment = ElasticSearchSegment(self.aggregateApi)
        self.logger = logging.getLogger("showCountServerApplication_log")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler("showCountServerApplication.log")
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)

    @cherrypy.expose
    @cherrypy.tools.json_out()

    def showCountForCustomSegment(self, segment):
        print("Entered in the show count")
        self.logger.debug("input parameter: %s", segment)
        segmentJson = json.loads(str(segment))
        countJson = self.elasticSearchSegment.getCountForSegment(segmentJson)
        self.logger.debug("response: %s", json.dumps(countJson))
        return countJson

    def showGids(self, segment):
        print("enterd in the show gids")
        self.logger.debug("input parameter: %s", segment)
        segmentJson = json.loads(str(segment))
        countJson = self.elasticSearchSegment.getCountForSegment(segmentJson)
        countGids = countJson[segmentJson['name']]['value']
        gidsJson = self.elasticSearchSegment.getGidsForSegement(segmentJson, countGids)
        self.logger.debug("response: %s", json.dumps(gidsJson))
        return json.dumps(gidsJson)
  
    showGids.exposed = True  

cherrypy.config.update({'server.socket_port': 8000,
                        'server.socket_host' : '172.31.11.124',
                        'engine.autoreload.on': False,
                        'log.access_file': './access.log',
                        'log.error_file': './error.log',
                        'tools.sessions.on': True,
                        'tools.encode.on': True,
                        'tools.encode.encoding': 'utf-8'})

server = Server()
server.socket_port = 8000
server.socket_host = 'localhost'
server.subscribe()

d = Daemonizer(cherrypy.engine)
d.subscribe()
cherrypy.quickstart(ShowCountServer())
