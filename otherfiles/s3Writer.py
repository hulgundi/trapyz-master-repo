import boto3
import pymysql
import requests
import time
import datetime

now = datetime.datetime.now()
s3 = boto3.resource('s3')
client = boto3.client('s3')
# s3.Bucket('trapyz-insights').put_object(Key='output_data', Body=data)

apiMap = {}
requestUrl = "http://52.206.33.14:8000/all?api="

try:
	selectQuery = "SELECT a.Api_ID, a.Apikey, b.username FROM ApikeyMap as a, users as b where a.Apikey != '' and a.Apikey = b.apikey"
	connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
	cursor = connect.cursor()
	cursor.execute(selectQuery)
	print("Successfully fetched data from API table!")
except:
	print("Exception occured while reading API table!")

for row in cursor:
	apiMap[str(row[0])] = row[1]
	response = requests.get(requestUrl + str(row[0]))
	response = client.put_object(Bucket='trapyz-insights', Body=response.text, Key='home/' + row[2] + '/' + now.strftime("%Y-%m-%d") + '/insights.txt')
	time.sleep(5)
	print("Successfully dumped for: " + row[2])
