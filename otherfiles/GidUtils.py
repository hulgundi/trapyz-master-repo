class GidUtils(object):
    def __init__(self):
        # lower case gids set.
        self.gidsToBeIgnored = set()
        self.gidsToBeIgnored.add("null")

    def canGidBeIgnored(self, gid):
        if gid is None:
            return True
        gidLowerCase = gid.lower()
        return gidLowerCase in self.gidsToBeIgnored

#obj = GidUtils()
#print (obj.canGidBeIgnored("null"), obj.canGidBeIgnored(None), obj.canGidBeIgnored("ranga"), obj.canGidBeIgnored("Null"))
