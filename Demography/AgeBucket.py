import abc

class AgeBucket(abc.ABC):
    @abc.abstractmethod
    def getBucketsForCurrentGroup(self, ageGroupRange):
        pass

class AgeBucketError(Exception):
    """Base class for Age bucket Error"""
    pass

class NoBucketsFoundError(AgeBucketError):
    """Raised when the input there are no buckets"""
    pass

class InvalidAgeGroupError(AgeBucketError):
    """Raised when invalid age group is being fetched in the buckets"""
    pass
