import abc

class CountableStrategy(abc.ABC):
    @abc.abstractmethod
    def decorateBlobWithCounters(self, countableData):
        pass

    @abc.abstractmethod
    def mergeTwoBlobs(self, existingCountableData, currentCountableData):
        pass
