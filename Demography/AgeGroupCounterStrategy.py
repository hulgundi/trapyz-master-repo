import logging
from AgeGroupCounter import AgeGroupCounter
from CountableStrategy import CountableStrategy

class AgeGroupCounterStrategy(CountableStrategy):
    def __init__(self):
        self.logger = logging.getLogger()

    def decorateBlobWithCounters(self, ageGroupRanges):
        countersForAgeGroups = []
        for ageGroup in ageGroupRanges:
            countersForAgeGroups.append (AgeGroupCounter(1, ageGroup))

        self._printHelper(countersForAgeGroups)
        return countersForAgeGroups
   
    def mergeTwoBlobs(self, existingAgeGroupData, currentAgeGroupData):
        mergedAgeGroupData = self._deepcopy(existingAgeGroupData)    
        for bucketedData in currentAgeGroupData:
            existingData = bucketedData.present_age_group(mergedAgeGroupData)
            if existingData is None:
                mergedAgeGroupData.append(bucketedData.clone())
            else:
                existingData.counter = existingData.counter + bucketedData.counter

        self._printHelper(mergedAgeGroupData)    
        return mergedAgeGroupData

    def _printHelper(self, countersForAgeGroups):
        self.logger.debug("countersForAgeGroups: %s", ', '.join(ageGroup.to_string() for ageGroup in countersForAgeGroups))
 
    def _deepcopy(self, ageGroupData):
        ageGroupDataClone = []
        for ageGroup in ageGroupData:
            ageGroupDataClone.append(ageGroup.clone())

        return ageGroupDataClone
    
#from AgeGroupRange import AgeGroupRange
#from CustomLengthBasedAgeBucket import CustomLengthBasedAgeBucket
#ageBucket = CustomLengthBasedAgeBucket([AgeGroupRange(1, 12), AgeGroupRange(13,17), AgeGroupRange(18, 24), AgeGroupRange(25, 34), AgeGroupRange(35, 45), AgeGroupRange(46, 54), AgeGroupRange(55, 64), AgeGroupRange(65, 200)])
#obj = AgeGroupCounterStrategy()
#ageGroups = obj.decorateBlobWithCounters(ageBucket.getBucketsForCurrentGroup(AgeGroupRange(3, 28)))
#existingAgeGroups = ageGroups
#ageGroups = obj.decorateBlobWithCounters(ageBucket.getBucketsForCurrentGroup(AgeGroupRange(13, 50)))
#merged = obj.mergeTwoBlobs(existingAgeGroups, ageGroups)
#print(', '.join(bucket.to_string() for bucket in merged))
