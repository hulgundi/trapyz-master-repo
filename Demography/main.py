import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler
from ObjectFactory import ObjectFactory
from DemographicDataPipeline import DemographicDataPipeline

logging.config.fileConfig('logging.conf')

objectFactory = ObjectFactory()
dataPipeline = DemographicDataPipeline(objectFactory)

dataPipeline.executeSingleLineAtATime("dailyHuntLogFile.txt")

