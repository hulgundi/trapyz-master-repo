class GenderCounter(object):
    def __init__(self, counter, gender):
        self.__counter = counter
        self.__gender = gender

    @property
    def counter(self):
        return self.__counter

    @counter.setter
    def counter(self, counter):
        self.__counter = counter

    @property
    def gender(self):
        return self.__gender

    @gender.setter
    def gender(self, gender):
        self.__gender = gender

    def __eq__(self, other):
        return other != None and self.counter == other.counter and self.gender == other.gender

    def isGenderSame(self, other):
        return other != None and self.gender == other.gender

    def to_string(self):
        return "counter: {} gender: {}".format(str(self.counter), self.gender)

    def clone(self):
        return GenderCounter(self.counter, self.gender)

    def convertToDict(self):
        return {"counter": self.counter, "gender": self.gender}

    @staticmethod
    def fromPersistentSource(genderCounterDict):
        return GenderCounter(int(genderCounterDict["M"]["counter"]["N"]), genderCounterDict["M"]["gender"]["S"])
