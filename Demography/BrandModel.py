import logging

class BrandModel(object):
    def __init__(self, brand_name, model_name):
        self.logger = logging.getLogger()
        self.__brand_name = brand_name
        self.__model_name = model_name

    @staticmethod
    def createEmptyInstance():
        BrandModel._EMPTY = BrandModel(None, None)

    @staticmethod
    def getEmptyInstance():
        return BrandModel._EMPTY

    @property
    def brand_name(self):
        return self.__brand_name

    @brand_name.setter
    def brand_name(self, brand_name):
        self.__brand_name = brand_name
    
    @property
    def model_name(self):
        return self.__model_name

    @model_name.setter
    def model_name(self, model_name):
        self.__model_name = model_name

    def log_it(self):
        self.logger.info("brand name: %s model name: %s", self.__brand_name, self.__model_name)    

    def to_string(self):
        return "brand name: {} model name: {}".format(self.__brand_name, self.__model_name)

    def __eq__(self, other):
        return other != None and self.__brand_name == other.__brand_name and self.__model_name == other.__model_name
