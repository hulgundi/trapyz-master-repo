import logging

class AgeGroupRange(object):
    def __init__(self, min_age, max_age):
        self.logger = logging.getLogger()
        self.__min_age = min_age
        self.__max_age = max_age

    def create_instance(self):
        return AgeGroupRange()

    @property
    def min_age(self):
        return self.__min_age

    @min_age.setter
    def min_age(self, min_age):
        self.__min_age = min_age
    
    @property
    def max_age(self):
        return self.__max_age

    @max_age.setter
    def max_age(self, max_age):
        self.__max_age = max_age

    def log_it(self):
        self.logger.info("min age: %d max age: %d", self.__min_age, self.__max_age)    

    def to_string(self):
        return "min age: {} max age: {}".format(self.__min_age, self.__max_age)

    def __eq__(self, other):
        return self.__min_age == other.__min_age and self.__max_age == other.__max_age

    def __lt__(self, other):
       return self.__min_age < other.__min_age

    def present_age_group(self, currentAgeGroups):
        for bucketedData in currentAgeGroups:
            is_equal = self.__eq__(bucketedData)
            if is_equal == True:
                return bucketedData

        return None
