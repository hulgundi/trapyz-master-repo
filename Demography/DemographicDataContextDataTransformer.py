import logging
from DemographicData import DemographicData

class DemographicDataContextDataTransformer(object):
    def __init__(self, ageGroupCounterStrategy, brandModelStrategy, genderCounterStrategy):
        self.logger = logging.getLogger()
        self.ageGroupCounterStrategy = ageGroupCounterStrategy
        self.brandModelStrategy = brandModelStrategy
        self.genderCounterStrategy = genderCounterStrategy

    def transform(self, demographicDataContext):
        demographicData = DemographicData()

        demographicData.gid = demographicDataContext.gid
        demographicData.brandModelCounter = self.brandModelStrategy.decorateBlobWithCounters(demographicDataContext.brandModel)
        demographicData.genderCounter = self.genderCounterStrategy.decorateBlobWithCounters(demographicDataContext.gender)
        demographicData.ageGroupCounterArr = self.ageGroupCounterStrategy.decorateBlobWithCounters(demographicDataContext.ageBuckets)

        demographicData.createdAtTimestamp = demographicDataContext.createdAtTimestamp
        demographicData.processingTimestamp = demographicDataContext.processingTimestamp

        self.logger.debug("demographicData: %s", demographicData.to_string())

        return demographicData
