import logging
from BrandModelCounter import BrandModelCounter
from CountableStrategy import CountableStrategy

class BrandModelCounterStrategy(CountableStrategy):
    def __init__(self):
        self.logger = logging.getLogger()

    def decorateBlobWithCounters(self, brandModel):
        brandModelCounter = BrandModelCounter(1, brandModel)

        self.logger.debug("brandModelCounter: %s", brandModelCounter.to_string())
        return brandModelCounter
   
    def mergeTwoBlobs(self, existingBrandModelData, currentBrandModelData):
        mergedBrandModelData = self._deepcopy(existingBrandModelData) 
        isNewBrandModel = True

        for brandModelData in mergedBrandModelData:
            if brandModelData.isBrandModelSame(currentBrandModelData):
                brandModelData.counter = currentBrandModelData.counter + brandModelData.counter
                isNewBrandModel = False

        if (isNewBrandModel):
            mergedBrandModelData.append(currentBrandModelData)

        self._printHelper(mergedBrandModelData)
        return mergedBrandModelData

    def _printHelper(self, brandModelDataArr):
        self.logger.debug("brandModelDataArr: %s", ', '.join(brandModelCounter.to_string() for brandModelCounter in brandModelDataArr))

    def _deepcopy(self, brandModelData):
        brandModelDataClone = []
        for brandModel in brandModelData:
            brandModelDataClone.append(brandModel.clone())

        return brandModelDataClone 

#from BrandModel import BrandModel
#obj = BrandModelCounterStrategy()
#brandModelWithCounter = obj.decorateBlobWithCounters(BrandModel("Samsung", "Note 3"))
#existingBrandModelWithCounter = brandModelWithCounter
#brandModelWithCounter = obj.decorateBlobWithCounters(BrandModel("Samsung", "Note 4"))
#existingBrandModelWithCounter_2 = brandModelWithCounter
#brandModelDataArr = obj.mergeTwoBlobs([existingBrandModelWithCounter, existingBrandModelWithCounter_2], brandModelWithCounter)
#print (', '.join(brandModelCounter.to_string() for brandModelCounter in brandModelDataArr))
