import logging
from AgeBucket import AgeBucket, AgeBucketError, NoBucketsFoundError, InvalidAgeGroupError 
from AgeGroupRange import AgeGroupRange

class CustomLengthBasedAgeBucket(AgeBucket):
    def __init__(self, bucketLengths):
        self.logger = logging.getLogger()
        self.sortedBucketLengths = sorted(bucketLengths)
        self.validate(self.sortedBucketLengths)

        self.minAge = self.sortedBucketLengths[0].min_age
        self.maxAge = self.sortedBucketLengths[len(self.sortedBucketLengths) - 1].max_age

    def validate(self, sortedBucketLengths):
        # check for the overlap
        index = 0
        for bucket in sortedBucketLengths:
            if index == 0:
                prev = bucket.max_age
                index = index + 1
            else:
                if (prev >= bucket.min_age):
                    raise RuntimeError('prev max age {} is more than current min age {}'.format(prev, bucket.min_age))
                prev = bucket.max_age

    def getBucketsForCurrentGroup(self, ageGroupRange):
        if ageGroupRange.max_age < ageGroupRange.min_age or ageGroupRange.max_age < self.minAge or ageGroupRange.min_age > self.maxAge:
            raise InvalidAgeGroupError

        buckets = []

        bucket_has_to_be_added = None
        for bucket in self.sortedBucketLengths:
            if bucket.max_age < ageGroupRange.min_age:
                continue
            elif bucket.min_age > ageGroupRange.max_age:
                continue
            else:
                buckets.append(bucket)

        if len(buckets) == 0:
            self.logger.info("No buckets found for age group %s", ageGroupRange.to_string())
            raise NoBucketsFoundError
          
        self.logger.debug("buckets: %s found for agegroup %s: ", ', '.join(bucket.to_string() for bucket in buckets),  ageGroupRange.to_string())
        return buckets

#obj = CustomLengthBasedAgeBucket([AgeGroupRange(1, 12), AgeGroupRange(13,17), AgeGroupRange(18, 24), AgeGroupRange(25, 34), AgeGroupRange(35, 45), AgeGroupRange(46, 54), AgeGroupRange(55, 64), AgeGroupRange(65, 200)])

#try:
#    buckets = obj.getBucketsForCurrentGroup(AgeGroupRange(3, 28))
#    print (', '.join(bucket.to_string() for bucket in buckets))
#except AgeBucketError:
#    print("exception here") 

#try:
#    buckets = obj.getBucketsForCurrentGroup(AgeGroupRange(15, 100))
#    print (', '.join(bucket.to_string() for bucket in buckets))
#except AgeBucketError:
#    print("exception here") 

#try:
#
#    buckets = obj.getBucketsForCurrentGroup(AgeGroupRange(-1, 300))
#    print (', '.join(bucket.to_string() for bucket in buckets))
#except AgeBucketError:
#    print("exception here") 
#
#try:
#
#    buckets = obj.getBucketsForCurrentGroup(AgeGroupRange(-3, -1))
#    print (', '.join(bucket.to_string() for bucket in buckets))
#except AgeBucketError:
#    print("exception here") 
#
#try:
#
#    buckets = obj.getBucketsForCurrentGroup(AgeGroupRange(3, 1))
#    print (', '.join(bucket.to_string() for bucket in buckets))
#except AgeBucketError:
#    print("exception here") 
#
#try:
#    
#    buckets = obj.getBucketsForCurrentGroup(AgeGroupRange(0, 2))
#    print (', '.join(bucket.to_string() for bucket in buckets))
#except AgeBucketError:
#    print("exception here") 
