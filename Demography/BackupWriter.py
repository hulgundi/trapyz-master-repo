import json
class BackupWriter(object):
    def __init__(self, fileName):
        self.fileName = open(fileName + "_backup", "w")
    def writeSingleLine(self, line):
        self.fileName.write(json.dumps(line) + "\n")
