from ElasticSearchWrapper import ElasticSearchWrapper
import json
import sys
import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler

class DemographyDataIndex(object):
    def __init__(self, logger):
        self.logger = logger

    def createIndexForBrand(self, indexName):
        obj = ElasticSearchWrapper(indexName)
        indexCreationPayload = {"mappings": self.indexMappings(), "settings" : self.indexSettings()}

        self.logger.debug("executing payload: %s", json.dumps(indexCreationPayload))

        obj.createIndex(indexCreationPayload)
        
    def indexMappings(self):
        return {"_doc":{"dynamic":"strict","properties":{"gid":{"type":"keyword"},"ageGroupCounter":{"type":"nested","properties":{"counter":{"type":"short"},"max_age":{"type":"short"},"min_age":{"type":"short"}}},"brandModelCounter":{"type":"nested","properties":{"model":{"type":"text","analyzer":"case_insensitive_analyzer"},"brand":{"type":"text","analyzer":"case_insensitive_analyzer"},"counter":{"type":"short"}}},"genderCounter":{"type":"nested","properties":{"counter":{"type":"short"},"gender":{"type":"keyword"}}},"createdAtTimestamp":{"type":"date","format":"epoch_millis"},"lastUpdatedTimestamp":{"type":"date","format":"epoch_millis"}}}}

    def indexSettings(self):
        return {"index":{"number_of_shards":3,"number_of_replicas":2},"analysis":{"analyzer":{"case_insensitive_analyzer":{"type":"custom","tokenizer":"standard","filter":["lowercase"]}}}}

if len(sys.argv) != 2:
    print('Usage: pass the index name in the first argument {}'.format(sys.argv[0]))
    exit(1)

logging.config.fileConfig('logging.conf')

logger = logging.getLogger()

obj = DemographyDataIndex(logger)
obj.createIndexForBrand(sys.argv[1])
