import logging

class DemographicDataContext(object):
    def __init__(self):
        self.logger = logging.getLogger("demographicDataParserLogger")
        self.gid = None
        self.brandModel = None
        self.gender = None
        self.ageBuckets = []
        self.createdAtTimestamp = -1
        self.processingTimestamp = -1

    def create_instance(self):
        return DemographicDataContext()

    @property
    def gid(self):
        return self.__gid

    @gid.setter
    def gid(self, gid):
        self.__gid = gid

    @property
    def brandModel(self):
        return self.__brandModel

    @brandModel.setter
    def brandModel(self, brandModel):
        self.__brandModel = brandModel

    @property
    def gender(self):
        return self.__gender

    @gender.setter
    def gender(self, gender):
        self.__gender = gender

    @property
    def ageBuckets(self):
        return self.__ageBuckets

    @ageBuckets.setter
    def ageBuckets(self, ageBuckets):
        self.__ageBuckets = ageBuckets

    @property
    def createdAtTimestamp(self):
        return self.__createdAtTimestamp

    @createdAtTimestamp.setter
    def createdAtTimestamp(self, createdAtTimestamp):
        self.__createdAtTimestamp = createdAtTimestamp

    @property
    def processingTimestamp(self):
        return self.__processingTimestamp

    @processingTimestamp.setter
    def processingTimestamp(self, processingTimestamp):
        self.__processingTimestamp = processingTimestamp
     
    def to_string(self):
        return "gid: {} brandModel: {} gender: {} ageBuckets: {} createdAtTimestamp: {} processingTimestamp: {}".format(self.gid, self.brandModel.to_string(), self.gender, ', '.join(bucket.to_string() for bucket in self.ageBuckets), str(self.createdAtTimestamp), str(self.processingTimestamp))

    
