from DemographicDataParser import DemographicDataParser
from DemographicDataParser import DemographicDataParseError
from DemographicDataParser import DemographicDataValidationError
from AgeGroupRange import AgeGroupRange
from DemographicDataContext import DemographicDataContext
import json
import time

class DailyHuntDemographicDataParser(DemographicDataParser):
    def __init__(self, phoneModelMapper, ageBucket):
        super(DailyHuntDemographicDataParser, self).__init__(phoneModelMapper, ageBucket)
        """
        This is how the data is currently coming in daily hunt.
        dhanbad_hi_Male_31-35_xiaomi_redmi note 5_low_2_6_11_12_13_106_243_247_250_254_261
        """
        self.GID_KEY = "gid"
        self.DEMOGRAPHIC_DATA_KEY = "ua"
        self.RECORD_CREATED_TIMESTAMP_KEY = "createdAt"
    
        self.BRAND = "brand"
        self.CITY = "city"
        self.LANGUAGE = "language"
        self.GENDER = "gender"
        self.AGE_GROUP = "ageGroup"
        self.MODEL = "model"
 
        self.column_to_index_mapping = {
            self.CITY : 0,
            self.LANGUAGE : 1,
            self.GENDER : 2,
            self.AGE_GROUP : 3,
            self.BRAND : 4,
            self.MODEL : 5,
            "daily_hunt_profiling_data" : 6,
            "indices_for_daily_hunt_starts_at" : 7
        }

        self.minimumFieldsInRawData = 8
        self.dataDelimiter = "_"
        self.fieldsForAgeBucket = 2
        self.ageBucketDelimiter = "-"
        self.currentTimeInMillis = lambda: int(round(time.time() * 1000))
        
    def parse(self, rawEntryAsStr):
        rawEntryDict = json.loads(rawEntryAsStr)
 
        if self.GID_KEY not in rawEntryDict or self.DEMOGRAPHIC_DATA_KEY not in rawEntryDict or self.RECORD_CREATED_TIMESTAMP_KEY not in rawEntryDict:
            raise DemographicDataValidationError("Either of {} or {} or {} not found.".format(self.GID_KEY, self.DEMOGRAPHIC_DATA_KEY, self.RECORD_CREATED_TIMESTAMP_KEY))

        demographicDataArr = rawEntryDict[self.DEMOGRAPHIC_DATA_KEY].split(self.dataDelimiter)

        if len(demographicDataArr) < self.minimumFieldsInRawData:
            raise DemographicDataParseError('couldnot parse the data {}'.format(rawEntryAsStr))
        
        demographicDataObj = DemographicDataContext()

        self.__updateGidData(demographicDataObj, rawEntryDict[self.GID_KEY])
        self.__updateBrandModelData(demographicDataObj, demographicDataArr)
        self.__updateGenderData(demographicDataObj, demographicDataArr)
        self.__updateAgeGroups(demographicDataObj, demographicDataArr)
        self.__updateTimestamps(demographicDataObj, rawEntryDict[self.RECORD_CREATED_TIMESTAMP_KEY])

        return demographicDataObj

    def __updateGidData(self, demographicDataObj, gid):
        demographicDataObj.gid = gid
        
    def __updateBrandModelData(self, demographicDataObj, demographicDataArr):
        brand = demographicDataArr[self.column_to_index_mapping[self.BRAND]]
        model = demographicDataArr[self.column_to_index_mapping[self.MODEL]]
        brandModel = self._mapBrandModelWithFallback(brand, model)
        demographicDataObj.brandModel = brandModel

    def __updateGenderData(self, demographicDataObj, demographicDataArr):
        genderRawStr = demographicDataArr[self.column_to_index_mapping[self.GENDER]]
        gender = self._mapGender(genderRawStr)
        if gender != None:
            demographicDataObj.gender = gender

    def __updateAgeGroups(self, demographicDataObj, demographicDataArr):
        ageGroupStr = demographicDataArr[self.column_to_index_mapping[self.AGE_GROUP]]
        ageGroup = ageGroupStr.split("-") 
        if len(ageGroup) == self.fieldsForAgeBucket:
            ageBuckets = self._mapAgeGroup(AgeGroupRange(int(ageGroup[0]), int(ageGroup[1])))
            if ageBuckets != None:
                demographicDataObj.ageBuckets = ageBuckets
        else:
            self.logger.warn("couldnot parse the age group str %s", ageGroupStr)

    def __updateTimestamps(self, demographicDataObj, createdAtTimestamp):
        demographicDataObj.createdAtTimestamp = createdAtTimestamp
        demographicDataObj.processingTimestamp = self.currentTimeInMillis()
   
        
    '''
    Not being currently used.
    '''
    def __updateCityData(self, demographicDataObj, demographicDataArr):
        city = demographicDataArr[self.column_to_index_mapping[self.CITY]]
        '''
        TODO: Fetch the right city details from the database and map it properly.
        '''
