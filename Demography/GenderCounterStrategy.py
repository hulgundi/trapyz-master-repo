import logging
from GenderCounter import GenderCounter
from CountableStrategy import CountableStrategy

class GenderCounterStrategy(CountableStrategy):
    def __init__(self):
        self.logger = logging.getLogger()

    def decorateBlobWithCounters(self, gender):
        genderCounter = GenderCounter(1, gender)

        self.logger.debug("genderCounter: %s", genderCounter.to_string())
        return genderCounter
   
    def mergeTwoBlobs(self, existingGenderData, currentGenderData):
        mergedGenderData = self._deepcopy(existingGenderData)    
        isNewGender = True

        for genderData in mergedGenderData:
            if genderData.isGenderSame(currentGenderData):
                genderData.counter = currentGenderData.counter + genderData.counter
                isNewGender = False

        if (isNewGender):
            mergedGenderData.append(currentGenderData)

        self._printHelper(mergedGenderData)
        return mergedGenderData

    def _printHelper(self, genderDataArr):
        self.logger.debug("genderDataArr: %s", ', '.join(genderCounter.to_string() for genderCounter in genderDataArr))

    def _deepcopy(self, genderData):
        genderDataClone = []
        for gender in genderData:
            genderDataClone.append(gender.clone())

        return genderDataClone 

#obj = GenderCounterStrategy()
#genderWithCounter = obj.decorateBlobWithCounters("Male")
#existingGenderWithCounter = genderWithCounter
#genderWithCounter = obj.decorateBlobWithCounters("Female")
#existingGenderWithCounter_2 = genderWithCounter
#genderDataArr = obj.mergeTwoBlobs([existingGenderWithCounter, existingGenderWithCounter_2], genderWithCounter)
#print (', '.join(genderCounter.to_string() for genderCounter in genderDataArr))
