from ElasticSearchWrapper import ElasticSearchWrapper
from CustomLengthBasedAgeBucket import CustomLengthBasedAgeBucket
from AgeGroupRange import AgeGroupRange
from DailyHuntDemographicDataParser import DailyHuntDemographicDataParser
from DemographicDataContextDataTransformer import DemographicDataContextDataTransformer
from DemographicDataAggregator import DemographicDataAggregator
from AgeGroupCounterStrategy import AgeGroupCounterStrategy
from BrandModelCounterStrategy import BrandModelCounterStrategy
from GenderCounterStrategy import GenderCounterStrategy
from DataStreamReader import DataStreamReader
from DynamoDbWrapper import DynamoDbWrapper
from ElasticSearchWrapper import ElasticSearchWrapper
from BackupWriter import BackupWriter
from BrandModel import BrandModel

'''
Poor's man implementation to build the Object Factory, Ideally we should think of an IOC here.
'''
class ObjectFactory(object):
    def __init__(self, phoneModelIndex = "phonemodel", bucketLengths = [AgeGroupRange(1, 12), AgeGroupRange(13,17), AgeGroupRange(18, 24), AgeGroupRange(25, 34), AgeGroupRange(35, 45), AgeGroupRange(46, 54), AgeGroupRange(55, 64), AgeGroupRange(65, 200)], esIndex = "demography", ddbTableName = "DemographicData", ddbPartitionKey = "gid"):
        self.phoneModelMapper = ElasticSearchWrapper(phoneModelIndex)
        self.ageBucket = CustomLengthBasedAgeBucket(bucketLengths)
        self.dailyHuntDemographicDataParser = DailyHuntDemographicDataParser(self.phoneModelMapper, self.ageBucket)
        self.ageGroupCounterStrategy = AgeGroupCounterStrategy()
        self.brandModelCounterStrategy = BrandModelCounterStrategy()
        self.genderCounterStrategy = GenderCounterStrategy()
        self.demographicDataContextDataTransformer = DemographicDataContextDataTransformer(self.ageGroupCounterStrategy, self.brandModelCounterStrategy, self.genderCounterStrategy)
        self.demographicDataAggregator = DemographicDataAggregator(self.ageGroupCounterStrategy, self.brandModelCounterStrategy, self.genderCounterStrategy)
        self.persistentSource = DynamoDbWrapper()
        self.searchSource = ElasticSearchWrapper(esIndex)
        self.persistentSourceName = ddbTableName
        self.persistentSourceKeyName = ddbPartitionKey

        '''
        initialize Static Instances
        '''
        BrandModel.createEmptyInstance()

    def getDemographicDataContextDataTransformer(self):
        return self.demographicDataContextDataTransformer

    def getDemographicDataAggregator(self):
        return self.demographicDataAggregator

    def getDataStreamReader(self, fileName):
        return DataStreamReader(fileName)

    def getParser(self, source):
        if (source == "DailyHunt"):
            return self.dailyHuntDemographicDataParser

        raise RuntimeError("source {} not supported".format(source))

    def getPersistentSource(self):
        return self.persistentSource

    def getSearchSource(self):
        return self.searchSource

    def getBackupWriter(self, fileName):
        return BackupWriter(fileName) 

    def getPersistentSourceName(self):
        return self.persistentSourceName

    def getPersistentSourceKeyName(self):
        return self.persistentSourceKeyName
