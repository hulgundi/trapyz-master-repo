import abc
import json
import logging
from BrandModel import BrandModel

class DemographicDataParser(abc.ABC):
    def __init__(self, brandModelMapper, ageBucket):
        self.brandModelMapper = brandModelMapper
        self.ageBucket = ageBucket
        self.logger = logging.getLogger("demographicDataParserLogger")
        '''
        Assuming gender can't have multiple values so, this is best to store the lower case possible values to the that we want to keep at our end.
        If this list starts growing bigger, the right thing would be to segregate this class to another and use composition pattern here.
        '''
        self.gender = {"male" : "Male", "m" : "Male", "female" : "Female", "f" : "Female"}
        '''
        Assuming we would have very few languages that we are going to support, this definitely would increase in future, segregate this logic to the 
        right class.
        '''
        self.language = {"hi" : "Hindi", "en" : "English", "hindi" : "Hindi", "english" : "English"}
        self.DEFAULT_MODEL_PREFIX = "Trapyz"        
        
    @abc.abstractmethod
    def parse(self, rawEntryAsStr):
        pass

    def _mapAgeGroup(self, ageGroupRange):
        try:
            buckets = self.ageBucket.getBucketsForCurrentGroup(ageGroupRange)
            self.logger.debug("all buckets: %s", ', '.join(bucket.to_string() for bucket in buckets))
            return buckets
        except AgeBucketError:
            self.logger.warn("exception while extracting the data for range %s", ageGroupRange.to_string())
        return None

    def _mapGender(self, gender):
        for key in self.gender:
            if str(gender).lower() == key:
                self.logger.debug("Mapped %s to %s", gender, self.gender[key])
                return self.gender[key]
        self.logger.warn("No mapping gender found at our end for %s", self.gender)
        return None

    def _mapLanguage(self, language):
        for key in self.language:
            if str(language).lower() == key:
                self.logger.debug("Mapped %s to %s", language, self.language[key])
                return self.language[key]
        self.logger.warn("No mapping language found at our end for %s", self.gender)
        return None

    def _mapBrandModel(self, brand, model):
        brandModelRes = self.brandModelMapper.getDataFromElasticSearch(self.__generateBrandModelQuery(brand, model))
        self.logger.debug("response: %s ", brandModelRes)
        json_obj = json.loads(brandModelRes)

        if len(json_obj["hits"]["hits"]) == 1: 
            return BrandModel(json_obj["hits"]["hits"][0]["_source"]["brand"], json_obj["hits"]["hits"][0]["_source"]["model"])

        return None    

    def _mapBrandModelWithFallback(self, brand, model):
        brandModel = self._mapBrandModel(brand, model)
        if brandModel is None:
            fallBack = self._mapBrandModel(brand, self.DEFAULT_MODEL_PREFIX + brand)
            if fallBack is None:
                return BrandModel.getEmptyInstance()
            return fallBack

        return brandModel

    def __generateBrandModelQuery(self, brand, model):
        return {"size":1,"query":{"bool":{"must":[{"match":{"brand": brand}},{"match":{"model": model}}]}}} 

class DemographicDataError(Exception):
    """Raised, for any demographic data related error."""
    pass

class DemographicDataParseError(DemographicDataError):
    """Raised, whenever data is not parseable."""
    pass
class DemographicDataValidationError(DemographicDataError):
    """Raised, whenever data is not parseable."""
    pass
