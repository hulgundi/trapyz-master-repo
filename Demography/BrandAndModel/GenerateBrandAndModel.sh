#!/bin/bash

cd /home/ubuntu/varunplay/demography/BrandAndModel

modelDir=models/
modelFileDatabase=brandandmodel.csv
indexName=phonemodel

wget "https://www.gsmarena.com/makers.php3" -O AllModels.html

awk -v modelDir=$modelDir 'match($0, /td.*a href=(.*phones.*.php)(.*)br.*devices.*a.*td/, m) {s = m[2]; gsub("<", "", s); gsub (">", "", s); printf("wget \"https://www.gsmarena.com/%s\" -O \"%s%s.html\"\n", m[1], modelDir, s);}' AllModels.html > execute.sh 

rm -rf models/*
chmod +x execute.sh
./execute.sh

python3 BrandCrawler.py $modelDir $modelFileDatabase 
python3 BrandIndex.py $indexName
python3 DumpBrandModelDataToES.py $modelFileDatabase $indexName
