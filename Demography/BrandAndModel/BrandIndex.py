from ElasticSearchWrapper import ElasticSearchWrapper
import json
import sys
import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler

class BrandIndex(object):
    def __init__(self, logger):
        self.logger = logger

    def createIndexForBrand(self, indexName):
        obj = ElasticSearchWrapper(indexName)
        indexCreationPayload = {"mappings": self.indexMappings(), "settings" : self.indexSettings()}

        self.logger.debug("executing payload: %s", json.dumps(indexCreationPayload))

        obj.createIndex(indexCreationPayload)
        
    def indexMappings(self):
        return {"_doc":{"dynamic":"strict","properties":{"brand":{"type":"text","analyzer":"case_insensitive_analyzer"},"model":{"type":"text","analyzer":"case_insensitive_analyzer"}}}}

    def indexSettings(self):
        return {"index":{"number_of_shards":3,"number_of_replicas":2},"analysis":{"analyzer":{"case_insensitive_analyzer":{"type":"custom","tokenizer":"standard","filter":["lowercase"]}}}}

if len(sys.argv) != 2:
    print('Usage: pass the index name in the first argument {}'.format(sys.argv[0]))
    exit(1)

logging.config.fileConfig('logging.conf')

logger = logging.getLogger("brandModelLogger")

obj = BrandIndex(logger)
obj.createIndexForBrand(sys.argv[1])
