from __future__ import print_function
import os
import re
import requests 
from pprint import pprint
import sys
from html.parser import HTMLParser 
import codecs
import csv
import logging
import logging.config

dumpHeadersFlag = True
count = 1

logging.config.fileConfig('logging.conf')
logger = logging.getLogger("brandModelLogger")

class BrandGetter(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'span':
            return
        self.parentTag = tag

    def handle_data(self, data):
        if self.lasttag != 'span' or self.parentTag != 'strong':
            return

        self.links.append(data)

class NextPageParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        attr = dict(attrs)
        if tag != 'a' or not 'class' in attr or not 'title' in attr or "disabled" in attr['class'] or 'Next page' != attr['title']:
            return
        logger.info ("%s", attr)
        self.nextPage = attr['href']

def extractFromDir():
    if len(sys.argv) != 3:
        print('Usage: {} first argument should be directory, second argument should be file to be written'.format(sys.argv[0]))
        return

    directory = sys.argv[1]
    fileToBeWritten = sys.argv[2]

    os.remove(fileToBeWritten)
    for filename in os.listdir(directory):
        if filename.endswith(".html"): 
            logger.info ("parising, %s ", os.path.join(directory, filename))
            currentFile = os.path.join(directory, filename)
            extract(currentFile, fileToBeWritten)
        else:
            logger.debug ("Rejected file %s", os.path.join(directory, filename))

def extract(fileName, fileToBeWritten):

    global dumpHeadersFlag, count
    allLinks = []

    with codecs.open(fileName, "r",encoding='utf-8', errors='ignore') as f:
        content = f.read().replace('\n', '')
   
    execute = True
    while (execute):

        parser = BrandGetter()
        parser.links = []
        parser.feed(content)
        for l in parser.links:
            if l and re.match('^[\-a-zA-Z0-9_\ \+\(\)\.\-\'\/\&\@\+\*:]+$', l):
                allLinks.append(l)
            else:
                logger.info ("rejected the link %s", l)

        nextPageParser = NextPageParser()
        nextPageParser.nextPage = None
    
        nextPageParser.feed(content)
    
        if not nextPageParser.nextPage is None:
            logger.info("next page link %s", nextPageParser.nextPage)
            nextPageUrl = "https://www.gsmarena.com/" + nextPageParser.nextPage
    
            r = requests.get(url = nextPageUrl) 
    
            content = r.text
        else:
            execute = False


    brandDetail = re.match( r'.*\/(.*).html', fileName, re.M|re.I)

    brandName = None
    if brandDetail:
        logger.info ("brandName %s: ", brandDetail.group(1))
        brandName = brandDetail.group(1)

    with open(fileToBeWritten, 'a+') as csvfile:
        fieldnames = ['_id', 'brand', 'model']
        csv.register_dialect('brandcsv', delimiter=',', quoting=csv.QUOTE_ALL)
        brandcsv = csv.get_dialect('brandcsv')
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, dialect=brandcsv)

        if dumpHeadersFlag:
            writer.writeheader()
            dumpHeadersFlag = False

        '''
        dump the generic model for this brand.
        '''
        _id = 'tr' + '_' + 'bm' + '_' + 'v1' + '_' + str(count)
        count = count + 1
        writer.writerow({'_id' : _id, 'brand' : brandName, 'model' : 'Trapyz' + brandName})
          
        for model in allLinks:
            _id = 'tr' + '_' + 'bm' + '_' + 'v1' + '_' + str(count)
            count = count + 1
            writer.writerow({'_id' : _id, 'brand' : brandName, 'model' : model})
    
extractFromDir() 
