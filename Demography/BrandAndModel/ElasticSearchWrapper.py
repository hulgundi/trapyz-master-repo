import boto3
from datetime import datetime, timedelta
import logging
import random
import requests
import json
import itertools
from requests_aws4auth import AWS4Auth

class ElasticSearchWrapper(object):
    def __init__(self, indexName):
        self.elasticSearchEndpoint = "http://juno-es.trapyz.com:9200/"
        self.indexName = indexName
        self.logger = logging.getLogger("elasticSearchLogger") 
    
    def send_signed(self, method, url, service="es", region="us-east-1", body=None):
        credentials = boto3.Session().get_credentials()
        auth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)
        fn = getattr(requests, method)
        # WE MIGHT HAVE STORED THIS "\n" in the data in elastic search for dates 19th July 2018, 2nd July 2018, 3th July 2018, 4th July 2018, 5th July 2018, 6th July 2018 
        # so, if we would have to pull out the document then we would have to replace last characters of the html content which is returned.
        # this }}\n}]}} should become }}}]}}
        #if body and not body.endswith("\n"):
        #    body += "\n"
        response = fn(url, auth=auth, data=body, headers={"Content-Type":"application/json"})
        self.logger.debug ("response: %s", response)
        if response.status_code == 200 or response.status_code == 201:
            htmlBody = response.content.decode('utf-8')
            self.logger.debug ("html body: %s", htmlBody) 
            return htmlBody
        elif response.status_code == 400:
            string = response.content.decode('utf-8')
            json_obj = json.loads(string)
            self.logger.debug("error :%s", json_obj["error"]["type"])
            return json_obj["error"]["type"]

        return None

    def storeBulkDataInElasticSearch(self, documentIdDocumentDict):
        url = self.elasticSearchEndpoint + "_bulk"
        postPayload = ""
        for documentId in documentIdDocumentDict:
            indexDetails = {"index" : {"_index" : self.indexName, "_type" : "_doc", "_id" : documentId} }
            postPayload = postPayload + indexDetails + "\n" + json.dumps(documentIdDocumentDict[documentId]) + "\n"
        self.logger.debug ("bulk details: %s", postPayload)
        self.send_signed("post", url, body = postPayload)

    def storeDataInElasticSearch(self, documentId, document):
        url = self.elasticSearchEndpoint + self.indexName + "/_doc/" + documentId
        self.logger.debug ("url: %s data : %s", url, json.dumps(document))
        self.send_signed("post", url, body=json.dumps(document))

    def createIndex(self, payload):
        url = self.elasticSearchEndpoint + self.indexName
        response = self.send_signed("put", url, body = json.dumps(payload))
        if response == "resource_already_exists_exception":
            self.send_signed("delete", url, body = json.dumps("{}"))
        self.logger.debug("payload executed :%s result :%s", json.dumps(payload),  json.dumps(self.send_signed("put", url, body = json.dumps(payload))))


    def getDataFromElasticSearch(self, queryDict):
        url = self.elasticSearchEndpoint + self.indexName + "/_search"
        self.logger.debug ("url: %s data : %s", url, json.dumps(queryDict))
        return self.send_signed("get", url, body=json.dumps(queryDict))
 
#obj = ElasticSearchWrapper(indexName = "geodataprocessedinsights")

#obj = ElasticSearchWrapper()
#obj.storeDataInElasticSearch({"subcategoryId":"139","storeUuid":"37637","info":{"city":"3","distance":187,"visits":1,"pin":"2513","created_at":1530537714834,"updated_at":1530701828},"gid":"varun","date_hour_min_store_api":"20180702_1321_37637_29","categoryId":"34","apiKey_date_hour_min":"29_20180702_1321","apiKey_ident_date":"29_20180702"})
#print (obj.getDataFromElasticSearch({"query":{"term":{"_id":{"value":"varun_29_20180702_1321_37637"}}}}))
#print (obj.getDataFromElasticSearch({"size":0,"aggs":{"users":{"filters":{"other_bucket_key":"other_messages","filters":{"visitsmorethan2":{"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[{"range":{"info.visits":{"gte":3}}},{"range":{"info.distance":{"lte":50}}}]}}}},{"bool":{"should":[{"term":{"categoryId":{"value":"50"}}},{"term":{"categoryId":{"value":"27"}}},{"term":{"categoryId":{"value":"14"}}},{"term":{"categoryId":{"value":"30"}}},{"term":{"categoryId":{"value":"9"}}}]}},{"term":{"apiKey":{"value":"10"}}},{"range":{"dateHourMin":{"gte":"2018-06-01T00:00"}}}]}},"visitsmorethan1":{"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[{"range":{"info.visits":{"gte":2}}},{"range":{"info.distance":{"lte":50}}}]}}}},{"bool":{"should":[{"term":{"categoryId":{"value":"50"}}},{"term":{"categoryId":{"value":"27"}}},{"term":{"categoryId":{"value":"14"}}},{"term":{"categoryId":{"value":"30"}}},{"term":{"categoryId":{"value":"9"}}}]}},{"term":{"apiKey":{"value":"10"}}},{"range":{"dateHourMin":{"gte":"2018-06-01T00:00"}}}]}},"visitsmorethan0":{"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[{"range":{"info.visits":{"lte":1}}},{"range":{"info.distance":{"lte":50}}}]}}}},{"bool":{"should":[{"term":{"categoryId":{"value":"50"}}},{"term":{"categoryId":{"value":"27"}}},{"term":{"categoryId":{"value":"14"}}},{"term":{"categoryId":{"value":"30"}}},{"term":{"categoryId":{"value":"9"}}}]}},{"term":{"apiKey":{"value":"10"}}},{"range":{"dateHourMin":{"gte":"2018-06-01T00:00"}}}]}}}}}}}))
