import os, sys
import logging.config
import csv
import logging
from logging.handlers import TimedRotatingFileHandler

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from ElasticSearchWrapper import ElasticSearchWrapper

logging.config.fileConfig('logging.conf')

logger = logging.getLogger("brandModelLogger")

def dumpToEs():
    if len(sys.argv) != 3:
        print('Usage: pass file which needs to be read in the first argument, pass the index name in the second argument {}'.format(sys.argv[0]))
        return

    fileName = sys.argv[1]
    indexName = sys.argv[2]
    obj = ElasticSearchWrapper(indexName)

    columns = []
    with open(fileName) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                logger.debug('Column names are :%s', ", ".join(row))
                columns = row
                line_count += 1
            else:
                logger.debug('data %s', ", ".join(row))
                line_count += 1
                obj.storeDataInElasticSearch(row[0], {columns[1] : row[1], columns[2] : row[2]})
        logger.info('Processed %s lines.', line_count)
        print('Processed {} lines.'.format( line_count))


dumpToEs()
