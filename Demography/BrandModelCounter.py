from BrandModel import BrandModel

class BrandModelCounter(BrandModel):
    def __init__(self, counter, brandModel):
        super().__init__(brandModel.brand_name, brandModel.model_name)
        self.__counter = counter

    @property
    def counter(self):
        return self.__counter

    @counter.setter
    def counter(self, counter):
        self.__counter = counter

    def __eq__(self, other):
        return other != None and self.__counter == other.counter and super().__eq__(other)

    def isBrandModelSame(self, other):
        return other != None and super().__eq__(other)

    def to_string(self):
        return "counter: {} brandModel: {}".format(self.__counter, super().to_string())

    def clone(self):
        return BrandModelCounter(self.counter, BrandModel(self.brand_name, self.model_name))

    def convertToDict(self):
        return {"counter" : self.counter, "brand" : self.brand_name, "model": self.model_name}

    @staticmethod
    def fromPersistentSource(brandModelCounterDict):
        if "S" in brandModelCounterDict["M"]["brand"]:
            return BrandModelCounter(int(brandModelCounterDict["M"]["counter"]["N"]), BrandModel(brandModelCounterDict["M"]["brand"]["S"], brandModelCounterDict["M"]["model"]["S"]))
        else:
            return BrandModelCounter(int(brandModelCounterDict["M"]["counter"]["N"]), BrandModel.getEmptyInstance())
