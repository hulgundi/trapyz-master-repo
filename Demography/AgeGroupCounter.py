from AgeGroupRange import AgeGroupRange

class AgeGroupCounter(AgeGroupRange):
    def __init__(self, counter, ageGroupRange):
        super().__init__(ageGroupRange.min_age, ageGroupRange.max_age)
        self.__counter = counter

    @property
    def counter(self):
        return self.__counter

    @counter.setter
    def counter(self, counter):
        self.__counter = counter

    def __eq__(self, other):
        return other != None and self.__counter == other.counter and super().__eq__(other)

    def __lt__(self, other):
        return super().__lt__(other)

    def to_string(self):
        return "counter: {} ageGroupRange: {}".format(self.__counter, super().to_string())

    def clone(self):
        return AgeGroupCounter(self.counter, AgeGroupRange(self.min_age, self.max_age))

    def convertToDict(self):
        return {"counter": self.counter, "min_age" : self.min_age, "max_age" : self.max_age}

    @staticmethod
    def fromPersistentSource(ageGroupCounterObj):
        return AgeGroupCounter(int(ageGroupCounterObj["M"]["counter"]["N"]), AgeGroupRange(int(ageGroupCounterObj["M"]["min_age"]["N"]), int(ageGroupCounterObj["M"]["max_age"]["N"])))
