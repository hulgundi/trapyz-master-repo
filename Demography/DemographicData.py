import logging

class DemographicData(object):
    def __init__(self):
        self.logger = logging.getLogger("demographicDataParserLogger")
        self.gid = None
        self.brandModelCounter = None
        self.genderCounter = None
        self.ageGroupCounterArr = []
        self.createdAtTimestamp = -1
        self.processingTimestamp = -1

    def create_instance(self):
        return DemographicData()

    @property
    def gid(self):
        return self.__gid

    @gid.setter
    def gid(self, gid):
        self.__gid = gid

    @property
    def brandModelCounter(self):
        return self.__brandModelCounter

    @brandModelCounter.setter
    def brandModelCounter(self, brandModelCounter):
        self.__brandModelCounter = brandModelCounter

    @property
    def genderCounter(self):
        return self.__genderCounter

    @genderCounter.setter
    def genderCounter(self, genderCounter):
        self.__genderCounter = genderCounter

    @property
    def ageGroupCounterArr(self):
        return self.__ageGroupCounterArr

    @ageGroupCounterArr.setter
    def ageGroupCounterArr(self, ageGroupCounterArr):
        self.__ageGroupCounterArr = ageGroupCounterArr

    @property
    def createdAtTimestamp(self):
        return self.__createdAtTimestamp

    @createdAtTimestamp.setter
    def createdAtTimestamp(self, createdAtTimestamp):
        self.__createdAtTimestamp = createdAtTimestamp

    @property
    def processingTimestamp(self):
        return self.__processingTimestamp

    @processingTimestamp.setter
    def processingTimestamp(self, processingTimestamp):
        self.__processingTimestamp = processingTimestamp
     
    def to_string(self):
        return "gid: {} brandModelCounter: {} genderCounter: {} ageGroupCounterArr: {} createdAtTimestamp: {} processingTimestamp: {}".format(self.gid, self.brandModelCounter.to_string(), self.genderCounter.to_string(), ', '.join(bucket.to_string() for bucket in self.ageGroupCounterArr), str(self.createdAtTimestamp), str(self.processingTimestamp))

    
