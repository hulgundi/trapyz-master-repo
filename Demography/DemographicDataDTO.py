import logging
from GenderCounter import GenderCounter
from BrandModelCounter import BrandModelCounter
from AgeGroupCounter import AgeGroupCounter

class DemographicDataDTO(object):
    def __init__(self):
        self.logger = logging.getLogger("demographicDataParserLogger")
        self.gid = None
        self.brandModelCounterArr = []
        self.genderCounterArr = []
        self.ageGroupCounterArr = []
        self.createdAtTimestamp = -1
        self.lastUpdatedTimestamp = -1

    @property
    def gid(self):
        return self.__gid

    @gid.setter
    def gid(self, gid):
        self.__gid = gid

    @property
    def brandModelCounterArr(self):
        return self.__brandModelCounterArr

    @brandModelCounterArr.setter
    def brandModelCounterArr(self, brandModelCounterArr):
        self.__brandModelCounterArr = brandModelCounterArr

    @property
    def genderCounterArr(self):
        return self.__genderCounterArr

    @genderCounterArr.setter
    def genderCounterArr(self, genderCounterArr):
        self.__genderCounterArr = genderCounterArr

    @property
    def ageGroupCounterArr(self):
        return self.__ageGroupCounterArr

    @ageGroupCounterArr.setter
    def ageGroupCounterArr(self, ageGroupCounterArr):
        self.__ageGroupCounterArr = ageGroupCounterArr

    @property
    def createdAtTimestamp(self):
        return self.__createdAtTimestamp

    @createdAtTimestamp.setter
    def createdAtTimestamp(self, createdAtTimestamp):
        self.__createdAtTimestamp = createdAtTimestamp

    @property
    def lastUpdatedTimestamp(self):
        return self.__lastUpdatedTimestamp

    @lastUpdatedTimestamp.setter
    def lastUpdatedTimestamp(self, lastUpdatedTimestamp):
        self.__lastUpdatedTimestamp = lastUpdatedTimestamp
     
    def to_string(self):
        return "gid: {} brandModelCounterArr: {} genderCounterArr: {} ageGroupCounterArr: {} createdAtTimestamp: {} lastUpdatedTimestamp: {}".format(self.gid, ', '.join(brandModelCounter.to_string() for brandModelCounter in self.brandModelCounterArr), ', '.join(genderCounter.to_string() for genderCounter in self.genderCounterArr), ', '.join(bucket.to_string() for bucket in self.ageGroupCounterArr), str(self.createdAtTimestamp), str(self.lastUpdatedTimestamp))

    @staticmethod 
    def fromPersistentSource(responseJson):
        obj = DemographicDataDTO.create_instance()
        if "Item" in responseJson:
            obj.gid = responseJson["Item"]["gid"]["S"]
            for genderCounterObj in responseJson["Item"]["genderCounter"]["L"]:
                obj.genderCounterArr.append(GenderCounter.fromPersistentSource(genderCounterObj))
            for brandModelCounterObj in responseJson["Item"]["brandModelCounter"]["L"]:
                obj.brandModelCounterArr.append(BrandModelCounter.fromPersistentSource(brandModelCounterObj))
            for ageGroupCounterObj in responseJson["Item"]["ageGroupCounter"]["L"]:
                obj.ageGroupCounterArr.append(AgeGroupCounter.fromPersistentSource(ageGroupCounterObj))
            obj.createdAtTimestamp = int(responseJson["Item"]["createdAtTimestamp"]["N"])
            obj.lastUpdatedTimestamp = int(responseJson["Item"]["lastUpdatedTimestamp"]["N"])
        return obj

    @staticmethod
    def create_instance():
        return DemographicDataDTO()

    def convertToDict(self):
        return {"gid" : self.gid, "genderCounter" : self.arrayHelper(self.genderCounterArr), "brandModelCounter" : self.arrayHelper(self.brandModelCounterArr), "ageGroupCounter" : self.arrayHelper(self.ageGroupCounterArr), "lastUpdatedTimestamp" : self.lastUpdatedTimestamp, "createdAtTimestamp" : self.createdAtTimestamp}

    def arrayHelper(self, dataArr):
        dictArr = []
        for data in dataArr:
           dictArr.append(data.convertToDict())

        return dictArr
