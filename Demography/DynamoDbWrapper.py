import boto3
from boto3.dynamodb.conditions import Key
from datetime import datetime, timedelta
import string
import logging
import time
from botocore.exceptions import ClientError
import json
from time import sleep

class DynamoDbWrapper(object):
    def __init__(self):
        self.dynamodb = boto3.resource("dynamodb")
        self.dynamodbClient = boto3.client('dynamodb')

        self.logger = logging.getLogger("dynamodbLogger")
        self.RETRY_EXCEPTIONS = ('ProvisionedThroughputExceededException', 'ThrottlingException')

    def genericQueryWrapper(self, tableName, primaryKey, primaryKeyName, sortKeyName, sortKeyPrefix, scanIndexForward, limit):
        filtering_exp = Key(primaryKeyName).eq(primaryKey) & Key(sortKeyName).begins_with(sortKeyPrefix)
        table = self.dynamodb.Table(tableName)
        response = table.query(KeyConditionExpression = filtering_exp, ScanIndexForward = scanIndexForward, Limit = limit)
        return response

    def storeData(self, tableName, data):
        table = self.dynamodb.Table(tableName)
        self.logger.debug("data: %s", data)
        response = table.put_item(Item = data) 
        self.logger.debug("response: %s", response)

    def getItem(self, tableName, primaryKeyName, partitionKey):
        response = self.dynamodbClient.get_item(TableName = tableName, Key = {primaryKeyName :{'S':str(partitionKey)}}) 
        self.logger.debug("response: %s", response)
        return response


    def batchGetItems(self, tableName, primaryPartitionKeyName, partitionKeyList):
        retries = 0
        requestItems = self.generateBatchRequest(tableName, primaryPartitionKeyName, partitionKeyList)
        completeResponse = []
        while True:
            try:
                response = self.dynamodbClient.batch_get_item(
                    RequestItems = requestItems,
                    ReturnConsumedCapacity='TOTAL')
                item = response['Responses']
                unprocessedPartitionKeys = response['UnprocessedKeys']
                completeResponse.append(item)
                self.logger.info("UnprocessedKeys: %s", json.dumps(response['UnprocessedKeys']))
                if not tableName in response['UnprocessedKeys']:
                    break
                requestItems = response['UnprocessedKeys']
            except ClientError as err:
                if err.response['Error']['Code'] not in self.RETRY_EXCEPTIONS:
                    raise
                self.logger.debug("Retrying with exponential backoff enabled. retries: %d. exception: %s", retries, err.response['Error']['Code'])
                sleep(2 ** retries)
                retries += 1
        return completeResponse              

    def batchWriteItems(self, tableName, itemsList):
        table = self.dynamodb.Table(tableName)
        with table.batch_writer() as batch:
            for item in itemsList:
                batch.put_item(Item = item)
 
    def generateBatchRequest(self, tableName, primaryPartitionKeyName, partitionKeyList):
        request = dict()                                                            
        request[tableName] = dict()
        request[tableName]['ConsistentRead'] = True
        request[tableName]['Keys'] = []
               
        for partitionKey in partitionKeyList:
            keyDict = dict()
            keyDict[primaryPartitionKeyName] = {"S": partitionKey}
            request[tableName]['Keys'].append(keyDict)

        return request

#obj = DynamoDbWrapper()
#dataAr = []
#for i in range(100):
#    data = {"gid" : "asdbada" + str(i), "BrandAndModelData" : [{"brand": "google", "model": "pixel 3", "counter": 1}, {"brand": "lg", "model": "nexus 5", "counter": 12}]}    
#    for j in range (1000):
#        data["BrandAndModelData"].append({"brand": "google", "model": "pixel " + str(j), "counter": j})
#    dataAr.append(data)
#obj.batchWriteItems("DemographicData", dataAr)
#dataAr = []
#for i in range(42):
#    dataAr.append("asdbada" + str(i))
#obj.batchGetItems("DemographicData", "gid", dataAr)
