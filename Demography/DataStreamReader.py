import logging

'''
Non-Thread Safe class. Create New Object for each file and make sure, that no two threads access it at the same time.
'''
class DataStreamReader(object):
    def __init__(self, fileName, batchSize = 1):
        self.batchSize = batchSize
        self.infile = open(fileName, 'r')
        self.logger = logging.getLogger()
            
    def readSingleLine(self):
        return self._readLinesHelper(1)

    def readBatch(self):
        return self._readLinesHelper(self.batchSize)

    def _readLinesHelper(self, batchSize):
        lines = [line for line in [self.infile.readline() for _ in range(batchSize)] if len(line) ]
        self.logger.debug("files read: %s", lines)
        return lines
