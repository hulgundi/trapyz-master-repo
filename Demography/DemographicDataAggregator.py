import logging
from DemographicDataDTO import DemographicDataDTO

class DemographicDataAggregator(object):
    def __init__(self, ageGroupCounterStrategy, brandModelStrategy, genderCounterStrategy):
        self.logger = logging.getLogger()
        self.ageGroupCounterStrategy = ageGroupCounterStrategy
        self.brandModelStrategy = brandModelStrategy
        self.genderCounterStrategy = genderCounterStrategy

    def merge(self, existingDemographicData, demographicData):
        demographicDataDTO = DemographicDataDTO()

        demographicDataDTO.gid = demographicData.gid
        demographicDataDTO.brandModelCounterArr = self.brandModelStrategy.mergeTwoBlobs(existingDemographicData.brandModelCounterArr, demographicData.brandModelCounter)
        demographicDataDTO.genderCounterArr = self.genderCounterStrategy.mergeTwoBlobs(existingDemographicData.genderCounterArr, demographicData.genderCounter)
        demographicDataDTO.ageGroupCounterArr = self.ageGroupCounterStrategy.mergeTwoBlobs(existingDemographicData.ageGroupCounterArr, demographicData.ageGroupCounterArr) 

        demographicDataDTO.createdAtTimestamp = demographicData.createdAtTimestamp if demographicDataDTO.createdAtTimestamp == -1 else demographicDataDTO.createdAtTimestamp
        demographicDataDTO.lastUpdatedTimestamp = demographicData.processingTimestamp

        self.logger.debug("demographicData: %s", demographicData.to_string())

        return demographicDataDTO
