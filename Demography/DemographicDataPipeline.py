import logging
from DemographicDataDTO import DemographicDataDTO
from DemographicDataParser import DemographicDataParseError

class DemographicDataPipeline(object):
    def __init__(self, objectFactory, source = "DailyHunt"):
        self.objectFactory = objectFactory
        self.source = source
        self.logger = logging.getLogger()

    def executeSingleLineAtATime(self, fileName):
        dataStreamReader = self.objectFactory.getDataStreamReader(fileName)
        parser = self.objectFactory.getParser(self.source)
        persistentSource = self.objectFactory.getPersistentSource()
        contextToDataTransformer = self.objectFactory.getDemographicDataContextDataTransformer()
        demographicDataAggregator = self.objectFactory.getDemographicDataAggregator()
        backupWriter = self.objectFactory.getBackupWriter(fileName)
        searchWriter = self.objectFactory.getSearchSource()
        persistentSourceName = self.objectFactory.getPersistentSourceName()
        persistentSourceKeyName = self.objectFactory.getPersistentSourceKeyName()
        
        while(True):
            lineAr = dataStreamReader.readSingleLine()
            if len(lineAr) == 0:
                break
            try:
                contextData = parser.parse(lineAr[0])
                self.logger.debug("contextData: %s", contextData.to_string())
                exisitingData = DemographicDataDTO.fromPersistentSource(persistentSource.getItem(persistentSourceName, persistentSourceKeyName, contextData.gid))
                self.logger.debug("exisitingData: %s", exisitingData.to_string())

                currentDataBlob = contextToDataTransformer.transform(contextData)
                mergedDataBlob = demographicDataAggregator.merge(exisitingData, currentDataBlob)
                self.logger.debug("mergedDataBlob: %s", mergedDataBlob.to_string())
                mergedDataBlobDict = mergedDataBlob.convertToDict()
                persistentSource.storeData(persistentSourceName, mergedDataBlobDict)
                searchWriter.storeDataInElasticSearch(contextData.gid, mergedDataBlobDict)
                backupWriter.writeSingleLine(mergedDataBlobDict)
            except DemographicDataParseError as e:
                self.logger.warn(e)
                pass 

            
 
