import pymysql
import json
import os
import sys

db = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')

def getStores():
    cursor = db.cursor()
    sql = "select a.Store_ID, b.lng, b.lat from StoreUuidMap as a, MasterRecordSet as b where a.Store_Uuid = b.UUID"
    try:
        cursor.execute(sql)
        for row in cursor.fetchall():
            print('"' + str(row[0]) + '","' + str(row[1]) + '","' + str(row[2]) + '"')
    except:
        print("Exception")
        db.rollback()

getStores()
