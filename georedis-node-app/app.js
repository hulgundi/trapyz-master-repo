var express = require('express');
var app = express();
var redis = require('redis');
var georedis = require('georedis');
var fs = require('fs');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var client, stores;
let userLatLongJson = [], storeCount, storeIndex = 0, userStoreJson = [], userIndex = 0;

client = redis.createClient();
geoclient = georedis.initialize(client);

app.get('/getNearbyStores', function (req, res) {
    var options, latLongJson = {}, radius;

    latLongJson["latitude"] = req.query.lat;
    latLongJson["longitude"] = req.query.lng;
    radius = req.query.rad;
    console.log("input: "+latLongJson["latitude"]+"+\t"+latLongJson["longitude"]+"\t"+radius);
    
    options = {
        withCoordinates: true,
        withHashes: false,
        withDistances: true,
        withValues:false,
        order: 'ASC',
        units: 'm',
        accurate: true
    }

    stores.nearby(latLongJson, radius, options, function(err, result){
        if (err) console.error(err);
        else {
            console.log(result);
            res.send(result);  
        } 
    })
});

app.get('/getUserLatLong', function(req, res) {
    var xhr, gid, requestUrl = "https://beta.trapyz.com/getgeologs.php?gid=";

    xhr = new XMLHttpRequest();
    gid = req.query.gid;
    requestUrl += gid;
    
    if (req.query.start != undefined && req.query.start !== '' && req.query.end != undefined && req.query.end !== '') {
        requestUrl += "&start=" + req.query.start + "&end=" + req.query.end;   
    }
    userLatLongJson = [];
    count = 0;
    xhr.onreadystatechange = function() {
        var keys, jsonFields = ["wifi", "geopoint"];

        if (xhr.readyState === 4 && xhr.status === 200) {
            responseObject = JSON.parse(xhr.responseText);
            for (var index = 0; index < jsonFields.length; index++) {
                for (var index2 = 0; index2 < responseObject[jsonFields[index]].length; index2++) {
                    var latLngJson = {};
                    
                    latLngJson.lat = responseObject[jsonFields[index]][index2]["lat"];
                    latLngJson.lng = responseObject[jsonFields[index]][index2]["lng"];
                    userLatLongJson[count++] = latLngJson;
                }
            }
            res.send(userLatLongJson);
        }
    };
    xhr.open("GET", requestUrl, true);
    xhr.send(null);
});

app.get('/getUserStore', function(req, res) {
    var gid, radius, userLatLongRequestUrl = "http://localhost:3000/getUserLatLong?";

    gid = req.query.gid;
    radius = req.query.rad;
    userStoreJson = [];
    userIndex = 0;
    userLatLongRequestUrl += "gid=" + gid;
    if (req.query.start != undefined && req.query.start !== '' && req.query.end != undefined && req.query.end !== '') {
        userLatLongRequestUrl += "&start=" + req.query.start + "&end=" + req.query.end;  
    }

    xhr1 = new XMLHttpRequest();
    xhr1.onreadystatechange = function() {
        if (xhr1.readyState === 4 && xhr1.status === 200) {
            storeIndex = 0;
            storeCount = userLatLongJson.length;
            getNearbyStores(res, radius);
        }
    };
    xhr1.open("GET", userLatLongRequestUrl, true);
    xhr1.send(null);
});

function getNearbyStores(responseInstance, radius) {
    var xhr, constructedUrl, nearbyStoresRequestUrl = "http://localhost:3000/getNearbyStores?"; 
    
    xhr = new XMLHttpRequest(); 
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            if (storeIndex < storeCount - 1) {
                var resultLength;

                responseObject = JSON.parse(xhr.responseText);
                storeIndex++;
                for (var index = 0; index < responseObject.length; index++) {
                    userStoreJson[userIndex++] = responseObject[index];
                }
                getNearbyStores(responseInstance, radius);
            } else {
                responseInstance.send(userStoreJson);
            }
        }
    };
    constructedUrl = nearbyStoresRequestUrl + "lng=" + userLatLongJson[storeIndex].lng + "&lat=" + userLatLongJson[storeIndex].lat + "&rad=" + radius;
    xhr.open("GET", constructedUrl, true);
    xhr.send(null); 
}

function loadData() {
    var data, memberName, lineReader;

    stores = geoclient.addSet('stores')

    lineReader = require('readline').createInterface({
        input: require('fs').createReadStream('AllRecords.csv')
    });

    lineReader.on('line', function (line) {
        var latLongJson = {};

        line = line.replace(/\"/g,"");
        data = line.split(",");
        memberName = data[0];
        latLongJson["longitude"] = data[1];
        latLongJson["latitude"] = data[2];
        
        stores.addLocation(memberName, latLongJson, function(err, result){
            if (err) {
                console.error(err);  
            } 
            else {
                //console.log('Location Added: ', result);
            }
        });
    });
}

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
    loadData();
});
