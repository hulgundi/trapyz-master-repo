import datetime

import boto3

s3Client = boto3.client('s3')

s3Resource = boto3.resource('s3')

count = 0

now = datetime.datetime.now().strftime("%Y/%m/%d")

prefixes = ["usergeopointlocation/" + now, "userwificonnectedlocation/" + now, "userwifiscanlocation/" + now]

for value in prefixes:
    kwargs = {'Bucket': 'usergeologs', 'Prefix': value}
    results = s3Client.list_objects_v2(**kwargs)

    for obj in results['Contents']:
        count = count + 1
        print(obj['Key'])
        filename = obj['Key'].split('/')
        s3Resource.Bucket('usergeologs').download_file(obj['Key'], filename[-1])

print(str(count) + ' S3 Files Downloaded')
