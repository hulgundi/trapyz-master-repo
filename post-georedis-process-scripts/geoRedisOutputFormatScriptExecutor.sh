date

# download the big file from s3  s3://data/file.gz
# aws s3 cp s3://data/file.gz data/
# gunzip file.gz 
# cd data
# split -l 100000 file.log   results in xaa, xab
# cd ..

python3 fetch_data.py data/ > output_data 2>> output_data
date
