import pymysql
import json
import os
import sys
import shutil
import concurrent.futures
import requests
import urllib.request
import threading
from GidUtils import GidUtils

lock = threading.Lock()

db = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
separator = "==================================================================================================="

gidUtils = GidUtils()

def readfile(filename):
    payload = {}
    fo = open(filename, 'r')
    for line in fo:
        l = []
        store = {}
        idstring = ''
        logfile = open("logfile", "a")
        line = json.loads(line)

        if gidUtils.canGidBeIgnored(str(line['gid'])):
            continue

        payload['lat'] = str(line['lat'])
        payload['lng'] = str(line['lng'])
        payload['gid'] = str(line['gid'])
        payload['apikey'] = str(apiMap[line['apikey']])
        payload['createdat'] = str(line['createdAt'])
        logfile.write("\n" + separator + "\nProcessing GID: " + payload['gid'])
        result = getNearStores(payload)
        for i in range(len(result)):
            if int(result[i]['distance']) > 300:
                continue
            idstring += "'" + result[i]['key'] + "' ,"
            store[result[i]['key']] = result[i]['distance']
        idstring = idstring.strip(',')
        logfile.write("\n" + idstring)
        logfile.close()
        lock.acquire()        
        getUserInterest(idstring, payload, filename, store)
        lock.release()

    fo.close()

def getNearStores(payload):
    data = []
    lat = payload['lat']
    lng = payload['lng']
    r = urllib.request.urlopen('http://172.31.28.193:3000/getNearbyStores?lng=' + lng + '&lat=' + lat + '&rad=300')
    response = json.loads(r.read().decode('utf-8'))
    return response

def getUserInterest(converted_string, payload, filename, store):
    print_dict = {}
    cursor = db.cursor()
    sql = "select a.Store_ID as uuid, b.sname, b.cat, b.subcat, c.id as city, d.id as pin from StoreUuidMap as a, MasterRecordSet as b, CityMap as c, PincodeMap as d where c.city = b.city and d.PincodeMap = b.pincode and a.Store_Uuid = b.UUID and a.Store_ID in (" + converted_string + ")"
    try:
       cursor.execute(sql)
       for row in cursor.fetchall():
           print_dict["uuid"] = str(row[0])
           print_dict["sname"] = row[1]
           print_dict["cat"] = str(catMap[row[2]])
           print_dict["subcat"] = str(subcatMap[row[3]])
           print_dict["distance"] = store[str(row[0])]
           print_dict["city"] = str(row[4])
           print_dict["pin"] = str(row[5])
           print_dict["gid"] = payload["gid"]
           print_dict["lat"] = payload["lat"]
           print_dict["lng"] = payload["lng"]
           print_dict["apikey"] = payload["apikey"]
           print_dict["createdat"] = payload["createdat"]                              
           print(json.dumps(print_dict))
    except:
        print("Store table Exception")
        db.rollback()

rootdir = sys.argv[1]
filelist = []

apiMap = {}
cursor = db.cursor()
sql = "select * from ApikeyMap"
try:
    cursor.execute(sql)
    for row in cursor.fetchall():
        apiMap[row[1]] = row[0]
except:
    print("API table Exception")
    db.rollback()

catMap = {}
cursor = db.cursor()
sql = "select * from CategoryMap"
try:
    cursor.execute(sql)
    for row in cursor.fetchall():
        catMap[row[1]] = row[0]
except:
    print("Cat table Exception")
    db.rollback()

subcatMap = {}
cursor = db.cursor()
sql = "select * from SubCategoryMap"
try:
    cursor.execute(sql)
    for row in cursor.fetchall():
        subcatMap[row[2]] = row[1]
except:
    print("Subcat table Exception")
    db.rollback()

for ro, subFolders, files in os.walk(rootdir):
    filelist = files
    break

with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
    for fileToBeProcessed in filelist:
        threadpool = executor.submit(readfile, rootdir + fileToBeProcessed)
executor.shutdown()
