import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TrapyzLoginPage from './TrapyzLoginPage';
// import TrapyzHomePage from './TrapyzHomePage';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<TrapyzLoginPage />, document.getElementById('root'));
registerServiceWorker();