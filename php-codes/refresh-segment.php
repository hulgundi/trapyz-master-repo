<?php
$servername = "172.31.0.193";
$username = "pwx";
$password = "ciscoplanet";
$dbname = "trapyz_beta";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$segmentName = urldecode($_GET["seg"]);
$api = urldecode($_GET["api"]);
$uniqueGidArray = array();
$queryJson;
$queryArray;
$segmentVisit;
$currentQueryCount = 0;
$totalQueryCount;
$uniqueCount = 0;

function executeQuery() {
    $queryArray = $GLOBALS['queryArray'];
    $intermediateArray = array();
    $currentQueryCount = $GLOBALS['currentQueryCount'];
    $totalQueryCount = $GLOBALS['totalQueryCount'];
    $value = $queryArray[$currentQueryCount];
    $queryJson = json_decode($value);
    $count = 0;
    
    $requestUrl = "https://canvas.trapyz.com/insights-" . $value->dimension->name . "?" . $value->dimension->name . "=" . $value->dimension->value . "&visit=" . $value->visit->value . "&vcon=" . $value->visit->condition . "&dist=" . $value->distance->value . "&dcon=" . $value->distance->condition . "&start=" . $value->duration->startDate . "&end=" . $value->duration->endDate . "&city=" . $value->city . "&pin=" . $value->pin;
    $content = file_get_contents($requestUrl);
    if (strlen($content) > 5) {
        $intermediateArray = explode(',', $content);
        foreach ($intermediateArray as $key) {
            $gidData = explode(':', $key);
            if (array_key_exists($gidData[0], $GLOBALS['uniqueGidArray'])) {
                $GLOBALS['uniqueGidArray'][$gidData[0]] += $gidData[1];
            } else {
                $GLOBALS['uniqueGidArray'][$gidData[0]] = $gidData[1];
            }   
        }
    }
    $currentQueryCount += 1;
    $GLOBALS['currentQueryCount'] = $currentQueryCount;
    if ($currentQueryCount >= $totalQueryCount) {
        foreach (array_keys($GLOBALS['uniqueGidArray']) as $key2) {
            if ($GLOBALS['uniqueGidArray'][$key2] >= $GLOBALS['segmentVisit']) {
                $count++;
            }
        }
        $GLOBALS['uniqueCount'] = $count;
    } else {
        executeQuery();
    }
}

$sql = "SELECT * FROM segments where apiKey = ".(int)$api." and name = '".$segmentName."'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $queryJson = json_decode($row['attributeJson']);
        $queryArray = $queryJson->queryList;
        $GLOBALS['segmentVisit'] = int($queryJson->segmentVisit);
        $totalQueryCount = sizeof($queryArray);
        executeQuery();
    }
    $sql = "SELECT segmentCountJson, createdDate FROM MasterPredefinedInsights WHERE api = ".(int)$api." order by createdDate desc limit 1";
    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {
        $segmentJson = json_decode($row["segmentCountJson"]);
        $segmentJson->$segmentName->value = $uniqueCount;
        $sql = "UPDATE MasterPredefinedInsights SET segmentCountJson = '".json_encode($segmentJson)."' WHERE api = ".(int)$api." AND createdDate = '".$row['createdDate']."'";
        $conn->query($sql);
        echo $uniqueCount;
    }
} else {
    echo false;
}
$conn->close();
?>

