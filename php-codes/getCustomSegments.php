<?php
$servername = "172.31.0.193";
$username = "pwx";
$password = "ciscoplanet";
$dbname = "trapyz_beta";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$api = $_GET["api"];

$sql = "SELECT * FROM `segments` WHERE apiKey = ".$api;

$response = array();

$responseJson = new stdClass();

$finalResultJson = new stdClass();

$result = $conn->query($sql);

if ($result->num_rows > 0) {
	$finalResultJson->segments = array();
	while ($row = $result->fetch_assoc()) {
		$responseJson->name = $row["name"];
	    $responseJson->desc = $row["description"];
	    $responseJson->api = $row["apiKey"];
	    if ($responseJson->api === "9") {
	    	$responseJson->api = "";
	    }
	    $responseJson->queryList = array();
	    $queryJson = json_decode($row["attributeJson"]);
		$queryUrl = "https://canvas.trapyz.com/insights-";
		foreach ($queryJson->queryList as $item) {
			$finalQueryUrl = $queryUrl . $item->dimension->name . "?" . $item->dimension->name . "=" . $item->dimension->value . "&visit=" . $item->visit->value . "&vcon=" . $item->visit->condition . "&dist=" . $item->distance->value . "&dcon=" . $item->distance->condition . "&start=" . $item->duration->startDate . "&end=" . $item->duration->endDate . "&city=" . $item->city . "&pin=" . $item->pin . "&api=" . $responseJson->api;
			array_push($responseJson->queryList, $finalQueryUrl);
		}
		array_push($finalResultJson->segments, json_encode($responseJson));
	}
	echo json_encode($finalResultJson);
} else {
    echo json_encode($finalResultJson);
}
$conn->close();
?>

