import requests
import time
import json
import pymysql

apiMap = {}
sapi = "9"
predefinedSegmentsList = []

class predefinedInsights():

	def __init__(self, api):
		self.getAllGidCountUrl = "http://52.206.33.14:8000/all?count=1"
		self.getGidsInEpochUrl = "http://52.206.33.14:8000/allbyepoch?start="
		self.getCategoryGidCountUrl = "http://52.206.33.14:8000/allbycat"
		self.currentInSeconds = int(round(time.time() * 1000))
		self.api = str(api)
		self.weekInSeconds = 604800000
		self.monthInSeconds = 2592000000
		self.totalUniqueGidCount = None
		self.lastWeekActiveGidCount = None
		self.lastMonthActiveGidCount = None
		self.categoryGidCountJson = None
		self.segmentCountJson = {}
		
	def getTotalUniqueGidCount(self):
		if self.api != sapi:
			self.totalUniqueGidCount = requests.get(self.getAllGidCountUrl + "&api=" + self.api).text
		else:
			self.totalUniqueGidCount = requests.get(self.getAllGidCountUrl).text
		if self.totalUniqueGidCount == "":
			self.totalUniqueGidCount = "0"
		return

	def getLastWeekActiveGidCount(self):
		if self.api != sapi:
			self.lastWeekActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.weekInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1" + "&api=" + self.api).text
		else:
			self.lastWeekActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.weekInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1").text						
		if self.lastWeekActiveGidCount == "":
			self.lastWeekActiveGidCount = "0"
		return

	def getLastMonthActiveGidCount(self):
		if self.api != sapi:
			self.lastMonthActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.monthInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1" + "&api=" + self.api).text
		else:
			self.lastMonthActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.monthInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1").text
		if self.lastMonthActiveGidCount == "":
			self.lastMonthActiveGidCount = "0"
		return

	def getCategoryGidCount(self):
		if self.api != sapi:
			response = requests.get(self.getCategoryGidCountUrl + "?api=" + self.api)
		else:
			response = requests.get(self.getCategoryGidCountUrl)
		if len(response.text) > 0:
			self.categoryGidCountJson = response.json()
		else:
			self.categoryGidCountJson = "{}"
		return

	def writeData(self):
		try:
			print("total " + self.totalUniqueGidCount + ": week " + self.lastWeekActiveGidCount + ": month " + self.lastMonthActiveGidCount)
			insertQuery = "INSERT INTO MasterPredefinedInsights VALUES(" + self.api + ", NOW(), " + self.totalUniqueGidCount + ", " + self.lastWeekActiveGidCount + ", " + self.lastMonthActiveGidCount + ", '" + json.dumps(self.categoryGidCountJson) + "', '')"
			connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
			cursor = connect.cursor()
			cursor.execute(insertQuery)
			connect.commit()
			print("Successfully Written Data to DB: " + self.api)
		except:
			print("Exception occured while writing to DB: " + self.api)

	def getPredefinedSegments(self):
		try:
			selectQuery = "SELECT * FROM PredefinedSegments";
			connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
			cursor = connect.cursor()
			cursor.execute(selectQuery)
			rows = cursor.fetchall()
			for row in rows:
				segmentName = row[1]
				segmentDescription = row[2]
				responseJson = json.loads(row[3])
				totalCount = 0
				for query in responseJson['queryList']:
					constructedQueryUrl = self.constructQueryString(query)
					totalCount += requests.get(constructedQueryUrl).text.count(",")
				self.segmentCountJson[segmentName] = {}
				self.segmentCountJson[segmentName]["value"] = totalCount
				self.segmentCountJson[segmentName]["type"] = "preset"
		except:
			print("Exception occured while generating predefined segments!")

	def getCustomSegments(self):
		try:
			selectQuery = "SELECT * FROM segments WHERE apiKey = " + self.api;
			connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
			cursor = connect.cursor()
			cursor.execute(selectQuery)
			rows = cursor.fetchall()
			for row in rows:
				segmentName = row[1]
				segmentDescription = row[2]
				responseJson = json.loads(row[3])
				totalCount = 0
				for query in responseJson['queryList']:
					constructedQueryUrl = self.constructQueryString(query)
					totalCount += requests.get(constructedQueryUrl).text.count(",")
				self.segmentCountJson[segmentName] = {}
				self.segmentCountJson[segmentName]["value"] = totalCount
				for item in predefinedSegmentsList:
					if (segmentName == item):
						print(segmentName)
					else:
						print("custom")
				self.segmentCountJson[segmentName]["type"] = "custom"
		except:
			print("Exception occured while generating custom segments!")

	def constructQueryString(self, queryConfig):
		queryString = "https://canvas.trapyz.com/insights-"
		if (self.api == sapi):
			api = ""
		else:
			api = self.api	
		finalQueryString = queryString + queryConfig['dimension']['name'] + "?" + queryConfig['dimension']['name'] + "=" + queryConfig['dimension']['value'] + "&visit=" + queryConfig['visit']['value'] + "&vcon=" + queryConfig['visit']['condition'] + "&dist=" + queryConfig['distance']['value'] + "&dcon=" + queryConfig['distance']['condition'] + "&start=" + str(queryConfig['duration']['startDate']).replace("None","") + "&end=" + str(queryConfig['duration']['endDate']).replace("None","") + "&city=" + queryConfig['city'] + "&pin=" + queryConfig['pin'] + "&api=" + api
		return finalQueryString

	def updateSegmentCountJson(self):
		recentDateQuery = "SELECT max(createdDate) FROM MasterPredefinedInsights"
		try:
			connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
			cursor = connect.cursor()
			cursor.execute(recentDateQuery)
			for row in cursor.fetchall():
				recentDate = row[0]
			cursor = connect.cursor()
			updateQuery = "UPDATE MasterPredefinedInsights SET segmentCountJson = '" + json.dumps(self.segmentCountJson) + "' where api = " + self.api + " AND createdDate = '" + str(recentDate) + "'"
			cursor.execute(updateQuery)
			connect.commit()
			print("Successfully Written Data to DB")
		except:
			print("==>> Exception occured while getting most recent entry in MasterPredefinedInsights")

def getPredefinedSegmentsList():
	selectQuery = "SELECT distinct(name) FROM PredefinedSegments";
	connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
	cursor = connect.cursor()
	cursor.execute(selectQuery)
	rows = cursor.fetchall()
	for row in rows:
		predefinedSegmentsList.append(row[0])
	print(predefinedSegmentsList)

try:
	getPredefinedSegmentsList()
	selectQuery = "SELECT * FROM ApikeyMap where Apikey != ''"
	connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
	cursor = connect.cursor()
	cursor.execute(selectQuery)
	print("Successfully fetched data from API table!")
except:
	print("Exception occured while reading API table!")

for row in cursor:
	apiMap[str(row[0])] = row[1]
	predefinedInsightsObject = predefinedInsights(row[0])
	# predefinedInsightsObject.getTotalUniqueGidCount()
	# predefinedInsightsObject.getLastWeekActiveGidCount()
	# predefinedInsightsObject.getLastMonthActiveGidCount()
	# predefinedInsightsObject.getCategoryGidCount()
	# predefinedInsightsObject.writeData()
	# predefinedInsightsObject.getPredefinedSegments()
	predefinedInsightsObject.getCustomSegments()
	break
	# predefinedInsightsObject.updateSegmentCountJson()
	# time.sleep(60)

