#!/usr/bin/python3
import requests
import os
import ssl
import json
import shutil
import pymysql
import time
import concurrent.futures

dbInstance = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')

apiList = []

segmentQueriesJson = {
	"auto-enthusiast" : ["http://52.206.33.14:8000/cat?cat=8&visit=2&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/subcat?subcat=2&visit=2&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/subcat?subcat=31&visit=2&vcon=ge&dist=100&dcon=lt"],
	"fashionista" : ["http://52.206.33.14:8000/cat?cat=4&visit=1&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=27&visit=1&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=33&visit=1&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=16&visit=1&vcon=ge&dist=100&dcon=lt"],
	"sports-enthusiast" : ["http://52.206.33.14:8000/cat?cat=29&visit=8&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=21&visit=8&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/subcat?subcat=77&visit=8&vcon=ge&dist=100&dcon=lt"],
	"home-maker" : ["http://52.206.33.14:8000/cat?cat=10&visit=5&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=13&visit=5&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=22&visit=5&vcon=ge&dist=100&dcon=lt"],
	"geek" : ["http://52.206.33.14:8000/cat?cat=11&visit=2&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=26&visit=2&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=35&visit=2&vcon=ge&dist=100&dcon=lt"],
	"foodie" : ["http://52.206.33.14:8000/cat?cat=11&visit=8&vcon=ge&dist=100&dcon=lt"],
	"health-freak" : ["http://52.206.33.14:8000/cat?cat=21&visit=5&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=10&visit=5&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/subcat?subcat=75&visit=5&vcon=ge&dist=100&dcon=lt"],
	"personal-care" : ["http://52.206.33.14:8000/cat?cat=27&visit=2&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/subcat?subcat=127&visit=2&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/subcat?subcat=41&visit=2&vcon=ge&dist=100&dcon=lt"],
	"affluent" : ["http://52.206.33.14:8000/cat?cat=10&visit=5&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=13&visit=5&vcon=ge&dist=100&dcon=lt", "http://52.206.33.14:8000/cat?cat=22&visit=50&vcon=ge&dist=100&dcon=lt"]
}

def getApiList():
	cursor = dbInstance.cursor()
	query = "SELECT * FROM ApikeyMap"
	try:
		cursor.execute(query)
		for row in cursor.fetchall():
			apiList.append(row[0])
		return
	except:
		print("==>> Exception occured while populating ApiList")

def getSegmentCounts():
	recentDate = ""
	monthInSeconds = 2592000000
	currentInSeconds = int(round(time.time() * 1000))
	cursor = dbInstance.cursor()
	query = "SELECT max(createdDate) FROM MasterPredefinedInsights"
	try:
		cursor.execute(query)
		for row in cursor.fetchall():
			recentDate = row[0]
	except:
		print("==>> Exception occured while getting most recent entry in MasterPredefinedInsights")

	cursor = dbInstance.cursor()
	for api in apiList:
		segmentJson = {}
		for segment in segmentQueriesJson:
			result = 0
			for url in segmentQueriesJson[segment]:
				response = requests.get(url + "&count=1&api=" + str(api))
				# response = requests.get(url + "&count=1&api=" + str(api) + "&start=" + str(currentInSeconds - monthInSeconds) + "&end=" + str(currentInSeconds))
				if (response.text != ""):
					result += int(response.text)
			segmentJson[segment] = result
		updateQuery = "UPDATE MasterPredefinedInsights SET segmentCountJson = '" + json.dumps(segmentJson) + "' where api = " + str(api) + " AND createdDate = '" + str(recentDate) + "'"
		cursor.execute(updateQuery)		
		dbInstance.commit()
		print("Updated for API: " + str(api))

getApiList()
getSegmentCounts()
