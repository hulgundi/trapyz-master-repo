<?php
$servername = "172.31.0.193";
$username = "pwx";
$password = "ciscoplanet";
$dbname = "trapyz_beta";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = ("SELECT * FROM MasterRecordSet");
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "{";
    $count = 0;
    while ($row = $result->fetch_assoc()) {
      $res = json_encode($row);
      $sname = addcslashes($row['sname'],"\\");
      $sname = addcslashes($sname,'"');

      if ($count == 0) {
        $str = '"'.$count.'":"'.trim($row['lat']).'_'.trim($row['lng']).'_'.trim($sname).'"';
      } else {
        $str = ',"'.$count.'":"'.trim($row['lat']).'_'.trim($row['lng']).'_'.trim($sname).'"';
      }
        $count = $count + 1;
        echo $str;
    }
    echo "}";
} else {
    echo false;
}

$conn->close();
?>
