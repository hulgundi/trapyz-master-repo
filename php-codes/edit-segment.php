<?php
$servername = "172.31.0.193";
$username = "pwx";
$password = "ciscoplanet";
$dbname = "trapyz_beta";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$data = ($_POST["segment"]);
$dataJson = json_decode($data);
$queryJson = new stdClass();
$queryJson->queryList = $dataJson->queryList;
$existingSegment = $dataJson->existingSegmentName;
$predefinedSegments = array();

$file = 'log_advanced_update.log';
file_put_contents($file, "\n\n", FILE_APPEND);
$currentTime = date("d:m:Y:h:i:sa----------> ");
file_put_contents($file, $currentTime, FILE_APPEND);
file_put_contents($file, "Custom Edit", FILE_APPEND);
file_put_contents($file, "\n\n", FILE_APPEND);

$sql = "SELECT name FROM PredefinedSegments";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		array_push($predefinedSegments, $row['name']);
	}
}

$sql = "SELECT id FROM segments where apiKey = ".(int)$dataJson->api." and name = '".$existingSegment."'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$sql = "UPDATE segments SET name = '".$dataJson->segmentName."', description = '".$dataJson->segmentDesc."', attributeJson = '".json_encode($queryJson)."' WHERE id = ".(int)$row['id'];
		$conn->query($sql);

		file_put_contents($file, $sql, FILE_APPEND);
		file_put_contents($file, "\n\n", FILE_APPEND);

		$sql = "SELECT segmentCountJson, createdDate FROM MasterPredefinedInsights WHERE api = ".(int)$dataJson->api." order by createdDate desc limit 1";
		$result = $conn->query($sql);

		while ($row = $result->fetch_assoc()) {
			$segmentJson = json_decode($row["segmentCountJson"]);
			unset($segmentJson->$existingSegment);
			
			$newSegment = new stdClass();
			foreach ($predefinedSegments as $value) {
				if ($value == $dataJson->segmentName) {
					$newSegment->type = "preset";
					break;
				} else {
					$newSegment->type = "custom";
				}
			}
			$newSegment->value = $dataJson->segmentCount;
			$segmentName = $dataJson->segmentName;
			$segmentJson->$segmentName = $newSegment;
			$sql = "UPDATE MasterPredefinedInsights SET segmentCountJson = '".json_encode($segmentJson)."' WHERE api = ".(int)$dataJson->api." AND createdDate = '".$row['createdDate']."'";
			$conn->query($sql);
			echo "Success";
			file_put_contents($file, $sql, FILE_APPEND);
			file_put_contents($file, "\nSuccess\n\n", FILE_APPEND);
		}
	}
} else {
	echo "Failed";
	file_put_contents($file, "Failed \n\n", FILE_APPEND);
}

$conn->close();
?>

