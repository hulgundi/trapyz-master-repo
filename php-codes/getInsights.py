import requests
import time
import json
import pymysql
from datetime import date,timedelta

apiMap = {}
sapi = "5"
predefinedSegmentsList = []
apiList = ["9"]

class predefinedInsights():

	def __init__(self, api):
		self.getAllGidCountUrl = "http://52.206.33.14:8000/all?count=1"
		self.getGidsInEpochUrl = "http://52.206.33.14:8000/allbyepoch?start="
		self.getCategoryGidCountUrl = "http://52.206.33.14:8000/allbycat"
		self.currentInSeconds = int(round(time.time() * 1000))
		self.api = str(api)
		self.weekInSeconds = 604800000
		self.monthInSeconds = 2592000000
		self.totalUniqueGidCount = None
		self.lastWeekActiveGidCount = None
		self.lastMonthActiveGidCount = None
		self.categoryGidCountJson = None
		self.segmentCountJson = {}
		
	def getTotalUniqueGidCount(self):
		self.totalUniqueGidCount = ''
		while self.totalUniqueGidCount == '':
			try:
				if self.api != sapi:
					self.totalUniqueGidCount = requests.get(self.getAllGidCountUrl + "&api=" + self.api).text
					print(self.getAllGidCountUrl + "&api=" + self.api)
				else:
					self.totalUniqueGidCount = requests.get(self.getAllGidCountUrl).text
					print(self.getAllGidCountUrl)
				if self.totalUniqueGidCount == "":
					self.totalUniqueGidCount = "0"
			except requests.exceptions.ConnectionError:
				print("Connection Error Happened Now wait")
				time.sleep(10)
				continue		
			print(self.totalUniqueGidCount)
		return

	def getLastWeekActiveGidCount(self):
		if self.api != sapi:
			self.lastWeekActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.weekInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1" + "&api=" + self.api).text
		else:
			self.lastWeekActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.weekInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1").text						
		if self.lastWeekActiveGidCount == "":
			self.lastWeekActiveGidCount = "0"
		return

	def getLastMonthActiveGidCount(self):
		if self.api != sapi:
			self.lastMonthActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.monthInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1" + "&api=" + self.api).text
		else:
			self.lastMonthActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.monthInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1").text
		if self.lastMonthActiveGidCount == "":
			self.lastMonthActiveGidCount = "0"
		return

	def getCategoryGidCount(self):
		if self.api != sapi:
			response = requests.get(self.getCategoryGidCountUrl + "?api=" + self.api)
		else:
			response = requests.get(self.getCategoryGidCountUrl)
		if len(response.text) > 0:
			self.categoryGidCountJson = response.json()
		else:
			self.categoryGidCountJson = "{}"
		return

	def writeData(self):
		try:
			insertQuery = "INSERT INTO MasterPredefinedInsights VALUES(" + self.api + ", NOW(), " + self.totalUniqueGidCount + ", " + self.lastWeekActiveGidCount + ", " + self.lastMonthActiveGidCount + ", '" + json.dumps(self.categoryGidCountJson) + "', '')"
			connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
			cursor = connect.cursor()
			cursor.execute(insertQuery)
			connect.commit()
			connect.close()
			print("Successfully Written Data to DB: " + self.api)
		except:
			print("Exception occured while writing to DB: " + self.api)

	def getCustomSegments(self, api):
		try:
			selectQuery = "SELECT * FROM segments WHERE apiKey = " + self.api;
			# print(selectQuery)
			connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
			cursor = connect.cursor()
			cursor.execute(selectQuery)
			rows =  cursor.fetchall()
			for row in rows:
				segmentName = row[1]
				segmentDescription = row[2]
				responseJson = json.loads(row[3])
				uniqueGidList = {}
				totalCount = 0
				for query in responseJson['queryList']:
					constructedQueryUrl = self.constructQueryString(api, query ,segmentName)
					print(constructedQueryUrl)
					gidlist = requests.get(constructedQueryUrl).text
					time.sleep(20)
					gidlist = gidlist.split(',')
					gidVisitList = (i.split(':') for i in gidlist if len(i) >= 15)
					for gid in gidVisitList:
						print(gid)
						if len(gid[0]) >= 15:
							if gid[0] in uniqueGidList:
								uniqueGidList[gid[0]] += int(gid[1])
							else:
								uniqueGidList[gid[0]] = int(gid[1])
					time.sleep(5)
				visitCondition = int(responseJson['segmentVisit'])
				for gid in uniqueGidList:
					if uniqueGidList[gid] >= visitCondition:
						totalCount += 1
				self.segmentCountJson[segmentName] = {}
				self.segmentCountJson[segmentName]["value"] = totalCount
				for item in predefinedSegmentsList:
					if (segmentName == item):
						self.segmentCountJson[segmentName]["type"] = "preset"
						break
					else:
						self.segmentCountJson[segmentName]["type"] = "custom"
			connect.close()
			self.updateSegmentCountJson(api)
		except:
			raise
			print("Exception occured while generating custom segments!")

	def constructQueryString(self, api, queryConfig, segmentName):
		queryString = "https://canvas.trapyz.com/insights-"
		if (api == sapi):
			api = "9"
		if segmentName in predefinedSegmentsList:
			currentTime = self.currentInSeconds
			# delta = 2592000
			offsetTime = self.currentInSeconds - self.monthInSeconds
			dateString = "&start=" + str(offsetTime).replace("None","") + "&end=" + str(currentTime).replace("None","") 
		else:
			dateString = "&start=" + str(queryConfig['duration']['startDate']).replace("None","") + "&end=" + str(queryConfig['duration']['endDate']).replace("None","") 
		
		finalQueryString = queryString + queryConfig['dimension']['name'] + "?" + queryConfig['dimension']['name'] + "=" + queryConfig['dimension']['value'] + "&visit=" + queryConfig['visit']['value'] + "&vcon=" + queryConfig['visit']['condition'] + "&dist=" + queryConfig['distance']['value'] + "&dcon=" + queryConfig['distance']['condition'] + dateString + "&city=" + queryConfig['city'] + "&pin=" + queryConfig['pin'] + "&api=" + api
		return finalQueryString

	def updateSegmentCountJson(self, api):
		recentDateQuery = "SELECT max(createdDate) FROM MasterPredefinedInsights"
		try:
			connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
			cursor = connect.cursor()
			cursor.execute(recentDateQuery)
			for row in cursor.fetchall():
				recentDate = row[0]
			cursor = connect.cursor()
			updateQuery = "UPDATE MasterPredefinedInsights SET segmentCountJson = '" + json.dumps(self.segmentCountJson) + "' where api = " + api + " AND createdDate = '" + str(recentDate) + "'"
			# cursor.execute(updateQuery)
			print(updateQuery)
			# connect.commit()
			connect.close()
			print("Successfully updated segmentCountJson to DB: " + api)
		except:
			print("==>> Exception occured while getting most recent entry in MasterPredefinedInsights")

def getPredefinedSegmentsList():
	selectQuery = "SELECT distinct(name) FROM PredefinedSegments";
	connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
	cursor = connect.cursor()
	cursor.execute(selectQuery)
	rows = cursor.fetchall()
	for row in rows:
		predefinedSegmentsList.append(row[0])
	connect.close()
	
# try:
# 	getPredefinedSegmentsList()
# 	selectQuery = "SELECT * FROM ApikeyMap where Apikey != ''"
# 	connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
# 	cursor = connect.cursor()
# 	cursor.execute(selectQuery)
# 	connect.close()
# 	print("Successfully fetched data from API table!")
# except:
# 	print("Exception occured while reading API table!")

getPredefinedSegmentsList()

# for row in cursor:
	#apiMap[str(row[0])] = row[1]
	# predefinedInsightsObject = predefinedInsights(row[0])

for item in apiList:
	# print(time.time())
	predefinedInsightsObject = predefinedInsights(item)
	print("preDef Insights Object")
	time.sleep(60)
	predefinedInsightsObject.getTotalUniqueGidCount()
	print("preDef Total Uniq Gid")
	time.sleep(60)
	predefinedInsightsObject.getLastWeekActiveGidCount()
	predefinedInsightsObject.getLastMonthActiveGidCount()
	time.sleep(60)
	predefinedInsightsObject.getCategoryGidCount()
	predefinedInsightsObject.writeData()
	time.sleep(60)
	predefinedInsightsObject.getCustomSegments(item)
	time.sleep(60)
