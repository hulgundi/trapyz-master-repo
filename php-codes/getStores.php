<?php
$servername = "172.31.0.193";
$username = "pwx";
$password = "ciscoplanet";
$dbname = "trapyz_beta";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$params = $_GET['params'];
$result = $conn->query($params);


if ($result->num_rows > 0) {
    echo "{";
    $count = 0;
    while ($row = $result->fetch_assoc()) {
      $res = json_encode($row);
      $sname = addcslashes($row['sname'],"\\");
      $sname = addcslashes($sname,'"');

      if ($count == 0) {
        $str = '"'.$row['UUID'].'":"'.trim($sname).'_'.trim($row['city']).'_'.trim($row['cat']).'_'.trim($row['subcat']).'_'.$row['pincode'].'_'.$row['Store_ID'].'"';
      } else {
        $str = ',"'.$row['UUID'].'":"'.trim($sname).'_'.trim($row['city']).'_'.trim($row['cat']).'_'.trim($row['subcat']).'_'.$row['pincode'].'_'.$row['Store_ID'].'"';
      }

      echo $str;
      $count = $count + 1;
    }
    echo "}";
} else {
    echo false;
}

$conn->close();
?>

