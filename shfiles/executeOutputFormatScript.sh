#!/bin/bash

cd ~/

rm -rf output_data logfile

date >> logfile

python3 GeoRedisOutputFormatScript.py s3Data/dump/ >> output_data

sed -i '/Exception/d' output_data

date >> logfile
