from collections import OrderedDict

class DynamoDataTransformer(object):
	def __init__(self):
		self.commonDelimiter = "_"

	def getStoreId(self, key):
		return key.split(self.commonDelimiter)[2]
	
	def transform(self, processedUserDataRowsCurrentHour, processedUserDataRowsPrevHour):
		storeToTimeStampDict = dict()
		sortedStoreToTimeStoreDict = dict()

		if processedUserDataRowsPrevHour is not None:
			self.transformRow(processedUserDataRowsPrevHour, storeToTimeStampDict)
		if processedUserDataRowsCurrentHour is not None:
			self.transformRow(processedUserDataRowsCurrentHour, storeToTimeStampDict)

		for key in storeToTimeStampDict:
			createdatMap = storeToTimeStampDict[key]
			sortedCreateAtMap = OrderedDict(sorted(createdatMap.items(), reverse = True))
			sortedStoreToTimeStoreDict[key] = sortedCreateAtMap
		
		return sortedStoreToTimeStoreDict		

	def transformRow(self, processedUserDataRows, storeToTimeStampDict):
		for i in range(len(processedUserDataRows)):
			storeId = self.getStoreId(processedUserDataRows[i]["date_hour_min_store_api"])
			if storeId not in storeToTimeStampDict:
				storeToTimeStampDict[storeId] = dict()
			storeToTimeStampDict[storeId][processedUserDataRows[i]["info"]["created_at"]] = processedUserDataRows[i]

#obj = DynamoDataTransformer()
#rawData = [{"info": {"created_at": "1519028214593", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Books", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc12fcfc-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Toys, Books, Gifts & Music", "date_hour_min_store_api": "20180219_0816_dc12fcfc-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214592", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Car & Bike care,4-wheeler", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc2f014c-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Automotive", "date_hour_min_store_api": "20180219_0816_dc2f014c-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214597", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Car & Bike care,4-wheeler", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc2f014c-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Automotive", "date_hour_min_store_api": "20180219_0816_dc2f014c-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214599", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Salons & Spas", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc4f15ae-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Personal Care", "date_hour_min_store_api": "20180219_0816_dc4f15ae-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214599", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Salons & Spas", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc2ff1e5-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Personal Care", "date_hour_min_store_api": "20180219_0816_dc2ff1e5-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214594", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Bars & Pubs", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc2dd293-10a6-11e8-96d5-0ac08446cc34", "categoryId": "F&B", "date_hour_min_store_api": "20180219_0816_dc2dd293-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214598", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Bars & Pubs", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc2dd293-10a6-11e8-96d5-0ac08446cc34", "categoryId": "F&B", "date_hour_min_store_api": "20180219_0816_dc2dd293-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214595", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Hotels", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc6508c9-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Hospitality", "date_hour_min_store_api": "20180219_0816_dc6508c9-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214596", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Salons & Spas", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc506ad4-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Personal Care", "date_hour_min_store_api": "20180219_0816_dc506ad4-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214599", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Salons & Spas", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dc506ad4-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Personal Care", "date_hour_min_store_api": "20180219_0816_dc506ad4-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}, {"info": {"created_at": "1519028214591", "visits": 1, "updated_at": 1521282784}, "gid": "gidv4", "subcategoryId": "Furniture", "apiKey_date_hour_min": "314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816", "storeUuid": "dbf61bcd-10a6-11e8-96d5-0ac08446cc34", "categoryId": "Home & Lifestyle", "date_hour_min_store_api": "20180219_0816_dbf61bcd-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72"}]
#print (rawData) 
#print (obj.transform(rawData, None))
