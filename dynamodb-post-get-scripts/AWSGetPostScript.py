from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr


dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('ProcessedUserDataHourWiseData')

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

class AwsProcessedData(object):
    def postLog(self, json_log):
        decoded_json = json.loads(json_log)

        res = table.put_item(
            Item = {
                "apiKey_date_hour": decoded_json["apiKey"] + "_" + decoded_json["date"] + "_" + decoded_json["hour"],
                "date_hour_apiKey": decoded_json["date"] + "_" + decoded_json["hour"] + "_" + decoded_json["apiKey"],
                "gid": decoded_json["gid"],
                "info": {
                     "created_at": decoded_json["created_at"],
                     "updated_at": decoded_json["updated_at"],
                     "visits": decoded_json["visits"]
                },
                "storeUuid": decoded_json["uuid"]
            }
        )

        if res['ResponseMetadata']['HTTPStatusCode'] != 200:
            print("failed to insert into dynanodb")
    
    def getLog(self, input_json):
        decoded_json = json.loads(input_json)
   
        print("trying to get item from table")
        response = table.get_item(
            Key = {
                "date_hour_apiKey": decoded_json["date"] + "_" + decoded_json["hour"] + "_" + decoded_json["apiKey"],
                "gid": decoded_json["gid"]
            })
        print(json.dumps(response['Item'], indent=4, cls=DecimalEncoder))
    

    def queryTable(self, gid):
        print("trying to get via query")
        response = table.query(
            KeyConditionExpression=Key('gid').eq(gid)
        )
        print(json.dumps(response['Items'], indent=4, cls=DecimalEncoder))

