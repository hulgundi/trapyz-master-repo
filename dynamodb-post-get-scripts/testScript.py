from AWSGetPostScript import AwsProcessedData

aws = AwsProcessedData()
input_json = '{"apiKey": "sample-api5", "date": "sample-date5", "hour": "sample-hour5", "gid": "sample-gid", "created_at": "sample-created", "updated_at": "sample-updated", "visits": "sample-visits", "uuid": "sample-uuid"}'
aws.postLog(input_json)

input_json = '{"date":"sample-date", "hour":"sample-hour", "apiKey":"sample-api", "gid":"sample-gid"}'
aws.getLog(input_json)

gid = 'sample-gid'
aws.queryTable(gid)
