import os
from concurrent.futures import ThreadPoolExecutor, wait
from itertools import islice

class InplaceDirectoryFileSplitterNew(object):
    def split(self, inputDir, lines_per_file=100, threads=1):
        onlyfiles = [f for f in os.listdir(inputDir) if os.path.isfile(os.path.join(inputDir, f))]
        lpf = lines_per_file        
        executor = ThreadPoolExecutor(max_workers = threads)
        jobs=[]
        
        for currentFile in onlyfiles:
            filepath = inputDir + "/" + currentFile
            a = executor.submit(self.callSplitter, filepath, lpf)
            jobs.append(a)

        wait(jobs)

        for currentFile in onlyfiles:
            os.remove(inputDir + "/" + currentFile)

    def callSplitter(self, filepath, lpf):
        path, filename = os.path.split(filepath)

        with open(filepath, 'r') as r:
            name, ext = os.path.splitext(filename)
            try:
                fileCount = 0
                w = open(os.path.join(path, '{}_{}{}'.format(name, fileCount, ext)), 'w')

                for i, line in enumerate(r):
                    if not i % lpf:
                        w.close()
                        filename = os.path.join(path, '{}_{}{}'.format(name, fileCount, ext))
                        fileCount = fileCount + 1
                        w = open(filename, 'w')
                    w.write(line)
            finally:
                w.close()

#obj = InplaceDirectoryFileSplitter()
#obj.split('/home/amit/dev/varunplay/Untitled', 20000)

