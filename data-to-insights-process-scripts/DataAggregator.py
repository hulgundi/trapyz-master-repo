from collections import OrderedDict
import time
import datetime
import json
from math import fabs

class DataAggregator(object):
	def __init__(self, logger, locationServiceClient):
		self.commonDelimiter = "_"
		self.ignoredKey = "threadDet"
		self.logger = logger
		self.locationServiceClient = locationServiceClient

	# Keeping all the business logic in this function for specifying where these instances are different or not.
	def isCurrrentPrevSame(self, distance):
		self.logger.info("distance: %s, %s", distance, distance <= 50)
		return distance <= 50

	def canBeMergedForTime(self, timeDifference):
		self.logger.info("timeDifference: %s, %s", timeDifference, (fabs(timeDifference) <= 1800))
		return fabs(timeDifference) <= 1800

	def getAggregatedData(self, storeToTimeStoreDictDynamo, sortedStoreToTimeStoreDictRedis):
		keysModified = dict()
		newKeysToBeAdded = dict()
		redisKeysAlreadyAdded = dict()

		for storeId in sortedStoreToTimeStoreDictRedis:
			for storeIdDynamo in storeToTimeStoreDictDynamo:
				alreadyStoredTimeStamp = next(iter(storeToTimeStoreDictDynamo[storeIdDynamo]))
				self.logger.debug("storeId: %s, storeId Dynamo: %s, alreadyStoredTimeStamp: %s", storeId, storeIdDynamo, alreadyStoredTimeStamp)
				distance = 0 if storeIdDynamo == storeId else self.locationServiceClient.getDistanceBetweenTwoStores(storeIdDynamo, storeId)
				if (self.isCurrrentPrevSame(distance)):
					for timeStamp in sortedStoreToTimeStoreDictRedis[storeId]:
						if (self.canBeMergedForTime(int(alreadyStoredTimeStamp) - int(timeStamp))):
							if storeIdDynamo not in keysModified:
								keysModified[storeIdDynamo] = dict()
							if alreadyStoredTimeStamp not in keysModified[storeIdDynamo]:
								keysModified[storeIdDynamo][alreadyStoredTimeStamp] = 0
							keysModified[storeIdDynamo][alreadyStoredTimeStamp] = keysModified[storeIdDynamo][alreadyStoredTimeStamp] + 1							
							if distance == 0:
								redisKeysAlreadyAdded[storeId] = 1
							else:
								if storeId not in newKeysToBeAdded:
									newKeysToBeAdded[storeId] = dict()
								if timeStamp not in newKeysToBeAdded[storeId]:
									newKeysToBeAdded[storeId][timeStamp] = 0
								newKeysToBeAdded[storeId][timeStamp] = newKeysToBeAdded[storeId][timeStamp] + int(storeToTimeStoreDictDynamo[storeIdDynamo][alreadyStoredTimeStamp]["info"]["visits"])
		aggregatedModifiedData = []
		aggregatedAddedData = []

		self.logger.debug("keysModified: %s, newKeysToBeAdded: %s", keysModified, newKeysToBeAdded)
		for storeId in keysModified:
			for timeStamp in keysModified[storeId]:
				storeToTimeStoreDictDynamo[storeId][timeStamp]["info"]["visits"] = storeToTimeStoreDictDynamo[storeId][timeStamp]["info"]["visits"] + keysModified[storeId][timeStamp]
				storeToTimeStoreDictDynamo[storeId][timeStamp]["info"]["updated_at"] = int(time.time())
				aggregatedModifiedData.append(storeToTimeStoreDictDynamo[storeId][timeStamp])

		self.logger.debug ("dynamo data needs to be rewritten: %s", aggregatedModifiedData)	

		for storeId in sortedStoreToTimeStoreDictRedis:
			if storeId in redisKeysAlreadyAdded:
				continue
			prevData = None
			lastTimeStamp = None
			for timeStamp in sortedStoreToTimeStoreDictRedis[storeId]:
				lastTimeStamp = timeStamp
				if prevData == None:
					prevData = sortedStoreToTimeStoreDictRedis[storeId][timeStamp]
					continue
				if self.canBeMergedForTime(int(prevData["info"]["created_at"]) - int(sortedStoreToTimeStoreDictRedis[storeId][timeStamp]["info"]["created_at"])):
					prevData["info"]["visits"] = prevData["info"]["visits"] + 1
				else:
					if storeId in newKeysToBeAdded and timeStamp in newKeysToBeAdded[storeId]:
						prevData["info"]["visits"] = prevData["info"]["visits"] + newKeysToBeAdded[storeId][timeStamp]
					prevData["info"]["updated_at"] = int(time.time())
					aggregatedAddedData.append(prevData)
					prevData = sortedStoreToTimeStoreDictRedis[storeId][timeStamp]
			if storeId in newKeysToBeAdded and lastTimeStamp in newKeysToBeAdded[storeId]:
				prevData["info"]["visits"] = prevData["info"]["visits"] + newKeysToBeAdded[storeId][lastTimeStamp]
			prevData["info"]["updated_at"] = int(time.time())
			aggregatedAddedData.append(prevData)

		self.logger.debug ("final aggregated data: %s", aggregatedAddedData)	
		return aggregatedModifiedData, aggregatedAddedData

#import logging
#from LocationServiceConnectionPoolHelper import LocationServiceConnectionPoolHelper
#logger = logging.getLogger("derive_stdin_stdout_log")
#logger.setLevel(logging.DEBUG)
## create file handler which logs even debug messages
#fh = logging.FileHandler("derive_stdin_stdout_log.log")
#fh.setLevel(logging.DEBUG)
## create formatter and add it to the handlers
#formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
#fh.setFormatter(formatter)
## add the handlers to the logger
#logger.addHandler(fh)
#locationServiceClient = LocationServiceConnectionPoolHelper()
#obj = DataAggregator(logger, locationServiceClient)

#dynamo = {"dbf61bcd-10a6-11e8-96d5-0ac08446cc34":{"1519028214591":{"info":{"created_at":"1519028214591","updated_at":1521282784,"visits":1},"subcategoryId":"Furniture","date_hour_min_store_api":"20180219_0816_dbf61bcd-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dbf61bcd-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Home & Lifestyle"}},"dc506ad4-10a6-11e8-96d5-0ac08446cc34":{"1519028214599":{"info":{"created_at":"1519028214599","updated_at":1521282784,"visits":1},"subcategoryId":"Salons & Spas","date_hour_min_store_api":"20180219_0816_dc506ad4-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc506ad4-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"PersonalCare"},"1519028214596":{"info":{"created_at":"1519028214596","updated_at":1521282784,"visits":1},"subcategoryId":"Salons & Spas","date_hour_min_store_api":"20180219_0816_dc506ad4-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc506ad4-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Personal Care"}},"dc2f014c-10a6-11e8-96d5-0ac08446cc34":{"1519028214597":{"info":{"created_at":"1519028214597","updated_at":1521282784,"visits":1},"subcategoryId":"Car & Bike care,4-wheeler","date_hour_min_store_api":"20180219_0816_dc2f014c-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2f014c-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Automotive"},"1519028214592":{"info":{"created_at":"1519028214592","updated_at":1521282784,"visits":1},"subcategoryId":"Car & Bike care,4-wheeler","date_hour_min_store_api":"20180219_0816_dc2f014c-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2f014c-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Automotive"}},"dc4f15ae-10a6-11e8-96d5-0ac08446cc34":{"1519028214599":{"info":{"created_at":"1519028214599","updated_at":1521282784,"visits":1},"subcategoryId":"Salons & Spas","date_hour_min_store_api":"20180219_0816_dc4f15ae-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc4f15ae-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Personal Care"}},"dc12fcfc-10a6-11e8-96d5-0ac08446cc34":{"1519028214593":{"info":{"created_at":"1519028214593","updated_at":1521282784,"visits":1},"subcategoryId":"Books","date_hour_min_store_api":"20180219_0816_dc12fcfc-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc12fcfc-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Toys, Books, Gifts & Music"}},"dc6508c9-10a6-11e8-96d5-0ac08446cc34":{"1519028214595":{"info":{"created_at":"1519028214595","updated_at":1521282784,"visits":1},"subcategoryId":"Hotels","date_hour_min_store_api":"20180219_0816_dc6508c9-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc6508c9-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Hospitality"}},"dc2dd293-10a6-11e8-96d5-0ac08446cc34":{"1519028214598":{"info":{"created_at":"1519028214598","updated_at":1521282784,"visits":1},"subcategoryId":"Bars & Pubs","date_hour_min_store_api":"20180219_0816_dc2dd293-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2dd293-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"F&B"},"1519028214594":{"info":{"created_at":"1519028214594","updated_at":1521282784,"visits":1},"subcategoryId":"Bars & Pubs","date_hour_min_store_api":"20180219_0816_dc2dd293-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2dd293-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"F&B"}},"dc2ff1e5-10a6-11e8-96d5-0ac08446cc34":{"1519028214599":{"info":{"created_at":"1519028214599","updated_at":1521282784,"visits":1},"subcategoryId":"Salons & Spas","date_hour_min_store_api":"20180219_0816_dc2ff1e5-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2ff1e5-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Personal Care"}}}

#redis = {"dbf61bcd-10a6-11e8-96d5-0ac08446cc34":{"1519028214591":{"info":{"created_at":"1519028214591","updated_at":1521282784,"visits":1},"subcategoryId":"Furniture","date_hour_min_store_api":"20180219_0816_dbf61bcd-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dbf61bcd-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Home & Lifestyle"}},"dc506ad4-10a6-11e8-96d5-0ac08446cc34":{"1519028214599":{"info":{"created_at":"1519028214599","updated_at":1521282784,"visits":1},"subcategoryId":"Salons & Spas","date_hour_min_store_api":"20180219_0816_dc506ad4-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc506ad4-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"PersonalCare"},"1519028214596":{"info":{"created_at":"1519028214596","updated_at":1521282784,"visits":1},"subcategoryId":"Salons & Spas","date_hour_min_store_api":"20180219_0816_dc506ad4-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc506ad4-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Personal Care"}},"dc2f014c-10a6-11e8-96d5-0ac08446cc34":{"1519028214597":{"info":{"created_at":"1519028214597","updated_at":1521282784,"visits":1},"subcategoryId":"Car & Bike care,4-wheeler","date_hour_min_store_api":"20180219_0816_dc2f014c-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2f014c-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Automotive"},"1519028214592":{"info":{"created_at":"1519028214592","updated_at":1521282784,"visits":1},"subcategoryId":"Car & Bike care,4-wheeler","date_hour_min_store_api":"20180219_0816_dc2f014c-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2f014c-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Automotive"}},"dc4f15ae-10a6-11e8-96d5-0ac08446cc34":{"1519028214599":{"info":{"created_at":"1519028214599","updated_at":1521282784,"visits":1},"subcategoryId":"Salons & Spas","date_hour_min_store_api":"20180219_0816_dc4f15ae-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc4f15ae-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Personal Care"}},"dc12fcfc-10a6-11e8-96d5-0ac08446cc34":{"1519028214593":{"info":{"created_at":"1519028214593","updated_at":1521282784,"visits":1},"subcategoryId":"Books","date_hour_min_store_api":"20180219_0816_dc12fcfc-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc12fcfc-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Toys, Books, Gifts & Music"}},"dc6508c9-10a6-11e8-96d5-0ac08446cc34":{"1519028214595":{"info":{"created_at":"1519028214595","updated_at":1521282784,"visits":1},"subcategoryId":"Hotels","date_hour_min_store_api":"20180219_0816_dc6508c9-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc6508c9-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Hospitality"}},"dc2dd293-10a6-11e8-96d5-0ac08446cc34":{"1519028214598":{"info":{"created_at":"1519028214598","updated_at":1521282784,"visits":1},"subcategoryId":"Bars & Pubs","date_hour_min_store_api":"20180219_0816_dc2dd293-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2dd293-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"F&B"},"1519028214594":{"info":{"created_at":"1519028214594","updated_at":1521282784,"visits":1},"subcategoryId":"Bars & Pubs","date_hour_min_store_api":"20180219_0816_dc2dd293-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2dd293-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"F&B"}},"dc2ff1e5-10a6-11e8-96d5-0ac08446cc34":{"1519028214599":{"info":{"created_at":"1519028214599","updated_at":1521282784,"visits":1},"subcategoryId":"Salons & Spas","date_hour_min_store_api":"20180219_0816_dc2ff1e5-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72","storeUuid":"dc2ff1e5-10a6-11e8-96d5-0ac08446cc34","gid":"gidv4","apiKey_date_hour_min":"314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816","categoryId":"Personal Care"}}}

#print (obj.getAggregatedData(dynamo, redis))
