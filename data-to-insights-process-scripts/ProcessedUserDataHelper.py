import boto3
from boto3.dynamodb.conditions import Key
from datetime import datetime, timedelta
import string
import logging
import time

class ProcessedUserDataHelper(object):
	def __init__(self, tableName = "GeoDataProcessedInsights"):
		self.dynamodb = boto3.resource("dynamodb")
		self.logger = logging.getLogger('derive_stdin_stdout_log_ddb')
		self.logger.setLevel(logging.DEBUG)
		# create file handler which logs even debug messages
		fh = logging.FileHandler('derive_stdin_stdout_log_ddb.log')
		fh.setLevel(logging.DEBUG)
		# create formatter and add it to the handlers
		formatter = logging.Formatter('%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s')
		fh.setFormatter(formatter)
		# add the handlers to the logger
		self.logger.addHandler(fh)
		self.logger.debug("Instantiated ProcessedUserDataHelper")
		self.tableName = tableName

	def getLastTwoHoursStoredData(self, gid, first, last):
		currentHourFormatted = time.strftime("%Y%m%d_%H", time.localtime(int(first)/1000))
		self.logger.debug("gid: %s time: %s", gid, currentHourFormatted)
		processedUserDataRowsCurrentHourResponse = self.genericQueryWrapper(self.tableName, gid, "gid", "date_hour_min_store_api", currentHourFormatted, False, 100)
		self.logger.debug("response: %s", processedUserDataRowsCurrentHourResponse)
		processedUserDataRowsCurrentHour = processedUserDataRowsCurrentHourResponse["Items"] if processedUserDataRowsCurrentHourResponse["Count"] >= 1 else None
		
		prevHourFormatted = time.strftime("%Y%m%d_%H", time.localtime(int(first)/1000 - 3600))
		if prevHourFormatted == currentHourFormatted:
			prevHourFormatted = time.strftime("%Y%m%d_%H", time.localtime(int(first)/1000 - 7200))
		processedUserDataRowsPrevHourResponse = self.genericQueryWrapper(self.tableName, gid, "gid", "date_hour_min_store_api", prevHourFormatted, False, 100)
		self.logger.debug("response: %s", processedUserDataRowsPrevHourResponse)
		processedUserDataRowsPrevHour = processedUserDataRowsPrevHourResponse["Items"] if processedUserDataRowsPrevHourResponse["Count"] >= 1 else None
		
		return processedUserDataRowsCurrentHour, processedUserDataRowsPrevHour
		
	def getLatestStoredData(self, gid):
		currentHourDateTime = datetime.now()
		currentHourFormatted = currentHourDateTime.strftime("%Y%m%d_%H")
		self.logger.debug("gid: %s time: %s", gid, currentHourFormatted)	
		response = self.genericQueryWrapper(self.tableName, gid, "gid", "date_hour_min_store_api", currentHourFormatted, False, 1)
		self.logger.debug("response: %s", response)
		if response["Count"] == 1:
			return response["Items"][0]
		lastHourDateTime = datetime.now() - timedelta(hours = 1)
		prevHourFormatted = lastHourDateTime.strftime("%Y%m%d_%H")
		if prevHourFormatted == currentHourFormatted:
			last_hour_date_time = datetime.now() - timedelta(hours = 2)
			prevHourFormatted = lastHourDateTime.strftime("%Y%m%d_%H")
		response = self.genericQueryWrapper(self.tableName, gid, "gid", "date_hour_min_store_api", prevHourFormatted, False, 1)
		if response["Count"] == 1:
			return response["Items"][0]
		return None
	
	def genericQueryWrapper(self, tableName, primaryKey, primaryKeyName, sortKeyName, sortKeyPrefix, scanIndexForward, limit):
		filtering_exp = Key(primaryKeyName).eq(primaryKey) & Key(sortKeyName).begins_with(sortKeyPrefix)
		table = self.dynamodb.Table(tableName)
		response = table.query(KeyConditionExpression = filtering_exp, ScanIndexForward = scanIndexForward, Limit = limit)
		return response

	def addDummyData(self):
		itemDict = {"gid" : "d44376ae-8fe8-4c1d-aa58-20acb38fcda9" , "apiKey_date_hour" : "314ac5b3c9e5c929bbc9170e13ea2b73_DATEHOUR", "date_hour_min_store_api" : "DATEHOUR_314ac5b3c9e5c929bbc9170e13ea2b73", "info" : {"created_at" : 1518899825, "updated_at" : 1518899825, "visits" : 4}, "storeUuid" : "0a3657c0-1274-11e8-82ed-255dcba280ac"}
		dateHourCom = [(datetime.now() + timedelta(hours = 10)).strftime("%Y%m%d_%H"), (datetime.now() - timedelta(hours = 10)).strftime("%Y%m%d_%H"), (datetime.now() - timedelta(hours = 6)).strftime("%Y%m%d_%H")]
		for i in dateHourCom:
			itemDictNew = dict()
			for key in itemDict.keys():
				if isinstance(itemDict[key], str):
					actualDataClone = itemDict[key]
					itemDictNew[key] = actualDataClone.replace("DATEHOUR", i)
				else:
					itemDictNew[key] = itemDict[key]
				
			response = self.storeData(itemDictNew)

	def storeData(self, data):
		table = self.dynamodb.Table(self.tableName)
		self.logger.debug("data: %s", data)
		response = table.put_item(Item = data)
		self.logger.debug("response: %s", response)
#obj = ProcessedUserDataHelper()
#print (obj.getLatestStoredData("d44376ae-8fe8-4c1d-aa58-20acb3"))
#data = {'subcategoryId': 'Salons & Spas', 'info': {'distance': '560.5921', 'created_at': '1519028214596', 'visits': 2, 'updated_at': 1521304947}, 'gid': 'gidv4', 'date_hour_min_store_api': '20180219_0816_dc506ad4-10a6-11e8-96d5-0ac08446cc34_314ac5b3c9e5c929bbc9170e13ea2b72', 'storeUuid': 'dc506ad4-10a6-11e8-96d5-0ac08446cc34', 'categoryId': 'Personal Care', 'apiKey_date_hour_min': '314ac5b3c9e5c929bbc9170e13ea2b72_20180219_0816'}

#print (obj.storeData(data))

#obj.addDummyData()
