import redis

class RedisWrapper(object):
	def __init__(self, redisPort = 6379):
		self.pool = redis.ConnectionPool(host = "localhost", port = redisPort, db = 0)
		self.expiryTimeInSecs = 10800

	def keyExists(self, gId, threadKey):
		currentInstance = redis.Redis(connection_pool = self.pool)
		if currentInstance.hget(gId, threadKey) is None:
			return False
		return True

	def storeKeyDataMap(self, gId, mapData):
		currentInstance = redis.Redis(connection_pool = self.pool)
		currentInstance.hmset(gId, mapData)
		currentInstance.expire(name = gId, time = self.expiryTimeInSecs)

	def storeKey(self, gId, field, value):
		self.storeKeyDataMap(gId, {field : value})

	def storeKeyWithThreadData(self, gId, field, value, threadKey, threadValue):
		self.storeKeyDataMap(gId, {field : value, threadKey : threadValue})

	def isWriteToFileEligible(self, gId, threadKey, threadValue):
		currentInstance = redis.Redis(connection_pool = self.pool)
		storedData = currentInstance.hget(gId, threadKey)
		if storedData is None:
			raise ValueError('threadKey is none')
		return storedData.decode("utf-8") == threadValue

	def getMapData(self, gId):
		currentInstance = redis.Redis(connection_pool = self.pool)
		storedMap = currentInstance.hgetall(gId)
		transformedMap = dict()
		for key in storedMap:
			transformedMap[key.decode("utf-8")] = storedMap[key].decode("utf-8")
		return transformedMap
