from urllib3 import HTTPConnectionPool

# This encapsulates all the apis and connection details.
class LocationServiceConnectionPoolHelper(object):
	def __init__(self):
		host = "172.31.28.193"
		port = 3000
		maxPoolSize = 2
		timeout = 5
		self.pool = HTTPConnectionPool(host = host, port = port, maxsize = maxPoolSize, timeout = timeout)

	def getDistanceBetweenTwoStores(self, store1, store2):
		return float(self.callGenericGet("getDistance", {"store1" : store1, "store2" : store2}))

	def callGenericGet(self, urlPath, queryParamsDict):
		response = self.pool.request ('GET' , "/" + urlPath, queryParamsDict)
		#return "245.12"
		if response == None or int(response.status) != 200 or len(response.data) == 0:
			raise ValueError(urlPath + " api didn't respond back")
		else:
			return response.data
# test executor
#obj = LocationServiceConnectionPoolHelper()
#print (obj.getDistanceBetweenTwoStores("dc14ecce-10a6-11e8-96d5-0ac08446cc34", "dc009b19-10a6-11e8-96d5-0ac08446cc34"))
