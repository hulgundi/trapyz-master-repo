from RedisWrapper import RedisWrapper
from LocationServiceConnectionPoolHelper import LocationServiceConnectionPoolHelper
from ProcessedUserDataHelper import ProcessedUserDataHelper
from RedisDataTransformer import RedisDataTransformer
from DataAggregator import DataAggregator
from DynamoDataTransformer import DynamoDataTransformer
from ElasticSearchWrapper import ElasticSearchWrapper
import logging
import subprocess
import threading
import time
import datetime
import sys
import os
import json
from concurrent.futures import ThreadPoolExecutor, wait
from urllib3 import HTTPConnectionPool
from collections import OrderedDict

logger = logging.getLogger("derive_stdin_stdout_log")
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler("derive_stdin_stdout_log.log")
fh.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
fh.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)

uniqGidLoggers = dict()

threadKey = "threadDet"
#currentHour = str(datetime.datetime.now().hour)
currentHour = datetime.datetime.now().strftime("%H%M%S")

locationServiceClient = LocationServiceConnectionPoolHelper()
processedUserDataClient = ProcessedUserDataHelper()
dataAggreator = DataAggregator(logger, locationServiceClient)
redisDataTransformer = RedisDataTransformer(logger)
dynamoDataTransformer = DynamoDataTransformer()
elasticSearchClient = ElasticSearchWrapper(indexName = "geodataprocessedinsights")

def getMachineName():
	cmd = ["ec2metadata" , "--instance-id"]
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE,stderr=subprocess.PIPE,stdin=subprocess.PIPE)
	out, err = p.communicate()
	return out.strip()

machineName = str(getMachineName())

mRedis = RedisWrapper()

# This method reads the raw file which contains, the long lat location, gid data and all other information including store uuid..
def readRawFile(filename):
	payload = {}
	fo = open(filename, "r")
	count = 0
	for line in fo:
		line = line.replace("\n","")
		count = count + 1
		logger.debug("LineCount: %d ExactData: %s", count, line)
		# add proper validations on the line, before dumping it to the redis. Validations on gid, uuid, lat, long, distance...
		handleRawLogRow(line)
	fo.close()

# this method will give the redis key, which is nothing but gid concatenated with current hour. 
def getRedisKey(gId):
	return gId + "_" + currentHour

# this method will give the redis hash map key, we have kept the store uuid here, because we want multiple distances.
def getRedisHashMapKey(createdAtTimestamp, storeUuid):
	return createdAtTimestamp + "_" + storeUuid

# this method helps to create unique loggers for unique threads, this is done so, that we don't have to segregate the gids later on to keep multiple threads executing at the same time.
def createLoggerIfNotExists():
	if threading.get_ident() not in uniqGidLoggers:
		uniqGidLogger = logging.getLogger("redis_gid_log"+str(threading.get_ident()))
		uniqGidLogger.setLevel(logging.INFO)
		# create file handler which logs even debug messages
		fhUniqGidLogger = logging.FileHandler("redisgidData/redis_gid_log" + str(threading.get_ident()) + ".log")
		fhUniqGidLogger.setLevel(logging.INFO)
		# create formatter and add it to the handlers
		formatterUniqGid = logging.Formatter("%(threadName)s %(message)s")
		fhUniqGidLogger.setFormatter(formatterUniqGid)
		# add the handlers to the logger
		uniqGidLogger.addHandler(fhUniqGidLogger)
		uniqGidLoggers[threading.get_ident()] = uniqGidLogger

# this method's main job is to group all the events for a gid. and get the unique gids.
# We are using redis to segregate the gid. We store all the events in the redis with the key, which is 
# returned from the method getRedisKey. And we store all rediskeys in files in the directory redisgidData.
def handleRawLogRow(line):
	try:
		lineData = json.loads(line)
		threadValue = machineName + str(threading.get_ident())
		redisKey = getRedisKey(lineData["gid"])
		redisHashMapKey = getRedisHashMapKey(lineData["createdat"], lineData["uuid"])
		logger.info("redisKey: %s", redisKey)
		if (mRedis.keyExists(redisKey, threadKey)):
			logger.info("key Exists already : %s", redisKey)
			mRedis.storeKey(redisKey, redisHashMapKey, line.strip())
		else:
			mRedis.storeKeyWithThreadData(redisKey, redisHashMapKey, line.strip(), threadKey, threadValue)
			if (mRedis.isWriteToFileEligible(redisKey, threadKey, threadValue)):
				createLoggerIfNotExists()
				uniqGidLoggers[threading.get_ident()].info(redisKey)
			else:
				logger.info("As per thread %s, redisKey %s is not eligible to be consumed", threadValue, redisKey)
	except er:
		print(er)

# Keeping all the business logic in this function for specifying where these instances are different or not.
def isCurrrentPrevSame(distance):
	logger.info("distance: %s, %s", distance, distance <= 50)
	return distance <= 50

# transform raw log to dynamo db row.
# this method looks deprecated now.
def transform(orderedDataMapRow):
	logger.debug ("transform method in GenerateDerivedAttributes called...")
	createdAtHourFormatted = time.strftime("%Y%m%d_%H", time.localtime(int(orderedDataMapRow["createdat"])/1000))
	date = createdAtHourFormatted.split("_")
	processedUserDataHourWiseData = {"gid" : orderedDataMapRow["gid"] , "apiKey_date_hour" : orderedDataMapRow["apikey"] + "_" + createdAtHourFormatted, "date_hour_apiKey" : createdAtHourFormatted + "_" + orderedDataMapRow["apikey"], "info" : {"created_at" : orderedDataMapRow["createdat"], "updated_at" : int(time.time()), "visits" : 1, "city" : int(orderedDataMapRow["city"]), "pin" : int(orderedDataMapRow["pin"])}, "storeUuid" : orderedDataMapRow["uuid"], "categoryId" : orderedDataMapRow["cat"], "subcategoryId" : orderedDataMapRow["subcat"], "apiKey" : orderedDataMapRow["apikey"], "apiKey_ident_date" : orderedDataMapRow["apikey"] + "_" + date[0]}
	return processedUserDataHourWiseData


def getFirstLastTimeStamp(sortedStoreToTimeStoreDictRedis):
	minimumTime = sys.maxsize
	maximumTime = 0
	for storeId in sortedStoreToTimeStoreDictRedis:
		for timestamp in sortedStoreToTimeStoreDictRedis[storeId]:
			minimumTime = min(minimumTime, int(timestamp))
			maximumTime = max(maximumTime, int(timestamp))

	return minimumTime, maximumTime

# this processGid method, has following algorithm:
# it first reads all the data from redis for that key. using redisKey method.
# data is nothing but a map of epoch timestamp to actual raw data which contains store uuid as well.
# this is unordered data which also, has one key of thread detail which was kept to make sure, we are able to 
# find the unique gids, in multithreaded env.
def processGid(line):
	lineData = line.split(" ")
	redisKey = lineData[1]
	logger.info("Started processing %s", redisKey)
	storedMapData = mRedis.getMapData(redisKey)
	logger.debug("Data from Redis with changes %s", storedMapData)
	sortedStoreToTimeStoreDictRedis = redisDataTransformer.transform(storedMapData)	
	logger.debug("Data From redis Transformer %s", sortedStoreToTimeStoreDictRedis)
	first, last = getFirstLastTimeStamp(sortedStoreToTimeStoreDictRedis)
	logger.debug("first: %s , last: %s times", first, last)
	processedUserDataRowsCurrentHour, processedUserDataRowsPrevHour = processedUserDataClient.getLastTwoHoursStoredData(redisKey.split("_")[0], first, last)
	logger.debug("dynamo data processedUserDataRowsCurrentHour :%s, processedUserDataRowsPrevHour: %s", processedUserDataRowsCurrentHour, processedUserDataRowsPrevHour)
	storeToTimeStoreDictDynamo = dynamoDataTransformer.transform(processedUserDataRowsCurrentHour, processedUserDataRowsPrevHour)
	logger.debug ("storeToTimeStoreDictDynamo: %s", storeToTimeStoreDictDynamo)
	aggregatedModifiedData, aggregatedAddedData = dataAggreator.getAggregatedData(storeToTimeStoreDictDynamo, sortedStoreToTimeStoreDictRedis)
	logger.debug("aggregatedModifiedData: %s aggregatedAddedData: %s", aggregatedModifiedData, aggregatedAddedData)
	for finalRow in aggregatedModifiedData:
		processedUserDataClient.storeData(finalRow)
		elasticSearchClient.storeDataInElasticSearch(finalRow)

	for finalRow in aggregatedAddedData:
		processedUserDataClient.storeData(finalRow)
		elasticSearchClient.storeDataInElasticSearch(finalRow)

# this method will read each rediskey and send it to processGid method.
def processFile(fileName):
	fo = open(fileName, "r")
	count = 0
	for line in fo:
		line = line.replace("\n", "")
		count = count + 1
		logger.debug("LineCount: %d ExactGidData: %s", count, line)
		processGid(line)
	fo.close()

# root dir for all the raw files, so, we process the files which have the store uuid.
rootdir = sys.argv[1]
# you can change the no. of threads here.
executor = ThreadPoolExecutor(max_workers=4)
jobs=[]
for ro, subFolders, files in os.walk(rootdir):
	for i in range(0, len(files)):
		a = executor.submit(readRawFile, rootdir+files[i])
		jobs.append(a)

# wait for all these jobs to finish.
wait(jobs)
executorT = ThreadPoolExecutor(max_workers=4)

# here we are reading all the different files from redisgidData which we just formed to keep all the rediskeys that needs to be processed.
for ro, subFolders, files in os.walk("redisgidData"):
	for i in range(0, len(files)):
                b = executorT.submit(processFile, "redisgidData/" + files[i])
