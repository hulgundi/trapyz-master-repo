from collections import OrderedDict
import time
import datetime
import json


class RedisDataTransformer(object):
	def __init__(self, logger):
		self.commonDelimiter = "_"
		self.ignoredKey = "threadDet"
		self.logger = logger

	def getTimestampStoreIdPair(self, key):
		return key.split(self.commonDelimiter)
	
	def transformRawData(self, orderedDataMapRow):
		createdAtHourFormatted = time.strftime("%Y%m%d" + self.commonDelimiter + "%H%M", time.localtime(int(orderedDataMapRow["createdat"])/1000))
		date = createdAtHourFormatted.split(self.commonDelimiter)
		processedUserDataHourWiseData = {"gid" : orderedDataMapRow["gid"] , "apiKey_date_hour_min" : orderedDataMapRow["apikey"] + self.commonDelimiter + createdAtHourFormatted, "date_hour_min_store_api" : createdAtHourFormatted + self.commonDelimiter + orderedDataMapRow["uuid"] + self.commonDelimiter + orderedDataMapRow["apikey"], "info" : {"created_at" : int(orderedDataMapRow["createdat"]), "updated_at" : int(time.time()), "visits" : 1, "distance" : int(orderedDataMapRow["distance"]), "pin" : str(orderedDataMapRow["pin"]), "city" : str(orderedDataMapRow["city"]) }, "storeUuid" : orderedDataMapRow["uuid"], "categoryId" : orderedDataMapRow["cat"], "subcategoryId" : orderedDataMapRow["subcat"], "apiKey_ident_date" : orderedDataMapRow["apikey"] + self.commonDelimiter + date[0] }
		return processedUserDataHourWiseData

	def transform(self, storedMapData):
		sortedStoreToTimeStoreDict = dict()
		storeToTimeSortedDict = dict()
		for key in storedMapData:
			if key == self.ignoredKey:
				continue
			createdAt, storeId = self.getTimestampStoreIdPair(key)
			if storeId not in storeToTimeSortedDict:
				storeToTimeSortedDict[storeId] = dict()
			storeToTimeSortedDict[storeId][createdAt] = self.transformRawData(json.loads(storedMapData[key]))

		for key in storeToTimeSortedDict:
			createdatMap = storeToTimeSortedDict[key]
			sortedCreateAtMap = OrderedDict(sorted(createdatMap.items()))
			sortedStoreToTimeStoreDict[key] = sortedCreateAtMap
		self.logger.debug("sortedStoreToTimeStoreDict: %s", sortedStoreToTimeStoreDict)
		return sortedStoreToTimeStoreDict	


#obj = RedisDataTransformer()
#rawData = dict()
#rawData["1519028214596_dc506ad4-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Salons & Spas\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214596\", \"lng\": \"77.6419614\", \"distance\": 560.5921, \"uuid\": \"dc506ad4-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"The 5th Element\", \"cat\": \"Personal Care\"}"
#rawData["1519028214591_dbf61bcd-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Furniture\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214591\", \"lng\": \"77.6419614\", \"distance\": 559.5921, \"uuid\": \"dbf61bcd-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"Cane Boutique\", \"cat\": \"Home & Lifestyle\"}"
#rawData["1519028214593_dc12fcfc-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Books\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214593\", \"lng\": \"77.6419614\", \"distance\": 71.7288, \"uuid\": \"dc12fcfc-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"Book Supply Bureau\", \"cat\": \"Toys, Books, Gifts & Music\"}"
#rawData["1519028214595_dc6508c9-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Hotels\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214595\", \"lng\": \"77.6419614\", \"distance\": 454.8581, \"uuid\": \"dc6508c9-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"The Paul Bangalore\", \"cat\": \"Hospitality\"}"
#rawData["1519028214599_dc506ad4-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Salons & Spas\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214599\", \"lng\": \"77.6419614\", \"distance\": 560.5921, \"uuid\": \"dc506ad4-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"The 5th Element\", \"cat\": \"Personal Care\"}"
#rawData["1519028214599_dc2ff1e5-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Salons & Spas\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214599\", \"lng\": \"77.6419614\", \"distance\": 560.5921, \"uuid\": \"dc2ff1e5-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"The 5th Element\", \"cat\": \"Personal Care\"}"
#rawData["1519028214599_dc4f15ae-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Salons & Spas\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214599\", \"lng\": \"77.6419614\", \"distance\": 560.5921, \"uuid\": \"dc4f15ae-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"The 5th Element\", \"cat\": \"Personal Care\"}"
#rawData["1519028214594_dc2dd293-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Bars & Pubs\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214594\", \"lng\": \"77.6419614\", \"distance\": 559.5921, \"uuid\": \"dc2dd293-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"Vembanad\", \"cat\": \"F&B\"}"
#rawData["threadDet"] = "b'i-0bb95d89f4ae1600c'139713059288832"
#rawData["1519028214592_dc2f014c-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Car & Bike care,4-wheeler\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214592\", \"lng\": \"77.6419614\", \"distance\": 552.5921, \"uuid\": \"dc2f014c-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"Trident Hyundai\", \"cat\": \"Automotive\"}"
#rawData["1519028214598_dc2dd293-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Bars & Pubs\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214598\", \"lng\": \"77.6419614\", \"distance\": 559.5921, \"uuid\": \"dc2dd293-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"Vembanad\", \"cat\": \"F&B\"}"
#rawData["1519028214597_dc2f014c-10a6-11e8-96d5-0ac08446cc34"] = "{\"subcat\": \"Car & Bike care,4-wheeler\", \"lat\": \"12.9503345\", \"createdat\": \"1519028214597\", \"lng\": \"77.6419614\", \"distance\": 552.5921, \"uuid\": \"dc2f014c-10a6-11e8-96d5-0ac08446cc34\", \"apikey\": \"314ac5b3c9e5c929bbc9170e13ea2b72\", \"gid\": \"gidv4\", \"sname\": \"Trident Hyundai\", \"cat\": \"Automotive\"}"
#print( rawData)
#print (obj.transform(rawData))
