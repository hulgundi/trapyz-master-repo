from elasticsearch import Elasticsearch, RequestsHttpConnection


def connectES(esEndPoint):
	print ('Connecting to the ES Endpoint {0}'.format(esEndPoint))
	try:
		esClient = Elasticsearch(
			hosts=[{'host': esEndPoint, 'port': 443}],
			use_ssl=True,
			verify_certs=True,
			connection_class=RequestsHttpConnection)
		return esClient
	except Exception as E:
		print("Unable to connect to {0}".format(esEndPoint))
		print(E)
	exit(3)

esClient = connectES('https://vpc-userdata-hkc5wqfat2tctkodwpqht2jjaq.us-east-1.es.amazonaws.com')

def createIndex(esClient):
	try:
		res = esClient.indices.exists('metadata-store')
		print("Index Exists ... {}".format(res))
		if res is False:
			esClient.indices.create('metadata-store', body=indexDoc)
			return 1
	except Exception as E:
		print("Unable to Create Index {0}".format("metadata-store"))
		print(E)
	exit(4)

createIndex(esClient)
