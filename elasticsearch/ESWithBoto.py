import boto3
import requests
import json
from requests_aws4auth import AWS4Auth

def send_signed(method, url, service='es', region='us-east-1', body=None):
    credentials = boto3.Session().get_credentials()
    print (credentials)
    auth = AWS4Auth(credentials.access_key, credentials.secret_key, 
                  region, service, session_token=credentials.token)

    fn = getattr(requests, method)
    if body and not body.endswith("\n"):
        body += "\n"
    response = fn(url, auth=auth, data=body, headers={"Content-Type":"application/json"})
    print (response)
    if response.status_code == 200 or response.status_code == 201:
        htmlBody = response.content
        print (htmlBody)


url = 'https://vpc-userdata-hkc5wqfat2tctkodwpqht2jjaq.us-east-1.es.amazonaws.com/blogs/blog'
doc = '''{
                 "author": "Jon2",
                 "title": "signing is easy!"
             }'''
#send_signed('post', url, body=doc)
doc1 = '''{
    "query": {
        "match_all": {}
    }
}'''
send_signed('get', url + "/_search", body=doc1)
