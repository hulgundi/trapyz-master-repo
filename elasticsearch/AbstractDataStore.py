from abc import ABCMeta, abstractmethod
import threading
import time
import logging
import random

class AbstractDataStore(metaclass=ABCMeta):
    def __init__(self, logFileName):
        self.bulkUploadLock = threading.Lock()
        self.logger = logging.getLogger(logFileName)
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler(logFileName)
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        self.logger.debug("Instantiated " + self.__class__.__name__)

    @abstractmethod
    def storeBulk(self, obj):
        pass
    
    @abstractmethod
    def storeSingleAsBulk(self, obj):
        pass

    @abstractmethod
    def storeSingle(self, obj):
        pass

    def storeSingle(self, obj, toBeStoredAsBulk):
        if toBeStoredAsBulk:
            self.bulkUploadLock.acquire()
            try:
                self.logger.debug("acquired the lock")
                storeSingleAsBulk(obj)
            finally:
                self.bulkUploadLock.release()
                self.logger.debug("released the lock")
        else:
            storeSingle(obj)
    
    
