import boto3
import gc
from boto3.dynamodb.conditions import Key, Attr
import time,threading
dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table("GeoDataProcessedInsights")
responseScanResult=[]
print(time.time())
def scanDDB(i):
	lastIndexKey=""
	count = 0
	while True and count <= 1:
		count = count + 1
		if(lastIndexKey!=""):
			response = table.scan(TableName="GeoDataProcessedInsights",TotalSegments=3, Segment=i,ExclusiveStartKey=lastIndexKey,FilterExpression="info.distance = :distance", ExpressionAttributeValues={":distance" : 237 })
		else:

			#response = table.scan(TableName="GeoDataProcessedInsights",TotalSegments=3, Segment=i, FilterExpression="#k_info.#k_distance = :distance", ExpressionAttributeValues={":distance" : { "N" : "237" }}, ExpressionAttributeNames = {"#k_info" : "info", "#k_distance" : "distance"})
			response = table.scan(TableName="GeoDataProcessedInsights",TotalSegments=3, Segment=i, FilterExpression="info.distance = :distance", ExpressionAttributeValues={":distance" : 237 })
		responseScanResult.append(response["Items"])
		print ( "segment: " + str(i), response["Items"])
		if("LastEvaluatedKey" in response):
			lastIndexKey = response["LastEvaluatedKey"]
		else:
			break

for i in range(0,3):
	scanDDB(i)
	print(str(i+1)+" Segment Done!!")
	print(gc.collect())
	print(time.time())
	print(len(responseScanResult))
