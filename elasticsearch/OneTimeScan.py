import boto3
import random
import requests
import json
import itertools
from requests_aws4auth import AWS4Auth

def send_signed(method, url, service='es', region='us-east-1', body=None):
    credentials = boto3.Session().get_credentials()
    print (credentials)
    auth = AWS4Auth(credentials.access_key, credentials.secret_key, 
                  region, service, session_token=credentials.token)

    fn = getattr(requests, method)
    if body and not body.endswith("\n"):
        body += "\n"
    response = fn(url, auth=auth, data=body, headers={"Content-Type":"application/json"})
    print (response)
    if response.status_code == 200 or response.status_code == 201:
        htmlBody = response.content
        print (htmlBody)


gidsArr = ["ad66f965-60e3-44c1-8ba0-4c835e576ffc" , "94ecf993-ba70-442a-82f2-7528ba5f43c3", "b1d7a089-739a-4134-9914-69fba811b10f", "3ba1c64f-fd2e-4fc0-a488-60be34119c07"]
categoriesArr = ["27", "14", "9", "30", "50"]
subcategoriesArr = ["1" , "2" , "3" , "4" , "5"]
datesArr = ["2018-06-10", "2018-06-11", "2018-06-12", "2018-06-13", "2018-06-14", "2018-06-09", "2018-06-08", "2018-06-15", "2018-06-16"]
hoursArr = ["01", "02", "03" , "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21" , "22", "23", "24"]

count = 0
for (gid, categoryIdsubcategoryId, date, hour) in itertools.product(gidsArr, zip (categoriesArr, subcategoriesArr), datesArr, hoursArr):
	break;
	count = count + 1
	print (count, gid, categoryIdsubcategoryId, date, hour)
	categoryId, subcategoryId = categoryIdsubcategoryId
	objectData = { "gid": gid,
		"categoryId": categoryId,
		"dateHourMin": date + "T" + hour + ":00",
		"subcategoryId": subcategoryId,
		"storeUuid": "20593",
		"info": {
			"createdAt": 1531746474,
			"updatedAt": 1531755048,
			"city": "5",
			"visits": random.randint(1, 3),
			"distance": random.randint(5,500),
			"pin": "4"
		},
		"apiKey": "10"
	}
	url = "https://search-geouserdata-n4qdbepki32rsntb5v3ww6ordu.us-east-1.es.amazonaws.com/geouserdata/_doc/" + gid + "_" + date.replace("-", "") + "_" + hour + "00_20593_10"
	print (url, json.dumps(objectData)) 
	send_signed('post', url, body=json.dumps(objectData))
	


url = 'https://search-geouserdata-n4qdbepki32rsntb5v3ww6ordu.us-east-1.es.amazonaws.com/geouserdata/_doc/ad66f965-60e3-44c1-8ba0-4c835e576ffc_20180716_1307_20593_9'
doc = '''{
  "gid": "ad66f965-60e3-44c1-8ba0-4c835e576ffc",
  "categoryId": "27",
  "dateHourMin": "2018-07-16T13:07",
  "subcategoryId": "41",
  "storeUuid": "20593",
  "info": {
    "createdAt": 1531746474,
    "updatedAt": 1531755048,
    "city": "5",
    "visits": 1,
    "distance": "879",
    "pin": "4"
  },
  "apiKey" : "9"
}'''
send_signed('post', url, body=doc)
doc1 = '''{
    "query": {
        "match_all": {}
    }
}'''
url = 'https://search-geouserdata-n4qdbepki32rsntb5v3ww6ordu.us-east-1.es.amazonaws.com/geouserdata/'
doc2 = '''{"size":0,"aggs":{"users":{"filters":{"other_bucket_key":"other_messages","filters":{"visitsmorethan1":{"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[{"range":{"info.visits":{"gte":2}}},{"range":{"info.distance":{"lte":50}}}]}}}},{"bool":{"should":[{"term":{"categoryId":{"value":"50"}}},{"term":{"categoryId":{"value":"27"}}},{"term":{"categoryId":{"value":"14"}}}]}},{"term":{"apiKey":{"value":"10"}}},{"range":{"dateHourMin":{"gte":"2018-06-01T00:00"}}}]}},"visitsmorethan0":{"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[{"range":{"info.visits":{"lte":1}}},{"range":{"info.distance":{"lte":50}}}]}}}},{"bool":{"should":[{"term":{"categoryId":{"value":"50"}}},{"term":{"categoryId":{"value":"27"}}}]}},{"term":{"apiKey":{"value":"10"}}},{"range":{"dateHourMin":{"gte":"2018-06-01T00:00"}}}]}}}}}}}'''
send_signed('get', url + "/_search", body=doc2)
