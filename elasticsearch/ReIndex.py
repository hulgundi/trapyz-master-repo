import boto3
import random
import requests
import json
import itertools
from requests_aws4auth import AWS4Auth

def send_signed(method, url, service='es', region='us-east-1', body=None):
    credentials = boto3.Session().get_credentials()
    print (credentials)
    auth = AWS4Auth(credentials.access_key, credentials.secret_key, 
                  region, service, session_token=credentials.token)

    fn = getattr(requests, method)
    if body and not body.endswith("\n"):
        body += "\n"
    response = fn(url, auth=auth, data=body, headers={"Content-Type":"application/json"})
    print (response)
    if response.status_code == 200 or response.status_code == 201:
        htmlBody = response.content
        print (htmlBody)




url = 'https://search-geouserdata-n4qdbepki32rsntb5v3ww6ordu.us-east-1.es.amazonaws.com/'
document = '''
{
  "source": {
    "index": "geodataprocessedinsights"
  },
  "dest": {
    "index": "geouserdata"
  }
}
'''
send_signed('post', url + "/_reindex", body = document)
