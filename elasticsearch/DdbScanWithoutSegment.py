from datetime import datetime, timedelta
from requests_aws4auth import AWS4Auth
import itertools
import boto3
import json
import decimal
import logging
from boto3.dynamodb.conditions import Key, Attr
import requests

dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table("ProcessedInsightsDDB")

logger = logging.getLogger('stdout_log')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('stdout.log')
fh.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s')
fh.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
# logger.debug("Instantiated ProcessedUserDataHelper")

apiKeyArr = ["29"]
maxDistanceSupported = 300
lastNDays = 19

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

def send_signed(method, url, service='es', region='us-east-1', body=None):
    credentials = boto3.Session().get_credentials()
    logger.debug (credentials)
    auth = AWS4Auth(credentials.access_key, credentials.secret_key, 
                  region, service, session_token=credentials.token)

    fn = getattr(requests, method)
    if body and not body.endswith("\n"):
        body += "\n"
    response = fn(url, auth=auth, data=body, headers={"Content-Type":"application/json"})
    logger.debug (response)
    if response.status_code == 200 or response.status_code == 201:
        htmlBody = response.content
        logger.debug (htmlBody)

def getLastDaysFormatted(lastNDays):
    datesArr = []
    # for i in range(0, lastNDays):
    #     datesArr.append((datetime.now() - timedelta(days = i)).strftime("%Y%m%d"))
    datesArr.append((datetime.now() - timedelta(days = lastNDays)).strftime("%Y%m%d"))
    return datesArr

def storeData(data):
    table = dynamodb.Table("GeoDataProcessedInsights")
    logger.debug("data: %s", data)
    response = table.put_item(Item = data)
    logger.debug("response: %s", response)        

def storeDataInDynamoWithProperMapping(processedInsightObj):
    logger.debug ("input: %s", json.dumps(processedInsightObj))
    processedInsight = {"apiKey_date_hour_min" : str(processedInsightObj["apiKey_date_hour_min"]), "apiKey_ident_date" : str(processedInsightObj["apiKey_ident_date"]), "categoryId" : str(processedInsightObj["categoryId"]), "date_hour_min_store_api" : str(processedInsightObj["date_hour_min_store_api"]), "gid" : str(processedInsightObj["gid"]), "info" : {"city" : str(processedInsightObj["info"]["city"]), "created_at" : int(processedInsightObj["info"]["created_at"]), "distance" : int(processedInsightObj["info"]["distance"]), "pin" : str(processedInsightObj["info"]["pin"]), "updated_at" : int(processedInsightObj["info"]["updated_at"]), "visits" : int(processedInsightObj["info"]["visits"])}, "storeUuid" : str(processedInsightObj["storeUuid"]), "subcategoryId" : str(processedInsightObj["subcategoryId"])}
    if processedInsight["info"]["distance"] <= maxDistanceSupported:
        logger.debug ("response : %s", json.dumps(processedInsight))
        storeData(processedInsight)
    else:
        logger.warn("As Ranga Sir suggested ignore this distance: %s", processedInsight["info"]["distance"])

def storeDataInElasticSearch(processedInsightObj):
    apiKey, date, hourMin = processedInsightObj["apiKey_date_hour_min"].split("_")
    dateHourMin = datetime.strptime(date + hourMin, "%Y%m%d%H%M").strftime("%Y-%m-%dT%H:%M")
    processedInsight = {"apiKey" : str(apiKey), "dateHourMin" : str(dateHourMin), "categoryId" : str(processedInsightObj["categoryId"]), "gid" : str(processedInsightObj["gid"]), "info" : {"city" : str(processedInsightObj["info"]["city"]), "createdAt" : int(processedInsightObj["info"]["created_at"]), "distance" : int(processedInsightObj["info"]["distance"]), "pin" : str(processedInsightObj["info"]["pin"]), "updatedAt" : int(processedInsightObj["info"]["updated_at"]), "visits" : int(processedInsightObj["info"]["visits"])}, "storeUuid" : str(processedInsightObj["storeUuid"]), "subcategoryId" : str(processedInsightObj["subcategoryId"])}
    if processedInsight["info"]["distance"] <= maxDistanceSupported:
        gid = processedInsight["gid"]
        url = "https://search-geouserdata-n4qdbepki32rsntb5v3ww6ordu.us-east-1.es.amazonaws.com/geodataprocessedinsights/_doc/" + gid + "_" + processedInsightObj["apiKey_date_hour_min"] + "_" + processedInsightObj["storeUuid"]
        logger.debug ("url: %s data : %s", url, json.dumps(processedInsight))
        send_signed('post', url, body=json.dumps(processedInsight))

datesArr = getLastDaysFormatted(lastNDays)
print(datesArr)
print('query')
for apiKey, dateCurr in itertools.product(apiKeyArr, datesArr):
    # response = table.scan(FilterExpression=Attr("apiKey_date_hour_min").begins_with(apiKey + "_" + dateCurr))
    response = table.query(IndexName='apiKey_ident_date-index',KeyConditionExpression=Key('apiKey_ident_date').eq(apiKey + "_" + dateCurr))
    # print(len(response['Items']))
    for i in response['Items']:
        strData = json.dumps(i, cls=DecimalEncoder)
        processedInsightObj = json.loads(strData)
        storeDataInDynamoWithProperMapping(processedInsightObj)
        try:
            storeDataInElasticSearch(processedInsightObj)
        except:
            logger.debug("Exception Occured while Inserting Data %s in Elastic Search ",processedInsightObj)
            raise

    while 'LastEvaluatedKey' in response:
#        response = table.scan(FilterExpression=Attr("apiKey_date_hour_min").begins_with(apiKey + "_" + dateCurr), ExclusiveStartKey=response['LastEvaluatedKey'])
        response = table.query(IndexName='apiKey_ident_date-index',KeyConditionExpression=Key('apiKey_ident_date').eq(apiKey + "_" + dateCurr), ExclusiveStartKey=response['LastEvaluatedKey'])
        # print(len(response['Items']))
        for i in response['Items']:
            strData = json.dumps(i, cls=DecimalEncoder)
            processedInsightObj = json.loads(strData)
            storeDataInDynamoWithProperMapping(processedInsightObj)
            try:
                storeDataInElasticSearch(processedInsightObj)
            except:
               logger.debug("Exception Occured while Inserting Data %s in Elastic Search ",processedInsightObj)
               raise

