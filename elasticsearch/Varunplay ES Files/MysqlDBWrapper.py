import logging
import json
from pprint import pprint
import pymysql
import datetime

class MysqlDBWrapper(object):
    def __init__(self):
        self.logger = logging.getLogger("mysql_log")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler("mysql.log")
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
       
        self.host = '172.31.0.193'
        self.user = 'pwx'
        self.passwd = 'ciscoplanet'
        self.db = 'trapyz_beta'

    def getCategoryMap(self):
        catMapQuery = "SELECT * FROM CategoryMap"
        try:
            connect = pymysql.connect(host = self.host, user = self.user, passwd = self.passwd, db = self.db)
            cursor = connect.cursor()
            cursor.execute(catMapQuery)
            catMap = {}
            for row in cursor.fetchall():
                catMap[str(row[0])] = str(row[1])
            self.logger.info("CategoryMap %s",json.dumps(catMap))
            connect.close()
        except:
            self.logger.error("Error Fetching CategoryMap")

        return catMap

    def getPredefinedSegments(self):
        return self.getAllRowsForGivenTable("segments")

    def getRecentCreatedDateFromMasterPredefinedInsightsForApi(self, api):
        connect = pymysql.connect(host = self.host, user = self.user, passwd = self.passwd, db = self.db)
        cursor = connect.cursor()
        recentDateQuery = "SELECT max(createdDate) FROM MasterPredefinedInsights" + " WHERE api = " + str(api) + " limit 1" 
        cursor.execute(recentDateQuery)
        recentDate = None
        for row in cursor.fetchall():
            recentDate = row[0]
        connect.close()
        return recentDate

    def updateMasterPredefinedInsights(self, api, createdDate, segmentCountJson, categoryUsersJson, usersMapAgainstApi):
        connect = pymysql.connect(host = self.host, user = self.user, passwd = self.passwd, db = self.db)
        cursor = connect.cursor()
        updateQuery = "UPDATE MasterPredefinedInsights SET segmentCountJson = '" + json.dumps(segmentCountJson) + "', categoryUsersJson = '" + json.dumps(categoryUsersJson) + "', totalUsers = " + str(usersMapAgainstApi[0]) + ", lastWeekActiveUsers = " + str(usersMapAgainstApi[1]) + ", lastMonthActiveUsers = " + str(usersMapAgainstApi[2])  + " where api = " + str(api) + " AND createdDate = '" + str(createdDate) + "'"
        cursor.execute(updateQuery)
        connect.commit()
        connect.close()
        self.logger.debug("Successfully updated segmentCountJson %s for api: %s for date: %s", json.dumps(segmentCountJson), api, str(createdDate))

    def createRowInMasterPredefinedInsights(self, api, segmentCountJson, categoryUsersJson, usersMapAgainstApi):
        connect = pymysql.connect(host = self.host, user = self.user, passwd = self.passwd, db = self.db)
        cursor = connect.cursor()
        insertQuery = "insert into MasterPredefinedInsights(api,createdDate,totalUsers,lastWeekActiveUsers,lastMonthActiveUsers,categoryUsersJson,segmentCountJson)" + "values('" + str(api) + "','" + str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")) + "'," + str(usersMapAgainstApi[0]) + "," + str(usersMapAgainstApi[1]) + "," + str(usersMapAgainstApi[2]) + ",'" + json.dumps(categoryUsersJson) + "','" + json.dumps(segmentCountJson) + "')"
        cursor.execute(insertQuery)
        connect.commit()
        connect.close()
        self.logger.debug("Successfully inserted row in the query: %s", insertQuery)
    
    def getAllRowsForGivenTable(self, table):
        ### just check the things for finite api keys...
        #queryForTable = "SELECT * FROM " + table + " where apiKey = 9 or apiKey = 29"
        #queryForTable = "SELECT * FROM " + table + " where apiKey = 33"
        queryForTable = "SELECT * FROM " + table
        try:
            connect = pymysql.connect(host = self.host, user = self.user, passwd = self.passwd, db = self.db)
            cursor = connect.cursor(pymysql.cursors.DictCursor)
            cursor.execute(queryForTable)
            tableAsArray = []
            for row in cursor.fetchall():
                tableAsArray.append(row)
                
            self.logger.info("Table %s as array %s", table, json.dumps(tableAsArray))
            return tableAsArray
        except:
            self.logger.error("Error Fetching CategoryMap")


#obj = MysqlDBWrapper()
#obj.getPredefinedSegments()

