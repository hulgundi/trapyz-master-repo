from ElasticSearchWrapper import ElasticSearchWrapper
import logging
import json
import datetime
import dateutil.relativedelta
import math

class ElasticSearchSegment(object):
    def __init__(self, aggregatedApi = "33"):
        self.elasticSearchClient = ElasticSearchWrapper(indexName = "geodataprocessedinsights")
        self.logger = logging.getLogger("segmentsSearch_log")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler("segmentsSearch.log")
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        self.aggregatedApi = aggregatedApi

    def getApiQuery(self, apiKey):
        return {"term":{"apiKey":{"value":str(apiKey)}}}

    def getCategoryQuery(self, categoryId):
        return {"term":{"categoryId":{"value":str(categoryId)}}}

    def getSubCategoryQuery(self, subCategoryId):
        return {"term":{"subcategoryId":{"value":str(subCategoryId)}}}
 
    def getStoreQuery(self, storeId):
        return {"term":{"storeUuid":{"value":str(storeId)}}}
        
    def getDistanceQuery(self, distance):
        return {"range":{"info.distance":{"lte":int(distance)}}}
     
    def getVisitsQuery(self, visits):
        return {"range":{"info.visits":{"gte":int(visits)}}}

    def getCityQuery(self, city):
        return {"term":{"info.city":{"value":str(city)}}}
    
    def getPinCodeQuery(self, pin):
        return {"term":{"info.pin":{"value":str(pin)}}}

    def getDateRangeQuery(self, startTimestampInMillis, endTimestampInMillis):
        return {"range":{"info.createdAt":{"format":"epoch_millis","gte":int(startTimestampInMillis),"lte":int(endTimestampInMillis)}}}

    def getDayOfWeekQuery(self, day):
        return {"script":{"script":{"source":"doc['info.createdAt'].value.getDayOfWeek() == params.day","lang":"painless","params":{"day":day}}}}

    def getTimeSlotQuery(self, minimum, maximum):
        return {"script":{"script":{"source":"doc['info.createdAt'].value.getHourOfDay() >= params.min && doc['info.createdAt'].value.getHourOfDay() < params.max","lang":"painless","params":{"min":minimum,"max":maximum}}}}

    def getCardinalityQuery(self):
        return {"DISTINCT":{"cardinality":{"field":"gid","precision_threshold": 100}}}

    def getBasicQuery(self, segmentName):
        return {"size":0,"aggs":{"users":{"aggs":self.getCardinalityQuery(),"filters":{"other_bucket_key":"other_messages","filters":{segmentName:{"bool":{"should":[]}}}}}}}

    def getBasicFilteredQuery(self):
        return {"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[]}}}}]}}
    
    def getSegmentCountJson(self, segmentName, responseJson, segment):
        apiKey = segment['apiKey']
        segmentCount = responseJson['aggregations']['users']['buckets'][segmentName]['DISTINCT']['value']
        segmentCountJson = {} 
        segmentCountJson[segmentName] = {}
        segmentCountJson[segmentName]["value"] = segmentCount
        segmentCountJson[segmentName]["type"] = "preset"

        if segment['attributeJson'] == None:
            segmentCountJson[segmentName]["isAdvance"] = 1
       
        return segmentCountJson

    def searchForSegment(self, segment):
        if segment['attributeJson'] == None:
            attributeJson = segment['attributeJSON2']    
        else:
            attributeJson = segment['attributeJson']

        attributeJsonObj = json.loads(attributeJson)
        segmentName = segment['name']
        apiKey = segment['apiKey']
        esQuery = self.getBasicQuery(segmentName)
        for filteredData in attributeJsonObj["queryList"]:
            dataQuery = self.getBasicFilteredQuery()

            # don't add the api query for the api aggregated API.
            if str(apiKey) != self.aggregatedApi:
                dataQuery["bool"]["must"].append(self.getApiQuery(apiKey))
            
            # add support for the distance.
            if "distance" in filteredData and "value" in filteredData["distance"] and filteredData["distance"]["value"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDistanceQuery(filteredData["distance"]["value"]))

            # add support for the visits.
            if "visit" in filteredData and "value" in filteredData["visit"] and filteredData["visit"]["value"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getVisitsQuery(filteredData["visit"]["value"]))

            # add support for the pin.
            if "pin" in filteredData and filteredData["pin"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getPinCodeQuery(filteredData["pin"]))

            # add support for the city.
            if "city" in filteredData and filteredData["city"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getCityQuery(filteredData["city"]))
           
            # add support for the timestamp.
            if "duration" in filteredData:
                if filteredData["duration"]["type"] == "week":
                    now = datetime.datetime.now()
                    lastWeeksTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(weeks = -1)).strftime("%s%f")) / 1000)
                    currentTimestampInMillis = int(int(now.strftime("%s%f")) / 1000)
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(lastWeeksTimestampInMillis, currentTimestampInMillis))
                
                elif filteredData["duration"]["type"] == "month":
                    now = datetime.datetime.now()
                    lastMonthsTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(months = -1)).strftime("%s%f")) / 1000)
                    currentTimestampInMillis = int(int(now.strftime("%s%f")) / 1000)
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(lastMonthsTimestampInMillis, currentTimestampInMillis))

                elif filteredData["duration"]["type"] == "custom":
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(int(filteredData["duration"]["startDate"]),int(filteredData["duration"]["endDate"])))

            if "day" in filteredData:
               dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDayOfWeekQuery(int(filteredData["day"]["value"])))

            if "slot" in filteredData:
               dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getTimeSlotQuery(int(filteredData["slot"]["min"]),int(filteredData["slot"]["max"])))

            # add support for the category.
            if "dimension" in filteredData and "name" in filteredData["dimension"] and "cat" == filteredData["dimension"]["name"]:
                dataQuery["bool"]["must"].append(self.getCategoryQuery(filteredData["dimension"]["value"]))
            elif "dimension" in filteredData and "name" in filteredData["dimension"] and "subcat" == filteredData["dimension"]["name"]:
                dataQuery["bool"]["must"].append(self.getSubCategoryQuery(filteredData["dimension"]["value"]))
            elif "dimension" in filteredData and "name" in filteredData["dimension"] and "store" == filteredData["dimension"]["name"]:
                dataQuery["bool"]["must"].append(self.getStoreQuery(filteredData["dimension"]["value"]))            

            esQuery["aggs"]["users"]["filters"]["filters"][segmentName]["bool"]["should"].append(dataQuery)

        self.logger.debug (json.dumps(esQuery))

        return esQuery

    def getCountForSegment(self, segment):
        response = self.elasticSearchClient.getDataFromElasticSearch(self.searchForSegment(segment))
        responseJson = json.loads(response.decode('utf8').replace("'", '"'))
        self.logger.info(responseJson)

        return self.getSegmentCountJson(segment["name"], responseJson, segment)

    def getGidsRevertSegmentJson(self, segmentName, responseJson, apiKey):
        gids = []
        for x in responseJson['aggregations']['users']['buckets'][segmentName]['All Gids']['buckets']:
            gids.append(x['key'])
        return gids

    def getShowGidsQuery(self, index, max_count):
        return {"All Gids":{"aggs":{"distinct":{"cardinality":{"field":"gid"}}},"terms":{"field":"gid","include":{ "partition": index, "num_partitions": max_count}, "size": 10000}}}
    def getBasicQueryForGids(self, segmentName, index, max_count):
        return {"size": 0, "aggs":{"users":{"aggs":self.getShowGidsQuery(index, max_count),"filters":{"other_bucket_key":"other_messages","filters":{segmentName:{"bool":{"should":[]}}}}}}}

    def gidsForSegment(self, segment, index, max_count):
        attributeJson = segment['attributeJson']
        attributeJsonObj = json.loads(attributeJson)
        segmentName = segment['name']
        apiKey = segment['apiKey']
        esQuery = self.getBasicQueryForGids(segmentName, index, max_count)
        for filteredData in attributeJsonObj["queryList"]:
            dataQuery = self.getBasicFilteredQuery()

            # don't add the api query for the api aggregated API.
            if str(apiKey) != self.aggregatedApi:
                dataQuery["bool"]["must"].append(self.getApiQuery(apiKey))
            
            # add support for the distance.
            if "distance" in filteredData and "value" in filteredData["distance"] and filteredData["distance"]["value"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDistanceQuery(filteredData["distance"]["value"]))

            # add support for the visits.
            if "visit" in filteredData and "value" in filteredData["visit"] and filteredData["visit"]["value"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getVisitsQuery(filteredData["visit"]["value"]))

            # add support for the pin.
            if "pin" in filteredData and filteredData["pin"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getPinCodeQuery(filteredData["pin"]))

            # add support for the city.
            if "city" in filteredData and filteredData["city"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getCityQuery(filteredData["city"]))
           
            # add support for the timestamp.
            if "duration" in filteredData:
                if filteredData["duration"]["type"] == "week":
                    now = datetime.datetime.now()
                    lastWeeksTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(weeks = -1)).strftime("%s%f")) / 1000)
                    currentTimestampInMillis = int(int(now.strftime("%s%f")) / 1000)
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(lastWeeksTimestampInMillis, currentTimestampInMillis))
                
                elif filteredData["duration"]["type"] == "month":
                    now = datetime.datetime.now()
                    lastMonthsTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(months = -1)).strftime("%s%f")) / 1000)
                    currentTimestampInMillis = int(int(now.strftime("%s%f")) / 1000)
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(lastMonthsTimestampInMillis, currentTimestampInMillis))

                elif filteredData["duration"]["type"] == "custom":
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(int(filteredData["duration"]["startDate"]),int(filteredData["duration"]["endDate"])))

            # add support for the category.
            if "dimension" in filteredData and "name" in filteredData["dimension"] and "cat" == filteredData["dimension"]["name"]:
                dataQuery["bool"]["must"].append(self.getCategoryQuery(filteredData["dimension"]["value"]))

            esQuery["aggs"]["users"]["filters"]["filters"][segmentName]["bool"]["should"].append(dataQuery)

        self.logger.debug (json.dumps(esQuery))

        return esQuery

    def getGidsForSegement(self, segment, totalCount):
        all_gids = []
        max_count = math.ceil(totalCount/9000)

        for i in range(0, max_count):
            response = self.elasticSearchClient.getDataFromElasticSearch(self.gidsForSegment(segment, i, max_count))
            responseJson = json.loads(response.decode('utf8').replace("'", '"'))
            self.logger.info(responseJson)
            all_gids = all_gids + self.getGidsRevertSegmentJson(segment["name"], responseJson, segment["apiKey"])

        return all_gids

    def getAndCountForSegement(self, segment):
        limit = len(json.loads(segment['attributeJson'])['queryList'])
        #import code; code.interact(local=dict(globals(), **locals()))
        first_query = {'description': 'new' ,'name': 'new test', 'attributeJson': json.dumps({ 'queryList': [json.loads(segment['attributeJson'])['queryList'][0]] }), 'apiKey': '33'}
        #import code; code.interact(local=dict(globals(), **locals()))
        countJson = self.getCountForSegment(first_query)
        countGids = countJson[segment['name']]['value']
        all_gids = self.getGidsForSegement(first_query, countGids)
        
        for i in range(1, limit):
            query = {'description': 'new' ,'name': 'new test', 'attributeJson': json.dumps({ 'queryList': [json.loads(segment['attributeJson'])['queryList'][i]] }), 'apiKey': '33'}
            #import code; code.interact(local=dict(globals(), **locals()))
            countJson = self.getCountForSegment(query)
            countGids = countJson[segment['name']]['value']
            new_all_gids = self.getGidsForSegement(query, countGids)      
            final_gids = set(all_gids) & set(new_all_gids)
            all_gids = list(final_gids)        

        return all_gids

# .......................................................................................................................New And Operation for gids
    def getAndBasicQuery(self, segmentName):
        return {"size":0,"aggs":{"users":{"aggs":self.getCardinalityQuery(),"filters":{"other_bucket_key":"other_messages","filters":{segmentName:{"bool":{"must":[]}}}}}}}
    
    def getGidsQuery(self, gids):
        return {"terms":{"gid":gids}}
    
    def newSearchForSegment(self, segment, gids):
        attributeJson = segment['attributeJson']
        attributeJsonObj = json.loads(attributeJson)
        segmentName = segment['name']
        apiKey = segment['apiKey']
        esQuery = self.getAndBasicQuery(segmentName)
        for filteredData in attributeJsonObj["queryList"]:
            dataQuery = self.getBasicFilteredQuery()

            # don't add the api query for the api aggregated API.
            if str(apiKey) != self.aggregatedApi:
                dataQuery["bool"]["must"].append(self.getApiQuery(apiKey))
            
            # add support for the distance.
            if "distance" in filteredData and "value" in filteredData["distance"] and filteredData["distance"]["value"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDistanceQuery(filteredData["distance"]["value"]))

            #dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getGidsQuery(gids))
            # add support for the visits.
            if "visit" in filteredData and "value" in filteredData["visit"] and filteredData["visit"]["value"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getVisitsQuery(filteredData["visit"]["value"]))

            # add support for the pin.
            if "pin" in filteredData and filteredData["pin"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getPinCodeQuery(filteredData["pin"]))

            # add support for the city.
            if "city" in filteredData and filteredData["city"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getCityQuery(filteredData["city"]))
           
            # add support for the timestamp.
            if "duration" in filteredData:
                if filteredData["duration"]["type"] == "week":
                    now = datetime.datetime.now()
                    lastWeeksTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(weeks = -1)).strftime("%s%f")) / 1000)
                    currentTimestampInMillis = int(int(now.strftime("%s%f")) / 1000)
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(lastWeeksTimestampInMillis, currentTimestampInMillis))
                
                elif filteredData["duration"]["type"] == "month":
                    now = datetime.datetime.now()
                    lastMonthsTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(months = -1)).strftime("%s%f")) / 1000)
                    currentTimestampInMillis = int(int(now.strftime("%s%f")) / 1000)
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(lastMonthsTimestampInMillis, currentTimestampInMillis))

                elif filteredData["duration"]["type"] == "custom":
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(int(filteredData["duration"]["startDate"]),int(filteredData["duration"]["endDate"])))

            if "day" in filteredData:
               dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDayOfWeekQuery(int(filteredData["day"]["value"])))

            if "slot" in filteredData:
               dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getTimeSlotQuery(int(filteredData["slot"]["min"]),int(filteredData["slot"]["max"])))

            # add support for the category.
            if "dimension" in filteredData and "name" in filteredData["dimension"] and "cat" == filteredData["dimension"]["name"]:
                dataQuery["bool"]["must"].append(self.getCategoryQuery(filteredData["dimension"]["value"]))
            elif "dimension" in filteredData and "name" in filteredData["dimension"] and "subcat" == filteredData["dimension"]["name"]:
                dataQuery["bool"]["must"].append(self.getSubCategoryQuery(filteredData["dimension"]["value"]))
            elif "dimension" in filteredData and "name" in filteredData["dimension"] and "store" == filteredData["dimension"]["name"]:
                dataQuery["bool"]["must"].append(self.getStoreQuery(filteredData["dimension"]["value"]))            

            esQuery["aggs"]["users"]["filters"]["filters"][segmentName]["bool"]["must"].append(dataQuery)
            esQuery["aggs"]["users"]["filters"]["filters"][segmentName]["bool"]["must"].append(self.getGidsQuery(gids))
        
        self.logger.debug (json.dumps(esQuery))

        return esQuery

    def getNewCountForSegment(self, segment, gids):
        response = self.elasticSearchClient.getDataFromElasticSearch(self.newSearchForSegment(segment, gids))
        responseJson = json.loads(response.decode('utf8').replace("'", '"'))
        self.logger.info(responseJson)

        return self.getSegmentCountJson(segment["name"], responseJson, segment["apiKey"])

    def getAndBasicQueryForGids(self, segmentName, index, max_count):
        return {"size": 0, "aggs":{"users":{"aggs":self.getShowGidsQuery(index, max_count),"filters":{"other_bucket_key":"other_messages","filters":{segmentName:{"bool":{"must":[]}}}}}}}

    def gidsAndForSegment(self, segment, index, max_count, gids):
        attributeJson = segment['attributeJson']
        attributeJsonObj = json.loads(attributeJson)
        segmentName = segment['name']
        apiKey = segment['apiKey']
        esQuery = self.getAndBasicQueryForGids(segmentName, index, max_count)
        for filteredData in attributeJsonObj["queryList"]:
            dataQuery = self.getBasicFilteredQuery()

            # don't add the api query for the api aggregated API.
            if str(apiKey) != self.aggregatedApi:
                dataQuery["bool"]["must"].append(self.getApiQuery(apiKey))
            
            # add support for the distance.
            if "distance" in filteredData and "value" in filteredData["distance"] and filteredData["distance"]["value"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDistanceQuery(filteredData["distance"]["value"]))
            
            #dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getGidsQuery(gids))
            # add support for the visits.
            if "visit" in filteredData and "value" in filteredData["visit"] and filteredData["visit"]["value"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getVisitsQuery(filteredData["visit"]["value"]))

            # add support for the pin.
            if "pin" in filteredData and filteredData["pin"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getPinCodeQuery(filteredData["pin"]))

            # add support for the city.
            if "city" in filteredData and filteredData["city"]:
                dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getCityQuery(filteredData["city"]))
           
            # add support for the timestamp.
            if "duration" in filteredData:
                if filteredData["duration"]["type"] == "week":
                    now = datetime.datetime.now()
                    lastWeeksTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(weeks = -1)).strftime("%s%f")) / 1000)
                    currentTimestampInMillis = int(int(now.strftime("%s%f")) / 1000)
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(lastWeeksTimestampInMillis, currentTimestampInMillis))
                
                elif filteredData["duration"]["type"] == "month":
                    now = datetime.datetime.now()
                    lastMonthsTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(months = -1)).strftime("%s%f")) / 1000)
                    currentTimestampInMillis = int(int(now.strftime("%s%f")) / 1000)
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(lastMonthsTimestampInMillis, currentTimestampInMillis))

                elif filteredData["duration"]["type"] == "custom":
                    dataQuery["bool"]["must"][0]["nested"]["query"]["bool"]["must"].append(self.getDateRangeQuery(int(filteredData["duration"]["startDate"]),int(filteredData["duration"]["endDate"])))

            # add support for the category.
            if "dimension" in filteredData and "name" in filteredData["dimension"] and "cat" == filteredData["dimension"]["name"]:
                dataQuery["bool"]["must"].append(self.getCategoryQuery(filteredData["dimension"]["value"]))

            esQuery["aggs"]["users"]["filters"]["filters"][segmentName]["bool"]["must"].append(dataQuery)
            esQuery["aggs"]["users"]["filters"]["filters"][segmentName]["bool"]["must"].append(self.getGidsQuery(gids))
        self.logger.debug (json.dumps(esQuery))

        return esQuery

    def getAndGidsForSegement(self, segment, totalCount, gids):
        all_gids = []
        max_count = math.ceil(totalCount/9000)

        for i in range(0, max_count):
            response = self.elasticSearchClient.getDataFromElasticSearch(self.gidsAndForSegment(segment, i, max_count, gids))
            responseJson = json.loads(response.decode('utf8').replace("'", '"'))
            self.logger.info(responseJson)
            all_gids = all_gids + self.getGidsRevertSegmentJson(segment["name"], responseJson, segment["apiKey"])

        return all_gids

    def getNewAndCountForSegement(self, segment):
        limit = len(json.loads(segment['attributeJson'])['queryList'])
        #import code; code.interact(local=dict(globals(), **locals()))
        first_query = {'description': 'new' ,'name': 'new test', 'attributeJson': json.dumps({ 'queryList': [json.loads(segment['attributeJson'])['queryList'][0]] }), 'apiKey': '33'}
        #import code; code.interact(local=dict(globals(), **locals()))
        countJson = self.getCountForSegment(first_query)
        countGids = countJson[segment['name']]['value']
        gids = self.getGidsForSegement(first_query, countGids)
        
        for i in range(1, limit):
            query = {'description': 'new' ,'name': 'new test', 'attributeJson': json.dumps({ 'queryList': [json.loads(segment['attributeJson'])['queryList'][i]] }), 'apiKey': '33'}
            #import code; code.interact(local=dict(globals(), **locals()))
            countJson = self.getNewCountForSegment(query, gids)
            countGids = countJson[segment['name']]['value']
            new_gids = self.getAndGidsForSegement(query, countGids, gids)      
            final_gids = set(gids) & set(new_gids)
            gids = list(final_gids)

        gidCountArr = [len(gids)]
        return gidCountArr

#obj = ElasticSearchSegment()
#segment = {"description":"Users who frequent auto dealerships and car accessory showrooms","id":120,"name":"Auto Enthusiast","attributeJson":"{\"queryList\":[{\"dimension\":{\"name\":\"cat\",\"value\":\"20\"},\"visit\":{\"condition\":\"ge\",\"value\":\"1\"},\"distance\":{\"condition\":\"lt\",\"value\":\"100\"},\"duration\":{\"type\":\"month\",\"startDate\":1533799128414,\"endDate\":1534403928414},\"city\":\"5\"},{\"dimension\":{\"name\":\"cat\",\"value\":\"8\"},\"visit\":{\"condition\":\"ge\",\"value\":\"1\"},\"distance\":{\"condition\":\"lt\",\"value\":\"100\"},\"duration\":{\"type\":\"week\",\"startDate\":1533799128414,\"endDate\":1534403928414},\"city\":\"5\"}],\"segmentVisit\":\"\"}","apiKey":29}
#obj.logger.info("segment complete Json: %s", json.dumps(obj.getCountForSegment(segment)))

