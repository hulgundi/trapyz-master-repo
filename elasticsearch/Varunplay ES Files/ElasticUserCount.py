from ElasticSearchWrapper import ElasticSearchWrapper
from MysqlDBWrapper import MysqlDBWrapper
import logging
import json
import datetime
import dateutil.relativedelta

class ElasticUserCount(object):
    def __init__(self, mysqlDBWrapper, aggregatedApi = "33"):
        self.elasticSearchClient = ElasticSearchWrapper(indexName = "geodataprocessedinsights")
        self.logger = logging.getLogger("analytics_log")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler("UserCount.log")
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        self.aggregatedApi = aggregatedApi

    def getApiQuery(self, apiKey):
        return {"term":{"apiKey":{"value":apiKey}}}

    def getCardinalityQuery(self):
        return {"DISTINCT":{"cardinality":{"field":"gid", "precision_threshold": 100}}}

    def getTimestampQueryOnCreatedAt(self, timestamp):
        return {"nested":{"query":{"range":{"info.createdAt":{"gte":timestamp,"format":"epoch_millis"}}},"score_mode":"avg","path":"info"}}       

    def generateTotalUsersQueryWithinTimestampForApi(self, timestamp, apiKey):
        if self.aggregatedApi == str(apiKey):
            esQuery = {"size":0,"aggs":self.getCardinalityQuery(),"query":self.getTimestampQueryOnCreatedAt(timestamp)}
        else:
            esQuery = {"size":0,"aggs":self.getCardinalityQuery(),"query":{"bool":{"must":[self.getTimestampQueryOnCreatedAt(timestamp),self.getApiQuery(apiKey)]}}}
        self.logger.info("ESQuery: %s for timestamp %d with apiKey: %s", json.dumps(esQuery), timestamp, apiKey)
        return esQuery

    def getTotalUsersForApi(self, apiKey):
        if self.aggregatedApi == str(apiKey):
            esQuery = {"size":0,"aggs":self.getCardinalityQuery()}
        else:
            esQuery = {"size":0,"aggs":self.getCardinalityQuery(),"query":self.getApiQuery(apiKey)}
        self.logger.info("ESQuery: %s for apiKey: %s", json.dumps(esQuery), apiKey)
        return esQuery

    def getUsersHelper(self, esQuery):
        usersResponse = self.elasticSearchClient.getDataFromElasticSearch(esQuery)
        usersJson = json.loads(usersResponse.decode('utf8').replace("'", '"'))
        return usersJson["aggregations"]["DISTINCT"]["value"]

    def getUsersQuery(self, apiKey):
        now = datetime.datetime.now()
        lastWeeksTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(weeks = -1)).strftime("%s%f")) / 1000)
        lastMonthsTimestampInMillis = int(int((now + dateutil.relativedelta.relativedelta(months = -1)).strftime("%s%f")) / 1000)
        return (self.getUsersHelper(self.getTotalUsersForApi(apiKey)), self.getUsersHelper(self.generateTotalUsersQueryWithinTimestampForApi(lastWeeksTimestampInMillis, apiKey)), self.getUsersHelper(self.generateTotalUsersQueryWithinTimestampForApi(lastMonthsTimestampInMillis, apiKey)))
 
#obj = ElasticUserCount(MysqlDBWrapper())
#print ("Total Users: %d, Last Week Active Users: %d, Last Month Active Users: %d" % obj.getUsersQuery("29"))

