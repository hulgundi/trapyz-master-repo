import boto3
from datetime import datetime, timedelta
import logging
import random
import requests
import json
import itertools
from requests_aws4auth import AWS4Auth

class ElasticSearchWrapper(object):
    def __init__(self, indexName = "geodataprocessedinsights"):
        self.elasticSearchEndpoint = "https://search-geouserdata-n4qdbepki32rsntb5v3ww6ordu.us-east-1.es.amazonaws.com/"
        self.indexName = indexName
        self.maxDistanceSupported = 300
        self.logger = logging.getLogger("elasticsearch_log")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler("elasticsearch.log")
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        # logger.debug("Instantiated ProcessedUserDataHelper")
    
    def send_signed(self, method, url, service="es", region="us-east-1", body=None):
        credentials = boto3.Session().get_credentials()
        auth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)
        fn = getattr(requests, method)
        # WE MIGHT HAVE STORED THIS "\n" in the data in elastic search for dates 19th July 2018, 2nd July 2018, 3th July 2018, 4th July 2018, 5th July 2018, 6th July 2018 
        # so, if we would have to pull out the document then we would have to replace last characters of the html content which is returned.
        # this }}\n}]}} should become }}}]}}
        #if body and not body.endswith("\n"):
        #    body += "\n"
        response = fn(url, auth=auth, data=body, headers={"Content-Type":"application/json"})
        self.logger.debug ("response: %s", response)
        if response.status_code == 200 or response.status_code == 201:
            htmlBody = response.content
            self.logger.debug ("html body: %s", htmlBody) 
            return htmlBody

        return None

    def storeDataInElasticSearch(self, processedInsightObj):
        apiKey, date, hourMin = processedInsightObj["apiKey_date_hour_min"].split("_")
        dateHourMin = datetime.strptime(date + hourMin, "%Y%m%d%H%M").strftime("%Y-%m-%dT%H:%M")
        processedInsight = {"apiKey" : str(apiKey), "dateHourMin" : str(dateHourMin), "categoryId" : str(processedInsightObj["categoryId"]), "gid" : str(processedInsightObj["gid"]), "info" : {"city" : str(processedInsightObj["info"]["city"]), "createdAt" : int(processedInsightObj["info"]["created_at"]), "distance" : int(processedInsightObj["info"]["distance"]), "pin" : str(processedInsightObj["info"]["pin"]), "updatedAt" : int(processedInsightObj["info"]["updated_at"]), "visits" : int(processedInsightObj["info"]["visits"])}, "storeUuid" : str(processedInsightObj["storeUuid"]), "subcategoryId" : str(processedInsightObj["subcategoryId"])}
        if processedInsight["info"]["distance"] <= self.maxDistanceSupported:
            gid = processedInsight["gid"]
            url = self.elasticSearchEndpoint + self.indexName + "/_doc/" + gid + "_" + processedInsightObj["apiKey_date_hour_min"] + "_" + processedInsightObj["storeUuid"]
            self.logger.debug ("url: %s data : %s", url, json.dumps(processedInsight))
            self.send_signed("post", url, body=json.dumps(processedInsight))

    def getDataFromElasticSearch(self, queryDict):
        url = self.elasticSearchEndpoint + self.indexName + "/_search"
        self.logger.debug ("url: %s data : %s", url, json.dumps(queryDict))
        return self.send_signed("get", url, body=json.dumps(queryDict))
 
    def getDataFromElasticSearchMumbai(self, queryDict):
        url = self.elasticSearchEndpoint + self.indexName + "/_search?scroll=1m"
        self.logger.debug ("url: %s data : %s", url, json.dumps(queryDict))
        return self.send_signed("get", url, body=json.dumps(queryDict))

#obj = ElasticSearchWrapper(indexName = "geodataprocessedinsights")

#obj = ElasticSearchWrapper()
#obj.storeDataInElasticSearch({"subcategoryId":"139","storeUuid":"37637","info":{"city":"3","distance":187,"visits":1,"pin":"2513","created_at":1530537714834,"updated_at":1530701828},"gid":"varun","date_hour_min_store_api":"20180702_1321_37637_29","categoryId":"34","apiKey_date_hour_min":"29_20180702_1321","apiKey_ident_date":"29_20180702"})
#print (obj.getDataFromElasticSearch({"query":{"term":{"_id":{"value":"varun_29_20180702_1321_37637"}}}}))
#print (obj.getDataFromElasticSearch({"size":0,"aggs":{"users":{"filters":{"other_bucket_key":"other_messages","filters":{"visitsmorethan2":{"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[{"range":{"info.visits":{"gte":3}}},{"range":{"info.distance":{"lte":50}}}]}}}},{"bool":{"should":[{"term":{"categoryId":{"value":"50"}}},{"term":{"categoryId":{"value":"27"}}},{"term":{"categoryId":{"value":"14"}}},{"term":{"categoryId":{"value":"30"}}},{"term":{"categoryId":{"value":"9"}}}]}},{"term":{"apiKey":{"value":"10"}}},{"range":{"dateHourMin":{"gte":"2018-06-01T00:00"}}}]}},"visitsmorethan1":{"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[{"range":{"info.visits":{"gte":2}}},{"range":{"info.distance":{"lte":50}}}]}}}},{"bool":{"should":[{"term":{"categoryId":{"value":"50"}}},{"term":{"categoryId":{"value":"27"}}},{"term":{"categoryId":{"value":"14"}}},{"term":{"categoryId":{"value":"30"}}},{"term":{"categoryId":{"value":"9"}}}]}},{"term":{"apiKey":{"value":"10"}}},{"range":{"dateHourMin":{"gte":"2018-06-01T00:00"}}}]}},"visitsmorethan0":{"bool":{"must":[{"nested":{"path":"info","score_mode":"avg","query":{"bool":{"must":[{"range":{"info.visits":{"lte":1}}},{"range":{"info.distance":{"lte":50}}}]}}}},{"bool":{"should":[{"term":{"categoryId":{"value":"50"}}},{"term":{"categoryId":{"value":"27"}}},{"term":{"categoryId":{"value":"14"}}},{"term":{"categoryId":{"value":"30"}}},{"term":{"categoryId":{"value":"9"}}}]}},{"term":{"apiKey":{"value":"10"}}},{"range":{"dateHourMin":{"gte":"2018-06-01T00:00"}}}]}}}}}}}))

