from ElasticSearchWrapper import ElasticSearchWrapper
from MysqlDBWrapper import MysqlDBWrapper
import logging
import json

class ElasticCategoryUser(object):
    def __init__(self, mysqlDBWrapper, aggregatedApi = "33"):
        self.elasticSearchClient = ElasticSearchWrapper(indexName = "geodataprocessedinsights")
        self.logger = logging.getLogger("analytics_log")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler("categoryUser.log")
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        self.catMap = mysqlDBWrapper.getCategoryMap()
        self.aggregatedApi = aggregatedApi

    def getApiQuery(self, apiKey):
        return {"term":{"apiKey":{"value":apiKey}}}

    def getCategoryQuery(self, categorySize):
        return {"field":"categoryId","size":categorySize}
       
    def getCardinalityQuery(self):
        return {"DISTINCT":{"cardinality":{"field":"gid","precision_threshold": 100}}}

    def getESQuery(self, apiKey, categorySize):
        if (self.aggregatedApi == str(apiKey)):
            esQuery = {"size":0,"_source":"false","aggs":{"GIDS":{"terms":self.getCategoryQuery(categorySize),"aggs":self.getCardinalityQuery()}}}
        else:
            esQuery = {"size":0,"_source":"false","query":self.getApiQuery(apiKey),"aggs":{"GIDS":{"terms":self.getCategoryQuery(categorySize),"aggs":self.getCardinalityQuery()}}}
        self.logger.info("ESQuery:%s",json.dumps(esQuery))
        return esQuery

    def getCategoryUserJson(self, apiKey):
        catUserJSON = {}
        categorySize = len(self.catMap)
        response = self.elasticSearchClient.getDataFromElasticSearch(self.getESQuery(apiKey, categorySize))
        responseJson = json.loads(response.decode('utf8').replace("'", '"'))
        for bucket in responseJson["aggregations"]["GIDS"]["buckets"]:
            category = bucket["key"]
            catName = self.catMap[category]
            catUserJSON[catName] = bucket["DISTINCT"]["value"]
        self.logger.info ("categeory json %s", json.dumps (catUserJSON))
        return catUserJSON

#obj = ElasticCategoryUser(MysqlDBWrapper())
#print(obj.getCategoryUserJson("9"))

