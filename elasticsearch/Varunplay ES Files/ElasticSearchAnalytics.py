from ElasticSearchWrapper import ElasticSearchWrapper
from ElasticCategoryUser import ElasticCategoryUser
from ElasticSearchSegment import ElasticSearchSegment
from MysqlDBWrapper import MysqlDBWrapper
import logging
import json
from ElasticUserCount import ElasticUserCount
import datetime

class ElasticSearchAnalytics(object):
    def __init__(self):
        self.elasticSearchClient = ElasticSearchWrapper(indexName = "geodataprocessedinsights")
        self.logger = logging.getLogger("analytics_log")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler("analytics.log")
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s %(threadName)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
        fh.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        # logger.debug("Instantiated ProcessedUserDataHelper")
        self.mysqlDBWrapper = MysqlDBWrapper()
        self.aggregatedApi = "33";
        self.elasticCategoryUser = ElasticCategoryUser(self.mysqlDBWrapper, self.aggregatedApi)
        self.elasticUserCount = ElasticUserCount(self.mysqlDBWrapper, self.aggregatedApi)
        self.elasticSearchSegment = ElasticSearchSegment(self.aggregatedApi)
        self.readSegmentsFromDb()
        self.segmentCountJsonAgainstApi = {}
        self.categoryJsonAgainstApi = {}
        self.usersMapAgainstApi = {}

    def readSegmentsFromDb(self):
        self.segments = self.mysqlDBWrapper.getPredefinedSegments()

    def readSegmentsFromFile(self, fileName):
        with open(fileName) as f:
            self.segments = json.load(f)

    def updateSegmentCountJson(self, segmentName, segmentCountJson, apiKey):
        if apiKey not in self.segmentCountJsonAgainstApi:
            self.segmentCountJsonAgainstApi[apiKey] = {}
        self.segmentCountJsonAgainstApi[apiKey][segmentName] = segmentCountJson[segmentName]

    def updateCategoryJson(self, apiKey):
        self.categoryJsonAgainstApi[apiKey] = self.elasticCategoryUser.getCategoryUserJson(apiKey)
    
    def updateToDB(self, apiKey):
        recentDate = self.mysqlDBWrapper.getRecentCreatedDateFromMasterPredefinedInsightsForApi(apiKey)
        if recentDate is None or not self.isTheRecentDateToday(recentDate):
            self.logger.info("No data found for apiKey %s, will be creating one.", apiKey)
            self.mysqlDBWrapper.createRowInMasterPredefinedInsights(apiKey, self.segmentCountJsonAgainstApi[apiKey], self.categoryJsonAgainstApi[apiKey], self.usersMapAgainstApi[apiKey])
        else:
            self.mysqlDBWrapper.updateMasterPredefinedInsights(apiKey, recentDate, self.segmentCountJsonAgainstApi[apiKey], self.categoryJsonAgainstApi[apiKey], self.usersMapAgainstApi[apiKey])

    def isTheRecentDateToday(self, recentDate):
        dateInDMYFormat = str(datetime.datetime.strptime(str(recentDate), "%Y-%m-%d %H:%M:%S").date())
        currentDateInDMYFormat = str(datetime.datetime.utcnow().date())
        
        return dateInDMYFormat == currentDateInDMYFormat

    def updateInsightsToDB(self):
        for apiKey in self.getApiKeys():
            self.updateToDB(apiKey)

    def updateUsersMap(self, apiKey):
        self.usersMapAgainstApi[apiKey] = self.elasticUserCount.getUsersQuery(apiKey)

    def getApiKeys(self):
        uniqueApiKeys = set()
        for segment in self.segments:
            uniqueApiKeys.add(segment["apiKey"])
        return uniqueApiKeys

    def searchSegmentData(self):
        for segment in self.segments:
            self.updateSegmentCountJson(segment["name"], self.elasticSearchSegment.getCountForSegment(segment), segment["apiKey"])

    def searchCategoryData(self):
        for apiKey in self.getApiKeys():
            self.updateCategoryJson(apiKey)   

    def searchUsersData(self):
        for apiKey in self.getApiKeys():
            self.updateUsersMap(apiKey)
 
obj = ElasticSearchAnalytics()
obj.searchSegmentData()
obj.searchCategoryData()
obj.searchUsersData()
obj.logger.info("segment complete Json: %s", json.dumps(obj.segmentCountJsonAgainstApi))
obj.logger.info("category compelte Json: %s", json.dumps(obj.categoryJsonAgainstApi))
obj.logger.info("users count complete Json: %s", json.dumps(obj.usersMapAgainstApi))
obj.updateInsightsToDB()

