$(function () {
    getMorris('line', 'line_chart');
    getMorris('bar', 'bar_chart');
    getMorris('area', 'area_chart');
    getMorris('donut', 'donut_chart');
});


function getMorris(type, element) {
    if (type === 'line') {
        Morris.Line({
            element: element,
            data: [{
                'period': '2015 Q3',
                'licensed': 3407,
                'sorned': 660
            }, {
                    'period': '2015 Q2',
                    'licensed': 3351,
                    'sorned': 629
                }, {
                    'period': '2015 Q1',
                    'licensed': 3269,
                    'sorned': 618
                }, {
                    'period': '2016 Q4',
                    'licensed': 3246,
                    'sorned': 661
                }, {
                    'period': '2009 Q4',
                    'licensed': 3171,
                    'sorned': 676
                }, {
                    'period': '2008 Q4',
                    'licensed': 3155,
                    'sorned': 681
                }, {
                    'period': '2007 Q4',
                    'licensed': 3226,
                    'sorned': 620
                }, {
                    'period': '2006 Q4',
                    'licensed': 3245,
                    'sorned': null
                }, {
                    'period': '2005 Q4',
                    'licensed': 3289,
                    'sorned': null
                }],
            xkey: 'period',
            ykeys: ['licensed', 'sorned'],
            labels: ['Licensed', 'Off the road'],
            lineColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)'],
            lineWidth: 3
        });
    } else if (type === 'bar') {
        Morris.Bar({
            element: element,
            data: [{
                x: '2015 Q1',
                y: 3,
                z: 2,
                a: 3
            }, {
                    x: '2015 Q2',
                    y: 2,
                    z: null,
                    a: 1
                }, {
                    x: '2015 Q3',
                    y: 0,
                    z: 2,
                    a: 4
                }, {
                    x: '2015 Q4',
                    y: 2,
                    z: 4,
                    a: 3
                }],
            xkey: 'x',
            ykeys: ['y', 'z', 'a'],
            labels: ['Y', 'Z', 'A'],
            barColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(0, 150, 136)'],
        });
    } else if (type === 'area') {
        Morris.Area({
            element: element,
            data: [{
                period: '2016 Q1',
                Levis: 2666,
                Starbucks: null,
                Lifestyle: 2647
            }, {
                    period: '2016 Q2',
                    Levis: 2778,
                    Starbucks: 2294,
                    Lifestyle: 2441
                }, {
                    period: '2016 Q3',
                    Levis: 4912,
                    Starbucks: 1969,
                    Lifestyle: 2501
                }, {
                    period: '2016 Q4',
                    Levis: 3767,
                    Starbucks: 3597,
                    Lifestyle: 5689
                }, {
                    period: '2015 Q1',
                    Levis: 6810,
                    Starbucks: 1914,
                    Lifestyle: 2293
                }, {
                    period: '2015 Q2',
                    Levis: 5670,
                    Starbucks: 4293,
                    Lifestyle: 1881
                }, {
                    period: '2015 Q3',
                    Levis: 4820,
                    Starbucks: 3795,
                    Lifestyle: 1588
                }, {
                    period: '2015 Q4',
                    Levis: 15073,
                    Starbucks: 5967,
                    Lifestyle: 5175
                }, {
                    period: '2012 Q1',
                    Levis: 10687,
                    Starbucks: 4460,
                    Lifestyle: 2028
                }, {
                    period: '2012 Q2',
                    Levis: 8432,
                    Starbucks: 5713,
                    Lifestyle: 1791
                }],
            xkey: 'period',
            ykeys: ['Levis', 'Starbucks', 'Lifestyle'],
            labels: ['Levis', 'Starbucks', 'Lifestyle'],
            pointSize: 2,
            hideHover: 'auto',
            lineColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(0, 150, 136)']
        });
    } else if (type === 'donut') {
        Morris.Donut({
            element: element,
            data: [{
                label: 'Lifestyle',
                value: 25
            }, {
                    label: 'Reliance Digital',
                    value: 40
                }, {
                    label: 'Starbucks',
                    value: 25
                }, {
                    label: 'Levis',
                    value: 10
                }],
            colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)'],
            formatter: function (y) {
                return y + '%'
            }
        });
    }
}