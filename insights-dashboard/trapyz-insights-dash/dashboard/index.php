<?php

session_start();
header('Access-Control-Allow-Origin: *');

header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers'); 

$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusUid = $sessData['status']['uid'];
    $statusRole = $sessData['status']['role'];
    $statusMsgType = $sessData['status']['type'];

if( !isset($statusRole)) {
    session_destroy();
	 header("Location:../");
	}
	
	} else if(empty($sessData['status']['msg']) && !isset($statusRole) ) {
		
	session_destroy();
	 header("Location:../");	
		
	}
?>


﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Home | Trapyz CONNECT</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>
    <!-- Bootstrap Core Css -->
    <link href="https://connect.trapyz.com/dashboard/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="https://connect.trapyz.com/dashboard/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="https://connect.trapyz.com/dashboard/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="https://connect.trapyz.com/dashboard/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="https://connect.trapyz.com/dashboard/css/themes/all-themes.css" rel="stylesheet" />
    <script src="https://connect.trapyz.com/dashboard/partials/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<style type="text/css">
#chart {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}	
#map {
        height: 100%;
      }
   #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
      #floating-panel {
        background-color: #fff;
        border: 1px solid #999;
        left: 25%;
        padding: 5px;
        position: absolute;
        top: 10px;
        z-index: 5;
      }
</style>

</head>

<body class="theme-blue">
  
    

    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
   <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index">Trapyz CONNECT</a>
            </div>
      <div class="collapse navbar-collapse" id="navbar-collapse">
                           
            
                <ul class="nav navbar-nav navbar-right" style=" margin-top: 15px;">
                
          
                


  <li  style="margin-top: -15px;">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">



                                  
                                    <li>
                                        <a href="../../userAccount?logoutSubmit=1">
                                           <i class="material-icons">input</i>Sign Out
                                        </a>
                                    </li>







                           
                        </ul>
                    </li>



                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
  
  <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            

         <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>

                    <li>
                        <a href="index">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                     
                   <li>
                        <a href="partials/editor">
                            <i class="material-icons">map</i>
                            <span> Editor</span>
                        </a>
                    </li> 
                    <li>
                        <a href="partials/analytics.v2">
                            <i class="material-icons">trending_up</i>
                            <span> Analytics</span>
                        </a>
                    </li> 
                                                 
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz CONNECT</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.4
                </div>
            </div>
            <!-- #Footer -->
        </aside>
      
    </section>

    <section class="content">


     <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content" id="usercount">
                            <div class="text">Users</div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">local_offer</i>
                        </div>
                        <div class="content" id="avgdwell">
                            <div class="text">Avg Dwell (mins)</div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">notifications</i>
                        </div>
                        <div class="content" id="mostsearched">
                            <div class="text">Most Searched</div>

                        </div>
                    </div>
                </div>
               
            </div>
            
            
  
    
                     
    
                   <div class="card"> <div class="container-fluid">
            
                  

            <div class="row clearfix" >
                            <div >







<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    
<br>
<br>                    
                    <div class="card">
                        <div class="body bg-teal" id="top5search" style="height: 250px;">
                            <div class="font-bold m-b--35">TOP 5 MOST SEARCHED </div>
                            
                        </div>
                    </div>
                </div>  
<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
<br>
<br>                    
     
                    <div class="card">
                        <div class="body bg-cyan" id="top5visit" style="height: 250px;">
                            <div class="m-b--35 font-bold">TOP 5 MOST VISITED (mins)</div>
                            
                        </div>
                    </div>
                </div>                
                


      </div>     
      	</div>
         <div class="row">
		<div id="chartdiv" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>      
                 </div>
   
                 
                 </div>
                 

                        



</div>



    </section>



    <script src="https://connect.trapyz.com/dashboard/partials/bial.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <!-- Bootstrap Core Css 
   <script src="https://connect.trapyz.com/dashboard/plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <script src="https://connect.trapyz.com/dashboard/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="https://connect.trapyz.com/dashboard/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="https://connect.trapyz.com/dashboard/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="https://connect.trapyz.com/dashboard/plugins/node-waves/waves.js"></script>



    
    <script src="https://connect.trapyz.com/dashboard/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="https://connect.trapyz.com/dashboard/plugins/dropzone/dropzone.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="https://connect.trapyz.com/dashboard/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Multi Select Plugin Js -->
    <script src="https://connect.trapyz.com/dashboard/plugins/multi-select/js/jquery.multi-select.js"></script>


    <script src="https://connect.trapyz.com/dashboard/plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="https://connect.trapyz.com/dashboard/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>


    <!-- Custom Js -->
    <script src="https://connect.trapyz.com/dashboard/js/admin.js"></script>
    <script src="https://connect.trapyz.com/dashboard/plugins/jquery-countto/jquery.countTo.js"></script>
    <script src="https://connect.trapyz.com/dashboard/js/pages/index.js"></script>
    <!-- Demo Js -->
    <script src="https://connect.trapyz.com/dashboard/js/demo.js"></script>
</body>

</html>