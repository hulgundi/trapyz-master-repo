<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusUid = $sessData['status']['uid'];
    $statusRole = $sessData['status']['role'];
    $statusMsgType = $sessData['status']['type'];

	} else if(empty($sessData['status']['msg']) && !isset($statusRole) ) {
		
	session_destroy();
	 header("Location:../../");	
		
	}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Push | Trapyz CONNECT</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
      <script src="../plugins/jquery/jquery.min.js"></script>


<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>

    <!-- Google Fonts --><script src="chartcontrol.js"></script>
<script src="https://harvesthq.github.io/chosen/chosen.jquery.js" type="text/javascript"></script>
<link href="https://harvesthq.github.io/chosen/chosen.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">





    <!-- Bootstrap Spinner Css -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/sweetalert/sweetalert.min.js"></script>



	<script src="https://cloud.github.com/downloads/uxebu/bonsai/bonsai-0.4.0.min.js"></script>
<script src="https://d3js.org/d3.v3.min.js"></script>


    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->

    <link href="../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
    
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>



    <!-- Custom Js -->
    <script src="../js/admin.js"></script>

</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../index">Trapyz CONNECT</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">


   <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">



                                    <li>
                                        <a href="javascript:void(0);">
                                          <i class="material-icons">person</i>Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="../../userAccount?logoutSubmit=1">
                                           <i class="material-icons">input</i>Sign Out
                                        </a>
                                    </li>







                           
                        </ul>
                    </li>

                   
                 
                  


                </ul> 
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info
            <div class="user-info">
                <div class="image">
                    <img src="../../images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
                    <div class="email">john.doe@example.com</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>  {enableHighAccuracy:false,maximumAge:Infinity, timeout:60000}
                    </div>
                </div>
            </div>
 -->
            <!-- Menu -->

      <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>
                    <li>
                        <a href="../index">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="editor">
                            <i class="material-icons">map</i>
                            <span>Editor</span>
                        </a>
                    </li>
                    <li>
                        <a href="analytics.v2">
                            <i class="material-icons">trending_up</i>
                            <span>Analytics</span>
                        </a>
                    </li> 
                                    <li>
                        <a href="shops">
                            <i class="material-icons">shopping_cart</i>
                            <span>Stores</span>
                        </a>
                    </li>       
                   <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">message</i>
                            <span>Push Notification</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                        <a href="push">
                                    <span>Create Campaign</span>
                                </a>
                            </li>
                            <li>
                                <a href="campaigns">
                                    <span>All Campaigns</span>
                                </a>
                            </li>
                          
                        </ul>
                    </li>    
                      <li> 
                        <a href="users">
                            <i class="material-icons">group_add</i>
                            <span>Users</span>
                        </a>
                    </li>                
                  <!--  <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Notifications <small>coming soon!</small></span>
                        </a>
                        
                    </li> -->
                  
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz CONNECT</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.1.7
                </div>
            </div>
            <!-- #Footer -->
        </aside>
      
    </section>

    <section class="content">
       <div id='console' style="display: none;"></div>
   <div class="card"> <div class="container-fluid">
            
                  
            <div class="row clearfix" >
                            <div >
            
<div id="selectdiv" style="display: none;"></div>            
            
    
</div>




                     

 <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 " > 
        
           
           
           <div id="input" >   

            <br>
            <br>






                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input class="form-control" id="title" required aria-required="true" onkeyup="myFunction()" type="text">
                                        <label class="form-label" style="font-size: 9px;">Message Title</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea id="body" cols="30" rows="5" class="form-control no-resize" required onkeyup="myFunction()" aria-required="true"></textarea>
                                        <label class="form-label" style="font-size: 9px;">Description</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="targetUrl" class="form-control"  required aria-required="true" type="text">
                                        <label class="form-label" style="font-size: 9px;">Target Url</label>
                                    </div>
                                </div>                                
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="icon" class="form-control" required onkeyup="myFunction()" aria-required="true" type="text">
                                        <label class="form-label" style="font-size: 9px;">Icon Url (ideally 192x192 or less px)</label>
                                    </div>
                                </div>
                                
                             
      

        <select data-placeholder="Select Tags" multiple="" class="form-control chosen-select-width" tabindex="16" id="test">

            
          </select>
  <div id="selectedlist" style="display:none;"></div>
          <span style="padding-left: 10px;"></span>
          
          
          <button class="btn btn-danger waves-effect" id="c-all">Clear All</button> 
                    <span style="padding-left: 10px;"></span>
          <button class="btn btn-success waves-effect" id="s-all">Select All</button>
          <br>
<div id="selectedlist" style="display:none;"></div>
           </div>
    
                <div id="pushdiv">  <input id="savecamp" class="filled-in chk-col-green" onclick="validate();" type="checkbox">
                <label for="savecamp">Save Campaign?</label>
                <br>
 <div class="form-group form-float" id="campdiv" style="display: none;">  
 
 
                
              <div class="form-line">
                            <br>            <input class="form-control" id="campname" aria-required="true" type="text">
                                        <label class="form-label" style="font-size: 11px;">Campaign Name</label>
                                    </div>  
                
                
                </div>
                
                
                
                
                </div>


<script type="text/javascript"> function myFunction() {
var titleinput = document.getElementById("title");
var bodyinput = document.getElementById("body");
var ptitle = document.getElementById("ptitle");
var pbody = document.getElementById("pbody");
var picon = document.getElementById("imgicon");
var iconurlsrc = document.getElementById("icon");

ptitle.innerHTML = titleinput.value; 
pbody.innerHTML = bodyinput.value; 

if (iconurlsrc.value) {
	picon.setAttribute("src", iconurlsrc.value);
	console.log("this");
}


}
</script>





</div>             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 " > 
<div class="row clearfix" > 

<br> <br> <br> <span> Message Preview </span><div class="card">  
 <div class="container-fluid">
<div class="col-lg-4 col-md-4" style="padding-right: 2px; padding-left: 2px;">
<img style="max-height: 100px;max-width:100px;align-self: center; margin-top: 5px;" src="default.png" id="imgicon">

</div>
<div class="col-lg-8 col-md-8">
<div class="row">
                      <div class="col-lg-10 col-md-10 " >               <p  style="margin-bottom: 5px; margin-top: 3px;text-align: left; margin-right: 15px; font-size: 10px; width: 85%; overflow: hidden; text-overflow: ellipsis;" id="ptitle"></p> </div>
                     <div class="col-lg-2 col-md-2 " >                 <p style="font-size:12px;"><i class="fa fa-times" aria-hidden="true"></i></p> </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-10 col-md-10 " >  <p style="margin-bottom: 5px;text-align: left; margin-right: 15px; font-size: 10px; width: 85%; height: 40px; overflow: hidden; text-overflow: ellipsis;" id="pbody"></p> </div>
                                      <div class="col-lg-2 col-md-2 " >  </div>
                                    </div> <div class="row">     
                                    <div class="col-lg-10 col-md-10 " > 
                                    </div>                              
                                      <div class="col-lg-2 col-md-2 " > <i class="fa fa-cog" aria-hidden="true" style="margin-botton: 3px;"></i> </div>
                                      
                                      </div>


</div>
</div>
</div>
</div>
<div class="row clearfix" > 

<h3> Audience Count</h3>
<span id="audicount" style="font-size: 25px; color: #42C20B;">0</span>
</div>

</div>  

</div>    

</div>        
               
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 " > 
                <input id="object" type="text" style="display: none;"/>
                <div id="player" style="display: none;"></div>
<script type="bs" id="bs">


function Sector(x, y, radius, startAngle, endAngle) {
  SpecialAttrPath.call(this, {
    radius: 0,
    startAngle: startAngle,
    endAngle: endAngle
  });
  this.attr({
    x: x,
    y: y,
    radius: radius,
    startAngle: startAngle,
    endAngle: endAngle
  });
}

Sector.prototype = Object.create(SpecialAttrPath.prototype);
Sector.prototype._make = function() {

  var attr = this._attributes,
      radius = attr.radius,
      startAngle = attr.startAngle,
      endAngle = attr.endAngle;

  var startX, startY, endX, endY;
  var diffAngle = Math.abs(endAngle - startAngle);

  this.startX = startX = radius * Math.cos(startAngle);
  this.startY = startY = radius * Math.sin(startAngle);
  if (diffAngle < Math.PI*2) {
    endX = radius * Math.cos(endAngle);
    endY = radius * Math.sin(endAngle);
  } else { // angles differ by more than 2*PI: draw a full circle
    endX = startX;
    endY = startY - .0001;
  }

  this.endX = endX;
  this.endY = endY;

  this.radiusExtentX = radius * Math.cos(startAngle + (endAngle - startAngle)/2);
  this.radiusExtentY = radius * Math.sin(startAngle + (endAngle - startAngle)/2);

  return this.moveTo(0, 0)
    .lineTo(startX, startY)
    .arcTo(radius, radius, 0, (diffAngle < Math.PI) ? 0 : 1, 1, endX, endY)
    .lineTo(0, 0);

};

Sector.prototype.getDimensions = function() {
  var x = this.attr('x'),
      y = this.attr('y'),
      left = Math.min(x, x + this.startX, x + this.endX, x + this.radiusExtentX),
      top = Math.min(y, y + this.startY, y + this.endY, y + this.radiusExtentY),
      right = Math.max(x, x + this.startX, x + this.endX, x + this.radiusExtentX),
      bottom = Math.max(y, y + this.startY, y + this.endY, y + this.radiusExtentY);
  console.log(y, y + this.startY, y + this.endY, y + this.radiusExtentY)
  return {
    left: left,
    top: top,
    width: right - left,
    height: bottom - top
  };
};


PieChart.BASE_COLOR = color('red');

function PieChart(data) {
  this.angle = 0;
  this.labelY = 30;
  this.kolor = PieChart.BASE_COLOR.clone();
  var n = 0;
  for (var i in data) {
    this.slice(i, data[i], n++);
  }
}

PieChart.prototype = {
  slice: function(name, value, i) {

    var start = this.angle,
        end = start + (Math.PI*2) * value/100,
        // Increase hue by .1 with each slice (max of 10 will work)
        kolor = this.kolor = this.kolor.clone().hue(this.kolor.hue()+.1);

    var s = new Sector(
      350, 200, 100,
      start,
      end
    );

    var animDelay = (i * 200) + 'ms';

    var label = this.label(name, value, kolor);

    label.attr({ opacity: 0 });

    s.stroke('#FFF', 3);
    s.fill(kolor);

    s.attr({
      endAngle: start,
      radius: 0
    }).addTo(stage).on('mouseover', over).on('mouseout', out);
    label.on('mouseover', over).on('mouseout', out);

    function over() {
      label.text.attr('fontWeight', 'bold');
      label.animate('.2s', {
        x: 40
      });
      s.animate('.2s', {
        radius: 170,
        fillColor: kolor.lighter(.1)
      }, {
        easing: 'sineOut'
      });
    }

    function out() {
      label.text.attr('fontWeight', '');
      label.animate('.2s', {
        x: 30
      });
      s.animate('.2s', {
        radius: 150,
        fillColor: kolor
      });
    }

    s.animate('.4s', {
      radius: 150,
      startAngle: start,
      endAngle: end
    }, {
      easing: 'sineOut',
      delay: animDelay
    });

    label.animate('.4s', {
      opacity: 1
    }, { delay: animDelay });

    this.angle = end;
  },

  label: function(name, v, fill) {

    var g = new Group().attr({
      x: 30,
      y: this.labelY,
      cursor: 'pointer'
    });

    var t = new Text(name ).addTo(g);
    var r = new Rect(0, 0, 20, 20, 5).fill(fill).addTo(g);

    t.attr({
      x: 30,
      y: 17,
      textFillColor: 'black',
      fontFamily: 'Arial',
      fontSize: '14'
    });

    g.addTo(stage);
    this.labelY += 30;

    g.text = t;

    return g;
  }
};







new PieChart(testobj1);

</script>



</div>
      </div>          
                        <div id="tablediv" class="body" style="display:none;" >
                            <table  id="table" class="table table-bordered table-striped table-hover dataTable js-exportable">
                              
                              
                            </table>
                        </div>
                        
 
              <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div id="skills">
      <div id="skillmap">
        <div class="skills-wrapper">
          <div class="skills-sunburst"></div>
          <div class="skills-chart">
            <div id="skills-chart-breadcrumb"></div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="skillsdata.js"></script>
    <script type="text/javascript" src="skills.js"></script>
           
            </div> -->
                                  
                        
                        
                   

            </div>
               



</div>

    </section>




</body>

</html>