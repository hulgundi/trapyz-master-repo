<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusUid = $sessData['status']['uid'];
    $statusRole = $sessData['status']['role'];
    $statusMsgType = $sessData['status']['type'];

	} else if(empty($sessData['status']['msg']) && !isset($statusRole) ) {
		
	session_destroy();
	 header("Location:../../");	
		
	}
?>

﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Stores List | Trapyz CONNECT</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Google Fonts -->
<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../index">Trapyz CONNECT</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">


   <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">



                                    <li>
                                        <a href="javascript:void(0);">
                                          <i class="material-icons">person</i>Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="../../userAccount?logoutSubmit=1">
                                           <i class="material-icons">input</i>Sign Out
                                        </a>
                                    </li>







                           
                        </ul>
                    </li>

                   
                 
                  


                </ul> 
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info
            <div class="user-info">
                <div class="image">
                    <img src="../../images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
                    <div class="email">john.doe@example.com</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
 -->
            <!-- Menu -->

         <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>

                    <li>
                        <a href="../index">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="editor">
                            <i class="material-icons">map</i>
                            <span>Editor</span>
                        </a>
                    </li>
                    <li>
                        <a href="analytics.v2">
                            <i class="material-icons">trending_up</i>
                            <span>Analytics</span>
                        </a>
                    </li>
                                        <li>
                        <a href="shops">
                            <i class="material-icons">shopping_cart</i>
                            <span>Stores</span>
                        </a>
                    </li>    
 <li>
                        <a href="push">
                            <i class="material-icons">message</i>
                            <span>Push Notfication</span>
                        </a>
                    </li>                   
             <!--       <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Notifications <small>coming soon!</small></span>
                        </a>
                        
                    </li> -->
                  
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz CONNECT</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.1.7
                </div>
            </div>
            <!-- #Footer -->
        </aside>
      
    </section>
<script type="text/javascript">

var xhr1;
   function getstores() {

 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr1 = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr1 = new ActiveXObject("Microsoft.XMLHTTP");
}
var data;
     xhr1.open("GET", "../../get_stores.php", true); 
     xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr1.send(data);
	 xhr1.onreadystatechange = display_data;
	function display_data() {
	 if (xhr1.readyState == 4) {
      if (xhr1.status == 200) {

  test2(xhr1.responseText);   

      } else {

      }
     }
	}
} 

function test2(readinput) {	

var a = readinput;	
var array = a.split("\n");
var names = [];

for  (var i =0, c = array.length; i < c; i++ ) {
	
var b = array[i].split(":");
var g = {
name: b[0],
floorno: b[1]

}
names.push(g);

	
}

names.pop();
	for (var a = names.length -1; a >= 0; a--) {


if (names[a].name.charAt(4) == 1 ) {

	names.splice(a, 1);
} else if (names[a].name.charAt(4) == 2) {
		names.splice(a, 1);
} 
else if (names[a].name.charAt(4) == 3) {
		names.splice(a, 1);
}
else if (names[a].name.charAt(4) == 4 ) {
		names.splice(a, 1);
} else if (names[a].name == "Fire Exit" )   {
			names.splice(a, 1);
}
else if (names[a].name == "Restrooms" )   {
			names.splice(a, 1);
}
else if (names[a].name == "Rest Rooms" )   {
			names.splice(a, 1);
}

}




    var myTableDiv = document.getElementById("tablediv");

    var table = document.createElement('table');
    table.className += " table";
    table.className += " table-bordered";
    table.className += " table-striped";
    table.className += " table-hover ";
    table.className += " dataTable";
    table.className += " js-exportable";


var thead = document.createElement('thead');
var tfoot = document.createElement('tfoot');

    
 var heading = new Array();
    heading[0] = "Store Name"
    heading[1] = "Floor No."
    heading[2] = "Category"
    heading[3] = "Unique Visitors"
    heading[4] = "Repeat Visitors"

  var tr = document.createElement('TR');
    thead.appendChild(tr);
    for (i = 0; i < heading.length; i++) {
        var th = document.createElement('TH')
        th.width = '350';
        th.appendChild(document.createTextNode(heading[i]));
        tr.appendChild(th);
    }
    table.appendChild(thead);

 var heading = new Array();
     heading[0] = "Store Name"
    heading[1] = "Floor No."
    heading[2] = "Category"
    heading[3] = "Unique Visitors"
    heading[4] = "Repeat Visitors"
  var tr = document.createElement('TR');
    tfoot.appendChild(tr);
    for (i = 0; i < heading.length; i++) {
        var th = document.createElement('TH')
        th.width = '350';
        th.appendChild(document.createTextNode(heading[i]));
        tr.appendChild(th);
    }
    table.appendChild(tfoot);
    
    var tbody = document.createElement('tbody');
    for (var i = 0; i < names.length; i++) {

    var row = document.createElement('TR');
    var td1 = document.createElement('Td');
    var td2 = document.createElement('Td');
    var td3 = document.createElement('Td');
    var td4 = document.createElement('Td');
    var td5 = document.createElement('Td');
    td1.width = '350';
    td2.width = '350';
    td3.width = '350';
    td4.width = '350';
    td5.width = '350';
    var a = Math.floor(10000 + Math.random() * 90000)
    a = a.toString().substring(0, 3);
    var b = Math.floor(1000 + Math.random() * 9000)
    b = b.toString().substring(0, 2);
    var myArray = ['Apparel', 'Food-Beverage', 'Books', 'Electronics', 'Toys', 'Sports', 'Footwear'];    
    var rand = myArray[Math.floor(Math.random() * myArray.length)];


    td1.appendChild(document.createTextNode(names[i].name));
    td2.appendChild(document.createTextNode(names[i].floorno));
    td3.appendChild(document.createTextNode(rand));
    td4.appendChild(document.createTextNode(a));
    td5.appendChild(document.createTextNode(b));
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    tbody.appendChild(row);
    table.appendChild(tbody);
   
   
   
   
   }
    
    
    
    





  

myTableDiv.appendChild(table);

	}



 window.onload = function() {

               getstores();
               
                
            };  
</script>
<script type="text/javascript">

/**
$(document).ready( function () {
    $('#table').DataTable();
} );**/
</script>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                   STORES LIST

                </h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">

                                     



                                
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div id="tablediv" class="body">
                            <table  id="table" class="table table-bordered table-striped table-hover dataTable js-exportable">
                              
                              
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>


    <!-- Custom Js -->
    <script src="../js/admin.js"></script>


    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
</body>

</html>