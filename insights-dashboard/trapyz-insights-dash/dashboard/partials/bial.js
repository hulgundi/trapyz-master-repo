var xhr;
var xhr1;
var xhr2;
var users = [];
var queries = [];
var locations = [];
var mostvisited = [];
var mostsearched = [];
var heatmaplocations = [];
var divusercount = document.getElementById("usercount");
var divavgdwell = document.getElementById("avgdwell");
var divmostsearched = document.getElementById("mostsearched");
function getUserdata() {

 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data;
       xhr.open("GET", "../../getuser.php", true);
  	    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                      
    	 xhr.send(data);
		 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
  setUsers(xhr.responseText);   
 
      } else {

      }
     }
	}
} 

function getQuerydata() {

 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr1 = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr1 = new ActiveXObject("Microsoft.XMLHTTP");
}
var data;
       xhr1.open("GET", "../../getquery.php", true);
  	    xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                      
    	 xhr1.send(data);
		 xhr1.onreadystatechange = display_data;
	function display_data() {
	 if (xhr1.readyState == 4) {
      if (xhr1.status == 200) {
  setQueries(xhr1.responseText);   
 
      } else {

      }
     }
	}
} 

function getLocdata() {

 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr2 = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr2 = new ActiveXObject("Microsoft.XMLHTTP");
}
var data;
       xhr2.open("GET", "../../getloc.php", true);
  	    xhr2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                      
    	 xhr2.send(data);
		 xhr2.onreadystatechange = display_data;
	function display_data() {
	 if (xhr2.readyState == 4) {
      if (xhr2.status == 200) {
  setLocations(xhr2.responseText);   
 
      } else {

      }
     }
	}
} 
function setUsers(readinput) {	
	
 users = JSON.parse(readinput);
 var usersdate = [];
for(var i =0; i < users.length; i++){

users[i].createdAt = formatDate(users[i].createdAt);
}

  var dateobj = {};
  for (var i = 0, j = users.length; i < j; i++) {
    dateobj[users[i].createdAt] = (dateobj[users[i].createdAt] || 0) + 1;
  }





  var chartdate = Object.assign(dateobj);
  var chartdatekeys = Object.keys(chartdate), chartlen = chartdatekeys.length, k = 0, chartprop, chartvalue;

  
 while (k < chartlen) {
 var dataobj1 = { 
 "date": chartdatekeys[k],
 "column-1": chartdate[chartdatekeys[k]]
};
   usersdate.push(dataobj1);
    k += 1;
  }


var tempdiv1 = document.createElement("div");
  tempdiv1.setAttribute("class", "number count-to");
  tempdiv1.setAttribute("data-from", 0);


var temptextnode1 = document.createTextNode(users.length);       

  tempdiv1.appendChild(temptextnode1);

usersdate.sort(function(b,a){
  //to sort the array based on date (ascending order) for the amcharts to work properly
  return new Date(b.date) - new Date(a.date);
});

  divusercount.appendChild(tempdiv1);

		AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "date",
					"dataProvider" : usersdate,
					"dataDateFormat": "YYYY-MM-DD",
					"theme": "default",
					"categoryAxis": {
						"minPeriod": "DD",
						"parseDates": true
					},
					"chartCursor": {
						"enabled": true,
						"categoryBalloonDateFormat": "DD MMM YYYY"
					},
					"chartScrollbar": {
						"enabled": true
					},
					"trendLines": [],
					"graphs": [
						{
							"bullet": "round",
							"id": "AmGraph-1",
							"title": "Total Users",
							"valueField": "column-1"
						}
						
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Total Users"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Total Users per day"
						}
					]
					
				}
			);



}

function formatDate(date) {
  var myDate =  new Date(date);

var year = myDate.getFullYear();

var month = myDate.getMonth() + 1;
if(month <= 9)
    month = '0'+month;

var day= myDate.getDate();
if(day <= 9)
    day = '0'+day;

var prettyDate = year.toString() +'-'+ month.toString() +'-'+ day.toString();

return prettyDate;
}
function setQueries(readinput) {	

queries = JSON.parse(readinput);
/**
 var tempobj = {};
  for (var i = 0, j = queries.length; i < j; i++) {
    tempobj[queries[i].query] = (tempobj[queries[i].query] || 0) + 1;

  }
  console.log(tempobj); **/

var temp = foo(queries);
var finalarray =[];
for (var i = 0; i < temp[0].length; i++) {
var tempobj = {
query :temp[0][i],
count : temp[1][i]

};
finalarray.push(tempobj);
}

var holder = {};
finalarray.forEach(function (d) {
    if(holder.hasOwnProperty(d.query)) {
       holder[d.query] = holder[d.query] + d.count;
    } else {       
       holder[d.query] = d.count;
    }
});


for(var prop in holder) {
    mostsearched.push({query: prop, count: holder[prop]});   
}




mostsearched.sort(function(a, b) {
    return parseFloat(b.count) - parseFloat(a.count);
});
var tempdiv1 = document.createElement("div");
  tempdiv1.setAttribute("class", "number count-to");
  tempdiv1.setAttribute("data-from", 0);


var temptextnode1 = document.createTextNode(mostsearched[0].query );       

  tempdiv1.appendChild(temptextnode1);

  
  divmostsearched.appendChild(tempdiv1);
  
  settop5(mostsearched, "top5search");	
}


  
function foo(arr) {
    var a = [], b = [], prev;
    
    arr.sort();
    for ( var i = 0; i < arr.length; i++ ) {
        if (arr[i].query != prev ) {
            a.push(arr[i].query);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = arr[i].query;
    }
    
    return [a, b];
}

function settop5(array, div) {
	
var top5div = document.getElementById(div);

var top5ul = document.createElement("ul");
top5ul.setAttribute("class", "dashboard-stat-list");  
  
var arraylen = 0;
if (array.length > 4) {

 while (arraylen < 5) {


var top5li = document.createElement("li");
	top5li.innerHTML += '<span style="margin-right: 10px;">' + array[arraylen].query + '</span>';
	
	var top5span = document.createElement("span");
top5span.setAttribute("class", "pull-right");
top5span.innerHTML += "<b>"+	array[arraylen].count + "</b>";
	
top5li.appendChild(top5span);
	
	top5ul.appendChild(top5li);
	
	    arraylen += 1;
  }	
} else {
	
	 while (arraylen < array.length) {

var top5li = document.createElement("li");
	top5li.innerHTML += '<span style="margin-right: 10px;">' + array[arraylen].query + '</span>';
	
	var top5span = document.createElement("span");
top5span.setAttribute("class", "pull-right");
top5span.innerHTML += "<b>"+	array[arraylen].count + "</b>";
	
top5li.appendChild(top5span);
	
	top5ul.appendChild(top5li);
	
		    arraylen += 1;
  }
	
}

top5div.appendChild(top5ul);	
	
}

function settop5visited(array, div) {
	
var top5div = document.getElementById(div);

var top5ul = document.createElement("ul");
top5ul.setAttribute("class", "dashboard-stat-list");  
  
var arraylen = 0;
if (array.length > 4) {

 while (arraylen < 5) {


var top5li = document.createElement("li");
	top5li.innerHTML += '<span style="margin-right: 10px;">' + array[arraylen].name + '</span>';
	
	var top5span = document.createElement("span");
top5span.setAttribute("class", "pull-right");
top5span.innerHTML += "<b>"+	Math.round(array[arraylen].dwell/60) + "</b>";
	
top5li.appendChild(top5span);
	
	top5ul.appendChild(top5li);
	
	    arraylen += 1;
  }	
} else {
	
	 while (arraylen < array.length) {

var top5li = document.createElement("li");
	top5li.innerHTML += '<span style="margin-right: 10px;">' + array[arraylen].name + '</span>';
	
	var top5span = document.createElement("span");
top5span.setAttribute("class", "pull-right");
top5span.innerHTML += "<b>"+	Math.round(array[arraylen].dwell/60) + "</b>";
	
top5li.appendChild(top5span);
	
	top5ul.appendChild(top5li);
	
		    arraylen += 1;
  }
	
}

top5div.appendChild(top5ul);	
	
}

function setLocations(readinput) {	

locations = JSON.parse(readinput);

var ad = 0;
var temp =[];
for(var i =0; i < locations.length; i++){

ad = ad + parseInt(locations[i].dwell);
var tempobj = {
name : locations[i].name,
dwell: locations[i].dwell
}
temp.push(tempobj);
var temploc = {
name : locations[i].name,
lat: locations[i].lat,
lng: locations[i].lng,
}
heatmaplocations.push(temploc);
}


var tempobj2 = {};
var obj = null;
for(var i=0; i < temp.length; i++) {
   obj=temp[i];

   if(!tempobj2[obj.name]) {
       tempobj2[obj.name] = obj;
   } else {
       tempobj2[obj.name].dwell += obj.dwell;
   }
}

for (var prop in tempobj2)
    mostvisited.push(tempobj2[prop]);
mostvisited.sort(function(a, b) {
    return parseFloat(b.dwell) - parseFloat(a.dwell);
});

settop5visited(mostvisited, "top5visit");

ad = Math.round(ad/users.length);
ad = Math.round(ad/60); //to mins
var tempdiv1 = document.createElement("div");
  tempdiv1.setAttribute("class", "number count-to");
  tempdiv1.setAttribute("data-from", 0);


var temptextnode1 = document.createTextNode(ad);       

  tempdiv1.appendChild(temptextnode1);

  
  divavgdwell.appendChild(tempdiv1)	
}
 

	getUserdata();
	getLocdata();
	getQuerydata();
