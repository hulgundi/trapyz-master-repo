var xhr;
var xhr1;
var xhr2;
var users = [];
var queries = [];
var locations = [];
var mostvisited = [];
var mostsearched = [];
var heatmaplocations = [];
var divusercount = document.getElementById("usercount");
var divavgdwell = document.getElementById("avgdwell");
var divmostsearched = document.getElementById("mostsearched");
function getUserdata() {

 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data;
       xhr.open("GET", "../../getuser.php", true);
  	    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                      
    	 xhr.send(data);
		 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
  setUsers(xhr.responseText);   
 
      } else {

      }
     }
	}
} 

function getQuerydata() {

 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr1 = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr1 = new ActiveXObject("Microsoft.XMLHTTP");
}
var data;
       xhr1.open("GET", "../../getquery.php", true);
  	    xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                      
    	 xhr1.send(data);
		 xhr1.onreadystatechange = display_data;
	function display_data() {
	 if (xhr1.readyState == 4) {
      if (xhr1.status == 200) {
  setQueries(xhr1.responseText);   
 
      } else {

      }
     }
	}
} 

function getLocdata() {

 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr2 = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr2 = new ActiveXObject("Microsoft.XMLHTTP");
}
var data;
       xhr2.open("GET", "../../getloc.php", true);
  	    xhr2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                      
    	 xhr2.send(data);
		 xhr2.onreadystatechange = display_data;
	function display_data() {
	 if (xhr2.readyState == 4) {
      if (xhr2.status == 200) {
  setLocations(xhr2.responseText);   
 
      } else {

      }
     }
	}
} 
function setUsers(readinput) {	

 users = JSON.parse(readinput);




}
function setQueries(readinput) {	

queries = JSON.parse(readinput);


var temp = foo(queries);
var finalarray =[];
for (var i = 0; i < temp[0].length; i++) {
var tempobj = {
query :temp[0][i],
count : temp[1][i]

};
finalarray.push(tempobj);
}

var holder = {};
finalarray.forEach(function (d) {
    if(holder.hasOwnProperty(d.query)) {
       holder[d.query] = holder[d.query] + d.count;
    } else {       
       holder[d.query] = d.count;
    }
});


for(var prop in holder) {
    mostsearched.push({query: prop, count: holder[prop]});   
}




mostsearched.sort(function(a, b) {
    return parseFloat(b.count) - parseFloat(a.count);
});

  

}


  
function foo(arr) {
    var a = [], b = [], prev;
    
    arr.sort();
    for ( var i = 0; i < arr.length; i++ ) {
        if (arr[i].query != prev ) {
            a.push(arr[i].query);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = arr[i].query;
    }
    
    return [a, b];
}

function settop5(array, div) {
	
var top5div = document.getElementById(div);

var top5ul = document.createElement("ul");
top5ul.setAttribute("class", "dashboard-stat-list");  
  
var arraylen = 0;
if (array.length > 4) {

 while (arraylen < 5) {


var top5li = document.createElement("li");
	top5li.innerHTML += '<span style="margin-right: 10px;">' + array[arraylen].query + '</span>';
	
	var top5span = document.createElement("span");
top5span.setAttribute("class", "pull-right");
top5span.innerHTML += "<b>"+	array[arraylen].count + "</b>";
	
top5li.appendChild(top5span);
	
	top5ul.appendChild(top5li);
	
	    arraylen += 1;
  }	
} else {
	
	 while (arraylen < array.length) {

var top5li = document.createElement("li");
	top5li.innerHTML += '<span style="margin-right: 10px;">' + array[arraylen].query + '</span>';
	
	var top5span = document.createElement("span");
top5span.setAttribute("class", "pull-right");
top5span.innerHTML += "<b>"+	array[arraylen].count + "</b>";
	
top5li.appendChild(top5span);
	
	top5ul.appendChild(top5li);
	
		    arraylen += 1;
  }
	
}

top5div.appendChild(top5ul);	
	
}

function settop5visited(array, div) {
	
var top5div = document.getElementById(div);

var top5ul = document.createElement("ul");
top5ul.setAttribute("class", "dashboard-stat-list");  
  
var arraylen = 0;
if (array.length > 4) {

 while (arraylen < 5) {


var top5li = document.createElement("li");
	top5li.innerHTML += '<span style="margin-right: 10px;">' + array[arraylen].name + '</span>';
	
	var top5span = document.createElement("span");
top5span.setAttribute("class", "pull-right");
top5span.innerHTML += "<b>"+	Math.round(array[arraylen].dwell/60) + "</b>";
	
top5li.appendChild(top5span);
	
	top5ul.appendChild(top5li);
	
	    arraylen += 1;
  }	
} else {
	
	 while (arraylen < array.length) {

var top5li = document.createElement("li");
	top5li.innerHTML += '<span style="margin-right: 10px;">' + array[arraylen].name + '</span>';
	
	var top5span = document.createElement("span");
top5span.setAttribute("class", "pull-right");
top5span.innerHTML += "<b>"+	Math.round(array[arraylen].dwell/60) + "</b>";
	
top5li.appendChild(top5span);
	
	top5ul.appendChild(top5li);
	
		    arraylen += 1;
  }
	
}

top5div.appendChild(top5ul);	
	
}

function setLocations(readinput) {	

locations = JSON.parse(readinput);

var ad = 0;
var temp =[];
for(var i =0; i < locations.length; i++){

ad = ad + parseInt(locations[i].dwell);
var tempobj = {
name : locations[i].name,
dwell: locations[i].dwell
}
temp.push(tempobj);
var temploc = {
name : locations[i].name,
lat: locations[i].lat,
lng: locations[i].lng,
}
heatmaplocations.push(temploc);
}

   initMap();
var tempobj2 = {};
var obj = null;
for(var i=0; i < temp.length; i++) {
   obj=temp[i];

   if(!tempobj2[obj.name]) {
       tempobj2[obj.name] = obj;
   } else {
       tempobj2[obj.name].dwell += obj.dwell;
   }
}

for (var prop in tempobj2)
    mostvisited.push(tempobj2[prop]);
mostvisited.sort(function(a, b) {
    return parseFloat(b.dwell) - parseFloat(a.dwell);
});



ad = Math.round(ad/users.length);
ad = Math.round(ad/60); //to mins

}
var map, heatmap;

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: {lat: 13.199989890538024, lng: 77.70951252903678},
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        heatmap = new google.maps.visualization.HeatmapLayer({
          data: getPoints(),
          radius: 50,
          map: map
        });
      }

      

      // Heatmap data: 500 Points
      function getPoints() {
      	var latlngarray = [];
console.log(heatmaplocations);
for (var i =0; i < heatmaplocations.length; i++) {
var temp = new google.maps.LatLng(heatmaplocations[i].lat, heatmaplocations[i].lng);
	latlngarray.push(temp);
}      


      return latlngarray;
      }

	getUserdata();
	getLocdata();
	getQuerydata();
