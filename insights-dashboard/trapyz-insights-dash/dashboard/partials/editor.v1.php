<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusUid = $sessData['status']['uid'];
    $statusRole = $sessData['status']['role'];
    $statusMsgType = $sessData['status']['type'];

	} else if(empty($sessData['status']['msg']) && !isset($statusRole) ) {
		
	session_destroy();
	 header("Location:../../");	
		
	}
?>
﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title> Floor Map Editor - Trapyz CONNECT</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>
    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="main.css" type="text/css" rel="stylesheet" />
    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
    <style type="text/css">
button:focus
{
    background-color:#fff;
    color: #f44336;
}    
 button:active
{
    background-color:#fff;
    color: #f44336;
}    
    </style>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar" id="nav1">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../stats">Trapyz CONNECT</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                           
            
                <ul class="nav navbar-nav navbar-right" style=" margin-top: 15px;">
                
          
                
                                <li>                                   <button id="waypoint" type="button" class="btn btn-block btn-lg waves-effect">Draw</button></li>
                                
                                <li>&nbsp; &nbsp; &nbsp;</li>
<li>                                <button id="edit" type="button" class="btn btn-re  btn-block btn-lg waves-effect"  >Edit</button></li>
                                <li>&nbsp; &nbsp; &nbsp;</li>
<li>                                <button id="delete" type="button" class="btn btn-re btn-block btn-lg waves-effect">Delete Node</button></li>
                                <li>&nbsp; &nbsp; &nbsp;</li>
<li>                                <button id="save" type="button" class="btn btn-re btn-block btn-lg waves-effect">Save</button></li>
                                <li>&nbsp; &nbsp; &nbsp;</li>
<li>                                <button id="clear" type="button" class="btn btn-re btn-block btn-lg waves-effect">Clear</button></li>
                        

                                <li>&nbsp; &nbsp; &nbsp;</li>
<li>                                <button id="show_help" type="button" class="btn btn-re btn-block btn-lg waves-effect">?</button>                    </li>
                                <li>&nbsp; &nbsp; &nbsp;</li>










  <li  style="margin-top: -15px;">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">



                                    <li>
                                        <a href="javascript:void(0);">
                                          <i class="material-icons">person</i>Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="../../userAccount?logoutSubmit=1">
                                           <i class="material-icons">input</i>Sign Out
                                        </a>
                                    </li>







                           
                        </ul>
                    </li>



                </ul>
            </div>
        </div>

    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            
    <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>
                    <li>
                        <a href="../stats">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="editor.v1">
                            <i class="material-icons">map</i>
                            <span>Editor</span>
                        </a>
                    </li>
                    <li>
                        <a href="analytics.v2">
                            <i class="material-icons">trending_up</i>
                            <span>Analytics</span>
                        </a>
                    </li> 
                              
                      <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">message</i>
                            <span>Push Notification</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                        <a href="push.v1">
                                    <span>Create Campaign</span>
                                </a>
                            </li>
                            <li>
                                <a href="campaigns.v1">
                                    <span>All Campaigns</span>
                                </a>
                            </li>
                          
                        </ul>
                    </li>    
                               
                  <!--  <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Notifications <small>coming soon!</small></span>
                        </a>
                        
                    </li> -->
                  
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz CONNECT</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.1.7
                </div>
            </div>
            <!-- #Footer -->
        </aside>
      
    </section>
<section class="content">
        <div class="container-fluid" >

            
            
   <noscript>
    <div id="noscript">
        Please, enable javascript in your browser
    </div>
</noscript>
  
<div id="wrapper">
        



        <div id="coords"></div>
        <div id="debug"></div>

    <div id="image_wrapper">
        <div id="image">
                            <input type="text" id="url" style="display: none;" />
            <img src="" alt="#" id="img" />
            <svg baseProfile="tiny" id="svg"   xmlns="http://www.w3.org/2000/svg"></svg>
        </div>
    </div>
</div>


<div id="code">
   
<button id="loadfloor1" class="btn btn-default waves-effect">Floor 1</button> 
<button id="loadfloor2" class="btn btn-default waves-effect">Floor 2</button> 
<button id="loadfloor3" class="btn btn-default waves-effect">Floor 3</button> 
<button id="loadfloor4" class="btn btn-default waves-effect">Floor 4</button> 

</div>
<script >
     $('document').ready(function(){
     				var zoominValue = 1;
     				var zoomoutValue = 1;
     				              	var zoomoutValuey = 1;
            $('.zoom').on('click', function(){               
							if ($(this).hasClass('_bigger')) {
							

             // 	zoominValue = zoomoutValue + 0.1;
              	              	            	$('#wrapper').css('transform', 'scale(' + zoominValue + ')');
              	              	            	$('#image_wrapper').css('transform', 'scale(' + zoominValue + ')'); 
                	            		            	
              	               	          //  $('#image_wrapper').css('transform', 'scale(' + zoominValue + ')'); 
              	              	            	 
zoomoutValue = 1;
zoomoutValuey = 1;
              	              	            	
              } else if ($(this).hasClass('_smaller')) {
              	zoomoutValue = zoomoutValue - 0.1;
            	zoomoutValuey = zoomoutValuey - 0.1;
              	            	$('#wrapper').css('transform', 'scale(' + zoomoutValue + ','  + zoomoutValuey +')');
              	            	//	$('#wrapper').css('transform', 'translate(' + 0 +'px,' + 0 + 'px)');
              	            	
              	              	           	$('#image_wrapper').css('transform', 'scale(' + zoomoutValue + ')');
              	            		//$('#image_wrapper').css('transform', 'translate(' + 0 +'px,' + 0 + 'px)');
              	          //      	$('#svg').css('transform', 'scale(' + zoomoutValue + ')');
 	 // $('#image_wrapper').css('width', '400px' );    
     //         	                	  $('#image_wrapper').css('height', '1000px' ); 
              } else {
              return false;
              }
            
            //	$('#image_wrapper').css('transform', 'scale(' + zoominValue + ')');

            });
       });
</script>
<div id="code1">
   
<button id="zoomin" class="btn btn-default waves-effect zoom _bigger " >   <i  style="font-size: 30px;" class="material-icons">zoom_in</i> </button> 
<button id="zoomout" class="btn btn-default waves-effect zoom _smaller"> <i  style="font-size: 30px;" class="material-icons">zoom_out</i>   </button> 


</div>
<form id="edit_details">
    <h5>Details</h5>
    <span class="close_button" title="close"></span>
    <p>
        <label for="name">Store Name</label>
        <input type="text" id="name" />
    </p>
    <p>
     <input type="checkbox" id="isDestination" class="filled-in" checked />
                                <label for="isDestination">isD</label>
    
   
        
    </p>
    <p> <input type="checkbox" id="isEscalator" class="filled-in" checked />
                                <label for="isEscalator">isE</label>

       
    </p>
    <button id="save_details" class="btn btn-primary waves-effect">Save</button>
</form>


<div id="from_html_wrapper">
    <form id="from_html_form">
        <h5>Loading areas</h5>
        <span class="close_button" title="close"></span>
        <p>
            <label for="code_input">Enter your html code:</label>
            <textarea id="code_input"></textarea>
        </p>
        <button id="load_code_button">Load</button>
    </form>
</div>
  

<div id="get_image_wrapper">
    <div id="get_image">
        <span title="close" class="close_button"></span>
        <div id="logo_get_image">
            
        </div>
        <div id="loading">Loading</div>
        <div id="file_reader_support">
            <label>Drag an image</label>
            <div id="dropzone">
                <span class="clear_button" title="clear">x</span> 
                <img src="" alt="preview" id="sm_img" />
            </div>

        </div>
        <br>

   
        <button id="button">UPLOAD</button>
        <input type="file" id="fileInput" style="display: none;">
        <input type="file" id="fileInputedge" style="display: none;">
     
        

    </div>
</div>


<div id="overlay"></div>
<div id="help">
    <span class="close_button" title="close"></span>
    <div class="txt">
       
    </div>
    <footer>
      
    </footer>
</div>

<script type="text/javascript" src="map.js"></script>


</div>

    </section>
    

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
</body>

</html><