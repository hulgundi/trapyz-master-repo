var maps = {}, selectedOption, categories = [], subcategories = [], stores = [], cities = [], pincodes = [], catTimer, subcatTimer, storeTimer, cityTimer, pinTimer, reverseMaps = {}, multiplier = 97, sapi = 9, api;

function generateQuery() {
	var queryUrl = "https://canvas.trapyz.com/insights-", queryType, queryCondition, distance, distanceCondition, visit, visitCondition, startDate, endDate, result, loadingBG;

	loadingBG = document.getElementById("loader-bg");
	if (loadingBG) {
		loadingBG.style.zIndex = 1000;
		loadingBG.style.opacity = 1;
	}
	document.getElementById("insights-result").innerHTML = "--";
	queryType = document.getElementById("query-select").value;
	queryCondition = reverseMaps[queryType][document.getElementById(queryType + "-select").value];
	distance = document.getElementById("distance-text").value;
	distanceCondition = document.getElementById("distance-select").value;
	visit = document.getElementById("visit-text").value;
	visitCondition = document.getElementById("visit-select").value;
	startDate = document.getElementById("date-from").value;
	endDate = document.getElementById("date-to").value;
	city = reverseMaps["city"][document.getElementById("city-select").value];
	pin = reverseMaps["pin"][document.getElementById("pin-select").value];
	queryUrl += queryType + "?" + queryType + "=" + queryCondition;

	if (distance && distanceCondition) {
		queryUrl += "&dist=" + distance + "&dcon=" + distanceCondition;
	}

	if (visit && visitCondition) {
		queryUrl += "&visit=" + visit + "&vcon=" + visitCondition;
	}

	if (startDate && endDate) {
		startDateMilli = new Date(startDate).getTime();
		endDateMilli = new Date(endDate).getTime();
		queryUrl += "&start=" + startDateMilli + "&end=" + endDateMilli;
	}

	if (city) {
		queryUrl += "&city=" + city;
	}

	if (pin) {
		queryUrl += "&pin=" + pin;
	}
	queryUrl += "&count=1";

	api = parseInt(sessionStorage.getItem('key'));
	if (api != sapi) {
		queryUrl += "&api=" + api;		
	}
	
	httpRequest(queryUrl, "", function(responseText) {
			var options = {
	                        useEasing: true,
	                        useGrouping: true,
	                        separator: '',
	                        decimal: '.',
	                };

			var counter = new CountUp('insights-result', 0, responseText * multiplier, 0, 3, options);
					var loadingBG;
					loadingBG = document.getElementById("loader-bg");
					if (loadingBG) {
						loadingBG.style.zIndex = 0;
						loadingBG.style.opacity = 0;
					}
	                if (!counter.error) {
	                        counter.start();
	                } else {
	                        console.error(counter.error);
	                }
		});
	}

function httpRequest(requestUrl, targetElementId, processFunction) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp["trapyzFunction"] = processFunction;

    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            xmlHttp["trapyzFunction"](xmlHttp.responseText,targetElementId);    
        } else {
            console.log("Waiting");
        }
    };
    xmlHttp.open("GET", requestUrl, true);
    xmlHttp.send(null);
}

function getMaps(mapType, arrayType) {
	var queryUrl = "https://canvas.trapyz.com/insights-getmap?mapType=" + mapType;

	httpRequest(queryUrl, arrayType, function(responseText, arrayType) {
		var targetElement, keys, optionElement, checkTimer;

		maps[mapType] = JSON.parse(responseText);
		keys = Object.keys(maps[mapType]);
		reverseMaps[mapType] = {};
		for (var index = 0; index < keys.length; index++) {
			arrayType.push(maps[mapType][keys[index]]);
			reverseMaps[mapType][maps[mapType][keys[index]]] = keys[index];
		}
	});
}

function removeDuplicates(list) {
	var filteredList = [];
	for (var index = 0; index < list.length; index++) {
		if (filteredList.indexOf(list[index]) == -1) {
			filteredList.push(list[index]);
		}
	}
	return filteredList;
}

function generateElement(elementType, styleClass, elementId, parentElement) {
    var element;

    element = document.createElement(elementType);
    element.className = styleClass;
    element.Id = elementId;
    parentElement.appendChild(element);
    return element;
}

function displayMap() {
	var selectElement, optionsElement, searchElements = ["cat-div", "subcat-div", "store-div"];

	selectElement = document.getElementById("query-select");
	optionsElement = selectElement.getElementsByTagName("option");
	for (var index = 0; index < optionsElement.length; index++) {
		if (optionsElement[index].selected == true) {
			for (var index2 = 0; index2 < searchElements.length; index2++) {
				if ((optionsElement[index].value + "-div") == searchElements[index2]) {
					document.getElementById(searchElements[index2]).style.display = "inline-block";
				} else {
					document.getElementById(searchElements[index2]).style.display = "none";
				}
			}
		}
	}
}

getMaps("cat", categories);
getMaps("subcat", subcategories);
getMaps("store", stores);
getMaps("city", cities);
getMaps("pin", pincodes);

catTimer = setInterval(function() {
	catSearchElement = document.getElementById("cat-select");
	if (catSearchElement) {
		clearInterval(catTimer);
		autocomplete(catSearchElement, categories);
	}
}, 1000);

subcatTimer = setInterval(function() {
	subcatSearchElement = document.getElementById("subcat-select");
	if (subcatSearchElement) {
		clearInterval(subcatTimer);
		autocomplete(subcatSearchElement, subcategories);
	}
}, 1000);

storeTimer = setInterval(function() {
	storeSearchElement = document.getElementById("store-select");
	if (storeSearchElement) {
		clearInterval(storeTimer);
		autocomplete(storeSearchElement, stores);
	}
}, 1000);


cityTimer = setInterval(function() {
	citySearchElement = document.getElementById("city-select");
	if (citySearchElement) {
		clearInterval(cityTimer);
		autocomplete(citySearchElement, cities);
	}
}, 1000);


pinTimer = setInterval(function() {
	pinSearchElement = document.getElementById("pin-select");
	if (pinSearchElement) {
		clearInterval(pinTimer);
		autocomplete(pinSearchElement, pincodes);
	}
}, 1000);

