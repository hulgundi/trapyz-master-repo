<!DOCTYPE html>
<html>

<head>
    <script type="text/javascript" src="./auto_complete.js"></script>
    <script type="text/javascript" src="../countUp/countUp.js/countUp.js"></script>
    <link rel="stylesheet" type="text/css" href="./auto_complete.css">
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Custom Queries | Trapyz Canvas</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>


    <script src="https://harvesthq.github.io/chosen/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .loader-bg {
            position: fixed;
            text-align: center;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            transition: 1s;
            opacity: 0;
            background: rgba(0, 0, 0, 0.7);
        }

        .loader-container {
            position: absolute;
            display: inline-block;
            z-index: 1000;
            top: 220px;
            left: 570px;
            background: #000;
            box-shadow: 6px 6px 24px #000;
            text-align: center;
            width: 200px;
            border-radius: 11px;
            height: 180px;
            padding-top: 30px;
            color: #FF9900;
        }

        .loader-gear {
            display: inline-block;
            width: 80px;
        }

        .loader-text {
            margin-top: 10px;
            font-weight: bold;
            font-style: italic;
        }
    </style>
    <link href="https://harvesthq.github.io/chosen/chosen.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="./custom_queries.js"></script>
    <link href="../plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Bootstrap Spinner Css -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/sweetalert/sweetalert.min.js"></script>
    <script src="https://cloud.github.com/downloads/uxebu/bonsai/bonsai-0.4.0.min.js"></script>
    <script src="https://d3js.org/d3.v3.min.js"></script>
    <script src="chartcontrolmain.js"></script>

    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->

    <link href="../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
    
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>
    <script src="./partials/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script type="text/javascript">
        var uname = sessionStorage.getItem('uname') || "";
        
        if (uname == "") {
            window.location = "https://canvas.trapyz.com/trapyz-insights-dash/dashboard/sign-in.html";
        } 

        function signout() {
            sessionStorage.removeItem('uname');
            window.location = "https://canvas.trapyz.com/trapyz-insights-dash/dashboard/sign-in.html";
        }
    
        window.onload = function() {
            var index = 0; fancyMessages = ["Applying Filters...", "Applying Machine learning...", "Applying AI Algorithm...", "Crunching Numbers...", "Almost There..."], messageElement = document.getElementById("fancy-text");
            messageElement.innerHTML = fancyMessages[index];
            setInterval(function() {
                if (++index >= fancyMessages.length) {
                    index = 0;
                }
                messageElement.innerHTML = fancyMessages[index];
            }, 1000);   
        }
    </script>

</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../stats">Trapyz Canvas</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <div onclick="signout()" style="cursor: pointer;">
                                   <i class="material-icons" style="vertical-align: middle;margin-right: 10px;margin-left: 10px;">input</i>Sign Out
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul> 
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">

        <div class="menu">
            <ul class="list">
                <li class="header">Menu</li>
                <li>
                   <a href="../stats">
                       <i class="material-icons">home</i>
                       <span>Dashboard</span>
                   </a>
                </li>
                <li>
                   <a href="custom_queries">
                      <i class="material-icons">accessibility</i>
                      <span>Custom Queries</span>
                   </a>
                </li> 
            </ul>
        </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz Canvas</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.4
                </div>
            </div>
            <!-- #Footer -->
        </aside>
    </section>
    <div id="query-container" class="insights-query-container">
    <div style="position: relative; top: 370px; text-align: left; left: 50px;">
        <div id="insights-result" class="insights-result">
            --
        </div>
    </div>
    <div style="position: relative; top: 70px;">
        <div class="query-box-1">
            <span style="font-size: 18px;">Dimension </span>
            <select id="query-select" onchange="displayMap()" class="query-type-select" style="display:block; margin-left: 0px; margin-top: 20px" >
                <option value="none">Select an option</option>
                <option value="cat">Category</option>
                <option value="subcat">Subcategory</option>
                <option value="store">Store</option>
            </select>
        <div id="cat-div" class="autocomplete">
                 <input id="cat-select" type="text" placeholder="Categories">
            </div>
            <div id="subcat-div" class="autocomplete">
                 <input id="subcat-select" type="text" placeholder="Subcategories">
            </div>
            <div id="store-div" class="autocomplete">
                 <input id="store-select" type="text" placeholder="Stores">
            </div>

        </div>
        <div class="query-box-2">
            <div style="margin-bottom: 20px;font-size: 18px;">Filters</div>
        <div style="text-align: right;margin-bottom: 10px;">
            <span>City</span>
            <div id="city-div" class="autocomplete">
                <input id="city-select" type="text" placeholder="Cities">
            </div>
        </div>
        <div style="text-align: right;margin-bottom: 10px;">
            <span>Area</span>
            <div id="pin-div" class="autocomplete">
                <input id="pin-select" type="text" placeholder="Area">
            </div>
        </div>
        <div style="text-align: right;margin-bottom: 10px;">
            <span>Geo-radius</span>
            <select id="distance-select" class="query-type-select">
                <option value="none">Select an option</option>
                <option value="gt">&gt;</option>
                <option value="lt">&lt;</option>
            </select>
            <input type="text" id="distance-text" style="margin-left: 20px;width: 60px;margin-right:10px">
        </div>
        <div style="text-align: right;margin-bottom: 10px;">
                <span>Visits</span>
                <select id="visit-select" class="query-type-select">
                    <option value="none">Select an option</option>
                    <option value="gt">&gt;</option>
                    <option value="lt">&lt;</option>
                </select>
                <input type="text" id="visit-text" style="margin-left: 20px;width: 60px;margin-right:10px">
        </div>
            <div style="text-align: right;margin-bottom: 10px;">
                <div style="text-align: left">Date</div>
            <span>From</span>
                <input type="date" id="date-from" style="margin-left: 10px;width: 160px;margin-right: 10px;">
            <span>To</span>
                <input type="date" id="date-to" style="margin-left: 10px;width: 160px;margin-right: 10px;">
            </div>
        </div>
    </div>
    <div style="position: absolute; bottom: 350px; text-align: left; left: 210px;">
        <div id="insights-submit" class="insights-submit" onclick="generateQuery()">
        Submit Query
        </div>
    </div>
    </div>
        
    </body>
    <div class="loader-bg" id="loader-bg">
        <div class="loader-container">
            <img src="gear.gif" class="loader-gear"></img>
            <div id="fancy-text" class="loader-text">
            </div>
        </div>
    </div>
</html>

