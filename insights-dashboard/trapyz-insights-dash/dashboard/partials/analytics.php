<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusUid = $sessData['status']['uid'];
    $statusRole = $sessData['status']['role'];
    $statusMsgType = $sessData['status']['type'];

	} else if(empty($sessData['status']['msg']) && !isset($statusRole) ) {
		
	session_destroy();
	 header("Location:../../");	
		
	}
?>

﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Analytics | Trapyz CONNECT</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
    <script src="amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<style type="text/css">
#chart {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}	
</style>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
  <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../index">Trapyz CONNECT</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">


   <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">



                                    <li>
                                        <a href="javascript:void(0);">
                                          <i class="material-icons">person</i>Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="../../userAccount?logoutSubmit=1">
                                           <i class="material-icons">input</i>Sign Out
                                        </a>
                                    </li>







                           
                        </ul>
                    </li>

                   
                 
                  


                </ul> 
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
   
  <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info
            <div class="user-info">
                <div class="image">
                    <img src="../../images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
                    <div class="email">john.doe@example.com</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>  {enableHighAccuracy:false,maximumAge:Infinity, timeout:60000}
                    </div>
                </div>
            </div>
 -->
            <!-- Menu -->

         <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>

                    <li>
                        <a href="../dashboard">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="../partials/editor">
                            <i class="material-icons">map</i>
                            <span>Editor</span>
                        </a>
                    </li>
                    <li>
                        <a href="../partials/analytics">
                            <i class="material-icons">trending_up</i>
                            <span>Analytics</span>
                        </a>
                    </li>
                                        <li>
                        <a href="../partials/shops">
                            <i class="material-icons">shopping_cart</i>
                            <span>Stores</span>
                        </a>
                    </li>     
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">message</i>
                            <span>Push Notification</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                        <a href="push">
                                    <span>Create Campaign</span>
                                </a>
                            </li>
                            <li>
                                <a href="campaigns">
                                    <span>All Campaigns</span>
                                </a>
                            </li>
                          
                        </ul>
                    </li>      <li> 
                        <a href="users">
                            <i class="material-icons">group_add</i>
                            <span>Users</span>
                        </a>
                    </li>             
                                
             <!--       <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Notifications <small>coming soon!</small></span>
                        </a>
                        
                    </li> -->
                  
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz CONNECT</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.1.7
                </div>
            </div>
            <!-- #Footer -->
        </aside>
      
    </section>

    <section class="content">
       <div id='console' style="display: none;"></div>
              <div id='console1' style="display: none;"></div>
     <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content">
                            <div class="text">Subscribers</div>
                            <div class="number count-to" data-from="0" data-to="560" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">local_offer</i>
                        </div>
                        <div class="content">
                            <div class="text">Campaigns</div>
                            <div class="number count-to" data-from="0" data-to="12" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">notifications</i>
                        </div>
                        <div class="content">
                            <div class="text">Notifications Sent</div>
                            <div class="number count-to" data-from="0" data-to="1090" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">check</i>
                        </div>
                        <div class="content">
                            <div class="text">CTR</div>
                            <div class="number count-to" data-from="0" data-to="20" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
<script type="text/javascript" id="scriptcode">
var xhr1;
function getstores() {
  if (window.XMLHttpRequest) {
    // Mozilla, Safari, ...
    xhr1 = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    // IE 8 and older
    xhr1 = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var data;
  xhr1.open("GET", "../../get_location.php", true);
  xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr1.send(data);
  xhr1.onreadystatechange = display_data;
  function display_data() {
    if (xhr1.readyState == 4) {
      if (xhr1.status == 200) {
        test2(xhr1.responseText);
      } else {
      }
    }
  }
}
var names = [];
function test2(readinput) {
  var a = readinput;
  var array = a.split("\n");

  for (var i = 0, c = array.length; i < c; i++) {
    var b = array[i].split(",");
    var g = {
      clientid: b[0],
      Locality: b[1],
      latitude: b[2],
      longitude: b[3],
      city: b[4],
      state: b[5],
      country: b[6],
      fingerprint: b[7],
      Browser: b[8],
      os: b[9],
      osversion: b[10],
      auth: b[12],
      p256dh: b[11],
      date: b[12],
      time: b[13]
    };
    names.push(g);
  }

  names.pop();
  var osfull = [];
  var osfullpie = [];
  for (var i = 0, j = names.length; i < j; i++) {
    var osfullobj = names[i].os + " " + names[i].osversion;
    osfull.push(osfullobj);
  }
  for (var i = 0, j = names.length; i < j; i++) {
    var osfullobj1 = '"' + names[i].os + " " + names[i].osversion + '"';
    osfullpie.push(osfullobj1);
  }

  var obj = {};
  for (var i = 0, j = names.length; i < j; i++) {
    obj[names[i].Locality] = (obj[names[i].Locality] || 0) + 1;
  }

  var obj1 = {};
  for (var i = 0, j = names.length; i < j; i++) {
    obj1[names[i].Browser] = (obj1[names[i].Browser] || 0) + 1;
  }

  var obj2 = {};
  for (var i = 0, j = names.length; i < j; i++) {
    obj2[osfull[i]] = (obj2[osfull[i]] || 0) + 1;
  }
  var obj3 = {};
  for (var i = 0, j = names.length; i < j; i++) {
    obj3[osfullpie[i]] = (obj3[osfullpie[i]] || 0) + 1;

  }
  
  
  
   var obj5 = {};
  for (var i = 0, j = names.length; i < j; i++) {
    obj5[names[i].city] = (obj5[names[i].city] || 0) + 1;
  }
  
  var chartcity = Object.assign(obj5);
  var chartcitykeys = Object.keys(chartcity), chartlen = chartcitykeys.length, k = 0, chartprop, chartvalue;

  var dataarray = [];
 while (k < chartlen) {
 var dataobj = { 
 "country": chartcitykeys[k],
 "visits": chartcity[chartcitykeys[k]]
};
   dataarray.push(dataobj);

    k += 1;
  }





var chart = AmCharts.makeChart( "chart", {
  "type": "serial",
  "theme": "light",
  "dataProvider": dataarray,
  "gridAboveGraphs": true,
  "startDuration": 1,
  "graphs": [ {
    "balloonText": "[[category]]: <b>[[value]]</b>",
    "fillAlphas": 0.8,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits"
  } ],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "gridAlpha": 0,
    "tickPosition": "start",
    "tickLength": 20
  },
  "export": {
    "enabled": true
  }

} );













  
  var merge = Object.assign(obj, obj1, obj2);
  console.log(obj2);

  var users = Object.values(merge);
  var merge1 = Object.assign(obj2);
  var users1 = Object.values(merge1);

  var totalusers = 0;
  var merge2 = Object.assign(obj1);
  var users2 = Object.values(merge2);

  var totalusers1 = 0;
  console.log(users1);
  for (var k = 0; k < users1.length; k++) {
  totalusers = totalusers + users1[k];
  
  }
 for (var k = 0; k < users2.length; k++) {
  totalusers1 = totalusers1 + users2[k];
  
  }

var othersleft = 0;
var othersleft1 = 0;
  for (var key in obj2) {
  if (obj2.hasOwnProperty(key)) {
    obj2[key] = Math.floor((obj2[key]/totalusers)*100);
othersleft = othersleft + obj2[key];
  }
}

 for (var key in obj1) {
  if (obj1.hasOwnProperty(key)) {
    obj1[key] = Math.floor((obj1[key]/totalusers)*100);
othersleft1 = othersleft1 + obj1[key];
  }
}
  obj1["Others"] = 100 - othersleft1;
  obj2["Others"] = 100 - othersleft;


  var ospie = Object.assign(obj2);
  var ospie1 = Object.assign(obj1);
  console.log(ospie);
  var c = (function() {
    return {
      log: function(msg) {
        consoleDiv = document.getElementById("console");

        text = document.createTextNode(msg);

        consoleDiv.appendChild(text);
      }
    };
  })();

  var log0 = JSON.stringify(ospie);
  c.log(log0);
  
var c1 = (function() {
    return {
      log: function(msg) {
        consoleDiv1 = document.getElementById("console1");

        text = document.createTextNode(msg);

        consoleDiv1.appendChild(text);
      }
    };
  })();

  var log11 = JSON.stringify(ospie1);
  console.log(log0, log11);
  c1.log(log11);  
  
  consoleDiv = document.getElementById("console");
  consoleDiv1 = document.getElementById("console1");

  var log = "var testobj1 =" + consoleDiv.innerHTML + ";" + "var testobj2 =" + consoleDiv1.innerHTML + ";";

  var ospieelement = document.getElementById("bs");
  ospieelement.innerHTML += log;
 var ospieelement1 = document.getElementById("bs1");
  ospieelement1.innerHTML += log;

  //  console.log(ospieelement.innerHTML, ospieelement1.innerHTML);
  var player = document.getElementById("player");
  var player1 = document.getElementById("player1");
  var b = bonsai.run(player, {
    code: log + document.getElementById("bs").innerHTML,
    height: 400,
    width: 600,
    framerate: 50
  });

  window.onresize = function() {
    player.style.left = (window.innerWidth - 202) / 2 - 350 + "px";
    player.style.top = window.innerHeight / 2 - 200 + "px";
  };

  window.onresize();
   var b1 = bonsai.run(player1, {
    code: log + document.getElementById("bs1").innerHTML,
    height: 400,
    width: 600,
    framerate: 50
  });

  window.onresize = function() {
    player1.style.left = (window.innerWidth - 202) / 2 - 350 + "px";
    player1.style.top = window.innerHeight / 2 - 200 + "px";
  };

  window.onresize();

  var tags = Object.keys(merge);
  console.log(users);
  console.log(tags);

  var objectdata = document.getElementById("object");
  objectdata.innerHTML = JSON.stringify(merge);
  objectdata.value = JSON.stringify(merge);

  var myTableDiv = document.getElementById("tablediv");

  var table = document.createElement("table");
  table.className += " table";
  table.className += " table-bordered";
  table.className += " table-striped";
  table.className += " table-hover ";
  table.className += " dataTable";
  table.className += " js-exportable";

  var thead = document.createElement("thead");
  var tfoot = document.createElement("tfoot");

  var heading = new Array();
  heading[0] = "Tags";
  heading[1] = "Users";
  var tr = document.createElement("TR");
  thead.appendChild(tr);
  for (i = 0; i < heading.length; i++) {
    var th = document.createElement("TH");
    th.width = "350";
    th.appendChild(document.createTextNode(heading[i]));
    tr.appendChild(th);
  }
  table.appendChild(thead);

  var heading = new Array();
  heading[0] = "Tags";
  heading[1] = "Users";
  var tr = document.createElement("TR");
  tfoot.appendChild(tr);
  for (i = 0; i < heading.length; i++) {
    var th = document.createElement("TH");
    th.width = "350";
    th.appendChild(document.createTextNode(heading[i]));
    tr.appendChild(th);
  }
  table.appendChild(tfoot);

  var tbody = document.createElement("tbody");

  var keys = Object.keys(merge), len = keys.length, i = 0, prop, value;

  var div = document.getElementById("selectdiv");

  var datalist = document.createElement("datalist");

  datalist.setAttribute("id", "list");

  var select = document.createElement("select");

  select.setAttribute("class", "form-control show-tick");

  select.setAttribute("multiple", "");
  select.setAttribute("data-live-search", "true");
  div.appendChild(select);
  while (i < len) {
    prop = keys[i];
    value = merge[prop];

    var row = document.createElement("TR");
    var td1 = document.createElement("Td");
    var td2 = document.createElement("Td");

    var option = document.createElement("option");
    option.value = keys[i];
    option.innerHTML = keys[i];
    datalist.appendChild(option);
    td1.width = "350";
    td2.width = "350";

    td1.appendChild(document.createTextNode(keys[i]));
    td2.appendChild(document.createTextNode(value));

    row.appendChild(td1);
    row.appendChild(td2);

    select.options[select.options.length] = new Option(keys[i], keys[i]);

    tbody.appendChild(row);
    table.appendChild(tbody);
    i += 1;
  }

  var inputdiv = document.getElementById("input");
  var input = document.createElement("input");
  input.setAttribute("type", "text");
  input.setAttribute("id", "txtAutoComplete");
  input.setAttribute("list", "list");
  input.setAttribute("placeholder", "Select Tag");
  input.setAttribute("class", "form-control");

  inputdiv.appendChild(input);
  inputdiv.appendChild(datalist);
  var breakline = document.createElement("br");
  var pushbutton = document.createElement("button");

  pushbutton.setAttribute(
    "class",
    "btn btn-block btn-lg btn-primary waves-effect"
  );
  pushbutton.setAttribute("type", "button");
  pushbutton.setAttribute("onclick", "pushnotify();");
  pushbutton.innerHTML = "Push";
  inputdiv.appendChild(breakline);
  inputdiv.appendChild(pushbutton);

  myTableDiv.appendChild(table);
}

function pushnotify() {
  var inputtag = document.getElementById("txtAutoComplete");
  var tag = inputtag.value;

  var chromeregeid = [];
  var firefoxendpoint;
  var count = 0;
  for (var i = 0, j = names.length; i < j; i++) {
    if ((tag == "Chrome" || tag == "Chromium") && tag == names[i].Browser) {
      console.log("this");
      var iconurl = document.getElementById("icon").value;
      var title = document.getElementById("title").value;
      var body = document.getElementById("body").value;
      var targetUrl = document.getElementById("targetUrl").value;
      var params = {
        regid: names[i].clientid,
        auth: names[i].auth,
        p256dh: names[i].p256dh,
        iconurl: iconurl,
        title: title,
        body: body,
        targetUrl: targetUrl,
        fingerprint: names[i].fingerprint
      };
      chromeregeid.push(params);
    } else if (tag == "Firefox" && tag == names[i].Browser) {
      /**
if (tag == names[i].Browser) {

               $.ajax({

  url: names[i].clientid,
  method: 'POST',
header: 'ttl: 60'
});

var post = new XMLHttpRequest();
var WEBPUSH_SERVER = 'https://services-qa-webpush.stage.mozaws.net/notify';  
  post.open('POST', WEBPUSH_SERVER);
var iconurl = document.getElementById("icon").value;
var title = document.getElementById("title").value;
var body = document.getElementById("body").value;
var targetUrl = document.getElementById("targetUrl").value;
  //Send the proper header information along with the request
 var obj = {"title" : title,
             "body" : body,
             "icon" : iconurl,
             "targetUrl" : targetUrl };

  

  var params = "endpoint=" + encodeURIComponent(names[i].clientid);
  params += "&TTL=" + "60";
  params += "&repeat=" + "1" * 1;
  params += "&delay=" + "1" * 1000;
  params += "&floodDelay=" + "0" * 1000;

  
    params += "&payload="+ JSON.stringify(obj);
    params += "&userPublicKey=" + names[i].p256dh;
  

  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
 

 
  post.send(params);













}**/

      console.log("this");
      var iconurl = document.getElementById("icon").value;
      var title = document.getElementById("title").value;
      var body = document.getElementById("body").value;
      var targetUrl = document.getElementById("targetUrl").value;
      var params = {
        regid: names[i].clientid,
        auth: names[i].auth,
        p256dh: names[i].p256dh,
        iconurl: iconurl,
        title: title,
        body: body,
        targetUrl: targetUrl,
        fingerprint: names[i].fingerprint
      };
      chromeregeid.push(params);
    } else {
      for (var i = 0, j = names.length; i < j; i++) {
        if (
          tag == names[i].Locality ||
            tag == names[i].os + " " + names[i].osversion &&
              (names[i].Browser == "Chrome" ||
                names[i].Browser == "Chromium" ||
                names[i].Browser == "Firefox")
        ) {
          console.log("this");
          var iconurl = document.getElementById("icon").value;
          var title = document.getElementById("title").value;
          var body = document.getElementById("body").value;
          var targetUrl = document.getElementById("targetUrl").value;
          var params = {
            regid: names[i].clientid,
            auth: names[i].auth,
            p256dh: names[i].p256dh,
            iconurl: iconurl,
            title: title,
            body: body,
            targetUrl: targetUrl,
            fingerprint: names[i].fingerprint
          };
          chromeregeid.push(params);
        } /**else if (tag == names[i].Locality && names[i].Browser == "Firefox" ) {

                    $.ajax({

  url: names[i].clientid,
  method: 'POST',
header: 'ttl: 60'
});

	
} **/
      }
    }

    var removeDuplicatesInPlace = function(arr) {
      var i, j, cur, found;
      for (i = arr.length - 1; i >= 0; i--) {
        cur = arr[i].fingerprint;
        found = false;
        for (j = i - 1; !found && j >= 0; j--) {
          if (cur === arr[j].fingerprint) {
            if (i !== j) {
              arr.splice(j, 1);
            }
            found = true;
          }
        }
      }
      return arr;
    };

    var senddata = removeDuplicatesInPlace(chromeregeid);

    if (senddata.length > 0) {
      var tempParams1 = JSON.stringify(senddata);

      $.ajax({
        url: "../../push.php",
        method: "POST",
        header: "ttl: 60",
        data: tempParams1,
        success: function(resdata) {},
        error: function(result, status, err) {
          console.log(result.responseText);
          alert(status.responseText);
          alert(err.Message);
        }
      });
    }
    count = count + senddata.length;
    chromeregeid.length = 0;
    senddata.length = 0;
  }
  console.log(count);
  if (count > 0) {
    alert("Sent to " + count + " users!");
  }

  count = 0;
}

window.onload = function() {
  getstores();
};


</script>    <div class="card"> <div class="container-fluid">
            
                  
            <div class="row clearfix" >
                            <div >
            
<div id="selectdiv" style="display: none;"></div>            
            
    
</div>




                     

 <div  style="display: none;"> 
        
           
           
           <div id="input" >   

            <br>
            <br>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input class="form-control" id="title" aria-required="true" type="text">
                                        <label class="form-label">Message Title</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea id="body" cols="30" rows="5" class="form-control no-resize"  aria-required="true"></textarea>
                                        <label class="form-label">Description</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="targetUrl" class="form-control"   aria-required="true" type="text">
                                        <label class="form-label">Target Url</label>
                                    </div>
                                </div>                                
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="icon" class="form-control"   aria-required="true" type="text">
                                        <label class="form-label">Icon Url</label>
                                    </div>
                                </div>
                                
                             
      


          
           </div>
    
                






</div>                        
               
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 " > 
                <input id="object" type="text" style="display: none;"/>
                <div id="player"></div>
<script type="bs" id="bs">


function Sector(x, y, radius, startAngle, endAngle) {
  SpecialAttrPath.call(this, {
    radius: 0,
    startAngle: startAngle,
    endAngle: endAngle
  });
  this.attr({
    x: x,
    y: y,
    radius: radius,
    startAngle: startAngle,
    endAngle: endAngle
  });
}

Sector.prototype = Object.create(SpecialAttrPath.prototype);
Sector.prototype._make = function() {

  var attr = this._attributes,
      radius = attr.radius,
      startAngle = attr.startAngle,
      endAngle = attr.endAngle;

  var startX, startY, endX, endY;
  var diffAngle = Math.abs(endAngle - startAngle);

  this.startX = startX = radius * Math.cos(startAngle);
  this.startY = startY = radius * Math.sin(startAngle);
  if (diffAngle < Math.PI*2) {
    endX = radius * Math.cos(endAngle);
    endY = radius * Math.sin(endAngle);
  } else { // angles differ by more than 2*PI: draw a full circle
    endX = startX;
    endY = startY - .0001;
  }

  this.endX = endX;
  this.endY = endY;

  this.radiusExtentX = radius * Math.cos(startAngle + (endAngle - startAngle)/2);
  this.radiusExtentY = radius * Math.sin(startAngle + (endAngle - startAngle)/2);

  return this.moveTo(0, 0)
    .lineTo(startX, startY)
    .arcTo(radius, radius, 0, (diffAngle < Math.PI) ? 0 : 1, 1, endX, endY)
    .lineTo(0, 0);

};

Sector.prototype.getDimensions = function() {
  var x = this.attr('x'),
      y = this.attr('y'),
      left = Math.min(x, x + this.startX, x + this.endX, x + this.radiusExtentX),
      top = Math.min(y, y + this.startY, y + this.endY, y + this.radiusExtentY),
      right = Math.max(x, x + this.startX, x + this.endX, x + this.radiusExtentX),
      bottom = Math.max(y, y + this.startY, y + this.endY, y + this.radiusExtentY);
  console.log(y, y + this.startY, y + this.endY, y + this.radiusExtentY)
  return {
    left: left,
    top: top,
    width: right - left,
    height: bottom - top
  };
};


PieChart.BASE_COLOR = color('yellow');

function PieChart(data) {
  this.angle = 0;
  this.labelY = 30;
  this.kolor = PieChart.BASE_COLOR.clone();
  var n = 0;
  for (var i in data) {
    this.slice(i, data[i], n++);
  }
}

PieChart.prototype = {
  slice: function(name, value, i) {

    var start = this.angle,
        end = start + (Math.PI*2) * value/100,
        // Increase hue by .1 with each slice (max of 10 will work)
        kolor = this.kolor = this.kolor.clone().hue(this.kolor.hue()+.1);

    var s = new Sector(
      350, 200, 100,
      start,
      end
    );

    var animDelay = (i * 200) + 'ms';

    var label = this.label(name, value, kolor);

    label.attr({ opacity: 0 });

    s.stroke('#FFF', 3);
    s.fill(kolor);

    s.attr({
      endAngle: start,
      radius: 0
    }).addTo(stage).on('mouseover', over).on('mouseout', out);
    label.on('mouseover', over).on('mouseout', out);

    function over() {
      label.text.attr('fontWeight', 'bold');
      label.animate('.2s', {
        x: 40
      });
      s.animate('.2s', {
        radius: 170,
        fillColor: kolor.lighter(.1)
      }, {
        easing: 'sineOut'
      });
    }

    function out() {
      label.text.attr('fontWeight', '');
      label.animate('.2s', {
        x: 30
      });
      s.animate('.2s', {
        radius: 150,
        fillColor: kolor
      });
    }

    s.animate('.4s', {
      radius: 150,
      startAngle: start,
      endAngle: end
    }, {
      easing: 'sineOut',
      delay: animDelay
    });

    label.animate('.4s', {
      opacity: 1
    }, { delay: animDelay });

    this.angle = end;
  },

  label: function(name, v, fill) {

    var g = new Group().attr({
      x: 30,
      y: this.labelY,
      cursor: 'pointer'
    });

    var t = new Text(name + ' (' + v + '%)').addTo(g);
    var r = new Rect(0, 0, 20, 20, 5).fill(fill).addTo(g);

    t.attr({
      x: 30,
      y: 17,
      textFillColor: 'black',
      fontFamily: 'Arial',
      fontSize: '14'
    });

    g.addTo(stage);
    this.labelY += 30;

    g.text = t;

    return g;
  }
};







new PieChart(testobj1);

</script>



</div>  

 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 " > 

                <div id="player1"></div>
<script type="bs" id="bs1">


function Sector(x, y, radius, startAngle, endAngle) {
  SpecialAttrPath.call(this, {
    radius: 0,
    startAngle: startAngle,
    endAngle: endAngle
  });
  this.attr({
    x: x,
    y: y,
    radius: radius,
    startAngle: startAngle,
    endAngle: endAngle
  });
}

Sector.prototype = Object.create(SpecialAttrPath.prototype);
Sector.prototype._make = function() {

  var attr = this._attributes,
      radius = attr.radius,
      startAngle = attr.startAngle,
      endAngle = attr.endAngle;

  var startX, startY, endX, endY;
  var diffAngle = Math.abs(endAngle - startAngle);

  this.startX = startX = radius * Math.cos(startAngle);
  this.startY = startY = radius * Math.sin(startAngle);
  if (diffAngle < Math.PI*2) {
    endX = radius * Math.cos(endAngle);
    endY = radius * Math.sin(endAngle);
  } else { // angles differ by more than 2*PI: draw a full circle
    endX = startX;
    endY = startY - .0001;
  }

  this.endX = endX;
  this.endY = endY;

  this.radiusExtentX = radius * Math.cos(startAngle + (endAngle - startAngle)/2);
  this.radiusExtentY = radius * Math.sin(startAngle + (endAngle - startAngle)/2);

  return this.moveTo(0, 0)
    .lineTo(startX, startY)
    .arcTo(radius, radius, 0, (diffAngle < Math.PI) ? 0 : 1, 1, endX, endY)
    .lineTo(0, 0);

};

Sector.prototype.getDimensions = function() {
  var x = this.attr('x'),
      y = this.attr('y'),
      left = Math.min(x, x + this.startX, x + this.endX, x + this.radiusExtentX),
      top = Math.min(y, y + this.startY, y + this.endY, y + this.radiusExtentY),
      right = Math.max(x, x + this.startX, x + this.endX, x + this.radiusExtentX),
      bottom = Math.max(y, y + this.startY, y + this.endY, y + this.radiusExtentY);
  console.log(y, y + this.startY, y + this.endY, y + this.radiusExtentY)
  return {
    left: left,
    top: top,
    width: right - left,
    height: bottom - top
  };
};


PieChart.BASE_COLOR = color('red');

function PieChart(data) {
  this.angle = 0;
  this.labelY = 30;
  this.kolor = PieChart.BASE_COLOR.clone();
  var n = 0;
  for (var i in data) {
    this.slice(i, data[i], n++);
  }
}

PieChart.prototype = {
  slice: function(name, value, i) {

    var start = this.angle,
        end = start + (Math.PI*2) * value/100,
        // Increase hue by .1 with each slice (max of 10 will work)
        kolor = this.kolor = this.kolor.clone().hue(this.kolor.hue()+.1);

    var s = new Sector(
      350, 200, 100,
      start,
      end
    );

    var animDelay = (i * 200) + 'ms';

    var label = this.label(name, value, kolor);

    label.attr({ opacity: 0 });

    s.stroke('#FFF', 3);
    s.fill(kolor);

    s.attr({
      endAngle: start,
      radius: 0
    }).addTo(stage).on('mouseover', over).on('mouseout', out);
    label.on('mouseover', over).on('mouseout', out);

    function over() {
      label.text.attr('fontWeight', 'bold');
      label.animate('.2s', {
        x: 40
      });
      s.animate('.2s', {
        radius: 170,
        fillColor: kolor.lighter(.1)
      }, {
        easing: 'sineOut'
      });
    }

    function out() {
      label.text.attr('fontWeight', '');
      label.animate('.2s', {
        x: 30
      });
      s.animate('.2s', {
        radius: 150,
        fillColor: kolor
      });
    }

    s.animate('.4s', {
      radius: 150,
      startAngle: start,
      endAngle: end
    }, {
      easing: 'sineOut',
      delay: animDelay
    });

    label.animate('.4s', {
      opacity: 1
    }, { delay: animDelay });

    this.angle = end;
  },

  label: function(name, v, fill) {

    var g = new Group().attr({
      x: 30,
      y: this.labelY,
      cursor: 'pointer'
    });


    var t = new Text(name + ' (' + v + '%)').addTo(g);
    var r = new Rect(0, 0, 20, 20, 5).fill(fill).addTo(g);

    t.attr({
      x: 30,
      y: 17,
      textFillColor: 'black',
      fontFamily: 'Arial',
      fontSize: '14'
    });

    g.addTo(stage);
    this.labelY += 30;

    g.text = t;

    return g;
  }
};







new PieChart(testobj2);

</script>



</div>
      </div>          
      <div class="row"> 


<div id="chart"></div>    



      </div>
                        <div id="tablediv" class="body" style="display: none;">
                            <table  id="table" class="table table-bordered table-striped table-hover dataTable js-exportable">
                              
                              
                            </table>
                        </div>
                        
 
              <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div id="skills">
      <div id="skillmap">
        <div class="skills-wrapper">
          <div class="skills-sunburst"></div>
          <div class="skills-chart">
            <div id="skills-chart-breadcrumb"></div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="skillsdata.js"></script>
    <script type="text/javascript" src="skills.js"></script>
           
            </div> -->
                                  
                        
                        
                   

            </div>
               



</div>

    </section>

 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <!-- Bootstrap Core Css 
   <script src="../plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>
	<script src="https://cloud.github.com/downloads/uxebu/bonsai/bonsai-0.4.0.min.js"></script>
<script src="https://d3js.org/d3.v3.min.js"></script>

   

 
    <script src="../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="../plugins/dropzone/dropzone.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="../plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Multi Select Plugin Js -->
    <script src="../plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- Jquery Spinner Plugin Js -->
    <script src="../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- noUISlider Plugin Js -->


    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Custom Js -->



    



  
    <!-- Jquery Core Js -->


    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Chart Plugins Js -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>
    <script src="../js/pages/index.js"></script>
    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
</body>

</html>