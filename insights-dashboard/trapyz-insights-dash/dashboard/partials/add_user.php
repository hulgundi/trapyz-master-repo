<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusUid = $sessData['status']['uid'];
    $statusRole = $sessData['status']['role'];
    $statusMsgType = $sessData['status']['type'];

	} else if(empty($sessData['status']['msg']) && !isset($statusRole) ) {
		
	session_destroy();
	 header("Location:../../");	
		
	}
?>
﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Add User | Trapyz CONNECT</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
    <link href="../plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
  <script
			  src="https://code.jquery.com/jquery-2.2.4.min.js"
			  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
			  crossorigin="anonymous"></script>
    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../index">Trapyz CONNECT</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">


   <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">



                                    <li>
                                        <a href="javascript:void(0);">
                                          <i class="material-icons">person</i>Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a  href="../../userAccount?logoutSubmit=1">
                                           <i class="material-icons">input</i>Sign Out
                                        </a>
                                    </li>







                           
                        </ul>
                    </li>

                   
                 
                  


                </ul> 
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            

         <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>

                    <li>
                        <a href="../index">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="../partials/editor">
                            <i class="material-icons">map</i>
                            <span>Editor</span>
                        </a>
                    </li>
                    <li>
                        <a href="../partials/analytics">
                            <i class="material-icons">trending_up</i>
                            <span>Analytics</span>
                        </a>
                    </li>
                                     <li>
                        <a href="../partials/shops">
                            <i class="material-icons">shopping_cart</i>
                            <span>Stores</span>
                        </a>
                    </li>  
                      




<li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">message</i>
                            <span>Push Notification</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                        <a href="../partials/push">
                                    <span>Create Campaign</span>
                                </a>
                            </li>
                            <li>
                                <a href="../partials/campaigns">
                                    <span>All Campaigns</span>
                                </a>
                            </li>
                          
                        </ul>
                    </li>









  <li> 
                        <a href="users">
                            <i class="material-icons">group_add</i>
                            <span>Users</span>
                        </a>
                    </li>                     
                  <!--  <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Notifications <small>coming soon!</small></span>
                        </a>
                        
                    </li> -->
                  
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz CONNECT</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.1.7
                </div>
            </div>
            <!-- #Footer -->
        </aside>
       
    </section>

    <section class="content">
        <div class="container-fluid">
             <div class="card">
            <div class="body">

              		<?php 
              		if($statusMsgType == "success") {
              			              		echo !empty($statusMsg)?' <p class="alert alert-success">'.$statusMsg.'</p>':''; 
              
              			}else {
              				
              				
              		    		echo !empty($statusMsg)?'<p class="alert alert-danger">'.$statusMsg.'</p>':''; 		
              			}
              		
              		
              		

              		
              		
              		
              		
              		?>
                           <form action="../../userAccount" method="post" name="login">

                    <div class="msg">Register a new user</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Full Name" required autofocus>
                        </div>
                    </div>
                 
                    
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="confirm_password" minlength="6" placeholder="Confirm Password" required>
                        </div>
                    </div>
                                        <div class="input-group col-md-3">
<select name="role" class="form-control show-tick">
                              <option value="admin">Admin</option>
<option value="member">Member</option>
                                    </select>


                    </div>
                    
                    <button class="btn btn-block btn-lg bg-pink waves-effect" name="signupSubmit" value="CREATE ACCOUNT" type="submit">SIGN UP</button>
                    
                    </form>

                   

            </div> 
            	
           
        </div>
          <div class="card" id="script" style="display:none;">
           <p class="alert alert-info">Add these scripts in your	root html file </p>
          <br>
          <div class="form-group form-float">
                                   
                                
          <div class="form-line">
                                        <textarea id="textscript" cols="30" rows="15" class="form-control no-resize" aria-required="true">
                                        
                                        For HTTPS websites, TRAPYZ CONNECT integration is a two-step process:


           1. Placing the service-worker.js file in the root directory.

                 Download link: https://goo.gl/uurk0a
                                       
                
           2. Adding the following script tags in the header section of your home page in the given order:               

                 <script>var uid = ""</script>

                 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

					  <script src="https://cdnjs.cloudflare.com/ajax/libs/ClientJS/0.1.11/client.min.js"></script>

					  <script src="https://connect.trapyz.com/sdk/trapyz.connect.sdk.js"> </script>               
                           
                                        
                                        
                                        </textarea>

                                    </div>
          </div>
          </div>  
        
        <?php 
              	if($statusMsgType == "success") {
              			              		echo'	
<script type="text/javascript">
var textarea = document.getElementById("textscript");
document.getElementById("script").setAttribute("style", "display: block;");
textarea.innerHTML += "UID =" + uid;


 </script> '; 
              
              			}
              		
              		
              		

              		
              		
              		
              		
              		?>        </div>
              		<script type="text/javascript">
console.log(uid);
</script>
    </section>

    <!-- Jquery Core Js -->

    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="../plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/examples/sign-up.js"></script>
</body>

</html>