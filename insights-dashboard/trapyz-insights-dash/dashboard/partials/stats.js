var multiplier = 97;

function httpRequest(requestUrl, processFunction) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp["trapyzFunction"] = processFunction;

    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            xmlHttp["trapyzFunction"](xmlHttp.responseText);    
        } else {
            console.log("Waiting");
        }
    };
    xmlHttp.open("GET", requestUrl, true);
    xmlHttp.send(null);
}

function getTotalUsers() {
    httpRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=total", function(result) {
        document.getElementById("totalUsers").innerHTML = result * multiplier;
    });
}

function generateElement(elementType, styleClass, elementId, parentElement) {
    var element;

    element = document.createElement(elementType);
    element.className = styleClass;
    element.Id = elementId;
    parentElement.appendChild(element);
    return element;
}

function getTopCatUsers() {
    httpRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=catgid", function(result) {
        var jsonResult, sortedResult = [], resultString = "", parentElement, titleDiv, messageDiv;
        
        parentElement = document.getElementById("catUsers");
        jsonResult = JSON.parse(result);
        for (var key in jsonResult) {
            sortedResult.push([key, jsonResult[key] * multiplier]);
        }
        sortedResult.sort(function(a, b) {
            return b[1] - a[1];
        });
        var categoryData = [], colorCodes = ["#33C1FF", "#FFC733", "#33FFD4", "#900C3F", "#FF6133"];
        for (var index = 0; index < sortedResult.length && index < 5; index++) {
            var dataObject = {};
            dataObject["category"] = sortedResult[index][0];
            dataObject["column-2"] = sortedResult[index][1];
            dataObject["color"] = colorCodes[index];
            categoryData.push(dataObject);
        }
        AmCharts.makeChart("chartdiv",
            {
                "type": "serial",
                "categoryField": "category",
                "startDuration": 1,
                "categoryAxis": {
                    "gridPosition": "start"
                },
                "trendLines": [],
                "graphs": [
                    {
                        "fillAlphas": 1,
                        "id": "AmGraph-2",
                        "type": "column",
                        "colorField": "color",
                        "lineColorField": "color",
                        "valueField": "column-2"
                    }
                ],
                "guides": [],
                "valueAxes": [
                    {
                        "id": "ValueAxis-1",
                        "title": "Unique Users"
                    }
                ],
                "allLabels": [],
                "balloon": {},
                "titles": [
                    {
                        "id": "Title-1",
                        "size": 15,
                        "text": "Category-wise Unique Users"
                    }
                ],
                "dataProvider": categoryData,
                "categoryAxis": {
                    "autoRotateAngle": 90,
                    "autoRotateCount": 0,
                    "gridPosition": "start",
                    "title": "",
                    "titleBold": false,
                    "titleFontSize": 1,
                    "titleRotation": 90
                }
            }
        );
    })
}

function getWeeklyActiveUsers() {
    var endDate, startDate, weekInSeconds = 604800;

    endDate = parseInt((new Date().getTime()) / 1000);
    startDate = endDate - weekInSeconds;
    httpRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=week", function(result) {
        document.getElementById("weekUsers").innerHTML = result * multiplier;
    })
}

function getMonthlyActiveUsers() {
    var endDate, startDate, monthInSeconds = 2592000;

    endDate = parseInt((new Date().getTime()) / 1000);
    startDate = endDate - monthInSeconds;
    httpRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=month", function(result) {
        document.getElementById("monthUsers").innerHTML = result * multiplier;
    })
}

window.onload = function() {
    getTotalUsers();
    getTopCatUsers();
    getWeeklyActiveUsers();
    getMonthlyActiveUsers();
}


