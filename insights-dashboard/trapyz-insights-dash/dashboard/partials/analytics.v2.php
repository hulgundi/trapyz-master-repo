<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusUid = $sessData['status']['uid'];
    $statusRole = $sessData['status']['role'];
    $statusMsgType = $sessData['status']['type'];

	} else if(empty($sessData['status']['msg']) && !isset($statusRole) ) {
		
	session_destroy();
	 header("Location:../../");	
		
	}
?>

﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Analytics | Trapyz CONNECT</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">
	<script type="text/javascript" src="amcharts.js"></script>
		<script type="text/javascript" src="https:http://cdn.amcharts.com/lib/3/serial.js"></script>
			<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<style type="text/css">
#map {
        height: 100%;
      }
</style>


    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../index">Trapyz CONNECT</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">



                                    <li>
                                        <a href="javascript:void(0);">
                                          <i class="material-icons">person</i>Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="../../index">
                                           <i class="material-icons">input</i>Sign Out
                                        </a>
                                    </li>







                           
                        </ul>
                    </li>


                       
                    
                </ul> 
            </div>
        </div>
    </nav>
    

       

    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            
        <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>

                    <li>
                        <a href="../index">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                     
                   <li>
                        <a href="editor">
                            <i class="material-icons">map</i>
                            <span> Editor</span>
                        </a>
                    </li> 
                    <li>
                        <a href="analytics.v2">
                            <i class="material-icons">trending_up</i>
                            <span> Analytics</span>
                        </a>
                    </li> 
                                                 
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz CONNECT</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.1.7
                </div>
            </div>
            <!-- #Footer -->
        </aside>
    
    </section>
 <section class="content">
        <div class="container-fluid">
           	<script type="text/javascript">
			AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "Zone",
					"autoMarginOffset": 40,
					"marginRight": 60,
					"marginTop": 60,
					"startDuration": 1,
					"fontSize": 13,
	            "color": "#0000FF",
					"theme": "default",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"bulletSize": 10,
							"id": "AmGraph-1",
							"lineThickness": 4,
							"title": "graph 1",
							"type": "smoothedLine",
							"valueField": "Total Visitors"
						},
						{
							"id": "AmGraph-4",
							"title": "graph 4",
							"valueField": "New Visitors"
						},
						{
							"id": "AmGraph-5",
							"title": "graph 5",
							"valueField": "Repeat Visitors"
						}
						
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"dataProvider": [
						{
							"Zone": "Zone 1",
							"Total Visitors": "86",
							"New Visitors": "9",
							"Repeat Visitors": 48
						},
						{
							"Zone": "Zone  2",
							"Total Visitors": "68",
							"New Visitors": "8",
							"Repeat Visitors": "60"
						},
						{
							"Zone": "Zone 3",
							"Total Visitors": "29",
							"New Visitors": "7",
							"Repeat Visitors": "22"
						},
						{
							"Zone": "Zone 4",
							"Total Visitors": "14",
							"New Visitors": "9",
							"Repeat Visitors": "5"
						},
						{
							"Zone": "Zone 5",
							"Total Visitors": "24",
							"New Visitors": "7",
							"Repeat Visitors": "14"
						},
						{
							"Zone": "Zone 6",
							"Total Visitors": "78",
							"New Visitors": "6",
							"Repeat Visitors": "72"
						},
						{
							"Zone": "Zone 7",
							"Total Visitors": "46",
							"New Visitors": "5",
							"Repeat Visitors": "41"
						}
					]
				}
			);
		</script>
		<script type="text/javascript">
		
			AmCharts.makeChart("chartdiv1",	
			{
	"type": "serial",
	"categoryField": "Zone",
	"autoMarginOffset": 40,
	"marginRight": 60,
	"marginTop": 60,
	"startDuration": 1,
	"fontSize": 13,
	            "color": "#000",
	"handDrawScatter": 1,
	"handDrawThickness": 0,
	"theme": "default",
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonText": "[[title]] of [[category]]:[[value]]",
			"bullet": "round",
			"bulletSize": 10,
			"id": "AmGraph-1",
			"lineThickness": 6,
			"title": "graph 1",
			"type": "smoothedLine",
			"valueField": "Total Visitors"
		},
		{
			"id": "AmGraph-9",
			"title": "graph 9",
			"valueField": "Avg Dwell-Time (in mins)"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": ""
		}
	],
	"allLabels": [],
	"balloon": {},
	"titles": [],
	"dataProvider": [
		{
			"Zone": "Zone 1",
			"Avg Dwell-Time (in mins)": 23
		},
		{
			"Zone": "Zone  2",
			"Avg Dwell-Time (in mins)": 27
		},
		{
			"Zone": "Zone 3",
			"Avg Dwell-Time (in mins)": 35
		},
		{
			"Zone": "Zone 4",
			"Avg Dwell-Time (in mins)": 47
		},
		{
			"Zone": "Zone 5",
			"Avg Dwell-Time (in mins)": 21
		},
		{
			"Zone": "Zone 6",
			"Avg Dwell-Time (in mins)": 57
		},
		{
			"Zone": "Zone 7",
			"Avg Dwell-Time (in mins)": 22
		}
	]
} );	
		
		</script>
            <div class="row clearfix" >
                <!-- Line Chart -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Zonal Visitor Information</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
		<div id="chartdiv" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
                        </div>
                        

                    </div>
                </div>
             
            </div>
            
           
            
            
            
        </div>
        

    <div class="card"> <div class="container-fluid">
            
                  

            <div class="row clearfix" >
                        <div class="body">
   
   
  
                   <div id="map" style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
                   </div>
             
            </div>  
</div>
                   </div>
    </section>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyo8Nmv5-8EiDc9f_9JORIXHxucg470xI&libraries=visualization">
    </script>
    <script src="https://connect.trapyz.com/dashboard/partials/bialanal.js"></script>
    <!-- Jquery Core Js -->
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <script src="../js/admin.js"></script>


    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
</body>

</html>