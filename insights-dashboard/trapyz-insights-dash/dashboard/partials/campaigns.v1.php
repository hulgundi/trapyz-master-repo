<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusUid = $sessData['status']['uid'];
    $statusRole = $sessData['status']['role'];
    $statusMsgType = $sessData['status']['type'];

	} else if(empty($sessData['status']['msg']) && !isset($statusRole) ) {
		
	session_destroy();
	 header("Location:../../");	
		
	}
?>

﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Campaigns List | Trapyz CONNECT</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">
     <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
        <link href="../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
        
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.material.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" rel="stylesheet" />    
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.material.min.js"></script>


    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

   

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>



    <!-- Custom Js -->
    <script src="../js/admin.js"></script>

</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../stats">Trapyz CONNECT</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">


   <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">



                                  
                                    <li>
                                        <a href="../../userAccount?logoutSubmit=1">
                                           <i class="material-icons">input</i>Sign Out
                                        </a>
                                    </li>







                           
                        </ul>
                    </li>

                   
                 
                  


                </ul> 
            </div>
        </div>
    </nav>
    <<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            

      <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>
                    <li>
                        <a href="../stats">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                   <li>
                        <a href="audience.v1">
                            <i class="material-icons">accessibility</i>
                            <span> Audience</span>
                        </a>
                    </li> 
                    <li>
                        <a href="campaigns.v1">
                            <i class="material-icons">local_offer</i>
                            <span>Campaigns</span>
                        </a>
                    </li> 
                    <li>
                        <a href="push.v1">
                            <i class="material-icons">message</i>
                            <span>Push Notification</span>
                        </a>
                    </li> 
                             
                   
                                     
                  <!--  <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Notifications <small>coming soon!</small></span>
                        </a>
                        
                    </li> -->
                  
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz CONNECT</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.4
                </div>
            </div>
            <!-- #Footer -->
        </aside>
      
    </section>

    <section class="content">
        <div class="container-fluid">
                   <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                           <h2>
                   CAMPAIGNS LIST

                </h2>
                         
                        </div>
               
                      <div id="tablediv" class="body">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
         <script type="text/javascript">
                        
var xhr1;
   function getstores() {

 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr1 = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr1 = new ActiveXObject("Microsoft.XMLHTTP");
}
var data;
     xhr1.open("GET", "../../get_camp.php?uid="+uid+"C", true); 
     xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr1.send(data);
	 xhr1.onreadystatechange = display_data;
	function display_data() {
	 if (xhr1.readyState == 4) {
      if (xhr1.status == 200) {

  test2(xhr1.responseText);   
 
      } else {

      }
     }
	}
} 
var names = [];

function test2(readinput) {	

readinput = JSON.stringify(readinput).replace(/'/g, "");
readinput = JSON.parse(readinput);
//console.log(readinput);
names = JSON.parse(readinput);
//console.log(names);
 var removeDuplicatesInPlace = function(arr) {
      var i, j, cur, found;
      for (i = arr.length - 1; i >= 0; i--) {
        cur = arr[i].cid;
        found = false;
        for (j = i - 1; !found && j >= 0; j--) {
          if (cur === arr[j].cid) {
            if (i !== j) {
              arr.splice(j, 1);
            }
            found = true;
          }
        }
      }
      return arr;
    };


names = removeDuplicatesInPlace(names);



//console.log(names);
    var myTableDiv = document.getElementById("tablediv");

    var table = document.createElement('table');
table.setAttribute("id", "camptable");
table.className += " table";
  table.className += " table-bordered";
  table.className += " table-striped";
  table.className += " table-hover ";
  table.className += " dataTable";
  table.className += " js-exportable";



var thead = document.createElement('thead');
var tfoot = document.createElement('tfoot');

    
 var heading = new Array();
    heading[0] = "Campaign Name"
    heading[1] = "Message Title"
    heading[2] = "Audience"
  heading[3] = "Seen"
  heading[4] = "Clicked"
 heading[5] = "Created On"

  var tr = document.createElement('TR');
    thead.appendChild(tr);
    for (i = 0; i < heading.length; i++) {
        var th = document.createElement('TH')
        th.width = '233';
        th.appendChild(document.createTextNode(heading[i]));
        tr.appendChild(th);
    }
    table.appendChild(thead);


    if (!Array.isArray(names)) {
    	names = Object.values(names);
    }
    var tbody = document.createElement('tbody');
   // var d = names.length ? names.length : Object.keys(names).length;

    for (var i = 0; i < names.length; i++) {
    //console.log(cdatetrim);
    var row = document.createElement('TR');
    var td1 = document.createElement('Td');
    var td2 = document.createElement('Td');
    var td3 = document.createElement('Td');
    var td4 = document.createElement('Td');
    var td5 = document.createElement('Td');
    var td6 = document.createElement('Td');
 
    td1.width = '233';
    td2.width = '233';
    td3.width = '233';
    td4.width = '233';
    td5.width = '233';
    td6.width = '233';
    
    var resendbutton = document.createElement("button");
    resendbutton.setAttribute("class","btn btn-lg btn-primary waves-effect" );
    resendbutton.setAttribute("onclick", "pushnotify(this);");
    resendbutton.setAttribute("id", i);
    resendbutton.innerHTML = "Resend";
    if (names[i].createdon) {
    	    var cdatetrim = names[i].createdon.substring(0, 10);
    } else {
    	var cdatetrim = "";
    }


    td1.appendChild(document.createTextNode(names[i].cname));
    td2.appendChild(document.createTextNode(names[i].title));
    td3.appendChild(document.createTextNode(names[i].aname));
    td4.appendChild(document.createTextNode(names[i].imp));
    td5.appendChild(document.createTextNode(names[i].actions));
    td6.appendChild(document.createTextNode(cdatetrim));

    
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    row.appendChild(td6);

    tbody.appendChild(row);
    table.appendChild(tbody);
   
   
   
   
   }
    
    
    
    





  

myTableDiv.appendChild(table);

	}

function pushnotify(e) {
var id = e.id
  
var cname = names[id].cname;
var dataobj = {
cname : cname,
uid : uid

}
var senddata = [];
senddata.push(dataobj);
  
      var tempParams1 = JSON.stringify(senddata);

      $.ajax({
        url: "../../push_up.php",
        method: "POST",
        header: "ttl: 60",
        data: tempParams1,
        success: function(resdata) {},
        error: function(result, status, err) {
          console.log(result.responseText);
          
        }
      });
      
   
   
 
}

 window.onload = function() {

               getstores();
               
                
            };                          
                        
                        </script>
                     
    </section>



</body>

</html>