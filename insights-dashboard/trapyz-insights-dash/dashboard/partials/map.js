var map = (function() {
  "use strict";
  /* Utilities */
  var click = 1;
  var nodecount = 0;
  var floorno = "";
  var helpercount = 0;
  var floorlastid = "";
  var floorlastidedge = "";
  var xhr;
  var xhr1;
  
  var xhr8;
  var firstflooredge = [];
  var secondflooredge = [];
  var thirdflooredge = [];
  var fourthflooredge = [];
  var firstfloor = [];
  var secondfloor = [];
  var thirdfloor = [];
  var fourthfloor = [];
  var array1 = [];
  var array2 = [];
  var beacondata = [];
  var imgh;
  var imgw;
  var beaconsdata = [];
  var utils = {
    getOffset: function(node) {
      var boxCoords = node.getBoundingClientRect();

      return {
        x: Math.round(boxCoords.left + window.pageXOffset),
        y: Math.round(boxCoords.top + window.pageYOffset)
      };
    },
    getRightCoords: function(x, y) {
      return {
        x: x - app.getOffset("x"),
        y: y - app.getOffset("y")
      };
    },
    id: function(str) {
      return document.getElementById(str);
    },
    hide: function(node) {
      node.style.display = "none";

      return this;
    },
    show: function(node) {
      node.style.display = "block";

      return this;
    },
    encode: function(str) {
      return str.replace(/</g, "&lt;").replace(/>/g, "&gt;");
    },
    foreach: function(arr, func) {
      for (var i = 0, count = arr.length; i < count; i++) {
        func(arr[i], i);
      }
    },
    foreachReverse: function(arr, func) {
      for (var i = arr.length - 1; i >= 0; i--) {
        func(arr[i], i);
      }
    },
    debug: (function() {
      var output = document.getElementById("debug");

      return function() {
        output.innerHTML = [].join.call(arguments, " ");
      };
    })(),
    stopEvent: function(e) {
      e.stopPropagation();
      e.preventDefault();

      return this;
    },
    extend: function(obj, options) {
      var target = {};

      for (var name in obj) {
        if (obj.hasOwnProperty(name)) {
          target[name] = options[name] ? options[name] : obj[name];
        }
      }

      return target;
    },
    inherits: (function() {
      var F = function() {};

      return function(Child, Parent) {
        F.prototype = Parent.prototype;
        Child.prototype = new F();
        Child.prototype.constructor = Child;
      };
    })()
  };

  var app = (function() {
    var domElements = {
      image_wrapper: utils.id("image_wrapper"),
      wrapper: utils.id("wrapper"),
      svg: utils.id("svg"),
      img: utils.id("img"),
      container: utils.id("image"),
      map: null
    },
      state = {
        offset: {
          x: 0,
          y: 0
        },
        appMode: null,
        currentType: null,
        editType: null,
        newArea: null,
        selectedArea: null,
        areas: [],
        helperpoints: [],
        coords: [],
        connect: [],
        floorno: 1,
        newhelperpoint: null,
        selectedhelperpoint: null,
        events: [],
        edges: [],
        newEdge: null,
        selectedEdge: null,
        isDraw: false,
        image: {
          src: null,
          filename: null,
          width: 0,
          height: 0
        }
      },
      KEYS = {
        F1: 112,
        TOP: 38,
        BOTTOM: 40,
        LEFT: 37,
        RIGHT: 39,
        I: 73,
        S: 83,
        C: 67
      };

    function recalcOffsetValues() {
      state.offset = utils.getOffset(domElements.container);
    }

    /* Get offset value */
    window.addEventListener("resize", recalcOffsetValues, false);

    /* Disable selection */
    domElements.container.addEventListener(
      "mousedown",
      function(e) {
        e.preventDefault();
      },
      false
    );

    /* Disable image dragging */
    domElements.img.addEventListener(
      "dragstart",
      function(e) {
        e.preventDefault();
      },
      false
    );

    /* Display cursor coordinates info */
    var cursor_position_info = (function() {
      var coords_info = utils.id("coords");

      return {
        set: function(coords) {
          coords_info.innerHTML = "x: " + coords.x + ", " + "y: " + coords.y;
        },
        empty: function() {
          coords_info.innerHTML = "";
        }
      };
    })();

    domElements.container.addEventListener(
      "mousemove",
      function(e) {
        cursor_position_info.set(utils.getRightCoords(e.pageX, e.pageY));
      },
      false
    );

    domElements.container.addEventListener(
      "mouseleave",
      function() {
        cursor_position_info.empty();
      },
      false
    );

    /* Add mousedown event for svg */
    function onSvgMousedown(e) {
      if (state.appMode === "editing") {
        if (e.target.parentNode.tagName === "g") {
          info.unload();
          checkedge();
          state.selectedArea = e.target.parentNode.obj;
          state.selectedArea.editingStartPoint = {
            x: e.pageX,
            y: e.pageY
          };
          if (e.target.classList.contains("helper")) {
            var helper = e.target;
            state.editType = helper.action;
            if (helper.n >= 0) {
              // if typeof selected_area == polygon
              state.selectedArea.selected_point = helper.n;
            }
            var connect = {
              x: check(
                utils.getRightCoords(e.pageX, e.pageY).x,
                utils.getRightCoords(e.pageX, e.pageY).y
              ).x,
              y: check(
                utils.getRightCoords(e.pageX, e.pageY).x,
                utils.getRightCoords(e.pageX, e.pageY).y
              ).y
            };
            var a = check(connect.x, connect.y);
            state.selectedArea.helperid = a.c;
            var t = app.helperpoints;
            var ty = app.coords;
            app.addConnector(connect);
            app
              .addEvent(
                app.domElements.container,
                "mousemove",
                state.selectedArea.onProcessEditing.bind(state.selectedArea)
              )
              .addEvent(
                app.domElements.container,
                "mouseup",
                state.selectedArea.onStopEditing.bind(state.selectedArea)
              );
          } else if (e.target.tagName === 'rect') {
                        state.editType = 'move';
                        
                        app.addEvent(app.domElements.container,
                                     'mousemove',
                                     state.selectedArea.onProcessEditing.bind(state.selectedArea))
                           .addEvent(app.domElements.container,
                                     'mouseup',
                                     state.selectedArea.onStopEditing.bind(state.selectedArea));
                    }
        } else {
          app.deselectAll();
          info.unload();
        }
      }
    }

    domElements.container.addEventListener("mousedown", onSvgMousedown, false);

    /* Add click event for svg */

    function onSvgClick(e) {
      if (state.appMode === "drawing" && !state.isDraw && state.currentType) {
        // code.hide();
        app.setIsDraw(true);
        state.newArea = Area.CONSTRUCTORS[
          state.currentType
        ].createAndStartDrawing(utils.getRightCoords(e.pageX, e.pageY));
      } else if (
        state.appMode === "delete" && e.target.classList.contains("helper")
      ) {
        var connect = {
          x: check(
            utils.getRightCoords(e.pageX, e.pageY).x,
            utils.getRightCoords(e.pageX, e.pageY).y
          ).x,
          y: check(
            utils.getRightCoords(e.pageX, e.pageY).x,
            utils.getRightCoords(e.pageX, e.pageY).y
          ).y,
          id: check(
            utils.getRightCoords(e.pageX, e.pageY).x,
            utils.getRightCoords(e.pageX, e.pageY).y
          ).c
        };
        deletenoderedraw(connect);
      }
    }
    domElements.container.addEventListener("click", onSvgClick, false);

    /** Add dblclick event for svg  **/

    function onAreaDblClick(e) {
      if (state.appMode === "editing") {
        if (e.target.tagName === "rect") {
          state.selectedhelperpoint = app.helperpoints[
            state.selectedArea.helperid
          ];
          info.load(state.selectedhelperpoint, e.pageX, e.pageY);
        }
      }
    }

    domElements.container.addEventListener("dblclick", onAreaDblClick, false);

    /* Add keydown event for document */
    function onDocumentKeyDown(e) {
      var ctrlDown = e.ctrlKey || e.metaKey; // PC || Mac

      switch (e.keyCode) {
        case KEYS.F1:
          help.show();
          e.preventDefault();

          break;

        case KEYS.TOP:
          if (state.appMode === "editing" && state.selectedArea) {
            state.selectedArea.move(0, -1);
            e.preventDefault();
          }

          break;

        case KEYS.BOTTOM:
          if (state.appMode === "editing" && state.selectedArea) {
            state.selectedArea.move(0, 1);
            e.preventDefault();
          }
          break;

        case KEYS.LEFT:
          if (state.appMode === "editing" && state.selectedArea) {
            state.selectedArea.move(-1, 0);
            e.preventDefault();
          }

          break;

        case KEYS.RIGHT:
          if (state.appMode === "editing" && state.selectedArea) {
            state.selectedArea.move(1, 0);
            e.preventDefault();
          }

          break;

        case KEYS.I:
          if (state.appMode === "editing" && state.selectedArea) {
            var coordsForAttributesForm = state.selectedArea.getCoordsForDisplayingInfo();

            info.load(
              state.selectedArea,
              coordsForAttributesForm.x + app.getOffset("x"),
              coordsForAttributesForm.y + app.getOffset("y")
            );
          }

          break;

        case KEYS.S:
          if (ctrlDown) {
            app.saveInLocalStorage();
          }

          break;

        case KEYS.C:
          if (state.appMode === "editing" && state.selectedArea && ctrlDown) {
            state.selectedArea = Area.copy(state.selectedArea);
          }

          break;
      }
    }

    document.addEventListener("keydown", onDocumentKeyDown, false);

    var localStorageWrapper = (function() {
      var KEY_NAME = "map";

      return {
        save: function() {
          var result = areasIO.toJSON();
          window.localStorage.setItem(KEY_NAME, result);
          // // console.info("Editor " + result + " saved");

          alert("Saved");
        },
        restore: function() {
          areasIO.fromJSON(window.localStorage.getItem(KEY_NAME));
        }
      };
    })();

    return {
      events: state.events,
      coords: state.coords,
      appMode: state.appMode,
      areas: state.areas,
      connect: state.connect,
      helperpoints: state.helperpoints,
      edges: state.edges,
      domElements: domElements,
      saveInLocalStorage: localStorageWrapper.save,
      loadFromLocalStorage: localStorageWrapper.restore,
      hide: function() {
        utils.hide(domElements.container);
        return this;
      },
      show: function() {
        utils.show(domElements.container);
        return this;
      },
      recalcOffsetValues: function() {
        recalcOffsetValues();
        return this;
      },
      setDimensions: function(width, height) {
        domElements.svg.setAttribute("width", width);
        domElements.svg.setAttribute("height", height);
        domElements.container.style.width = width + "px";
        domElements.container.style.height = height + "px";
        return this;
      },
      setDimensions1: function(width, height) {
        domElements.svg.setAttribute("width", width);
        domElements.svg.setAttribute("height", height);
        domElements.container.style.width = width + "px";
        domElements.container.style.height = height + "px";
        //  domElements.img.style.width = width + "px";
        //  domElements.img.style.height = height + "px";
        return this;
      },
      loadImage: function(url) {
        get_image.showLoadIndicator();
        domElements.img.src = url;
        state.image.src = url;

        domElements.img.onload = function() {
          get_image.hideLoadIndicator().hide();
          imgw = domElements.img.width;
          imgh = domElements.img.height;
          app
            .show()
            .setDimensions(domElements.img.width, domElements.img.height)
            .recalcOffsetValues();
        };
        return this;
      },
      preview: (function() {
        domElements.img.setAttribute("usemap", "#map");
        domElements.map = document.createElement("map");
        domElements.map.setAttribute("name", "map");
        domElements.container.appendChild(domElements.map);

        return function() {
          info.unload();
          app.setShape(null);
          utils.hide(domElements.svg);
          domElements.map.innerHTML = app.getHTMLCode();
          code.print();
          return this;
        };
      })(),
      hidePreview: function() {
        utils.show(domElements.svg);
        domElements.map.innerHTML = "";
        return this;
      },
      addNodeToSvg: function(node) {
        domElements.svg.appendChild(node);
        return this;
      },
      removeNodeFromSvg: function(node) {
        domElements.svg.removeChild(node);
        return this;
      },
      removeNodeFromG: function(node) {
        domElements.svg.removeChild(node);
        return this;
      },
      getOffset: function(arg) {
        switch (arg) {
          case "x":
          case "y":
            return state.offset[arg];
        }
      },
      clear: function() {
        state.areas.length = 0;
        state.edges.length = 0;
        state.helperpoints.length = 0;
        state.areas.length = 0;
        state.coords.length = 0;
        while (domElements.svg.childNodes[0]) {
          domElements.svg.removeChild(domElements.svg.childNodes[0]);
        }

        info.unload();
        return this;
      },
      removeObject: function(obj) {
        utils.foreach(state.areas, function(x, i) {
          if (x === obj) {
            state.areas.splice(i, 1);
          }
        });
        utils.foreach(state.edges, function(x, i) {
          if (x === obj) {
            state.edges.splice(i, 1);
          }
        });
        utils.foreach(state.helperpoints, function(x, i) {
          if (x === obj) {
            state.helperpoints.splice(i, 1);
          }
        });
        obj.remove();
        return this;
      },
      deselectAll: function() {
        utils.foreach(state.areas, function(x) {
          x.deselect();
        });

        return this;
      },
      getIsDraw: function() {
        return state.isDraw;
      },
      setIsDraw: function(arg) {
        state.isDraw = arg;
        return this;
      },
      setMode: function(arg) {
        state.appMode = arg;
        return this;
      },
      getMode: function() {
        return state.appMode;
      },
      setShape: function(arg) {
        state.currentType = arg;
        return this;
      },
      getShape: function() {
        return state.currentType;
      },
      addObject: function(object) {
        state.areas.push(object);
        return this;
      },
      addHelper: function(object) {
        state.helperpoints.push(object);
        return this;
      },
      addEdge: function(object) {
        state.edges.push(object);
        return this;
      },
      addCoords: function(object) {
        state.coords.push(object);
        return this;
      },
      addConnector: function(object) {
        state.connect.push(object);
        return this;
      },
      getNewArea: function() {
        return state.newArea;
      },
      resetNewArea: function() {
        state.newArea = null;
        return this;
      },
      getSelectedArea: function() {
        return state.selectedArea;
      },
      setSelectedArea: function(obj) {
        state.selectedArea = obj;
        return this;
      },
      getEditType: function() {
        return state.editType;
      },
      setFilename: function(str) {
        state.image.filename = str;
        return this;
      },
      setEditClass: function() {
        domElements.container.classList.remove("draw");
        domElements.container.classList.add("edit");

        return this;
      },
      setDrawClass: function() {
        domElements.container.classList.remove("edit");
        domElements.container.classList.add("draw");

        return this;
      },
      setDefaultClass: function() {
        domElements.container.classList.remove("edit");
        domElements.container.classList.remove("draw");

        return this;
      },
      addEvent: function(target, eventType, func) {
        state.events.push(new AppEvent(target, eventType, func));
        return this;
      },
      addEvent1: function(target, eventType, func1, func2) {
        state.events.push(new AppEvent1(target, eventType, func1, func2));
        return this;
      },
      removeAllEvents: function() {
        utils.foreach(state.events, function(x) {
          x.remove();
        });
        state.events.length = 0;
        return this;
      },
      getHTMLCode: function(arg) {
        var html_code = "";
        var txtcode = "";
        if (arg) {
          if (!state.helperpoints.length) {
            return "0 objects";
          }
          for (var i = 0, count = state.helperpoints.length; i < count; i++) {
            var z = state.helperpoints[i];
            html_code += utils.encode(Helper.toHTMLMapElementString(z, i));
            txtcode += Helper.toHTMLMapElementString(z, i) + "\n";
          }
        } else {
          utils.foreachReverse(state.helperpoints, function(Helper) {
            html_code += Helper.toHTMLMapElementString();
          });
        }
        download("trapyz-nodes.txt", txtcode);

        return html_code;
      },
      getHTMLCodeEdge: function(arg) {
        var txtcode = "";
        var html_code = "";
        if (arg) {
          if (!state.edges.length) {
            return "0 objects";
          }
          for (var i = 0, count = state.edges.length; i < count; i++) {
            var z = state.edges[i];
            html_code += "" +
              utils.encode(Edge.toHTMLMapElementString(z, i)) +
              "<br />";
            txtcode += Edge.toHTMLMapElementString(z, i) + "\n";
          }
        } else {
          utils.foreachReverse(state.edges, function(c) {
            html_code += Edge.toHTMLMapElementString();
          });
        }

        download("trapyz-edges.txt", txtcode);
        return html_code;
      }
    };
  })();

  function AppEvent(target, eventType, func) {
    this.target = target;
    this.eventType = eventType;
    this.func = func;

    target.addEventListener(eventType, func, false);
  }

  AppEvent.prototype.remove = function() {
    this.target.removeEventListener(this.eventType, this.func, false);
  };

  function deletenoderedraw(connect) {
    var a = "";
    var c = app.edges.length;
    var t = [];
    for (var i = app.edges.length - 1; i >= 0; i--) {
      if (
        Math.abs(connect.x - app.edges[i]._to.x) < 10 &&
          Math.abs(connect.y - app.edges[i]._to.y) < 10
      ) {
        app.edges[i]._el.remove();
        app.edges.splice(i, 1);
        t.push(i);
      } else if (
        Math.abs(connect.x - app.edges[i]._from.x) < 10 &&
          Math.abs(connect.y - app.edges[i]._from.y) < 10
      ) {
        app.edges[i]._el.remove();
        app.edges.splice(i, 1);
        t.push(i);
      }
    }

    var array3 = [];
    var array4 = [];
    var to_id = "";
    var from_id = "";
    if (floorno == 1) {
      // array.concat(firstfloor);
      array3 = firstfloor;
      array4 = firstflooredge;
      // firstfloor.length = 0;
    } else if (floorno == 2) {
      array3 = secondfloor;
      array4 = secondflooredge;
    } else if (floorno == 3) {
      array3 = thirdfloor;
      array4 = thirdflooredge;
    } else if (floorno == 4) {
      array3 = fourthfloor;
      array4 = fourthflooredge;
    }

    for (var i = array3.length - 1; i >= 0; i--) {
      if (array3[i].x == connect.x && array3[i].y == connect.y) {
        to_id = array3[i].id;
        array3.splice(i, 1);
      }
    }

    for (var i = array4.length - 1; i >= 0; i--) {
      if (array4[i].to == to_id || array4[i].from == to_id) {
        array4.splice(i, 1);
      }
    }

    //	console.log(floorno);
    //console.log(firstfloor.length);
    //		console.log(array3.length);
    app.helperpoints[connect.id]._el.remove();
    app.helperpoints.splice(connect.id, 1);
    app.coords.splice(connect.id, 1);
  }

  function download(filename, text) {
    var element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/plain;charset=utf-8," + encodeURIComponent(text)
    );
    element.setAttribute("download", filename);

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  function newCoords(i, x, y) {
    app.helperpoints[i]._coords.x = x;
    app.helperpoints[i]._coords.y = y;
  }
	    /**
     * The constructor of helpers points
     * Helper is small svg-rectangle with some actions
     * 
     * @constructor
     * @param node {DOMElement} - a node for inserting helper
     * @param x {number} - x-coordinate of helper
     * @param y {number} - y-coordinate of helper
     * @param action {string} - an action by click of this helper (e.g. 'move')
     */
    function HelperZ(node, x, y, action) {

        this._el = document.createElementNS(Area.SVG_NS, 'rect');
        
        this._el.classList.add(HelperZ.CLASS_NAME);
        this._el.setAttribute('height', HelperZ.SIZE);
        this._el.setAttribute('width', HelperZ.SIZE);
        this._el.setAttribute('x', x + HelperZ.OFFSET);
        this._el.setAttribute('y', y + HelperZ.OFFSET);

        node.appendChild(this._el);
                    	console.log(x,y, this._el);
        this._el.action = action; // TODO: move 'action' from dom el to data-attr
        this._el.classList.add(Helper.ACTIONS_TO_CURSORS[action]);
    }
    
    HelperZ.SIZE = 5;
    HelperZ.OFFSET = -Math.ceil(HelperZ.SIZE / 2);
    HelperZ.CLASS_NAME = 'helper';
    HelperZ.ACTIONS_TO_CURSORS = {
        'move'            : 'move',
        'editLeft'        : 'e-resize',
        'editRight'       : 'w-resize',
        'editTop'         : 'n-resize',
        'editBottom'      : 's-resize',
        'editTopLeft'     : 'nw-resize',
        'editTopRight'    : 'ne-resize',
        'editBottomLeft'  : 'sw-resize',
        'editBottomRight' : 'se-resize',
        'movePoint'       : 'pointer'
    };

    /**
     * Set coordinates for this helper
     * 
     * @param x {number} - x-coordinate
     * @param y {number} - y-coordinate
     * @returns {Helper}
     */
    HelperZ.prototype.setCoords = function(x, y) {
        this._el.setAttribute('x', x + HelperZ.OFFSET);
        this._el.setAttribute('y', y + HelperZ.OFFSET);
        
        return this;
    };
    
    /**
     * Set id of this helper in list of parent's helpers
     * 
     * @param id {number} 
     * @returns {Helper}
     */
    HelperZ.prototype.setId = function(id) {
        // TODO: move n-field from DOM-element to data-attribute
        this._el.n = id;
        
        return this;
    };
      
  
  
  
  function Helper(node, x, y, action, attributes, beacondata) {
    this._el = document.createElementNS(Area.SVG_NS, "rect");
    this._el.classList.add(Helper.CLASS_NAME);
    this._el.setAttribute("height", Helper.SIZE);
    this._el.setAttribute("width", Helper.SIZE);
    var ax = x - 5;
    var ay = y - 5;
    this._el.setAttribute("x", ax);
    this._el.setAttribute("y", ay);
    node.appendChild(this._el);
    this._el.action = action; // TODO: move "action" from dom el to data-attr
    this._el.classList.add(Helper.ACTIONS_TO_CURSORS[action]);
    var gui = guidGenerator();
    var n = "node" + floorno * 1000 + helpercount + nodecount;
    var isD = "FALSE";
    var isE = "FALSE";
    this._attributes = {
      name: n,
      isDestination: isD,
      isEscalator: isE
    };
    this._coords = {
      x: x,
      y: y
    };
    var points = {
      x: x,
      y: y
    };
   
    function guidGenerator() {
      var S4 = function() {
        return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
      };

      return S4() + S4();
    }

    this._id = n;
    app.addCoords(points);
    app.addHelper(this);
    if (attributes) {
      this.setInfoAttributes(attributes);
    }
    
 this._beacondata = {
		beaconid: "",
		nodeid: this._id    
    }
   if (beacondata) {
      this.setBeacondata(beacondata);
    }
    helpercount++;
  }

  Helper.SIZE = 10;
  Helper.OFFSET = -Math.ceil(Helper.SIZE / 2);
  Helper.CLASS_NAME = "helper";
  Helper.ACTIONS_TO_CURSORS = {
    move: "move",
    editLeft: "e-resize",
    editRight: "w-resize",
    editTop: "n-resize",
    editBottom: "s-resize",
    editTopLeft: "nw-resize",
    editTopRight: "ne-resize",
    editBottomLeft: "sw-resize",
    editBottomRight: "se-resize",
    movePoint: "pointer"
  };

  Helper.prototype.setCoords = function(x, y) {
    this._el.setAttribute("x", x - 5);
    this._el.setAttribute("y", y - 5);

    return this;
  };

  Helper.prototype.setId = function(id) {
    this._el.n = id;

    return this;
  };
  Helper.prototype.remove = function() {
    app.removeNodeFromG(this);
  };

  Helper.prototype.setInfoAttributes = function(attributes) {
    this._attributes.name = attributes.name;
    this._attributes.isDestination = attributes.isDestination;
    this._attributes.isEscalator = attributes.isEscalator;
  };
  Helper.prototype.setBeacondata = function(data) {
    this._beacondata.beaconid = data.beaconid;
    this._beacondata.nodeid = data.nodeid;

  };
  Helper.toHTMLMapElementString = function(z, i) {
    var res = "" +
      z._id +
      ":" +
      z._attributes.name +
      ":" +
      z._coords.x +
      ":" +
      z._coords.y +
      ":" +
      floorno +
      ":" +
      z._attributes.isDestination +
      ":" +
      z._attributes.isEscalator +
      "";
    return res;
  };
  Helper.prototype.ABSTRACT_METHOD;

  Edge.toHTMLMapElementString = function(z, i) {
    var str = "" + z._id + ":" + z._from.id + ":" + z._to.id + "";
    return str;
  };

  var dataupdate = function(z) {
    var arrayfloor = [];

    for (var i = 0, c = app.helperpoints.length; i < c; i++) {
      var g = {
        //id: "node" + floorno*1000 + arrayfloor.length,
        id: app.helperpoints[i]._id,
        name: app.helperpoints[i]._attributes.name,
        x: app.helperpoints[i]._coords.x,
        y: app.helperpoints[i]._coords.y,
        floorno: floorno,
        isD: app.helperpoints[i]._attributes.isDestination,
        isE: app.helperpoints[i]._attributes.isEscalator,
        beaconid: app.helperpoints[i]._beacondata.beaconid,
        nodeid: app.helperpoints[i]._beacondata.nodeid
      };
      arrayfloor.push(g);
      /**for (var j = 0, c = app.edges.length; j < c; j++) {
	
	if (app.helperpoints[i]._id == app.edges[j]._from.id ) {
		
		app.edges[j]._from.id = g.id;
		
		} else if (app.helperpoints[i]._id == app.edges[j]._to.id) {
			
					app.edges[j]._to.id = g.id;
			
			}
	}**/
    }

    z = arrayfloor;
  //  console.log(z);
  //  console.log(app.helperpoints);
    return z;
  };

  var dataupdateedge = function(z) {
    var arrayedge = [];

    for (var i = 0, c = app.edges.length; i < c; i++) {
      var g = {
        //id: "edge" + floorno*1000 + arrayedge.length,
        id: app.edges[i]._id,
        from: app.edges[i]._from.id,
        to: app.edges[i]._to.id
      };
      arrayedge.push(g);
    }
    z = arrayedge;
  //  console.log(z);
  //  console.log(app.edges);
    return z;
  };

  function Edge(node, coords, x, y, fromnode, tonode) {
    var i = coords.length;
    var fromnodeid = app.helperpoints[
      checkfromnodeid(
        app.helperpoints[fromnode]._coords.x,
        app.helperpoints[fromnode]._coords.y
      )
    ]._id;
    var tonodeid = app.helperpoints[checktonodeid(x, y)]._id;
    this._from = {
      x: coords[i - 2].x,
      y: coords[i - 2].y,
      id: fromnodeid
    };

    this._to = {
      x: x,
      y: y,
      id: tonodeid
    };

    function guidGenerator() {
      var S4 = function() {
        return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
      };

      return S4() + S4();
    }

    this._id = "edge" + floorno * 1000 + app.edges.length;
    this._el = document.createElementNS(Area.SVG_NS, "polyline");
    var polygonPointsAttrValue = this._from.x +
      " " +
      this._from.y +
      " " +
      this._to.x +
      " " +
      this._to.y +
      " ";
    this._el.setAttribute("points", polygonPointsAttrValue);
    node.appendChild(this._el);
    app.addEdge(this);
  }

  function Edgeimport(fromnode, tonode) {
    var fromcoords = checkfrom(fromnode);
    var tocoords = checkfrom(tonode);
    this._from = {
      x: fromcoords.x,
      y: fromcoords.y,
      id: fromnode
    };

    this._to = {
      x: tocoords.x,
      y: tocoords.y,
      id: tonode
    };
    function guidGenerator() {
      var S4 = function() {
        return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
      };

      return S4() + S4();
    }

    this._id = "edge" + floorno * 1000 + app.edges.length;
    this._el = document.createElementNS(Area.SVG_NS, "polyline");
    var polygonPointsAttrValue = this._from.x +
      " " +
      this._from.y +
      " " +
      this._to.x +
      " " +
      this._to.y +
      " ";
    this._el.setAttribute("points", polygonPointsAttrValue);
    app.addNodeToSvg(this._el);
    app.addEdge(this);
  }

  Edge.prototype.remove = function() {
    app.removeNodeFromG(this);
  };

  function moveEdge(selectedhelperpoint, startpoint) {
    for (var i = 0, c = app.edges.length; i < c; i++) {
      if (
        Math.abs(startpoint.x - app.edges[i]._to.x) < 10 &&
          Math.abs(startpoint.y - app.edges[i]._to.y) < 10
      ) {
        var polygonPointsAttrValue = app.edges[i]._from.x +
          " " +
          app.edges[i]._from.y +
          " " +
          selectedhelperpoint.x +
          " " +
          selectedhelperpoint.y +
          " ";
        app.edges[i]._to.x = selectedhelperpoint.x;
        app.edges[i]._to.y = selectedhelperpoint.y;
        app.edges[i]._el.setAttribute("points", polygonPointsAttrValue);
      } else if (
        Math.abs(startpoint.x - app.edges[i]._from.x) < 10 &&
          Math.abs(startpoint.y - app.edges[i]._from.y) < 10
      ) {
        var polygonPointsAttrValue = selectedhelperpoint.x +
          " " +
          selectedhelperpoint.y +
          " " +
          app.edges[i]._to.x +
          " " +
          app.edges[i]._to.y +
          " ";
        app.edges[i]._from.x = selectedhelperpoint.x;
        app.edges[i]._from.y = selectedhelperpoint.y;
        app.edges[i]._el.setAttribute("points", polygonPointsAttrValue);
      }
    }
  }

  function check(x, y) {
    var a = 1;
    for (var i = 0, c = app.helperpoints.length; i < c; i++) {
      if (
        Math.abs(x - app.helperpoints[i]._coords.x) < 10 &&
          Math.abs(y - app.helperpoints[i]._coords.y) < 10
      ) {
        x = app.helperpoints[i]._coords.x;
        y = app.helperpoints[i]._coords.y;
        a = 0;
        c = i;
      }
    }

    var r = {
      a: a,
      x: x,
      y: y,
      c: c
    };

    return r;
  }

  function checkfrom(from) {
    for (var i = 0, c = app.helperpoints.length; i < c; i++) {
      if (from == app.helperpoints[i]._id) {
        var t = {
          x: app.helperpoints[i]._coords.x,
          y: app.helperpoints[i]._coords.y
        };

        return t;
      }
    }
  }

  function checkedge() {
    for (var i = 0, c = app.edges.length; i < c; i++) {
      if (
        app.edges[i]._to.x == app.edges[i]._from.x &&
          app.edges[i]._to.y == app.edges[i]._from.y
      ) {
        app.edges.splice(i, 1);
      }
    }
  }
  function checkfromnodeid(x, y) {
    for (var i = 0, c = app.helperpoints.length; i < c; i++) {
      if (
        x == app.helperpoints[i]._coords.x && y == app.helperpoints[i]._coords.y
      ) {
        x = app.helperpoints[i]._coords.x;
        y = app.helperpoints[i]._coords.y;

        var t = i;
      }
    }
    return t;
  }

  function checktonodeid(x, y) {
    for (var i = 0, c = app.helperpoints.length; i < c; i++) {
      if (
        x == app.helperpoints[i]._coords.x && y == app.helperpoints[i]._coords.y
      ) {
        x = app.helperpoints[i]._coords.x;
        y = app.helperpoints[i]._coords.y;

        var t = i;
      }
    }
    return t;
  }

  function Area(type, coords, attributes) {
    if (this.constructor === Area) {
      throw new Error("This is abstract class");
    }

    this._type = type;

    this._coords = coords;

    this._groupEl = document.createElementNS(Area.SVG_NS, "g");
    app.addNodeToSvg(this._groupEl);
    this._groupEl.obj = this;
    this._helpers = {};
    app.addObject(this);
  }
  Area.SVG_NS = "http://www.w3.org/2000/svg";
  Area.CLASS_NAMES = {
    SELECTED: "selected",
    WITH_HREF: "with_href"
  };
  Area.CONSTRUCTORS = {
  	 rectangle: Rectangle,	
    waypoint: Wayp
  };
  Area.REGEXP = {
    AREA: /<area(?=.*? shape="(rect|poly|waypo)")(?=.*? coords="([\d ,]+?)")[\s\S]*?>/gmi,
    DELIMETER: / ?, ?/
  };
  Area.HTML_NAMES_TO_AREA_NAMES = {
    poly: "polygon",
    waypo: "waypoint",
    rect : 'rectangle',
  };

  
  
  Area.ATTRIBUTES_NAMES = ["NAME", "ISDESTINATION", "ISESCALATOR"];

  Area.prototype.ABSTRACT_METHOD = function() {
    throw new Error("This is abstract method");
  };

  Area.prototype.setSVGCoords = Area.prototype.setCoords = Area.prototype.dynamicDraw = Area.prototype.onProcessDrawing = Area.prototype.onStopDrawing = Area.prototype.edit = Area.prototype.dynamicEdit = Area.prototype.onProcessEditing = Area.prototype.onStopEditing = Area.prototype.toString = Area.prototype.getCoordsForDisplayingInfo = Area.prototype.ABSTRACT_METHOD;

  Area.prototype.redraw = function(coords) {
   // this.setSVGCoords(app.coords);
        this.setSVGCoords(coords ? coords : this._coords);
    return this;
  };

  Area.prototype.remove = function() {
    app.removeNodeFromSvg(this._groupEl);
  };

  Area.prototype.move = function(dx, dy) {
    return this;
  };

  Area.prototype.select = function() {
    this._el.classList.add(Area.CLASS_NAMES.SELECTED);

    return this;
  };

  Area.prototype.deselect = function() {
    return this;
  };

  Area.prototype.setStyleOfElementWithHref = function() {
    return this;
  };

  Area.prototype.unsetStyleOfElementWithHref = function() {
    return this;
  };

  Area.prototype.toJSON = function() {
    return {
      type: this._type,
      coords: this._coords,
      attributes: this._attributes
    };
  };

  Area.fromJSON = function(params) {
    var AreaConstructor = Area.CONSTRUCTORS[params.type];

    if (!AreaConstructor) {
      throw new Error("This area type is not valid");
    }

    if (!AreaConstructor.testCoords(params.coords)) {
      throw new Error("This coords is not valid for " + params.type);
    }

    app.setIsDraw(true);

    var area = new AreaConstructor(params.coords, params.attributes);

    app.setIsDraw(false).resetNewArea();
    pop;
    return area;
  };

  Area.copy = function(originalArea) {
    return Area.fromJSON(originalArea.toJSON()).move(10, 10).select();
  };
	    
    
    /* ---------- Constructors for real areas ---------- */

    /**
     * The constructor for rectangles
     * 
     * (x, y) -----
     * |          | height
     * ------------
     *     width
     *
     * @constructor
     * @param coords {Object} - object with parameters of new area (x, y, width, height)
     *                          if some parameter is undefined, it will set 0
     * @param attributes {Object} [attributes=undefined] - attributes for area (e.g. href, title) 
     */
    function Rectangle(coords, attributes) {
        Area.call(this, 'rectangle', coords, attributes);
        
        /**
         * @namespace
         * @property {number} x - Distance from the left edge of the image to the left side of the rectangle
         * @property {number} y - Distance from the top edge of the image to the top side of the rectangle
         * @property {number} width - Width of rectangle
         * @property {number} height - Height of rectangle
         */
        this._coords = {
            x : coords.x || 0, 
            y : coords.y || 0,
            width : coords.width || 0, 
            height : coords.height || 0
        };
    
        this._el = document.createElementNS(Area.SVG_NS, 'rect');
        this._groupEl.appendChild(this._el);
        
        var x = coords.x - this._coords.width / 2,
            y = coords.y - this._coords.height / 2;
        
        this._helpers = {
            center : new HelperZ(this._groupEl, x, y, 'move'),
            top : new HelperZ(this._groupEl, x, y, 'editTop'),
            bottom : new HelperZ(this._groupEl, x, y, 'editBottom'),
            left : new HelperZ(this._groupEl, x, y, 'editLeft'),
            right : new HelperZ(this._groupEl, x, y, 'editRight'),
            topLeft : new HelperZ(this._groupEl, x, y, 'editTopLeft'),
            topRight : new HelperZ(this._groupEl, x, y, 'editTopRight'),
            bottomLeft : new HelperZ(this._groupEl, x, y, 'editBottomLeft'),
            bottomRight : new HelperZ(this._groupEl, x, y, 'editBottomRight')
        };
        
        this.redraw();
    }
    utils.inherits(Rectangle, Area);
    
    /**
     * Set attributes for svg-elements of area by new parameters
     * 
     * -----top------
     * |            |
     * ---center_y---
     * |            |
     * ----bottom----
     * 
     * @param coords {Object} - Object with coords of this area (x, y, width, height)
     * @returns {Rectangle} - this rectangle
     */
    Rectangle.prototype.setSVGCoords = function(coords) {
    	console.log(coords);
        this._el.setAttribute('x', coords.x);
        this._el.setAttribute('y', coords.y);
        this._el.setAttribute('width', coords.width);
        this._el.setAttribute('height', coords.height);
        
        var top = coords.y,
            center_y = coords.y + coords.height / 2,
            bottom = coords.y + coords.height,
            left = coords.x,
            center_x = coords.x + coords.width / 2,
            right = coords.x + coords.width;
    
        this._helpers.center.setCoords(center_x, center_y);
        this._helpers.top.setCoords(center_x, top);
        this._helpers.bottom.setCoords(center_x, bottom);
        this._helpers.left.setCoords(left, center_y);
        this._helpers.right.setCoords(right, center_y);
        this._helpers.topLeft.setCoords(left, top);
        this._helpers.topRight.setCoords(right, top);
        this._helpers.bottomLeft.setCoords(left, bottom);
        this._helpers.bottomRight.setCoords(right, bottom);
        
        return this;
    };
    
    /**
     * Set coords for this area
     * 
     * @param coords {coords}
     * @returns {Rectangle} - this rectangle
     */
    Rectangle.prototype.setCoords = function(coords) {
    	console.log(coords);
        this._coords.x = coords.x;
        this._coords.y = coords.y;
        this._coords.width = coords.width;
        this._coords.height = coords.height;
        
        return this;
    };
    
    /**
     * Calculates new coordinates in process of drawing
     * 
     * @param x {number} - x-coordinate of cursor
     * @param y {number} - y-coordinate of cursor
     * @param isSquare {boolean}
     * @returns {Object} - calculated coords of this area
     */
    Rectangle.prototype.dynamicDraw = function(x, y, isSquare) {
        var newCoords = {
            x : this._coords.x,
            y : this._coords.y,
            width : x - this._coords.x,
            height: y - this._coords.y
        };
        console.log(newCoords);
        if (isSquare) {
            newCoords = Rectangle.getSquareCoords(newCoords);
        }
        
        newCoords = Rectangle.getNormalizedCoords(newCoords);
                console.log(newCoords);
        this.redraw(newCoords);
        
        return newCoords;
    };
    
    /**
     * Handler for drawing process (by mousemove)
     * It includes only redrawing area by new coords 
     * (this coords doesn't save as own area coords)
     * 
     * @params e {MouseEvent} - mousemove event
     */
    Rectangle.prototype.onProcessDrawing = function(e) {
        var coords = utils.getRightCoords(e.pageX, e.pageY);
        console.log(coords);    
        this.dynamicDraw(coords.x, coords.y, e.shiftKey);
    };
    
    /**
     * Handler for drawing stoping (by second click on drawing canvas)
     * It includes redrawing area by new coords 
     * and saving this coords as own area coords
     * 
     * @params e {MouseEvent} - click event
     */
    Rectangle.prototype.onStopDrawing = function(e) {
        var coords = utils.getRightCoords(e.pageX, e.pageY);
        
        this.setCoords(this.dynamicDraw(coords.x, coords.y, e.shiftKey)).deselect();
        
        app.removeAllEvents()
           .setIsDraw(false)
           .resetNewArea();
    };
    
    /**
     * Changes area parameters by editing type and offsets
     * 
     * @param {string} editingType - A type of editing (e.g. 'move')
     * @returns {Object} - Object with changed parameters of area 
     */
    Rectangle.prototype.edit = function(editingType, dx, dy) {
        var tempParams = Object.create(this._coords);
        
        switch (editingType) {
            case 'move':
                tempParams.x += dx;
                tempParams.y += dy;
                break;
            
            case 'editLeft':
                tempParams.x += dx; 
                tempParams.width -= dx;
                break;
            
            case 'editRight':
                tempParams.width += dx;
                break;
            
            case 'editTop':
                tempParams.y += dy;
                tempParams.height -= dy;
                break;
            
            case 'editBottom':
                tempParams.height += dy;
                break;
               
            case 'editTopLeft':
                tempParams.x += dx;
                tempParams.y += dy;
                tempParams.width -= dx;
                tempParams.height -= dy;
                break;
                
            case 'editTopRight':
                tempParams.y += dy;
                tempParams.width += dx;
                tempParams.height -= dy;
                break;
            
            case 'editBottomLeft':
                tempParams.x += dx;
                tempParams.width -= dx;
                tempParams.height += dy;
                break;
            
            case 'editBottomRight':
                tempParams.width += dx;
                tempParams.height += dy;
                break;
        }

        return tempParams;
    };
    
    /**
     * Calculates new coordinates in process of editing
     * 
     * @param coords {Object} - area coords 
     * @param saveProportions {boolean}
     * @returns {Object} - new coordinates of area
     */
    Rectangle.prototype.dynamicEdit = function(coords, saveProportions) {
    	console.log("yes")
        coords = Rectangle.getNormalizedCoords(coords);
        
        if (saveProportions) {
            coords = Rectangle.getSavedProportionsCoords(coords);
        }
        
        this.redraw(coords);
        
        return coords;
    };
    
    /**
     * Handler for editing process (by mousemove)
     * It includes only redrawing area by new coords 
     * (this coords doesn't save as own area coords)
     * 
     * @params e {MouseEvent} - mousemove event
     */
    Rectangle.prototype.onProcessEditing = function(e) {
    	console.log("yes");
        return this.dynamicEdit(
            this.edit(
                app.getEditType(),
                e.pageX - this.editingStartPoint.x,
                e.pageY - this.editingStartPoint.y
            ),
            e.shiftKey
        );
    };
    
    /**
     * Handler for editing stoping (by mouseup)
     * It includes redrawing area by new coords 
     * and saving this coords as own area coords
     * 
     * @params e {MouseEvent} - mouseup event
     */
    Rectangle.prototype.onStopEditing = function(e) {
        this.setCoords(this.onProcessEditing(e));
        app.removeAllEvents();
    };

    /**
     * Returns string-representation of this rectangle
     * 
     * @returns {string}
     */
    Rectangle.prototype.toString = function() {
        return 'Rectangle {x: '+ this._coords.x + 
               ', y: ' + this._coords.y + 
               ', width: ' + this._coords.width + 
               ', height: ' + this._coords.height + '}';
    }
    
    /**
     * Returns html-string of area html element with params of this rectangle
     * 
     * @returns {string}
     */
    Rectangle.prototype.toHTMLMapElementString = function() {
        var x2 = this._coords.x + this._coords.width,
            y2 = this._coords.y + this._coords.height;
            
        return '<area shape="rect" coords="' // TODO: use template engine
            + this._coords.x + ', '
            + this._coords.y + ', '
            + x2 + ', '
            + y2
            + '"'
            + (this._attributes.href ? ' href="' + this._attributes.href + '"' : '')
            + (this._attributes.alt ? ' alt="' + this._attributes.alt + '"' : '')
            + (this._attributes.title ? ' title="' + this._attributes.title + '"' : '')
            + ' />';
    };

    /**
     * Returns coords for area attributes form
     * 
     * @returns {Object} - object width coordinates of point
     */
    Rectangle.prototype.getCoordsForDisplayingInfo = function() {
        return {
            x : this._coords.x,
            y : this._coords.y
        };
    };
    
    /**
     * Returns true if coords is valid for rectangles and false otherwise
     *
     * @static
     * @param coords {Object} - object with coords for new rectangle
     * @return {boolean}
     */
    Rectangle.testCoords = function(coords) {
        return coords.x && coords.y && coords.width && coords.height;
    };
    
    /**
     * Returns true if html coords array is valid for rectangles and false otherwise
     *
     * @static
     * @param coords {Array} - coords for new rectangle as array
     * @return {boolean}
     */
    Rectangle.testHTMLCoords = function(coords) {
        return coords.length === 4;
    };

    /**
     * Return rectangle coords object from html array
     * 
     * @param htmlCoordsArray {Array}
     * @returns {Object}
     */
    Rectangle.getCoordsFromHTMLArray = function(htmlCoordsArray) {
        if (!Rectangle.testHTMLCoords(htmlCoordsArray)) {    
            throw new Error('This html-coordinates is not valid for rectangle');
        }

        return {
            x : htmlCoordsArray[0],
            y : htmlCoordsArray[1],
            width : htmlCoordsArray[2] - htmlCoordsArray[0],
            height : htmlCoordsArray[3] - htmlCoordsArray[1]
        };
    };

    /**
     * Fixes coords if width or/and height are negative
     * 
     * @static
     * @param coords {Object} - Coordinates of this area 
     * @returns {Object} - Normalized coordinates of area
     */
    Rectangle.getNormalizedCoords = function(coords) {
        if (coords.width < 0) {
            coords.x += coords.width;
            coords.width = Math.abs(coords.width);
        }
        
        if (coords.height < 0) {
            coords.y += coords.height;
            coords.height = Math.abs(coords.height);
        }
        
        return coords;
    };
    
    /**
     * Returns coords with equivivalent width and height
     * 
     * @static
     * @param coords {Object} - Coordinates of this area 
     * @returns {Object} - Coordinates of area with equivivalent width and height
     */
    Rectangle.getSquareCoords = function(coords) {
        var width = Math.abs(coords.width),
            height = Math.abs(coords.height);
        
        if (width > height) {
            coords.width = coords.width > 0 ? height : -height;
        } else {
            coords.height = coords.height > 0 ? width : -width;
        }
        
        return coords;
    };
    
    /**
     * Returns coords with saved proportions of original area
     * 
     * @static
     * @param coords {Object} - Coordinates of this area 
     * @param originalCoords {Object} - Coordinates of the original area
     * @returns {Object} - Coordinates of area with saved proportions of original area
     */
    Rectangle.getSavedProportionsCoords = function(coords, originalCoords) {
        var originalProportions = coords.width / coords.height,
            currentProportions = originalCoords.width / originalCoords.height;
        
        if (currentProportions > originalProportions) {
            coords.width = Math.round(coords.height * originalProportions);
        } else {
            coords.height = Math.round(coords.width / originalProportions);
        }
        
        return coords;
    };
    
    /**
     * Creates new rectangle and adds drawing handlers for DOM-elements
     * 
     * @static
     * @param firstPointCoords {Object}
     * @returns {Rectangle}
     */
    Rectangle.createAndStartDrawing = function(firstPointCoords) {
        var newArea = new Rectangle({
            x : firstPointCoords.x,
            y : firstPointCoords.y,
            width: 0,
            height: 0
        });
        
        app.addEvent(app.domElements.container, 'mousemove', newArea.onProcessDrawing.bind(newArea))
           .addEvent(app.domElements.container, 'click', newArea.onStopDrawing.bind(newArea));
           
        return newArea;
    };
    

  function Wayp(coords) {
    Area.call(this, "waypoint", coords);

    this._coords = {
      points: coords.points || [{ x: 0, y: 0 }],
      isOpened: coords.isOpened || false
    };

    this._helpers = {
      points: []
    };

    var a = check(this._coords.points[0].x, this._coords.points[0].y);
    this._from = a.c;
    this._to = null;

    if (a.a == 1) {
      new Helper(this._groupEl, a.x, a.y, "movePoint");

      var points = {
        x: a.x,
        y: a.y
      };

      var x = a.x;
      var y = a.y;
    }

    this._selectedPoint = -1;
  }
  utils.inherits(Wayp, Area);

  Wayp.prototype.close = function() {
    this._coords.isOpened = false;
  };

  Wayp.prototype.setSVGCoords = function(coords) {
    var helper = app.helperpoints[this.helperid];
    helper.setCoords(app.coords[this.helperid].x, app.coords[this.helperid].y);
    return this;
  };

  Wayp.prototype.setCoords = function(coords) {
    return this;
  };

  Wayp.prototype.addPoint = function(x, y) {
    var a = check(x, y);

    if (a.a == 1) {
      var helper = new Helper(this._groupEl, a.x, a.y, "movePoint");
      helper.setId(this._helpers.points.length);
      var points = {
        x: a.x,
        y: a.y
      };
    }

    this._coords.points.push({
      x: a.x,
      y: a.y
    });
    var to = a.c;
    this._to = to;
    var from = this._from;
    if (to != from) {
      var edge = new Edge(
        this._groupEl,
        this._coords.points,
        a.x,
        a.y,
        from,
        to
      );
    }
    this._from = this._to;
    return this;
  };

  Wayp.prototype.dynamicDraw = function(x, y, isRightAngle) {
    var temp_coords = {
      points: [].concat(this._coords.points)
    };

    if (isRightAngle) {
      var rightPointCoords = Polygon.getRightAngleLineLastPointCoords(
        this._coords,
        { x: x, y: y }
      );
      x = rightPointCoords.x;
      y = rightPointCoords.y;
    }

    temp_coords.points.push({ x: x, y: y });

    return temp_coords;
  };

  Wayp.prototype.dynamicEdge = function(x, y) {
    var i = this._coords.points.length;
    var from = {
      x: this._coords.points[i - 1].x,
      y: this._coords.points[i - 1].y
    };
    var to = {
      x: x,
      y: y
    };
    var polygonPointsAttrValue = from.x +
      " " +
      from.y +
      " " +
      to.x +
      " " +
      to.y +
      " ";
    this._el.setAttribute("points", polygonPointsAttrValue);
    return this;
  };
  Wayp.prototype.onProcessDrawing = function(e) {
    var coords = utils.getRightCoords(e.pageX, e.pageY);
    this.dynamicDraw(coords.x, coords.y, e.shiftKey);
  };

  Wayp.prototype.onAddPointDrawing = function(e) {
    var newPointCoords = utils.getRightCoords(e.pageX, e.pageY);
    this.addPoint(newPointCoords.x, newPointCoords.y);
  };

  Wayp.prototype.onStopDrawing = function(e) {
    if (e.type == "click" || e.type == "keydown" && e.keyCode == 13) {
      this.close();

      app.removeAllEvents().setIsDraw(false).resetNewArea();
    }

    e.stopPropagation();
  };

  Wayp.prototype.edit = function(editingType, dx, dy) {
    var tempParams = Object.create(app.coords);
    var tp = Object.create(this._coords);
    switch (editingType) {
      case "move":
        for (var i = 0, c = tempParams.length; i < c; i++) {
          tempParams[i].x += dx;
          tempParams[i].y += dy;
        }
        break;

      case "movePoint":
        tempParams[this.helperid].x += dx;
        tempParams[this.helperid].y += dy;
        break;
    }

    return tempParams;
  };

  Wayp.prototype.onProcessEditing = function(e) {
    app.helperpoints[this.helperid]._coords.x = app.coords[this.helperid].x;
    app.helperpoints[this.helperid]._coords.y = app.coords[this.helperid].y;

    var editType = app.getEditType();

    this.redraw(
      this.edit(
        editType,
        e.pageX - this.editingStartPoint.x,
        e.pageY - this.editingStartPoint.y
      )
    );

    this.editingStartPoint.x = e.pageX;
    this.editingStartPoint.y = e.pageY;
  };

  Wayp.prototype.onStopEditing = function(e) {
    var editType = app.getEditType();

    this
      .setCoords(
        this.edit(
          editType,
          e.pageX - this.editingStartPoint.x,
          e.pageY - this.editingStartPoint.y
        )
      )
      .redraw();

    var startpoint = {
      x: app.coords[this.helperid].x,
      y: app.coords[this.helperid].y
    };
    var editingStartPoint = {
      x: app.connect[0].x,
      y: app.connect[0].y
    };

    app.helperpoints[this.helperid]._coords.x = app.coords[this.helperid].x;
    app.helperpoints[this.helperid]._coords.y = app.coords[this.helperid].y;
    moveEdge(startpoint, editingStartPoint);
    app.connect.splice(0, 1);
    app.removeAllEvents();
  };

  Wayp.prototype.getCoordsForDisplayingInfo = function() {
    return {
      x: this._coords.points[0].x,
      y: this._coords.points[0].y
    };
  };

  Wayp.testCoords = function(coords) {
    return coords.points.length >= 2;
  };

  Wayp.testHTMLCoords = function(coords) {
    return coords.length >= 6 && coords.length % 2 === 0;
  };

  Wayp.getCoordsFromHTMLArray = function(htmlCoordsArray) {
    if (!Polygon.testHTMLCoords(htmlCoordsArray)) {
      throw new Error("This html-coordinates is not valid for polygon");
    }

    var points = [];
    for (var i = 0, c = htmlCoordsArray.length / 2; i < c; i++) {
      points.push({
        x: htmlCoordsArray[2 * i],
        y: htmlCoordsArray[2 * i + 1]
      });
    }

    return {
      points: points
    };
  };

  Wayp.createAndStartDrawing = function(firstPointCoords) {
    var newArea = new Wayp({
      points: [firstPointCoords],
      isOpened: true
    });
    if (click == 0) {
      app.addEvent(
        app.domElements.container,
        "mousemove",
        newArea.onProcessDrawing.bind(newArea)
      );
      app
        .addEvent(
          app.domElements.container,
          "click",
          newArea.onAddPointDrawing.bind(newArea)
        )
        .addEvent(document, "keydown", newArea.onStopDrawing.bind(newArea));
    }
    return newArea;
  };

  var help = (function() {
    var block = utils.id("help"),
      overlay = utils.id("overlay"),
      close_button = block.querySelector(".close_button");

    function hide() {
      utils.hide(block);
      utils.hide(overlay);
    }

    function show() {
      utils.show(block);
      utils.show(overlay);
    }

    overlay.addEventListener("click", hide, false);

    close_button.addEventListener("click", hide, false);

    return {
      show: show,
      hide: hide
    };
  })();

  var code = (function() {
    var block = utils.id("code"), content = utils.id("code_content");
    //   close_button = block.querySelector(".close_button");

    // close_button.addEventListener("click", function(e) {
    //     utils.hide(block);
    //     e.preventDefault();
    //  }, false);

    return {
      print: function() {
        content.innerHTML = app.getHTMLCode(true);
        utils.show(block);
      },
      hide: function() {
        utils.hide(block);
      }
    };
  })();

  var edgecode = (function() {
    var block = utils.id("code"), content = utils.id("code_content");
    //  close_button = block.querySelector(".close_button");

    return {
      print: function() {
        content.innerHTML = app.getHTMLCodeEdge(true);
        utils.show(block);
      },
      hide: function() {
        utils.hide(block);
      }
    };
  })();

  var info = (function() {
    var form = utils.id("edit_details"),
      header = form.querySelector("h5"),
      name = utils.id("name"),
      isDestination = utils.id("isDestination"),
      isEscalator = utils.id("isEscalator"),
      isBeacon = utils.id("isBeacon"),
      beaconid = utils.id("beaconid"),
      nodeid = utils.id("nodeid"),
      save_button = utils.id("save_details"),
      close_button = form.querySelector(".close_button"),
      sections = form.querySelectorAll("p"),
      obj,
      x,
      y,
      temp_x,
      temp_y;

    function changedReset() {
      form.classList.remove("changed");
      utils.foreach(sections, function(x) {
        x.classList.remove("changed");
      });
    }

    function save(e) {
      var isD, isE;

      if (document.getElementById("isDestination").checked) {
        isD = "true";
      } else {
        isD = "false";
      }

      if (document.getElementById("isEscalator").checked) {
        isE = "true";
      } else {
        isE = "false";
      }
    //  console.log(obj);
      obj.setInfoAttributes({
        name: document.getElementById("name").value,
        isDestination: isD,
        isEscalator: isE
      });
 		obj.setBeacondata({
       beaconid: document.getElementById("beaconid").value,
       nodeid: document.getElementById("nodeid").value
      });
      function createnodename(x, y, name) {
        var u = document.createElement("text");
        var id = guidGenerator();
        u.setAttribute("id", id);
        app.domElements.svg.appendChild(u);
        u.setAttribute("x", x + 3);
        u.setAttribute("y", y + 3);
        u.setAttribute("font-family", "Verdana");
        u.setAttribute("font-size", 12);
        u.setAttribute("fill", "blue");

        document.getElementById(id).innerHTML = document.getElementById(
          "name"
        ).value;
        app.domElements.svg.appendChild(u);
      }

      e.preventDefault();
    }

    function unload() {
      obj = null;
      changedReset();
      utils.hide(form);
    }

    function setCoords(x, y) {
      form.style.left = x + 5 + "px";
      form.style.top = y + 5 + "px";
    }

    function moveEditBlock(e) {
      setCoords(x + e.pageX - temp_x, y + e.pageY - temp_y);
    }

    function stopMoveEditBlock(e) {
      x = x + e.pageX - temp_x;
      y = y + e.pageY - temp_y;
      setCoords(x, y);

      app.removeAllEvents();
    }

    function change() {
      form.classList.add("changed");
      this.parentNode.classList.add("changed");
    }

    save_button.addEventListener("click", save, false);

    name.addEventListener(
      "keydown",
      function(e) {
        e.stopPropagation();
      },
      false
    );
    isDestination.addEventListener(
      "keydown",
      function(e) {
        e.stopPropagation();
      },
      false
    );
    isEscalator.addEventListener(
      "keydown",
      function(e) {
        e.stopPropagation();
      },
      false
    );

    name.addEventListener("change", change, false);
    isDestination.addEventListener("change", change, false);
    isEscalator.addEventListener("change", change, false);

    close_button.addEventListener("click", unload, false);

    header.addEventListener(
      "mousedown",
      function(e) {
        temp_x = e.pageX, temp_y = e.pageY;

        app.addEvent(document, "mousemove", moveEditBlock);
        app.addEvent(header, "mouseup", stopMoveEditBlock);

        e.preventDefault();
      },
      false
    );

    return {
      load: function(object, new_x, new_y) {
        obj = object;
        console.log(object);
        name.value = object._attributes.name ? object._attributes.name : "";
        isDestination.value = object._attributes.isDestination
          ? object._attributes.isDestination
          : "";
        isEscalator.value = object._attributes.isEscalator
          ? object._attributes.isEscalator
          : "";
          
        beaconid.value = object._beacondata.beaconid ? object._beacondata.beaconid : "";
        nodeid.value = object._beacondata.nodeid ? object._beacondata.nodeid : "";
        console.log(beaconid.value, object._beacondata.beaconid);
        utils.show(form);
        if (new_x && new_y) {
          x = new_x;
          y = new_y;
          setCoords(x, y);
        }
      },
      unload: unload
    };
  })();

  var from_html_form = (function() {
    var form = utils.id("from_html_wrapper"),
      code_input = utils.id("code_input"),
      load_button = utils.id("load_code_button"),
      close_button = form.querySelector(".close_button");

    function load(e) {
      if (Area.createAreasFromHTMLOfMap(code_input.value)) {
        hide();
      }

      e.preventDefault();
    }

    function hide() {
      utils.hide(form);
    }

    load_button.addEventListener("click", load, false);

    close_button.addEventListener("click", hide, false);

    return {
      show: function() {
        code_input.value = "";
        utils.show(form);
      },
      hide: hide
    };
  })();

  var get_image = (function() {
    var block = utils.id("get_image_wrapper"),
      loading_indicator = utils.id("loading"),
      button = utils.id("button"),
      filename = null,
      last_changed = null;

    var drag_n_drop = (function() {
      var dropzone = utils.id("dropzone"),
        inputfile = utils.id("button1"),
        dropzone_clear_button = dropzone.querySelector(".clear_button"),
        sm_img = utils.id("sm_img");
      function openAttachment() {
        document.getElementById("inputfile").click();
      }

      function fileSelected(input) {
        document.getElementById("button1").value = "File: " +
          input.files[0].name;
      }

      function testFile(type) {
        switch (type) {
          case "image/jpeg":
          case "image/gif":
          case "image/png":
            return true;
        }
        return false;
      }

      dropzone.addEventListener(
        "dragover",
        function(e) {
          utils.stopEvent(e);
        },
        false
      );

      dropzone.addEventListener(
        "dragleave",
        function(e) {
          utils.stopEvent(e);
        },
        false
      );

      dropzone.addEventListener(
        "drop",
        function(e) {
          utils.stopEvent(e);

          var reader = new FileReader(), file = e.dataTransfer.files[0];

          if (testFile(file.type)) {
            dropzone.classList.remove("error");

            reader.readAsDataURL(file);

            reader.onload = function(e) {
              sm_img.src = e.target.result;
              sm_img.style.display = "inline-block";
              filename = file.name;
              utils.show(dropzone_clear_button);
              last_changed = drag_n_drop;
            };
          } else {
            clearDropzone();
            dropzone.classList.add("error");
          }
        },
        false
      );

      function clearDropzone() {
        sm_img.src = "";

        utils.hide(sm_img).hide(dropzone_clear_button);

        dropzone.classList.remove("error");
      }

      dropzone_clear_button.addEventListener("click", clearDropzone, false);

      return {
        clear: clearDropzone,
        init: function() {
          dropzone.draggable = true;
          this.clear();
          utils.hide(sm_img).hide(dropzone_clear_button);
        },
        test: function() {
          return Boolean(sm_img.src);
        },
        getImage: function() {
          return sm_img.src;
        }
      };
    })();

    var url_input = (function() {
      document.getElementById("url").value = "../../floor_images/mapair.png";
      var url = utils.id("url");

      function testUrl(str) {
        var url_str = str.trim(), temp_array = url_str.split("."), ext;

        if (temp_array.length > 1) {
          ext = temp_array[temp_array.length - 1].toLowerCase();
          switch (ext) {
            case "jpg":
            case "jpeg":
            case "gif":
            case "png":
              return true;
          }
        }

        return false;
      }

      function onUrlChange() {
        setTimeout(
          function() {
            if (url.value.length) {
              utils.show(url_clear_button);
              last_changed = url_input;
            } else {
              utils.hide(url_clear_button);
              last_changed = drag_n_drop;
            }
          },
          0
        );
      }

      url.addEventListener("keypress", onUrlChange, false);
      url.addEventListener("change", onUrlChange, false);
      url.addEventListener("paste", onUrlChange, false);

      function clearUrl() {
        url.value = "";
        utils.hide(url_clear_button);
        url.classList.remove("error");
        last_changed = url_input;
      }

      return {
        init: function() {},
        test: function() {
          if (testUrl(url.value)) {
            url.classList.remove("error");
            return true;
          } else {
            url.classList.add("error");
          }
          return false;
        },
        getImage: function() {
          var tmp_arr = url.value.split("/");
          filename = tmp_arr[tmp_arr.length - 1];

          return url.value.trim();
        }
      };
    })();

    /* Block init */
    function init() {
      utils.hide(loading_indicator);
      drag_n_drop.init();
      url_input.init();
    }
    init();

    /* Block clear */
    function clear() {
      drag_n_drop.clear();
      last_changed = null;
    }

    /* Selected image loading */
    function onButtonClick(e) {
      app.loadImage(url_input.getImage()).setFilename(filename);

      floorno = document.getElementById("floorno").value;
      e.preventDefault();
    }

    button.addEventListener("click", onButtonClick, false);

    var loadfloor1 = utils.id("loadfloor1");
    var loadfloor2 = utils.id("loadfloor2");
    var loadfloor3 = utils.id("loadfloor3");
    var loadfloor4 = utils.id("loadfloor4");

    loadfloor1.addEventListener("click", seturlfloor1, false);
    loadfloor2.addEventListener("click", seturlfloor2, false);
    loadfloor3.addEventListener("click", seturlfloor3, false);
    loadfloor4.addEventListener("click", seturlfloor4, false);

    function seturlfloor1() {
      nodecount = 0;
      document.getElementById("url").value = "../../floor_images/mapair.png";
      app.loadImage(url_input.getImage()).setFilename(filename);

      app.clear();
      //helpercount = 0;
      floorno = 1;
      drawnodes(1);
      drawedges(1);
      //test1(xhr.responseText, 1);
      //  test2(xhr1.responseText, 1);   
    }

    function seturlfloor2() {
      nodecount = 0;
      document.getElementById("url").value = "../../floor_images/mapair11.png";
      app.loadImage(url_input.getImage()).setFilename(filename);
      // //console.log("t");

      app.clear();

      // helpercount = 0;
      floorno = 2;
      drawnodes(2);
      drawedges(2);
      // test1(xhr.responseText, 2);
      //test2(xhr1.responseText, 2);  
    }
    function seturlfloor3() {
      nodecount = 0;
      document.getElementById("url").value = "../../floor_images/mapair12.png";
      app.loadImage(url_input.getImage()).setFilename(filename);

      app.clear();

      //helpercount = 0;
      floorno = 3;
      drawnodes(3);
      drawedges(3);
      //test1(xhr.responseText, 3);
      //  test2(xhr1.responseText,3);  
    }
    function seturlfloor4() {
      nodecount = 0;
      document.getElementById("url").value = "../../floor_images/L1I.png";
      app.loadImage(url_input.getImage()).setFilename(filename);

      app.clear();

      //helpercount = 0;
      floorno = 4;
      drawnodes(4);
      drawedges(4);
      //test1(xhr.responseText, 4);
      // test2(xhr1.responseText, 4);  
    }

    function getnodes() {
      if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        xhr = new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        // IE 8 and older
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var data;
      xhr.open("GET", "../../get_nodes.php", true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.send(data);
      xhr.onreadystatechange = display_data;
      function display_data() {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            test1(xhr.responseText);
            //   var tempParams = Object.create(xhr.responseText);
            //  // //console.log(tempParams);
            //document.getElementById("suggestion").innerHTML = xhr.responseText;
          } else {
          }
        }
      }
    }
    function getedges() {
      if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        xhr1 = new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        // IE 8 and older
        xhr1 = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var data;
      xhr1.open("GET", "../../get_edges.php", true);
      xhr1.setRequestHeader(
        "Content-Type",
        "application/x-www-form-urlencoded"
      );
      xhr1.send(data);
      xhr1.onreadystatechange = display_data;
      function display_data() {
        if (xhr1.readyState == 4) {
          if (xhr1.status == 200) {
            test2(xhr1.responseText);
            //   var tempParams = Object.create(xhr.responseText);
            //  // //console.log(tempParams);
            //document.getElementById("suggestion").innerHTML = xhr.responseText;
          } else {
          }
        }
      }
    }
    
  function getbeacons() {
      if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        xhr8 = new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        // IE 8 and older
        xhr8 = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var data;
      xhr8.open("GET", "../../get_beacons.php", true);
      xhr8.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr8.send(data);
      xhr8.onreadystatechange = display_data;

      function display_data() {
        if (xhr8.readyState == 4) {
          if (xhr8.status == 200) {
           processbeacons(xhr8.responseText);
            //   var tempParams = Object.create(xhr.responseText);
            //  // //console.log(tempParams);
            //document.getElementById("suggestion").innerHTML = xhr.responseText;
          } else {
          }
        }
      }
    }    
    
    
    window.onload = function() {
    	getbeacons();
      getnodes();
      getedges();
     seturlfloor1();
    };

    /**   function performClick(e) {
   var elem = document.getElementById('fileInput');
   if(elem && document.createEvent) {
      var evt = document.createEvent("MouseEvents");
      evt.initEvent("click", true, false);
      elem.dispatchEvent(evt);
   }
   
   	var fileInput = document.getElementById('fileInput');
		//var fileDisplayArea = document.getElementById('fileDisplayArea');

		fileInput.addEventListener('change', function(e) {
			var file = fileInput.files[0];
			//var textType = /text.;

			if (file.type.match(textType)) {
				var reader = new FileReader();
			

				reader.onload = function(e) {
					var readinput = reader.result;
					test1(readinput);
					// //console.log(readinput);
				}

				reader.readAsText(file);	

			} 
		});
               e.preventDefault();
} **/

    function test1(readinput) {
      var a = readinput;
      // //console.log(a);
      var array = a.split("\n");
      var helpers = [];

      var floorwise = [];

      for (var i = 0, c = array.length; i < c; i++) {
        var b = array[i].split(":");
        parseInt();
        var g = {
          id: b[0],
          name: b[1],
          x: parseInt(b[2]),
          y: parseInt(b[3]),
          floorno: b[4],
          isD: b[5],
          isE: b[6],
          beaconid:"",
          nodeid: b[0]
        };
        helpers.push(g);
      }

      for (var i = 0, c = helpers.length; i < c; i++) {
        if (helpers[i].floorno == 1) {
          firstfloor.push(helpers[i]);
        } else if (helpers[i].floorno == 2 && helpers[i].id.replace("node","").charAt(0) == 2) {
          secondfloor.push(helpers[i]);
        } else if (helpers[i].id.replace("node","").charAt(0) == 3) {
          thirdfloor.push(helpers[i]);
        } else if (helpers[i].floorno == 4) {
          fourthfloor.push(helpers[i]);
        }
      }
    }

    function test2(readinput) {
      var a = readinput;
      var array = a.split("\n");
      var edges = [];
      for (var i = 0, c = array.length; i < c; i++) {
        var b = array[i].split(":");
        var g = {
          id: b[0],
          from: b[1],
          to: b[2]
        };
        edges.push(g);
      }

      for (var i = 0, c = edges.length; i < c; i++) {
        if (edges[i].id.replace("edge", "").charAt(0) == 1) {
          firstflooredge.push(edges[i]);
        } else if (edges[i].id.replace("edge", "").charAt(0) == 2 ) {
          secondflooredge.push(edges[i]);
        } else if (edges[i].id.replace("edge", "").charAt(0) == 3) {
          thirdflooredge.push(edges[i]);
        } else if (edges[i].id.replace("edge", "").charAt(0) == 4) {
          fourthflooredge.push(edges[i]);
        }
      }
      //	//console.log(firstflooredge);
    }
		
		function processbeacons(readinput) {
     		 var a = readinput;
				console.log(readinput);
     		 var array = a.split("\n");


      		for (var i = 0, c = array.length; i < c; i++) {
     				   var b = array[i].split(";");
       			 var g = {
        			  beaconid:b[0],
         		 nodeid: b[1]
        				};
        beaconsdata.push(g);
      }


    }
    function findBeaconid(nodeid){
    	var beaconid ="";
    	for(var i =0; i < beaconsdata.length; i++){
    	
			if(beaconsdata[i].nodeid === nodeid){
						
			beaconid = beaconsdata[i].beaconid;
			}    	
    	
    	}
    	return beaconid;
    }
    function drawnodes(floorno) {
      //var array = [];
		console.log(beaconsdata);
      if (floorno == 1) {
        // array.concat(firstfloor);
        array1 = firstfloor;
        // firstfloor.length = 0;
      } else if (floorno == 2) {
        array1 = secondfloor;
        // array.concat(secondfloor);
        //secondfloor.length = 0;
      } else if (floorno == 3) {
        array1 = thirdfloor;
        // array.concat(thirdfloor);
        //thirdfloor.length = 0;
      } else if (floorno == 4) {
        array1 = fourthfloor;
        // array.concat(fourthfloor);
        //fourthfloor.length = 0;
      }


      for (var i = 0, c = array1.length; i < c; i++) {
        var firstPointCoords = {
          x: array1[i].x,
          y: array1[i].y
        };
        Wayp.createAndStartDrawing(firstPointCoords);
      }
      for (var i = 0, c = array1.length; i < c; i++) {
        app.helperpoints[i]._id = array1[i].id;
        app.helperpoints[i]._attributes.name = array1[i].name;
        app.helperpoints[i]._attributes.isDestination = array1[i].isD;
        app.helperpoints[i]._attributes.isEscalator = array1[i].isE;
        app.helperpoints[i]._beacondata.beaconid = findBeaconid(array1[i].id);
        app.helperpoints[i]._beacondata.nodeid = array1[i].id;
      }
      //e.preventDefault();
      // array.length=0;	              
    }
    function drawedges(floorno) {
      //var array =[];
      if (floorno == 1) {
        array2 = firstflooredge;
        //	firstflooredge.length = 0;
      } else if (floorno == 2) {
        array2 = secondflooredge;
        //	secondflooredge.length = 0;
      } else if (floorno == 3) {
        array2 = thirdflooredge;
        //	 	thirdflooredge.length = 0;
      } else if (floorno == 4) {
        array2 = fourthflooredge;
        //array == fourthflooredge;
        //fourthflooredge.length = 0;
      }
      //console.log(array2.length);
      //console.log(firstflooredge.length);
     // console.log(secondflooredge.length);
      //console.log(thirdflooredge.length);
      //console.log(fourthflooredge.length);
      for (var i = 0, c = array2.length; i < c; i++) {
        var fromnode = array2[i].from;
        var tonode = array2[i].to;
        var edge = new Edgeimport(fromnode, tonode);
      }
      //array2.length=0;	
    }

    function show() {
      clear();
      utils.show(block);
    }

    function hide() {
      utils.hide(block);
    }

    /* Returned object */
    return {
      show: function() {
        app.hide();
        show();

        return this;
      },
      hide: function() {
        app.show();
        hide();

        return this;
      },
      showLoadIndicator: function() {
        utils.show(loading_indicator);

        return this;
      },
      hideLoadIndicator: function() {
        utils.hide(loading_indicator);

        return this;
      },
      url_input: function() {
        url_input();
        return this;
      }
    };
  })();
  get_image.show();

  /* Buttons and actions */
  var buttons = (function() {
    var all = utils.id("nav1").getElementsByTagName("button"),
      zoomin = utils.id("zoomin"),
      zoomout = utils.id("zoomout"),
      save = utils.id("save"),
      //  load = utils.id("load"),
      //  polygon = utils.id("polygon"),
      waypoint = utils.id("waypoint"),
      rectangle = utils.id("rectangle"),
      //   importfile = utils.id("importfile"),
      deletenode = utils.id("delete"),
      edit = utils.id("edit"),
      clear = utils.id("clear"),
      // from_html = utils.id("from_html"),
      to_html = utils.id("to_html"),
      //  to_edge = utils.id("to_edge"),
      //   new_image = utils.id("new_image"),
      show_help = utils.id("show_help");

    function deselectAll() {
      utils.foreach(all, function(x) {
        x.classList.remove(Area.CLASS_NAMES.SELECTED);
      });
    }

    function selectOne(button) {
      deselectAll();
      button.classList.add(Area.CLASS_NAMES.SELECTED);
    }

    function onSaveButtonClick(e) {
      // Save in localStorage
      app.saveInLocalStorage();

      e.preventDefault();
    }

    function onLoadButtonClick(e) {
      // Load from localStorage
      app.clear().loadFromLocalStorage();

      e.preventDefault();
    }

    function onShapeButtonClick(e) {
      click = 0;
      nodecount = 20;
      app
        .setMode("drawing")
        .setDrawClass()
        .setShape(this.id)
        .deselectAll()
        .removeAllEvents()
        .setIsDraw(false)
        .hidePreview();
      info.unload();
      selectOne(this);

      e.preventDefault();
    }
    function onDeleteButtonClick(e) {
      app.setMode("delete").removeAllEvents().setIsDraw(false).resetNewArea();

      selectOne(this);
      e.stopPropagation();
      e.preventDefault();
    }

    function zoominclick(e) {
      var zoominValue = 1;
      //var zoomoutValue = 1;
      console.log("this");
      // 	zoominValue = zoomoutValue + 0.1;
      imgw = imgw * zoominValue;
      imgh = imgh * zoominValue;
      app.setDimensions1(imgw, imgh);
    }

    function zoomoutclick(e) {
      console.log("this");
      var zoomoutValue = 1;

      imgw = imgw * zoomoutValue;
      imgh = imgh * zoomoutValue;
      app.setDimensions1(imgw, imgh);
    }

    function onClearButtonClick(zoomvalue) {
      // Clear all
      if (confirm("Clear all?")) {
        app
          .setMode(null)
          .setDefaultClass()
          .setShape(null)
          .clear()
          .hidePreview();
        deselectAll();
        helpercount = 0;
      }

      zoomvalue.preventDefault();
    }

    function onFromHtmlButtonClick(e) {
      // Load areas from html
      from_html_form.show();

      e.preventDefault();
    }

    function onToHtmlButtonClick(e) {
      // Generate html code only
      info.unload();
      //   code.print();

      app.getHTMLCode(true);

      e.preventDefault();
    }
    function onToHtmlButtonClickEdge(e) {
      // Generate html code only
      info.unload();
      //  edgecode.print();
      app.getHTMLCodeEdge(true);
      e.preventDefault();
    }

    function onSaveClick() {
      if (floorno == 1) {
        // array.concat(firstfloor);

        firstfloor = dataupdate(firstfloor);
        firstflooredge = dataupdateedge(firstflooredge);
      } else if (floorno == 2) {
        secondfloor = dataupdate(secondfloor);
        secondflooredge = dataupdateedge(secondflooredge);
      } else if (floorno == 3) {
       // console.log(thirdfloor);
     //   console.log(thirdflooredge);
        thirdfloor = dataupdate(thirdfloor);
        thirdflooredge = dataupdateedge(thirdflooredge);
       // console.log(thirdfloor);
        //console.log(thirdflooredge);
      } else if (floorno == 4) {
        fourthfloor = dataupdate(fourthfloor);
        fourthflooredge = dataupdateedge(fourthflooredge);
      }

      var new_array_floor = [];
      new_array_floor = new_array_floor.concat(
        firstfloor,
        secondfloor,
        thirdfloor,
        fourthfloor
      );
      var new_array_edge = [];
      new_array_edge = new_array_edge.concat(
        firstflooredge,
        secondflooredge,
        thirdflooredge,
        fourthflooredge
      );

      var tempParams1 = JSON.stringify(new_array_floor);
      var tempParams2 = JSON.stringify(new_array_edge);
var beaconsarray = [];
console.log(new_array_floor.length);
for (var i = 0; i < new_array_floor.length; i++) {
	if (new_array_floor[i].beaconid !="") {
	var newbobj ={
beacon_id : new_array_floor[i].beaconid,	
node_id: new_array_floor[i].nodeid
	
	}
	beaconsarray.push(newbobj);
	
	
	}
	}
	var tempParams3 = JSON.stringify(beaconsarray);

  		$.ajax({
        url: "../../post_beacon_new.php",
        method: "POST",
        data: tempParams3
      });
      $.ajax({
        url: "../../post_nodes_air.php",
        method: "POST",
        data: tempParams1
      });

      $.ajax({
        url: "../../post_edges_air.php",
        method: "POST",
        data: tempParams2
      });
    }
    

    function onEditButtonClick(e) {
      if (app.getMode() === "editing") {
        app
          .setMode(null)
          .setDefaultClass()
          .removeAllEvents()
          .setIsDraw(false)
          .resetNewArea()
          .deselectAll();
        deselectAll();
      } else {
        app
          .setShape(null)
          .setMode("editing")
          .removeAllEvents()
          .setIsDraw(false)
          .resetNewArea()
          .setEditClass();
        selectOne(this);
      }
      e.stopPropagation();
      app.hidePreview();
      e.preventDefault();
    }
    function onConnectButtonClick(e) {
      app.setShape(null).setMode("connect").setEditClass();
      selectOne(this);
      e.stopPropagation();
      app.hidePreview();
      e.preventDefault();
    }

    function onNewImageButtonClick(e) {
      // New image - clear all and back to loading image screen
      if (confirm("Discard all changes?")) {
        app
          .setMode(null)
          .setDefaultClass()
          .setShape(null)
          .setIsDraw(false)
          .clear()
          .hide()
          .hidePreview();
        deselectAll();
        get_image.show();
      }

      e.preventDefault();
    }

    function onShowHelpButtonClick(e) {
      // help.show();

      e.preventDefault();
    }
        rectangle.addEventListener('click', onShapeButtonClick, false);
    //   connect.addEventListener("click", onConnectButtonClick, false);
    // zoomin.addEventListener("click", zoominclick, false);
    //        zoomout.addEventListener("click", zoomoutclick, false);
    save.addEventListener("click", onSaveClick, false);
    //   load.addEventListener("click", onLoadButtonClick, false);
    // polygon.addEventListener("click", onShapeButtonClick, false);
    // importedgefile.addEventListener("click", performClickedge, false);
    // importfile.addEventListener("click", performClick, false);
    waypoint.addEventListener("click", onShapeButtonClick, false);
    clear.addEventListener("click", onClearButtonClick, false);
    deletenode.addEventListener("click", onDeleteButtonClick, false);
    // from_html.addEventListener("click", onFromHtmlButtonClick, false);
    //  to_html.addEventListener("click", onToHtmlButtonClick, false);
    //  to_edge.addEventListener("click", onToHtmlButtonClickEdge, false);
    edit.addEventListener("click", onEditButtonClick, false);
    // new_image.addEventListener("click", onNewImageButtonClick, false);
    show_help.addEventListener("click", onShowHelpButtonClick, false);
  })();
})();
