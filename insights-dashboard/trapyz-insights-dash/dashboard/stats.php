﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Home | Trapyz Canvas</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<?php echo '<script type="text/javascript"> var uid = "' .$statusUid.  '"; var role = "'.$statusRole.'"; </script>';?>
    <!-- Bootstrap Core Css -->
    <link href="./plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="./plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="./plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="./css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="./css/themes/all-themes.css" rel="stylesheet" />
    <script src="./partials/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<style type="text/css">
			#chartdiv a {
				display: none !important;
			}
			#chartdiv {
				height: 520px !important;
				width: 400px !important;
			}
		</style>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript">
            var uname = sessionStorage.getItem('uname') || "";
            
            if (uname == "") {
                window.location = "https://canvas.trapyz.com/trapyz-insights-dash/dashboard/sign-in.html";
            } 

            function signout() {
                sessionStorage.removeItem('uname');
                window.location = "https://canvas.trapyz.com/trapyz-insights-dash/dashboard/sign-in.html";
            }
        </script>
</head>

<body class="theme-blue">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
   <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="stats">Trapyz Canvas</a>
            </div>
      <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right" style=" margin-top: 15px;">
            <li  style="margin-top: -15px;">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">more_vert</i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <div onclick="signout()" style="cursor: pointer;">
                                   <i class="material-icons" style="vertical-align: middle;margin-right: 10px;margin-left: 10px;">input</i>Sign Out
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
  
  <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            

         <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>

                    <li>
                        <a href="stats">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                     
                   <li>
                        <a href="partials/custom_queries">
                            <i class="material-icons">accessibility</i>
                            <span>Custom Queries</span>
                        </a>
                    </li> 
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">Trapyz Canvas</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.4
                </div>
            </div>
            <!-- #Footer -->
        </aside>
      
    </section>

    <section class="content">
       <div id='console' style="display: none;"></div>
              <div id='console1' style="display: none;"></div>
     <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content" id="subid">
                            <div class="text">Total Users</div>
			    <div id="totalUsers" class="insights-content"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content" id="campid">
                            <div class="text">Total active users lask week</div>
			    <div id="weekUsers" class="insights-content"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content" id="notifid">
                            <div class="text">Total active users last month</div>
                            <div id="monthUsers" class="insights-content"></div>
                        </div>
                    </div>
                </div>
            </div>
            
           <div class="card">
		<div id="chartdiv" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
    
           </div>

    </section>
<script src="./partials/stats.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <!-- Bootstrap Core Css 
   <script src="./plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <script src="./plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="./plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="./plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="./plugins/node-waves/waves.js"></script>
	<script src="https://cloud.github.com/downloads/uxebu/bonsai/bonsai-0.4.0.min.js"></script>
<script src="https://d3js.org/d3.v3.min.js"></script>

    
    <script src="./plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="./plugins/dropzone/dropzone.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="./plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Multi Select Plugin Js -->
    <script src="./plugins/multi-select/js/jquery.multi-select.js"></script>


    <script src="./plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="./plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>


    <!-- Custom Js -->
    <script src="./js/admin.js"></script>
    <script src="./plugins/jquery-countto/jquery.countTo.js"></script>
    <script src="./js/pages/index.js"></script>
    <!-- Demo Js -->
    <script src="./js/demo.js"></script>
</body>

</html>

