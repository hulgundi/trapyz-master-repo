import requests
import time
import json
import pymysql

apiMap = {}
sapi = "9"

class predefinedInsights():

	def __init__(self, api):
		self.getAllGidCountUrl = "http://52.206.33.14:8000/all?count=1"
		self.getGidsInEpochUrl = "http://52.206.33.14:8000/allbyepoch?start="
		self.getCategoryGidCountUrl = "http://52.206.33.14:8000/allbycat"
		self.currentInSeconds = int(round(time.time() * 1000))
		self.api = str(api)
		self.weekInSeconds = 604800000
		self.monthInSeconds = 2592000000
		self.totalUniqueGidCount = None
		self.lastWeekActiveGidCount = None
		self.lastMonthActiveGidCount = None
		self.categoryGidCountJson = None

	def getTotalUniqueGidCount(self):
		if self.api != sapi:
			self.totalUniqueGidCount = requests.get(self.getAllGidCountUrl + "&api=" + self.api).text
		else:
			self.totalUniqueGidCount = requests.get(self.getAllGidCountUrl).text
		return

	def getLastWeekActiveGidCount(self):
		if self.api != sapi:
			self.lastWeekActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.weekInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1" + "&api=" + self.api).text
		else:
			self.lastWeekActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.weekInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1").text						
		return

	def getLastMonthActiveGidCount(self):
		if self.api != sapi:
			self.lastMonthActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.monthInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1" + "&api=" + self.api).text
		else:
			self.lastMonthActiveGidCount = requests.get(self.getGidsInEpochUrl + str(self.currentInSeconds - self.monthInSeconds) + "&end=" + str(self.currentInSeconds) + "&count=1").text
		return

	def getCategoryGidCount(self):
		if self.api != sapi:
			response = requests.get(self.getCategoryGidCountUrl + "?api=" + self.api)
		else:
			response = requests.get(self.getCategoryGidCountUrl)
		if len(response.text) > 0:
			self.categoryGidCountJson = response.json()
		else:
			self.categoryGidCountJson = "{}"
		return

	def writeData(self):
		try:
			insertQuery = "INSERT INTO MasterPredefinedInsights VALUES(" + self.api + ", CURRENT_DATE(), " + self.totalUniqueGidCount + ", " + self.lastWeekActiveGidCount + ", " + self.lastMonthActiveGidCount + ", '" + json.dumps(self.categoryGidCountJson) + "')"
			connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
			cursor = connect.cursor()
			cursor.execute(insertQuery)
			connect.commit()
			print("Successfully Written Data to DB")
		except:
			print("Exception occured!")

try:
	selectQuery = "SELECT * FROM ApikeyMap where Apikey != ''"
	connect = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
	cursor = connect.cursor()
	cursor.execute(selectQuery)
	print("Successfully fetched data from API table!")
except:
	print("Exception occured while reading API table!")

for row in cursor:
	apiMap[str(row[0])] = row[1]
	predefinedInsightsObject = predefinedInsights(row[0])
	predefinedInsightsObject.getTotalUniqueGidCount()
	predefinedInsightsObject.getLastWeekActiveGidCount()
	predefinedInsightsObject.getLastMonthActiveGidCount()
	predefinedInsightsObject.getCategoryGidCount()
	predefinedInsightsObject.writeData()
	time.sleep(10)