<?php
$servername = "172.31.0.193";
$username = "pwx";
$password = "ciscoplanet";
$dbname = "trapyz_beta";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$case = $_GET["case"];
$api = $_GET["api"];

$sql = "SELECT * FROM MasterPredefinedInsights where createdDate = (SELECT max(createdDate) FROM MasterPredefinedInsights) AND api = " . $api;	

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
	if ($case == "total") {
		echo $row["totalUsers"];
	} elseif ($case == "week") {
		echo $row["lastWeekActiveUsers"];
	} elseif ($case == "month") {
		echo $row["lastMonthActiveUsers"];
	} elseif ($case == "catgid") {
		echo $row["categoryUsersJson"];
	}    
} else {
    echo "0";
}
$conn->close();
?>