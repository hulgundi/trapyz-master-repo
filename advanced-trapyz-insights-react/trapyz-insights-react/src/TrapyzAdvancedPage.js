import React, { Component } from 'react';
import loaderImage from './loader.gif';
import tagCrossImage from './tag cross.png';
import trapyzLogo2 from './trapyz-logo.png';
import ReactDOM from 'react-dom';
import TrapyzHomePage from './TrapyzHomePage';
import './TrapyzAdvancedPage.css';

function preventDefaultAction(event) {
  event.stopPropagation();
}

function hideList() {
    window.removeEventListener("keyup",escapeEvent);
    document.getElementById("city-list").classList.add('d-none');
    document.getElementById("category-list").classList.add('d-none');  
    document.getElementById("sub-category-list").classList.add('d-none');
    document.getElementById("area-list").classList.add('d-none');
    document.getElementById("store-list").classList.add('d-none');
}

function escapeEvent(event){
    event.preventDefault();
    if(event.keyCode === 27){
	hideList();
    }
}

function makeAjaxGetRequest(requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();
  
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };
  if (synchType === undefined) {
    synchType = true;
  }
  xmlHttp.open("GET", requestUrl, synchType);
  xmlHttp.send(null);
}

function readTextFile(file, callbackFunction, misc, synchType)
{
    var rawFile = new XMLHttpRequest();
    
    rawFile.onreadystatechange = function () {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                /*var allText = rawFile.responseText;
                  alert(allText);*/
		callbackFunction(this, misc)
            }
        }
    };
    
  if (synchType === undefined) {
    synchType = true;
  }
    rawFile.open("GET", file, synchType);
    rawFile.send(null);
}


function makeAjaxPostRequest(str, requestUrl, callbackFunction, misc, synchType) {
    var xmlHttp = new XMLHttpRequest();
    var formData = new FormData();

  formData.append('segment', str);
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };
  if (synchType === undefined) {
    synchType = true;
  }
  xmlHttp.open("POST", requestUrl, synchType);
  xmlHttp.send(formData);
}

function showHomePage() {
    ReactDOM.render(<TrapyzHomePage />, document.getElementById('root'));  
}

function loadCities() {
    makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap?mapType=city", function(response) {
        var responseJson, keys = [], citiesList = [], index;

	if (response.responseText === "\n") {
	  responseJson = "";
	} else {
          responseJson = JSON.parse(response.responseText);
	}
	  
        keys = Object.keys(responseJson);
	citiesList.push(<option value="999999">All</option>);
        for (index = 0; index < keys.length; index++) {
          citiesList.push(
            <option value={keys[index]}>
              {responseJson[keys[index]]}
            </option>
          );
        }

        window.instanceOfThis.setState({cityItems: citiesList});
        window.instanceOfThis.setState({cityEditItems: citiesList});
      });  
}

function loadCategories() {
    makeAjaxGetRequest("https://canvas.trapyz.com/getCategory.php", function(response) {
        var responseJson, keys = [], categories = [], index, categoryList = {}, categoryList1 = {};
	window.instanceOfThis.setState({categoriesList: []});

	if (response.responseText === "\n") {
	  responseJson = "";
	} else {
          responseJson = JSON.parse(response.responseText);
	}
	  
        keys = Object.keys(responseJson);
	categories.push(<option value="999999">All</option>);

	for (index = 0; index < keys.length; index++) {
	    categoryList[responseJson[keys[index]]] = keys[index];
	    categoryList1[keys[index]] = responseJson[keys[index]];
          categories.push(
            <option value={keys[index]}>
              {responseJson[keys[index]]}
            </option>
          );

        }
        window.instanceOfThis.setState({categoriesList: categories});
	window.instanceOfThis.setState({onlyCategories: categoryList});
	window.instanceOfThis.setState({onlyCategoryList: categoryList1});
      });  
}

function loadSubCategories() {
      makeAjaxGetRequest("https://canvas.trapyz.com/getSubCategories.php", function(response) {
          var responseJson, keys = [], subCategoryList = {}, index, onlySubCategoryList = {}, subC = [];
	  
	  if (response.responseText === "\n") {
	      responseJson = "";
	  } else {
              responseJson = JSON.parse(response.responseText);
	  }
	  
          keys = Object.keys(responseJson);
          for (index = 0; index < keys.length; index++) {
	      subC = responseJson[keys[index]].split('_')
	      onlySubCategoryList[subC[0]] = keys[index];
              subCategoryList[keys[index]] = responseJson[keys[index]];
          }
	  
	  window.instanceOfThis.setState({onlySubCategories: onlySubCategoryList});
          window.instanceOfThis.setState({subCategoryItems: subCategoryList});
      });  
}

function loadAreas() {
    loader(true);
      makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap-city.php", function(response) {
        var responseJson, keys = [], areasList = {}, index;

	if (response.responseText === "\n") {
	  responseJson = "";
	} else {
          responseJson = JSON.parse(response.responseText);
	}
	  
        keys = Object.keys(responseJson);
        for (index = 0; index < keys.length; index++) {
          areasList[keys[index]] = responseJson[keys[index]];
        }
          window.instanceOfThis.setState({areaItems: areasList});
	  loader(false);
      });  
}

function loader(loading) {
    if(loading) {
	document.getElementById('loader').classList.remove('d-none');
    } else {
	document.getElementById('loader').classList.add('d-none');
    }
}

function filterUniqueQuery(query) {
    var newQuery = query.map((q) => JSON.stringify(q));
    var uniqueQuery = newQuery.filter((v, i, a) => a.indexOf(v) === i);
    var finalQuery = uniqueQuery.map((q) => JSON.parse(q));
    return finalQuery;
}

function convertArrayOfObjectsToCSV(args) {
        var result,lineDelimiter, data;
        data = args.data || null;
        if (data == null || !data.length) {
            return null;
        }
        lineDelimiter = args.lineDelimiter || '\n';
        result = 'GIDS,Total Stores'+lineDelimiter;
        data.forEach(function(item) {
            result += item+lineDelimiter;
        });
        return result;
    }

function downloadCSV(ulist) {
  var data, filename, link;
  //var parentElement = document.getElementById(trapyz.segmentName);
   
  var csv = convertArrayOfObjectsToCSV({
      data: ulist
  });
    
      filename = 'export.csv';
	var blobdata = new Blob([csv],{type : 'text/csv'});

      link = document.createElement('a');
      link.setAttribute("href", window.URL.createObjectURL(blobdata));
      link.setAttribute('download', filename); document.body.appendChild(link);
      link.click();
}

function importSname(arr) {
    var baseRequestUrl = 'https://canvas.trapyz.com/getStoresSname1.php', encoded_uri = encodeURIComponent('SELECT MasterRecordSet.sname, StoreUuidMap.Store_ID FROM MasterRecordSet, StoreUuidMap where MasterRecordSet.UUID = StoreUuidMap.Store_Uuid'), countQueryJson;
    loader(true);
    var hash = {};
    makeAjaxGetRequest(baseRequestUrl, function(response) {
	console.log(response.responseText);
	loader(false);
	countQueryJson = JSON.parse(response.responseText);
	/*countQueryJson.forEach(function(val){
	    hash[val.split('_____')[1]] = val.split('_____')[0];
	});*/
	
	var storeHash = countQueryJson;
	var csvData = [], gid = [], q2 = [], totalStoreCount = 0;
	
	    arr.forEach(function(q){
		gid = [];
		gid.push(q[0]);
		q.shift();
		q2 = q.map(q1 => storeHash[q1].replace(',',''));
		gid.push(q2.length);
		totalStoreCount = totalStoreCount + q2.length;
		gid = gid.concat(q2);
		csvData.push(gid);
	    });
	    csvData.push(['Average Stores per GID', totalStoreCount/arr.length]);
	    downloadCSV(csvData);
    });
}
/*
function showCount() {
    loader(true);
    var queryList = window.instanceOfThis.state.query, baseRequestUrlTest;
    var new_hash = {'description': 'new' ,'name': 'new test', 'attributeJson': JSON.stringify({ 'queryList': queryList }), 'apiKey': '33'};
    var str = JSON.stringify(new_hash);
    var baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat";
    addQueryResult();
    var andOrQuerySelection = Array.from(document.getElementsByName('query-selection')).find(input => input.checked).value;

    if(andOrQuerySelection === 'AND') {
	//baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat-and";
	baseRequestUrlTest = 'http://127.0.0.1:8000/showAndQueryCount?segment=';
    } else {
	baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat";
	//baseRequestUrlTest = 'http://127.0.0.1:8000/showCountForCustomSegment?segment=';
    }
    
    var encoded_uri = encodeURIComponent(str);
    makeAjaxGetRequest(baseRequestUrlTest + encoded_uri, function(response) {
    //makeAjaxPostRequest(str, baseRequestUrl, function(response) {
        console.log(response.responseText);
	loader(false);
	var countQueryJson = JSON.parse(response.responseText), countQuery = 0;
        //countQuery = countQueryJson["new test"]["value"];
	
	if(andOrQuerySelection === 'AND') {
	    //document.getElementsByClassName('segment-count')[0].innerHTML = Object.length(countQueryJson);
	    importSname(countQueryJson);
	    var storeHash = window.instanceOfThis.state.sname;
	    var csvData = [], gid = [], q2 = [];
	    countQueryJson.forEach(function(q){
		gid = []
		gid.push(q[0]);
		q2 = q.map(q1 => storeHash[q1]);
		gid.concat(q2);
		csvData.push(gid);
	    });
	    downloadCSV(csvData);
	} else {
	    countQuery = countQueryJson["new test"]["value"];
	    document.getElementsByClassName('segment-count')[0].innerHTML = countQuery;
	}
	return;
    });
    }*/

function showCount() {
    loader(true);
    var queryList = window.instanceOfThis.state.query, baseRequestUrlTest;
    var new_hash = {'description': 'new' ,'name': 'new test', 'attributeJson': JSON.stringify({ 'queryList': queryList }), 'apiKey': '33'};
    var str = JSON.stringify(new_hash);
    var baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat";
    addQueryResult();
    var andOrQuerySelection = Array.from(document.getElementsByName('query-selection')).find(input => input.checked).value;

    if(andOrQuerySelection === 'AND') {
        baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat-and";
        //baseRequestUrlTest = 'http://127.0.0.1:8000/showAndQueryCount?segment=';
    } else {
        baseRequestUrl = "https://canvas.trapyz.com/new-insights-cat";
        //baseRequestUrlTest = 'http://127.0.0.1:8000/showCountForCustomSegment?segment=';
    }

    //var encoded_uri = encodeURIComponent(str);
    //makeAjaxGetRequest(baseRequestUrlTest + encoded_uri, function(response) {
    makeAjaxPostRequest(str, baseRequestUrl, function(response) {
        console.log(response.responseText);
        loader(false);
        var countQueryJson = JSON.parse(response.responseText), countQuery = 0;
        //countQuery = countQueryJson["new test"]["value"];

        if(andOrQuerySelection === 'AND') {
            document.getElementsByClassName('segment-count')[0].innerHTML = countQueryJson[0];
        } else {
            countQuery = countQueryJson["new test"]["value"];
            document.getElementsByClassName('segment-count')[0].innerHTML = countQuery;
        }
        return;
    });
}

function resetQuery() {
    document.getElementById('area-box').classList.add('d-none');
    document.getElementById('store-box').classList.add('d-none');
    document.getElementById('sub-category-box').classList.add('d-none');
    document.getElementById('enter-city').value = '';
    document.getElementById('enter-area').value = '';
    document.getElementById('start-date').value = '';
    document.getElementById('end-date').value = '';
    document.getElementById('enter-category').value = '';
    document.getElementById('enter-sub-category').value = '';
    document.getElementById('enter-area').value = '';
    document.getElementById('enter-store').value = '';
    window.instanceOfThis.state.query = [{'city': '','dimension': {'name': '', 'value':''},'distance':{'condition': '', 'value': ''},'duration':{'type': '', 'startDate': '', 'endDate': ''},'pin': '','visit':{'condition': '', 'value': ''}}];
    window.instanceOfThis.state.cityTag = [];
    window.instanceOfThis.state.areaTag = [];
    window.instanceOfThis.state.categoryTag = [];
    window.instanceOfThis.state.subCategoryTag = [];
    window.instanceOfThis.state.storeTag = [];
    window.instanceOfThis.state.startDate = '';
    window.instanceOfThis.state.endDate = '';
    window.instanceOfThis.state.visit = '';
    window.instanceOfThis.state.distance = '';
    window.instanceOfThis.state.timeSlotTag = [];
    window.instanceOfThis.state.dayTag = [];
    document.getElementById('query-string').innerText = '';
    document.getElementById('visits').value = '';
    document.getElementById('distance').value = '';
    document.getElementById('add-city').classList.add('d-none');
    document.getElementById('add-visits').classList.add('d-none');
    document.getElementById('add-category').classList.add('d-none');
    document.getElementById('add-distance').classList.add('d-none');
    document.getElementById('add-date').classList.add('d-none');
    document.getElementById('city-list').classList.add('d-none');
    document.getElementById('area-list').classList.add('d-none');
    document.getElementById('category-list').classList.add('d-none');
    document.getElementById('sub-category-list').classList.add('d-none');
    document.getElementById('store-list').classList.add('d-none');
    document.getElementsByClassName('segment-count')[0].innerHTML = 0;
    document.getElementById('clear-visits').classList.add('d-none');
    document.getElementById('clear-distance').classList.add('d-none');
    document.getElementById('clear-date').classList.add('d-none');
    document.getElementById('add-day-of-week').classList.add('d-none');
    document.getElementById('add-time-slot').classList.add('d-none');
    document.getElementById('day-of-week').value = '';
    document.getElementById('time-slot').value = '';
    selectOR();
    addQueryResult();
}

function pickStoresByCity(selectCity) {
    var index, selectedStores = [];
    var stores = Object.values(window.instanceOfThis.state.selectedStores)

    if(selectCity.toString() !== '') {
	for(index = 0; index < stores.length; index++) {
	    if(parseInt(selectCity, 10) === parseInt(stores[index]['cityId'], 10)) {
		selectedStores.push(stores[index]);
	    }
	}
    }

    convertStores(selectedStores);
}

function pickStoresByArea(selectArea) {
    var index, selectedStores = [], areaStore;
    var selectCity = parseInt(window.instanceOfThis.state.areaItems[selectArea].split('_')[1], 10);
    pickStoresByCity(selectCity);
    var stores = Object.values(window.instanceOfThis.state.selectedStores);
    var category = document.getElementById('enter-category').value;
    var subCategory = document.getElementById('enter-sub-category').value;
    
    if(selectCity.toString() !== '') {
	for(index = 0; index < stores.length; index++) {
	    if(parseInt(selectCity, 10) === parseInt(stores[index]['cityId'], 10)) {
		if(selectArea.toString() !== '') {
		    areaStore = stores[index]['areas'].find((a) => parseInt(a['id'], 10) === parseInt(selectArea, 10));
		    if(areaStore) {
			selectedStores.push(stores[index]);
		    }
	         } else if(selectArea.toString() === '') {
		    selectedStores.push(stores[index]);
		}
	    }
	}
    }
    
    var selectedStoresFilter = [], selectCategory = window.instanceOfThis.state.currentCategory, selectSubCategory = window.instanceOfThis.state.currentSubCategory;
    
    if(category !== '' && subCategory === '') {
	for(index = 0; index < selectedStores.length; index++) {
	    if(parseInt(selectCategory, 10) === parseInt(selectedStores[index]['categoryId'], 10)) {
		selectedStoresFilter.push(selectedStores[index]);
	    }
	}

	selectedStores = selectedStoresFilter;
    }
    
    if(subCategory !== '') {	
	for(index = 0; index < selectedStores.length; index++) {
	    if(parseInt(selectCategory, 10) === parseInt(selectedStores[index]['categoryId'], 10)) {
		if(parseInt(selectSubCategory, 10) === parseInt(selectedStores[index]['subCategoryId'], 10)) {
		    selectedStoresFilter.push(selectedStores[index]);
		}
	    }
	}

	selectedStores = selectedStoresFilter;
    }

    convertStores(selectedStores);
}

function pickStoresByCategory(selectCategory) {
    var index, selectedStores = [], stores = [];
    var area = document.getElementById('enter-area').value;
    var city = document.getElementById('enter-city').value;

    if(selectCategory.toString() !== '') {
	if(city !== '') {
	    pickStoresByCity(parseInt(window.instanceOfThis.state.currentCity, 10));
	}
	
	if(area !== '') {
	    pickStoresByArea(window.instanceOfThis.state.currentArea);
	}
	
	stores = Object.values(window.instanceOfThis.state.selectedStores);
	
	for(index = 0; index < stores.length; index++) {
	    if(parseInt(selectCategory, 10) === parseInt(stores[index]['categoryId'], 10)) {
		selectedStores.push(stores[index]);
	    }
	}
    }
    
    convertStores(selectedStores);
}

function pickStoresBySubCategory(selectSubCategory) {
    var index, selectedStores = [], stores = [];
    var selectCategory = parseInt(window.instanceOfThis.state.subCategoryItems[selectSubCategory].split('_')[1], 10);
    var area = document.getElementById('enter-area').value;
    var city = document.getElementById('enter-city').value;

    if(city !== '') {
	pickStoresByCity(parseInt(window.instanceOfThis.state.currentCity, 10));
    }
    
    if(area !== '') {
	pickStoresByArea(window.instanceOfThis.state.currentArea);
    }

    stores = Object.values(window.instanceOfThis.state.selectedStores);

    if(selectCategory.toString() !== '') {
	for(index = 0; index < stores.length; index++) {
	    if(parseInt(selectCategory, 10) === parseInt(stores[index]['categoryId'], 10)) {
		if(selectSubCategory.toString() !== '') {
		    if(parseInt(selectSubCategory, 10) === parseInt(stores[index]['subCategoryId'], 10)) {
			selectedStores.push(stores[index]);
		    }
	         } else if(selectSubCategory.toString() === '') {
		    selectedStores.push(stores[index]);
		}
	    }
	}
    }
    
    convertStores(selectedStores);
}

function convertStores(selectedStores) {
    var filteredStores = [];
    window.instanceOfThis.state.selectedStores = [];
    
    for(var index=0; index < selectedStores.length; index++) {
	filteredStores.push(selectedStores[index]);
    }

    window.instanceOfThis.state.selectedStores = filteredStores;
}

function pickAreas(selectCity) {
    var areasJson, keys = [], areasList = [], areaParts, cityId, index, hash = {};
    window.instanceOfThis.setState({selectedAreaItems: []});
  
  areasJson = window.instanceOfThis.state.areaItems;
  keys = Object.keys(areasJson);
  for (index = 0; index < keys.length; index++) {
    areaParts = areasJson[keys[index]].split("_");
    cityId = areaParts[1];
    if (parseInt(cityId, 10) === selectCity) {
	hash = {areaName: areaParts[0], areaId: keys[index]};
	areasList.push(hash);
    }
  }

  window.instanceOfThis.setState({selectedAreaItems: areasList});
}

function pickSubCategories(selectCategory) {
    var subCategoryJson, keys = [], subCategories = [], subCategoryParts, categoryId, index, hash;
     window.instanceOfThis.setState({selectedSubCategories: []});
  
  subCategoryJson = window.instanceOfThis.state.subCategoryItems;
  keys = Object.keys(subCategoryJson);
  for (index = 0; index < keys.length; index++) {
    subCategoryParts = subCategoryJson[keys[index]].split("_");
    categoryId = subCategoryParts[1];
      if (parseInt(categoryId, 10) === selectCategory) {
	  hash = {subCategoryName: subCategoryParts[0], subCategoryId: keys[index]};
	  subCategories.push(hash);
    }
  }

    window.instanceOfThis.setState({selectedSubCategories: subCategories});
}

function selectStores(storeText) {
    var storeList = window.instanceOfThis.state.selectedStores, filteredStoreList = [], index;
    if(storeText !== '') {
	document.getElementById('store-list').classList.remove('d-none');

	for(index=0; index < storeList.length; index++) {
	    if (storeList[index].sname.toLowerCase().indexOf(storeText.toLowerCase()) > -1) {
		filteredStoreList.push(
		  <li value={storeList[index].storeId} onClick={selectStoreFromList.bind(this)}>
			{storeList[index].sname}
		  </li>
		);
            }
	}
	
	window.instanceOfThis.state.selectedStoresList = filteredStoreList;
	window.instanceOfThis.setState({selectedStoresList: filteredStoreList});
	
    } else {
	document.getElementById('store-list').classList.add('d-none');
	window.instanceOfThis.state.selectedStoresList = [];
	document.getElementById('add-store').classList.add('d-none');
    }
    window.addEventListener("keyup",escapeEvent);
}

function selectArea() {
    var areaList = window.instanceOfThis.state.selectedAreaItems, filteredAreaList = [], areaText = document.getElementById('enter-area').value, index, cityTag = window.instanceOfThis.state.cityTag;
    if(areaText !== '' && cityTag.length !== 0) {
	document.getElementById('area-list').classList.remove('d-none');

	for(index=0; index < areaList.length; index++) {
	    if (areaList[index].areaName.toLowerCase().indexOf(areaText.toLowerCase()) > -1) {
		filteredAreaList.push(
		  <li value={areaList[index].areaId} onClick={selectAreaFromList.bind(this)}>
			{areaList[index].areaName}
		  </li>
		);
            }
	}
	filteredAreaList.sort((a,b) => (a.props.children.toLowerCase().indexOf(areaText) - b.props.children.toLowerCase().indexOf(areaText)));
	window.instanceOfThis.setState({selectedArea: filteredAreaList});
	
    } else {
	document.getElementById('area-list').classList.add('d-none');
	window.instanceOfThis.setState({selectedArea: []});
	document.getElementById('add-area').classList.add('d-none');
    }
    window.addEventListener("keyup",escapeEvent);
}

function selectSubCategories() {
    var subCategoryList = window.instanceOfThis.state.selectedSubCategories, filteredSubCategoryList = [], subCategoryText = document.getElementById('enter-sub-category').value, index;
    if(subCategoryText !== '') {
	document.getElementById('sub-category-list').classList.remove('d-none');

	for(index=0; index < subCategoryList.length; index++) {
	    if (subCategoryList[index].subCategoryName.toLowerCase().indexOf(subCategoryText.toLowerCase()) > -1) {
		filteredSubCategoryList.push(
		  <li value={subCategoryList[index].subCategoryId} onClick={selectSubCategoryFromList.bind(this)}>
			{subCategoryList[index].subCategoryName}
		  </li>
		);
            }
	}
	filteredSubCategoryList.sort((a,b) => (a.props.children.toLowerCase().indexOf(subCategoryText) - b.props.children.toLowerCase().indexOf(subCategoryText)));
	window.instanceOfThis.setState({selectedSubCategory: filteredSubCategoryList});
	
    } else {
	document.getElementById('sub-category-list').classList.add('d-none');
	window.instanceOfThis.setState({selectedSubCategory: []});
	document.getElementById('add-sub-category').classList.add('d-none');
    }
    window.addEventListener("keyup",escapeEvent);
}

function selectCity() {
    var cityList = window.instanceOfThis.state.cityItems, filteredCityList = [], cityText = document.getElementById('enter-city').value, index;
    document.getElementById('enter-area').value = '';
    
    if(cityText !== '') {
	document.getElementById('city-list').classList.remove('d-none');

	for(index=0; index < cityList.length; index++) {
	    if (cityList[index].props.children.toLowerCase().indexOf(cityText.toLowerCase()) > -1) {
		filteredCityList.push(
		  <li value={cityList[index].props.value} onClick={selectCityFromList.bind(this)}>
			{cityList[index].props.children}
		  </li>
		);
            }
	}
	filteredCityList.sort((a,b) => (a.props.children.toLowerCase().indexOf(cityText) - b.props.children.toLowerCase().indexOf(cityText)));
	window.instanceOfThis.setState({selectedCity: filteredCityList});
	
    } else {
	document.getElementById('city-list').classList.add('d-none');
	if(window.instanceOfThis.state.cityTag.length < 1) {
	    document.getElementById('store-box').classList.add('d-none');
	    document.getElementById('sub-category-box').classList.add('d-none');
	}
	if(document.getElementById('enter-category').value === '') {
	    document.getElementById('store-box').classList.add('d-none');
	}
	document.getElementById('area-box').classList.add('d-none');
	document.getElementById('add-city').classList.add('d-none');
	window.instanceOfThis.setState({selectedCity: []});
    }
    window.addEventListener("keyup",escapeEvent);
}

function selectCategory() {
    var categoryList = window.instanceOfThis.state.categoriesList, filteredCategoryList = [], categoryText = document.getElementById('enter-category').value, index;
    
    if(categoryText !== '') {
	document.getElementById('category-list').classList.remove('d-none');

	for(index=0; index < categoryList.length; index++) {
	    if (categoryList[index].props.children.toLowerCase().indexOf(categoryText.toLowerCase()) > -1) {
		filteredCategoryList.push(
		  <li value={categoryList[index].props.value} onClick={selectCategoryFromList.bind(this)}>
			{categoryList[index].props.children}
		  </li>
		);
            }
	}
	filteredCategoryList.sort((a,b) => (a.props.children.toLowerCase().indexOf(categoryText) - b.props.children.toLowerCase().indexOf(categoryText)));
	window.instanceOfThis.setState({selectedCategory: filteredCategoryList});
	
    } else {
	document.getElementById('category-list').classList.add('d-none');
	document.getElementById('sub-category-box').classList.add('d-none');
	window.instanceOfThis.setState({selectedCategory: []});
	document.getElementById('add-category').classList.add('d-none');
	if(document.getElementById('enter-city').value === '') {
	    document.getElementById('store-box').classList.add('d-none');
	}
    }
    window.addEventListener("keyup",escapeEvent);
}

function selectCategoryFromList(target) {
    document.getElementById('enter-category').value = target.currentTarget.innerText;
    hideCategoryList();
    var categoryTag = window.instanceOfThis.state.categoryTag;
    if(!(categoryTag.find((tag) => (tag.props.value === target.currentTarget.value)))) {
	document.getElementById('add-category').classList.remove('d-none');
	document.getElementById('sub-category-box').classList.remove('d-none');
	document.getElementById('store-box').classList.remove('d-none');
    }

    if(window.instanceOfThis.state.subCategoryTag.find(tag => (parseInt(window.instanceOfThis.state.subCategoryItems[tag.props.value].split('_')[1], 10) === target.currentTarget.value)) || (target.currentTarget.value === 999999 && categoryTag.length > 0)) {
	document.getElementById('add-category').classList.add('d-none');
	document.getElementById('sub-category-box').classList.add('d-none');
	document.getElementById('store-box').classList.add('d-none');
    }

    if(categoryTag.find((tag) => (tag.props.value === 999999))) {
	document.getElementById('add-category').classList.add('d-none');
	document.getElementById('sub-category-box').classList.add('d-none');
	document.getElementById('store-box').classList.add('d-none');
    }

    if(target.currentTarget.value === 999999) {
	document.getElementById('sub-category-box').classList.add('d-none');
	document.getElementById('store-box').classList.add('d-none');
    }
    
    window.instanceOfThis.setState({currentCategory: target.currentTarget.value});
    pickSubCategories(target.currentTarget.value);
    pickStoresByCategory(target.currentTarget.value);
}

function selectSubCategoryFromList(target) {
    document.getElementById('enter-sub-category').value = target.currentTarget.innerText;
    hideSubCategoryList();
    var subCategoryTag = window.instanceOfThis.state.subCategoryTag;
    if(!(subCategoryTag.find((tag) => (tag.props.value === target.currentTarget.value)))) {
	document.getElementById('add-sub-category').classList.remove('d-none');
    }
    window.instanceOfThis.setState({currentSubCategory: target.currentTarget.value});
    pickStoresBySubCategory(target.currentTarget.value);
    document.getElementById('add-category').classList.add('d-none');
}

function selectAreaFromList(target) {
    document.getElementById('enter-area').value = target.currentTarget.innerText;
    hideAreaList();
    var areaTag = window.instanceOfThis.state.areaTag;
    var cityTag = window.instanceOfThis.state.cityTag;
    var areaCityId = parseInt(window.instanceOfThis.state.areaItems[target.currentTarget.value].split('_')[1], 10);
    
    if(!(areaTag.find((tag) => (tag.props.value === target.currentTarget.value)))) {
	if(cityTag.find(tag => tag.props.value === areaCityId)) {
	    document.getElementById('add-area').classList.remove('d-none');
	}
    }
    
    window.instanceOfThis.setState({currentArea: target.currentTarget.value});
    pickStoresByArea(target.currentTarget.value);
}

function selectStoreFromList(target) {
    document.getElementById('enter-store').value = target.currentTarget.innerText;
    hideStoreList();
    var storeTag = window.instanceOfThis.state.storeTag;
    if(!(storeTag.find((tag) => (tag.props.value === target.currentTarget.value)))) {
	document.getElementById('add-store').classList.remove('d-none');
    }
    window.instanceOfThis.setState({currentStore: target.currentTarget.value});
}

function selectCityFromList(target) {
    document.getElementById('enter-city').value = target.currentTarget.innerText;
    var cityId = target.currentTarget.value, cityTag = window.instanceOfThis.state.cityTag; // dimension = {'name': '', 'value': ''};
    hideCityList();
    
    if(!(cityTag.find((tag) => (tag.props.value === cityId)))) {
	document.getElementById('add-city').classList.remove('d-none');
    }

    if(cityTag.length !== 0 && cityTag.find((tag) => (tag.props.value === cityId))) {
	document.getElementById('area-box').classList.remove('d-none');
	document.getElementById('store-box').classList.remove('d-none');
    }

    if(cityTag.find((tag) => (tag.props.value === 999999))) {
      document.getElementById('add-city').classList.add('d-none');
      document.getElementById('area-box').classList.add('d-none');
	document.getElementById('store-box').classList.add('d-none');
    }

    window.instanceOfThis.setState({currentCity: target.currentTarget.value});
    window.instanceOfThis.state.currentCity = target.currentTarget.value;
    pickAreas(cityId);
    pickStoresByCity(cityId);
}

function addCategoryQuery() { //---------------------------------------------------Old complicated add Category Query
    var categoryId = window.instanceOfThis.state.currentCategory;   
    var categoryTag = window.instanceOfThis.state.categoryTag;
    var subCategoryTag = window.instanceOfThis.state.subCategoryTag;
    var storeTag = window.instanceOfThis.state.storeTag;
    var query = window.instanceOfThis.state.query, queryList = [];
    var cityId = window.instanceOfThis.state.query[0].city;
    var pins = window.instanceOfThis.state.areaTag.map(a => a.props.value);
    var cityIds = window.instanceOfThis.state.cityTag.map(a => a.props.value), visit = window.instanceOfThis.state.visit, distance = window.instanceOfThis.state.distance, startDate = window.instanceOfThis.state.startDate, endDate = window.instanceOfThis.state.endDate;
    var type = ((startDate || endDate) ? "custom" : "");
    var categoryName = window.instanceOfThis.state.categoriesList.find((cat) => (parseInt(cat.props.value, 10) === categoryId)).props.children;

    if(categoryTag.length === 0 && subCategoryTag.length === 0 && storeTag.length === 0 && categoryId !== 999999) {
	window.instanceOfThis.state.query.forEach(function (q) {
	    q.dimension.name = 'cat';
	    q.dimension.value = categoryId;
	    q.visit.value = visit;
	    q.distance.value = distance;
	    q.duration.type = type;
	    q.duration.startDate = startDate;
	    q.duration.endDate = endDate;
	});
	
      document.getElementById("query-string").innerText = JSON.stringify(query);
    } else if((categoryTag.length > 0 || subCategoryTag.length > 0 || storeTag.length > 0) && categoryId !== 999999) {
	if(cityIds.length <= 1) {
	    if(pins.length > 0) {
		pins.forEach(function(pin) {
		    queryList.push({'city':cityId,'dimension':{'name': 'cat', 'value': categoryId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': pin,'visit':{'condition': '', 'value': visit}});
		});
	    } else if(pins.length === 0){
		queryList.push({'city':cityId,'dimension':{'name': 'cat', 'value': categoryId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': '','visit':{'condition': '', 'value': visit}});
	    }
	} else if(cityIds.length > 1) {
	    if(pins.length === 0) {
		cityIds.forEach(function(city) {
		    queryList.push({'city':city,'dimension':{'name': 'cat', 'value': categoryId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': '','visit':{'condition': '', 'value': visit}});
		});
	    }
	}
	
	queryList.forEach((q) => query.push(q));
	document.getElementById("query-string").innerText = JSON.stringify(query);
    } else if(categoryId === 999999) {
	window.instanceOfThis.state.query = [{'city':cityId,'dimension':{'name': '', 'value': ''},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': '','visit':{'condition': '', 'value': visit}}];
	document.getElementById("query-string").innerText = JSON.stringify(query);
    }

    categoryTag.push(<div className="d-inline-flex tag pl-1" value={categoryId}><span>{categoryName}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeCategoryTag.bind(this)} value={categoryId}/></div>);
    window.instanceOfThis.setState({categoryTag: categoryTag});
    addQueryResult();
    document.getElementById('add-category').classList.add('d-none');
    document.getElementById('sub-category-box').classList.add('d-none');
    document.getElementById('enter-category').value = '';
    checkDayOfWeekQuery();//---------------------------------------------After refactor not needed
    checkTimeSlotQuery();
}

function newAddCategoryQuery() {//------------------------------------------------------new Simplified add category Query
    var query = window.instanceOfThis.state.query, newQueries = [], newQuery = {};
    var categoryId = window.instanceOfThis.state.currentCategory;
    var categoryTag = window.instanceOfThis.state.categoryTag;
    var categoryName = window.instanceOfThis.state.categoriesList.find((cat) => (parseInt(cat.props.value, 10) === categoryId)).props.children;
    
    if(categoryId !== 999999) {
	query.forEach(function(q) {
	    if(q.dimension.name !== '') {
		newQueries.push(q);
	    }
	    
	    newQuery = JSON.parse(JSON.stringify(q));
	    newQuery.dimension.name = 'cat';
	    newQuery.dimension.value = categoryId;
	    newQueries.push(newQuery);
	});
    } else {
	query.forEach(function(q) {
	    q.dimension.name = '';
	    q.dimension.value = '';
	    newQueries.push(q);
	});
    }

    newQueries = filterUniqueQuery(newQueries); 
    window.instanceOfThis.setState({query: newQueries});
    window.instanceOfThis.state.query = newQueries;
    document.getElementById("query-string").innerText = JSON.stringify(newQueries);
    document.getElementById('enter-category').value = '';
    categoryTag.push(<div className="d-inline-flex tag pl-1" value={categoryId}><span>{categoryName}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeCategoryTag.bind(this)} value={categoryId}/></div>);
    window.instanceOfThis.setState({categoryTag: categoryTag});
    document.getElementById('sub-category-box').classList.add('d-none');
    document.getElementById('enter-category').value = '';
    addQueryResult();
    document.getElementById('add-category').classList.add('d-none');
}

function addSubCategoryQuery() {//------------------------------------------------------old complicated add sub category Query
    var subCategoryId = window.instanceOfThis.state.currentSubCategory;
    var cityId = window.instanceOfThis.state.query[0].city;
    var pins = window.instanceOfThis.state.areaTag.map(a => a.props.value);
    var subCategoryTag = window.instanceOfThis.state.subCategoryTag;
    var categoryTag = window.instanceOfThis.state.categoryTag;
    var query = window.instanceOfThis.state.query, queryList = [];
    var storeTag = window.instanceOfThis.state.storeTag;
    var cityIds = window.instanceOfThis.state.cityTag.map(a => a.props.value);
    var subCategoryName = window.instanceOfThis.state.subCategoryItems[subCategoryId].split('_')[0], visit = window.instanceOfThis.state.visit, distance = window.instanceOfThis.state.distance, startDate = window.instanceOfThis.state.startDate, endDate = window.instanceOfThis.state.endDate;
    var type = ((startDate || endDate) ? "custom" : "");
    
    if(subCategoryTag.length === 0 && categoryTag.length === 0 && storeTag.length === 0) {
	window.instanceOfThis.state.query.forEach(function (q) {
	    q.dimension.name = 'subcat';
	    q.dimension.value = subCategoryId;
	    q.visit.value = visit;
	    q.distance.value = distance;
	    q.duration.type = type;
	    q.duration.startDate = startDate;
	    q.duration.endDate = endDate;
	});
	
      document.getElementById("query-string").innerText = JSON.stringify(query);
    } else if(categoryTag.length > 0 || subCategoryTag.length > 0 || storeTag.length > 0) {
	if(cityIds.length <= 1) {
	    if(pins.length > 0) {
		pins.forEach(function(pin) {
		    queryList.push({'city':cityId,'dimension':{'name': 'subcat', 'value': subCategoryId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': pin,'visit':{'condition': '', 'value': visit}});
		});
	    } else if(pins.length === 0) {
		queryList.push({'city':cityId,'dimension':{'name': 'subcat', 'value': subCategoryId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': '','visit':{'condition': '', 'value': visit}});
	    }
	} else if(cityIds.length > 1) {
	    if(pins.length === 0) {
		cityIds.forEach(function(city) {
		    queryList.push({'city':city,'dimension':{'name': 'subcat', 'value': subCategoryId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': '','visit':{'condition': '', 'value': visit}});
		});
	    }
	}
	
	queryList.forEach((q) => query.push(q));
	document.getElementById("query-string").innerText = JSON.stringify(query);
    }

    document.getElementById('enter-sub-category').value = '';
    window.instanceOfThis.setState({selectedStores: []});
    window.instanceOfThis.setState({currentSubCategory: ''});

    var categoryId = window.instanceOfThis.state.subCategoryItems[subCategoryId].split('_')[1];
    var categoryName = window.instanceOfThis.state.categoriesList.find((cat) => cat.props.value === categoryId).props.children;
    var virtualCategory = categoryTag.filter(tag => !tag.props.value);
    if(!virtualCategory.find(vc => vc.props.children.props.value.split(':')[1] === categoryId)) {
	var subCategoryIds = [];
	subCategoryIds.push(subCategoryId);
	categoryTag.push(<div className="d-inline-flex virtual-tag pl-1"><span value={subCategoryIds + ":" + categoryId}>{categoryName}</span></div>);
	window.instanceOfThis.setState({categoryTag: categoryTag});
    } else if(virtualCategory.find(vc => vc.props.children.props.value.split(':')[1] === categoryId)) {
	var cat = virtualCategory.find(vc => vc.props.children.props.value.split(':')[1] === categoryId);
	var subcats = cat.props.children.props.value.split(':')[0].split(',');
	subcats.push(subCategoryId.toString());
	var reducedCategoryTag = [];
	categoryTag.forEach(function(vc) {
	    if(vc.props.value) {
		reducedCategoryTag.push(vc);
	    } else {
		if(vc.props.children.props.value.split(':')[1] !== categoryId) {
		    reducedCategoryTag.push(vc);
		}
	    }
	});
	reducedCategoryTag.push(<div className="d-inline-flex virtual-tag pl-1"><span value={subcats + ":" + categoryId}>{categoryName}</span></div>);
	window.instanceOfThis.setState({categoryTag: reducedCategoryTag});
    }
	
    subCategoryTag.push(<div className="d-inline-flex tag pl-1" value={subCategoryId}><span>{subCategoryName}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeSubCategoryTag.bind(this)} value={subCategoryId}/></div>);
    
    window.instanceOfThis.setState({subCategoryTag: subCategoryTag});
    addQueryResult();
    document.getElementById('add-sub-category').classList.add('d-none');
    checkDayOfWeekQuery();//---------------------------------------------After refactor not needed
    checkTimeSlotQuery();//---------------------------------------------After refactor not needed
}

function newAddSubCategoryQuery() {//------------------------------------------------------new Simplified add sub category Query
    var query = window.instanceOfThis.state.query, newQueries = [], newQuery = {};
    var subCategoryId = window.instanceOfThis.state.currentSubCategory;
    var subCategoryTag = window.instanceOfThis.state.subCategoryTag;
    var subCategoryName = window.instanceOfThis.state.subCategoryItems[subCategoryId].split('_')[0];
    var categoryTag = window.instanceOfThis.state.categoryTag;
    
    query.forEach(function(q) {
	if(q.dimension.name !== '') {
	    newQueries.push(q);
	}
	
	newQuery = JSON.parse(JSON.stringify(q));
	newQuery.dimension.name = 'subcat';
	newQuery.dimension.value = subCategoryId;
	newQueries.push(newQuery);
    });
    
    newQueries = filterUniqueQuery(newQueries); 
    window.instanceOfThis.setState({query: newQueries});
    window.instanceOfThis.state.query = newQueries;
    document.getElementById("query-string").innerText = JSON.stringify(newQueries);
    
    document.getElementById('enter-sub-category').value = '';
    window.instanceOfThis.setState({selectedStores: []});
    window.instanceOfThis.setState({currentSubCategory: ''});

    var categoryId = window.instanceOfThis.state.subCategoryItems[subCategoryId].split('_')[1];
    var categoryName = window.instanceOfThis.state.categoriesList.find((cat) => cat.props.value === categoryId).props.children;
    var virtualCategory = categoryTag.filter(tag => !tag.props.value);
    if(!virtualCategory.find(vc => vc.props.children.props.value.split(':')[1] === categoryId)) {
	var subCategoryIds = [];
	subCategoryIds.push(subCategoryId);
	categoryTag.push(<div className="d-inline-flex virtual-tag pl-1"><span value={subCategoryIds + ":" + categoryId}>{categoryName}</span></div>);
	window.instanceOfThis.setState({categoryTag: categoryTag});
    } else if(virtualCategory.find(vc => vc.props.children.props.value.split(':')[1] === categoryId)) {
	var cat = virtualCategory.find(vc => vc.props.children.props.value.split(':')[1] === categoryId);
	var subcats = cat.props.children.props.value.split(':')[0].split(',');
	subcats.push(subCategoryId.toString());
	var reducedCategoryTag = [];
	categoryTag.forEach(function(vc) {
	    if(vc.props.value) {
		reducedCategoryTag.push(vc);
	    } else {
		if(vc.props.children.props.value.split(':')[1] !== categoryId) {
		    reducedCategoryTag.push(vc);
		}
	    }
	});
	reducedCategoryTag.push(<div className="d-inline-flex virtual-tag pl-1"><span value={subcats + ":" + categoryId}>{categoryName}</span></div>);
	window.instanceOfThis.setState({categoryTag: reducedCategoryTag});
    }
	
    subCategoryTag.push(<div className="d-inline-flex tag pl-1" value={subCategoryId}><span>{subCategoryName}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeSubCategoryTag.bind(this)} value={subCategoryId}/></div>);
    
    window.instanceOfThis.setState({subCategoryTag: subCategoryTag});
    addQueryResult();
    document.getElementById('add-sub-category').classList.add('d-none');
}

function addStoreQuery() {  //-----------------------------------------------------------------Old Complicated Add Store Query
    var storeId = window.instanceOfThis.state.currentStore;
    var cityId = window.instanceOfThis.state.currentCity;
    var pins = window.instanceOfThis.state.areaTag.map(a => a.props.value);
    var subCategoryTag = window.instanceOfThis.state.subCategoryTag;
    var categoryTag = window.instanceOfThis.state.categoryTag;
    var storeTag = window.instanceOfThis.state.storeTag;
    var query = window.instanceOfThis.state.query, queryList = [];
    var cityIds = window.instanceOfThis.state.cityTag.map(a => a.props.value), visit = window.instanceOfThis.state.visit, distance = window.instanceOfThis.state.distance, startDate = window.instanceOfThis.state.startDate, endDate = window.instanceOfThis.state.endDate;
    var type = ((startDate || endDate) ? "custom" : "");
    
    if(subCategoryTag.length === 0 && categoryTag.length === 0 && storeTag.length === 0) {
	window.instanceOfThis.state.query.forEach(function (q) {
	    q.dimension.name = 'store';
	    q.dimension.value = storeId;
	    q.visit.value = visit;
	    q.distance.value = distance;
	    q.duration.type = type;
	    q.duration.startDate = startDate;
	    q.duration.endDate = endDate;
	});
	
      document.getElementById("query-string").innerText = JSON.stringify(query);
    } else if(categoryTag.length > 0 || subCategoryTag.length > 0 || storeTag.length > 0) {
	if(cityIds.length <= 1) {
	    if(pins.length > 0) {
		pins.forEach(function(pin) {
		    queryList.push({'city':cityId,'dimension':{'name': 'store', 'value': storeId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': pin,'visit':{'condition': '', 'value': visit}});
		});
	    } else if(pins.length === 0) {
		queryList.push({'city':cityId,'dimension':{'name': 'store', 'value': storeId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': '','visit':{'condition': '', 'value': visit}});
	    }
	} else if(cityIds.length > 1) {
	    if(pins.length === 0) {
		cityIds.forEach(function(city) {
		    if(parseInt(window.instanceOfThis.state.storeHash[storeId].cityId, 10) === city) {
			queryList.push({'city':city,'dimension':{'name': 'store', 'value': storeId},'distance':{'condition': '', 'value': distance},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': '','visit':{'condition': '', 'value': visit}});
		    }
		});
	    }
	}
	
	queryList.forEach((q) => query.push(q));
	document.getElementById("query-string").innerText = JSON.stringify(query);
    }
    
    document.getElementById('enter-store').value = '';
    storeTag.push(<div className="d-inline-flex tag pl-1" value={storeId}><span title={window.instanceOfThis.state.storeHash[storeId].city}>{window.instanceOfThis.state.storeHash[storeId].sname}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeStoreTag.bind(this)} value={storeId}/></div>);
    window.instanceOfThis.setState({storeTag: storeTag});
    window.instanceOfThis.setState({currentStore: ''});
    addQueryResult();
    document.getElementById('add-store').classList.add('d-none');
    checkDayOfWeekQuery();//---------------------------------------------After refactor not needed
    checkTimeSlotQuery();//---------------------------------------------After refactor not needed
}

function newAddStoreQuery() {   //-----------------------------------------------------------------new Simplified Add Store Query
    var query = window.instanceOfThis.state.query, newQueries = [], newQuery = {};
    var storeId = window.instanceOfThis.state.currentStore;
    var storeCityId = parseInt(window.instanceOfThis.state.storeHash[storeId].cityId, 10);
    var storeTag = window.instanceOfThis.state.storeTag;
    
    query.forEach(function(q) {
	if(q.dimension.name !== '') {
	    newQueries.push(q);
	}

	if(q.city === storeCityId) {   // ------------------------------------------------------------Also need to add condition for pin checking
	    newQuery = JSON.parse(JSON.stringify(q));
	    newQuery.dimension.name = 'store';
	    newQuery.dimension.value = storeId;
	    newQueries.push(newQuery);
	}
    });

    newQueries = filterUniqueQuery(newQueries); 
    window.instanceOfThis.setState({query: newQueries});
    window.instanceOfThis.state.query = newQueries;
    document.getElementById("query-string").innerText = JSON.stringify(newQueries);
    document.getElementById('enter-store').value = '';
    storeTag.push(<div className="d-inline-flex tag pl-1" value={storeId}><span title={window.instanceOfThis.state.storeHash[storeId].city}>{window.instanceOfThis.state.storeHash[storeId].sname}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeStoreTag.bind(this)} value={storeId}/></div>);
    window.instanceOfThis.setState({storeTag: storeTag});
    window.instanceOfThis.setState({currentStore: ''});
    addQueryResult();
    document.getElementById('add-store').classList.add('d-none');
}

function addCityQuery() {
    var cityId = window.instanceOfThis.state.currentCity, cityTag = window.instanceOfThis.state.cityTag, index;
    var query = window.instanceOfThis.state.query, queryList = [];
    var newQuery = JSON.parse(JSON.stringify(query)), cityHash = {}, visit = window.instanceOfThis.state.visit, distance = window.instanceOfThis.state.distance, startDate = window.instanceOfThis.state.startDate, endDate = window.instanceOfThis.state.endDate;
    var type = ((startDate || endDate) ? "custom" : "");

    var stores = query.map(q => ((q.dimension.name === 'store') ? q.dimension.value : ''));
    stores = stores.filter(a => a !== '');
    window.instanceOfThis.state.cityItems.map((q) => (cityHash[q.props.value] = q.props.children));

    if(query.toString() === '') {
	query = [{'city': '','dimension': {'name': '', 'value':''},'distance':{'condition': '', 'value': ''},'duration':{'type': type, 'startDate': startDate, 'endDate': endDate},'pin': '','visit':{'condition': '', 'value': ''}}];
    }

    for(index=0; index < query.length; index++) {
	if(query[index].pin === '' || query[index].city === cityId) {
	    query[index].city = cityId;
	    query[index].visit.value = visit;
	    query[index].distance.value = distance;
	    query[index].duration.type = type;
	    query[index].duration.startDate = startDate;
	    query[index].duration.endDate = endDate;
	} else if(query[index].pin !== '' && query[index].city !== cityId) {
	    query[index].city = cityId;
	    query[index].pin = '';
	    query[index].visit.value = visit;
	    query[index].distance.value = distance;
	    query[index].duration.type = type;
	    query[index].duration.startDate = startDate;
	    query[index].duration.endDate = endDate;
	}

	if(stores.find(s => (s === query[index].dimension.value && query[index].dimension.name === 'store'))) {
	    query[index].dimension.name = '';
	    query[index].dimension.value = '';
	}
	
	queryList.push(query[index]);
    }

    if(window.instanceOfThis.state.categoryTag.length > 0 || window.instanceOfThis.state.categoryTag.length > 0) {
	queryList = queryList.filter(q => q.dimension.name !== '');
    }
    
    if(cityTag.length > 0) {
	newQuery = newQuery.concat(queryList);
    } else {
	newQuery = queryList;
    }

    if(cityId === 999999) {
	newQuery.forEach(q => q.city = '');
    }
    
    var newUniqueQuery = filterUniqueQuery(newQuery);
    
    document.getElementById("query-string").innerText = JSON.stringify(newUniqueQuery);
    window.instanceOfThis.setState({query: newUniqueQuery});
    window.instanceOfThis.query = newUniqueQuery;
    
    if(cityId === 999999) {
	cityTag = [];
    }
    
    cityTag.push(<div className="d-inline-flex tag pl-1" value={cityId}><span>{cityHash[cityId]}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeCityTag.bind(this)} value={cityId}/></div>);
    window.instanceOfThis.setState({cityTag: cityTag});
    window.instanceOfThis.state.cityTag = cityTag;
    addQueryResult();
    if(cityId === 999999 || cityId === '') {
	document.getElementById('area-box').classList.add('d-none');
	document.getElementById('store-box').classList.add('d-none');
    } else {
	document.getElementById('area-box').classList.remove('d-none');
	document.getElementById('store-box').classList.remove('d-none');
    }

    if(cityId !== 999999 && document.getElementById('enter-area').value !== '') {
	document.getElementById('add-area').classList.remove('d-none');
    }
    
    document.getElementById('add-city').classList.add('d-none');
}

function addAreaQuery() {
    var areaId = window.instanceOfThis.state.currentArea, areaTag = window.instanceOfThis.state.areaTag;
    var areaCityId = parseInt(window.instanceOfThis.state.areaItems[areaId].split('_')[1], 10);
    var areaName = window.instanceOfThis.state.areaItems[areaId].split('_')[0];
    var query = window.instanceOfThis.state.query;
    var newQuery = [], hash = {}, visit = window.instanceOfThis.state.visit, distance = window.instanceOfThis.state.distance, startDate = window.instanceOfThis.state.startDate, endDate = window.instanceOfThis.state.endDate;
    var type = ((startDate || endDate) ? "custom" : "");
    
    if(areaTag.length === 0) { // && query[0].pin === '' && parseInt(query[0].city, 10) === areaCityId
	query.forEach(function(q) { 
	    if(q.pin === '' && parseInt(q.city, 10) === areaCityId) {
		q.pin = areaId;
		q.visit.value = visit;
		q.distance.value = distance;
		q.duration.type = type;
		q.duration.startDate = startDate;
		q.duration.endDate = endDate;
	    }
	});
    } else if(areaTag.length > 0) {
	query.forEach(function(q) {
	    if(q.pin === '' && parseInt(q.city, 10) === areaCityId) {
		q.pin = areaId;
		q.visit.value = visit;
		q.distance.value = distance;
		q.duration.type = type;
		q.duration.startDate = startDate;
		q.duration.endDate = endDate;
	    } else if(q.pin !== '' && parseInt(q.city, 10) === areaCityId) {
		hash = JSON.parse(JSON.stringify(q));
		hash.pin = areaId;
		hash.visit.value = visit;
		hash.distance.value = distance;
		hash.duration.type = type;
		hash.duration.startDate = startDate;
		hash.duration.endDate = endDate;
		newQuery.push(hash);
	    }
	});
	if(newQuery.length > 0) {
            newQuery.forEach((q) => query.push(q));
	}
     }
    
    var newUniqueQuery = filterUniqueQuery(query);  
    document.getElementById("query-string").innerText = JSON.stringify(newUniqueQuery);
    window.instanceOfThis.setState({query: newUniqueQuery});
    window.instanceOfThis.query = newUniqueQuery;
    window.instanceOfThis.setState({currentArea: ''});
    areaTag.push(<div className="d-inline-flex tag pl-1" value={areaId}><span title={window.instanceOfThis.state.cityItems.find(city => city.props.value === areaCityId.toString()).props.children}>{areaName}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeAreaTag.bind(this)} value={areaId}/></div>);
    window.instanceOfThis.setState({areaTag: areaTag});
    document.getElementById('enter-area').value = '';
    addQueryResult();
    document.getElementById('add-area').classList.add('d-none');
}

function removeAreaTag(target) {
    var areaId = parseInt(target.target.attributes.value.value, 10);
    var areaTag = window.instanceOfThis.state.areaTag;
    if(areaTag.length === 1) {
	window.instanceOfThis.state.query.forEach(function(q) {
	    if(q.pin === areaId) {
		q.pin = '';
	    }
	});
    } else if(areaTag.length > 1) {
	var reducedQuery = window.instanceOfThis.state.query.filter(q => (q.pin !== areaId));
	window.instanceOfThis.state.query = reducedQuery;
    }
    
    var reducedAreaTag = areaTag.filter(q => ((q.props.value !== areaId)));
    window.instanceOfThis.setState({areaTag: reducedAreaTag});
    window.instanceOfThis.state.areaTag = reducedAreaTag;
    addQueryResult();
    document.getElementById("query-string").innerText = JSON.stringify(window.instanceOfThis.state.query);
}

function removeCityTag(target) {
    var cityId = parseInt(target.target.attributes.value.value, 10);
    var cityTag = window.instanceOfThis.state.cityTag;
    if(cityTag.length === 1) {
	window.instanceOfThis.state.query.forEach(function(q) {
	    if(q.city === cityId) {
		q.city = '';
		q.pin = '';
		
		if(q.dimension.name === 'store') {
		    q.dimension.name = '';
		    q.dimension.value = '';
		}
	    }
	});
    } else if(cityTag.length > 1) {
	var reducedQuery = window.instanceOfThis.state.query.filter(q => (q.city !== cityId));
	window.instanceOfThis.state.query = reducedQuery;
    }
    
    var reducedCityTag = cityTag.filter(q => ((q.props.value !== cityId)));
    window.instanceOfThis.setState({cityTag: reducedCityTag});
    window.instanceOfThis.state.cityTag = reducedCityTag;
    
    var filteredQuery = filterUniqueQuery(window.instanceOfThis.state.query);
    window.instanceOfThis.state.query = filteredQuery;
    document.getElementById("query-string").innerText = JSON.stringify(window.instanceOfThis.state.query);

    if(!window.instanceOfThis.state.cityTag.find((tag) => (tag.props.value === cityId))) {
	document.getElementById('add-city').classList.remove('d-none');
    }

    if((reducedCityTag.length === 0 && document.getElementById('enter-city').value === '') || reducedCityTag.find((tag) => (document.getElementById('enter-city').value === tag.props.children[0].props.children))) {
	document.getElementById('add-city').classList.add('d-none');
    }

    var storeTag = window.instanceOfThis.state.storeTag;
    storeTag = storeTag.filter((tag) => tag.props.children[0].props.title !== target.target.parentElement.innerText.trim());
    window.instanceOfThis.state.storeTag = storeTag;

    var areaTag = window.instanceOfThis.state.areaTag;
    areaTag = areaTag.filter((tag) => tag.props.children[0].props.title !== target.target.parentElement.innerText.trim());
    window.instanceOfThis.state.areaTag = areaTag;
    addQueryResult();
}

function removeCategoryTag(target) {    
    var categoryId = parseInt(target.target.attributes.value.value, 10);
    var trapyz = window.instanceOfThis.state;
    
    if(trapyz.categoryTag.length === 1 && trapyz.subCategoryTag.length === 0 && trapyz.storeTag.length === 0) {
	window.instanceOfThis.state.query.forEach(function(q) {
	    if(q.dimension.value === categoryId && q.dimension.name === 'cat') {
		q.dimension.name = '';
		q.dimension.value = '';
	    }
	});
    } else if(trapyz.categoryTag.length > 1 || trapyz.subCategoryTag.length > 0 || trapyz.storeTag.length > 0) {
	var reducedQuery = window.instanceOfThis.state.query.filter(q => ((q.dimension.name === 'cat' && q.dimension.value !== categoryId)||(q.dimension.name === 'subcat')||(q.dimension.name === 'store')||(q.dimension.name === '')));
	window.instanceOfThis.state.query = reducedQuery;
    }

    window.instanceOfThis.state.query = filterUniqueQuery(window.instanceOfThis.state.query);

    var categoryTag = window.instanceOfThis.state.categoryTag;
    var reducedCategoryTag = categoryTag.filter(q => ((q.props.value !== categoryId)));
    window.instanceOfThis.setState({categoryTag: reducedCategoryTag});
    window.instanceOfThis.state.categoryTag = reducedCategoryTag;
    addQueryResult();
    document.getElementById("query-string").innerText = JSON.stringify(window.instanceOfThis.state.query);
}

function removeSubCategoryTag(target) {    
    var subCategoryId = parseInt(target.target.attributes.value.value, 10), trapyz = window.instanceOfThis.state, categoryTag = trapyz.categoryTag.filter(tag => (tag.props.className.indexOf(' tag') > -1 ));

    if(categoryTag.length === 0 && trapyz.subCategoryTag.length === 1 && trapyz.storeTag.length === 0) {
	window.instanceOfThis.state.query.forEach(function(q) {
	    if(q.dimension.value === subCategoryId && q.dimension.name === 'subcat') {
		q.dimension.name = '';
		q.dimension.value = '';
	    }
	});
    } else if(categoryTag.length > 0 || trapyz.subCategoryTag.length > 1 || trapyz.storeTag.length > 0) {
	var reducedQuery = window.instanceOfThis.state.query.filter(q => ((q.dimension.name === 'subcat' && q.dimension.value !== subCategoryId)||(q.dimension.name === 'cat')||(q.dimension.name === 'store')||(q.dimension.name === '')));
	window.instanceOfThis.state.query = reducedQuery;
    }
    window.instanceOfThis.state.query = filterUniqueQuery(window.instanceOfThis.state.query);
    var subCategoryTag = window.instanceOfThis.state.subCategoryTag;
    var reducedSubCategoryTag = subCategoryTag.filter(q => ((q.props.value !== subCategoryId)));
    window.instanceOfThis.setState({subCategoryTag: reducedSubCategoryTag});
    window.instanceOfThis.state.subCategoryTag = reducedSubCategoryTag;

    var categoryId = window.instanceOfThis.state.subCategoryItems[subCategoryId].split('_')[1];
    var categoryName = window.instanceOfThis.state.categoriesList.find((cat) => cat.props.value === categoryId).props.children;
    var categoryTag = window.instanceOfThis.state.categoryTag;
    var virtualCategory = categoryTag.filter(tag => !tag.props.value);
    var cat = virtualCategory.find(vc => vc.props.children.props.value.split(':')[1] === categoryId);
    var subcats = cat.props.children.props.value.split(':')[0].split(',');
    if(subcats.length === 1) {
	var reducedCategoryTag = [];
	categoryTag.forEach(function(vc) {
	    if(vc.props.value) {
		reducedCategoryTag.push(vc);
	    } else {
		if(vc.props.children.props.value.split(':')[1] !== categoryId) {
		    reducedCategoryTag.push(vc);
		}
	    }
	});
	window.instanceOfThis.setState({categoryTag: reducedCategoryTag});
	window.instanceOfThis.state.categoryTag = reducedCategoryTag;
    } else if(subcats.length > 1) {
	subcats = subcats.filter(s => s !== subCategoryId.toString());
	var reducedCategoryTag = [];
	categoryTag.forEach(function(vc) {
	    if(vc.props.value) {
		reducedCategoryTag.push(vc);
	    } else {
		if(vc.props.children.props.value.split(':')[1] !== categoryId) {
		    reducedCategoryTag.push(vc);
		}
	    }
	});

	if(window.instanceOfThis.state.query.length === 0) {
	    window.instanceOfThis.state.query = [{'city': '','dimension': {'name': '', 'value':''},'distance':{'condition': '', 'value': ''},'duration':{'type': '', 'startDate': '', 'endDate': ''},'pin': '','visit':{'condition': '', 'value': ''}}];
	}
	
	reducedCategoryTag.push(<div className="d-inline-flex virtual-tag pl-1"><span value={subcats + ":" + categoryId}>{categoryName}</span></div>);
	window.instanceOfThis.setState({categoryTag: reducedCategoryTag});
	window.instanceOfThis.state.categoryTag = reducedCategoryTag;
    }
    
    addQueryResult();
    document.getElementById("query-string").innerText = JSON.stringify(window.instanceOfThis.state.query);
}

function removeStoreTag(target) {    
    var storeId = parseInt(target.target.attributes.value.value, 10), trapyz = window.instanceOfThis.state;;

    if(trapyz.categoryTag.length === 0 && trapyz.subCategoryTag.length === 0 && trapyz.storeTag.length === 1) {
	window.instanceOfThis.state.query.forEach(function(q) {
	    if(q.dimension.value === storeId && q.dimension.name === 'store') {
		q.dimension.name = '';
		q.dimension.value = '';
	    }
	});
    } else if(trapyz.categoryTag.length > 0 || trapyz.subCategoryTag.length > 0 || trapyz.storeTag.length > 1) {
	var reducedQuery = window.instanceOfThis.state.query.filter(q => ((q.dimension.name === 'store' && q.dimension.value !== storeId)||(q.dimension.name === 'cat')||(q.dimension.name === 'subcat')||(q.dimension.name === '')));
	window.instanceOfThis.state.query = reducedQuery;
    }
    window.instanceOfThis.state.query = filterUniqueQuery(window.instanceOfThis.state.query);
    var storeTag = window.instanceOfThis.state.storeTag;
    var reducedStoreTag = storeTag.filter(q => ((q.props.value !== storeId)));
    window.instanceOfThis.setState({storeTag: reducedStoreTag});
    window.instanceOfThis.state.storeTag = reducedStoreTag;
    addQueryResult();
    document.getElementById("query-string").innerText = JSON.stringify(window.instanceOfThis.state.query);
}

function hideCityList() {
    document.getElementById('city-list').classList.add('d-none');
}

function hideCategoryList() {
    document.getElementById('category-list').classList.add('d-none');
}

function hideAreaList() {
    document.getElementById('area-list').classList.add('d-none');
}

function hideStoreList() {
    document.getElementById('store-list').classList.add('d-none');
}

function hideSubCategoryList() {
    document.getElementById('sub-category-list').classList.add('d-none');
}

function addQueryResult() {
    var queryOutput = [], queryHash = {cities: [], areas: [], categories: [], subCategories: [], stores: []}, cities = window.instanceOfThis.state.cityTag;
    var areas = window.instanceOfThis.state.areaTag, categories = window.instanceOfThis.state.categoryTag, subCategories = window.instanceOfThis.state.subCategoryTag, stores = window.instanceOfThis.state.storeTag;
    
    queryHash.cities.push(window.instanceOfThis.state.cityTag.map(q => q.props.value));
    queryHash.areas.push(window.instanceOfThis.state.areaTag.map(q => q.props.value));
    queryHash.categories.push(window.instanceOfThis.state.categoryTag.map(q => q.props.value));
    queryHash.subCategories.push(window.instanceOfThis.state.subCategoryTag.map(q => q.props.value));
    queryHash.stores.push(window.instanceOfThis.state.storeTag.map(q => q.props.value));

    queryOutput.push(
	    <table className="table table-bordered table-striped d-none">
	      <thead className="thead-dark">
	        <tr>
	          <th>Cities</th>
	          <th>Areas</th>
	          <th>Categories</th>
	          <th>Sub Categories</th>
	          <th>Stores</th>
	        </tr>
	      </thead>
	      <tbody>
	        <tr>
	          <td>{cities}</td>
	          <td>{areas}</td>
	          <td>{categories}</td>
	          <td>{subCategories}</td>
	          <td>{stores}</td>
                </tr>
	      </tbody>
	    </table>
    );

    window.instanceOfThis.setState({queryResult: queryOutput});
    window.instanceOfThis.state.queryResult = queryOutput;
}

function addDateToQuery() {
    var query = window.instanceOfThis.state.query, startDate = document.getElementById('start-date').value, endDate = document.getElementById('end-date').value;

    if(startDate !== '' || endDate !== '') {
	var parseStartDate = new Date(parseInt(startDate.split('-')[0], 10), (parseInt(startDate.split('-')[1], 10) - 1), parseInt(startDate.split('-')[2], 10));
	var parseEndDate = new Date(parseInt(endDate.split('-')[0], 10), (parseInt(endDate.split('-')[1], 10) - 1), parseInt(endDate.split('-')[2], 10), 23, 59, 59);

	query.forEach(function(q) {
	    q.duration.type = "custom";
	    q.duration.startDate = parseStartDate.getTime();
	    q.duration.endDate = parseEndDate.getTime();
	});
    }

    window.instanceOfThis.setState({startDate: parseStartDate.getTime()});
    window.instanceOfThis.setState({endDate: parseEndDate.getTime()});
    document.getElementById('clear-date').classList.remove('d-none');
    document.getElementById('add-date').classList.add('d-none');
    document.getElementById('query-string').innerText = JSON.stringify(query);
}

function addVisitsToQuery() {
    var query = window.instanceOfThis.state.query, visits = document.getElementById('visits').value, condition = document.getElementById('visits-comparision').value;

    if(visits !== '') {
	query.forEach(function(q) {
	    q.visit.condition = condition;
	    q.visit.value = visits;
	});
    }
    
    document.getElementById('add-visits').classList.add('d-none');
    document.getElementById('clear-visits').classList.remove('d-none');
    window.instanceOfThis.setState({visit: visits});
    document.getElementById('query-string').innerText = JSON.stringify(query);
}

function newAddDaysToQuery() {
    var query = window.instanceOfThis.state.query, dayOfWeek = document.getElementById('day-of-week').value, days = {1:'Monday', 2:'Tuesday', 3:'Wednesday', 4:'Thursday', 5:'Friday', 6:'Saturday', 7:'Sunday'}, dayTag = window.instanceOfThis.state.dayTag, newQuery = [];
    
    if(dayOfWeek !== '') {
	if(dayTag.length === 0) {
	    query.forEach(function(q) {
		q.day = {'value': dayOfWeek};
	    });
	} else if(dayTag.length > 0) {
	    newQuery = JSON.parse(JSON.stringify(query));
	    newQuery.forEach(function(q) {
		q.day = {'value': dayOfWeek};
	    });
	    newQuery.forEach(q => query.push(q));
	}
    }
    
    var filteredQuery = filterUniqueQuery(window.instanceOfThis.state.query);
    dayTag.push(<div className="d-inline-flex tag pl-1" value={dayOfWeek}><span>{days[dayOfWeek]}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeDayTag.bind(this)} value={days[dayOfWeek]}/></div>);
    window.instanceOfThis.setState({dayTag: dayTag});
    window.instanceOfThis.setState({query: filteredQuery});
    window.instanceOfThis.state.query = filteredQuery;
    document.getElementById('query-string').innerText = JSON.stringify(filteredQuery);
    document.getElementById('add-day-of-week').classList.add('d-none');
}

function newAddTimeSlotToQuery() {
    var query = window.instanceOfThis.state.query, timeSlot = document.getElementById('time-slot').value, timeSlotTag = window.instanceOfThis.state.timeSlotTag, newQuery = [], slots = {1:{min: 5, max: 8}, 2:{min: 8, max: 12}, 3:{min: 12, max: 18}, 4:{min: 18, max: 21}, 5:{min:21, max:24}, 6:{min:0, max:5}}, slotStates = {1:"Dawn(5AM-8AM)", 2:"Morning(8AM-12PM)", 3:"Afternoon(12PM-6PM)", 4:"Evening(6PM-9PM)", 5:"Night(9PM-12AM)", 6:"Late Night(12AM-5AM)"};
    
    if(timeSlot !== '') {
	if(timeSlotTag.length === 0) {
	    query.forEach(function(q) {
		q.slot = slots[timeSlot];
	    });
	} else if(timeSlotTag.length > 0) {
	    newQuery = JSON.parse(JSON.stringify(query));
	    newQuery.forEach(function(q) {
		q.slot = slots[timeSlot];
	    });
	    newQuery.forEach(q => query.push(q));
	}
    }
    
    var filteredQuery = filterUniqueQuery(window.instanceOfThis.state.query);
    timeSlotTag.push(<div className="d-inline-flex tag pl-1" value={timeSlot}><span>{slotStates[timeSlot]}</span><img src={tagCrossImage} className="remove-tag" alt="Tag Remove" onClick={removeTimeSlotTag.bind(this)} value={timeSlot}/></div>);
    window.instanceOfThis.setState({timeSlotTag: timeSlotTag});
    window.instanceOfThis.setState({query: filteredQuery});
    window.instanceOfThis.state.query = filteredQuery;
    document.getElementById('query-string').innerText = JSON.stringify(filteredQuery);
    document.getElementById('add-time-slot').classList.add('d-none');
}

function addDistanceToQuery() {
    var query = window.instanceOfThis.state.query, distance = document.getElementById('distance').value, condition = document.getElementById('distance-comparision').value;
    if(distance !== '') {
	query.forEach(function(q) {
	    q.distance.condition = condition;
	    q.distance.value = distance;
	});
    }

    document.getElementById('add-distance').classList.add('d-none');
    document.getElementById('clear-distance').classList.remove('d-none');
    window.instanceOfThis.setState({distance: distance});
    document.getElementById('query-string').innerText = JSON.stringify(query);
}

function clearDistance() {
    var query = window.instanceOfThis.state.query;

    query.forEach(function(q) {
	q.distance.condition = '';
	q.distance.value = '';
    });

    document.getElementById('clear-distance').classList.add('d-none');
    document.getElementById('distance').value = '';
    window.instanceOfThis.setState({distance: ''});
    window.instanceOfThis.state.distance = '';
    document.getElementById('query-string').innerText = JSON.stringify(query);
}

function clearVisit() {
    var query = window.instanceOfThis.state.query;

    query.forEach(function(q) {
	q.visit.condition = '';
	q.visit.value = '';
    });

    document.getElementById('clear-visits').classList.add('d-none');
    document.getElementById('visits').value = '';
    window.instanceOfThis.setState({visit: ''});
    window.instanceOfThis.state.visit = '';
    document.getElementById('query-string').innerText = JSON.stringify(query);
}

function clearDate() {
    var query = window.instanceOfThis.state.query;

    query.forEach(function(q) {
	q.duration.type = '';
	q.duration.startDate = null;
	q.duration.endDate = null;
    });

    document.getElementById('clear-date').classList.add('d-none');
    document.getElementById('start-date').value = '';
    document.getElementById('end-date').value = '';
    window.instanceOfThis.setState({startDate: null});
    window.instanceOfThis.setState({endDate: null});
    window.instanceOfThis.state.startDate = '';
    window.instanceOfThis.state.endDate = '';
    document.getElementById('query-string').innerText = JSON.stringify(query);
}

function removeDayTag(target) {
    var query = window.instanceOfThis.state.query, day = target.target.attributes.value.value, a = [], h, newQuery = [], days = {'Monday':'1', 'Tuesday': '2', 'Wednesday':'3', 'Thursday': '4', 'Friday': '5', 'Saturday':'6', 'Sunday':'7'}, dayTag = window.instanceOfThis.state.dayTag;

    if(dayTag.length === 1) {
	query.forEach(function(q) {
	    if(q.day.value === days[day]) {
		a = Object.entries(q).filter(e => e[0] !== 'day');
		h = {};
		a.forEach(b => h[b[0]] = b[1]);
		newQuery.push(h);
	    }
	});
    } else if(dayTag.length > 0) {
	newQuery = query.filter(q => q.day.value !== days[day]);
    }
    
    dayTag = dayTag.filter(d => d.props.value !== days[day]);
    window.instanceOfThis.setState({dayTag: dayTag});
    window.instanceOfThis.state.dayTag = dayTag;
    window.instanceOfThis.setState({query: newQuery});
    document.getElementById('query-string').innerText = JSON.stringify(newQuery);
    checkDaysBlank();
}

function removeTimeSlotTag(target) {
    var query = window.instanceOfThis.state.query, timeSlot = target.target.attributes.value.value, a = [], h, newQuery = [], timeSlotTag = window.instanceOfThis.state.timeSlotTag, slots = {1:{min: 5, max: 8}, 2:{min: 8, max: 12}, 3:{min: 12, max: 18}, 4:{min: 18, max: 21}, 5:{min:21, max:24}, 6:{min:0, max:5}};

    if(timeSlotTag.length === 1) {
	query.forEach(function(q) {
	    if(JSON.stringify(q.slot) === JSON.stringify(slots[timeSlot])) {
		a = Object.entries(q).filter(e => e[0] !== 'slot');
		h = {};
		a.forEach(b => h[b[0]] = b[1]);
		newQuery.push(h);
	    }
	});
    } else if(timeSlotTag.length > 0) {
	newQuery = query.filter(q => JSON.stringify(q.slot) !== JSON.stringify(slots[timeSlot]));
    }
    
    timeSlotTag = timeSlotTag.filter(d => d.props.value !== timeSlot);
    window.instanceOfThis.setState({timeSlotTag: timeSlotTag});
    window.instanceOfThis.state.timeSlotTag = timeSlotTag;
    window.instanceOfThis.setState({query: newQuery});
    document.getElementById('query-string').innerText = JSON.stringify(newQuery);
    checkTimeSlotBlank();
}

function createNewCategoryMap() {
    var categories = window.instanceOfThis.state.categoriesList.map((c) => c.props);
    var multipleCategories = categories.filter(c => (c.children.split(',').length > 1));
    var singleCategories = categories.filter(c => (c.children.split(',').length === 1));
    multipleCategories.forEach(
	function(cat) {
	    cat.children.split(',').filter((c) => (c === singleCategories.filter))
	    
	});
}

function checkDaysBlank() {
    var dayTag = window.instanceOfThis.state.dayTag, day = document.getElementById('day-of-week').value;

    if(day !== '' && !dayTag.find(d => d.props.value === day)) {
	document.getElementById('add-day-of-week').classList.remove('d-none');
    } else {
	document.getElementById('add-day-of-week').classList.add('d-none');
    }
}

function checkTimeSlotBlank() {
    var timeSlotTag = window.instanceOfThis.state.timeSlotTag, timeSlot = document.getElementById('time-slot').value;
    
    if(timeSlot !== '' && !timeSlotTag.find(d => d.props.value === timeSlot)) {
	document.getElementById('add-time-slot').classList.remove('d-none');
    } else {
	document.getElementById('add-time-slot').classList.add('d-none');
    }
}

function checkDistanceBlank() {
    if(document.getElementById('distance').value !== '') {
	document.getElementById('add-distance').classList.remove('d-none');
    } else {
	document.getElementById('add-distance').classList.add('d-none');
    }
}

function checkVisitsBlank() {
    if(document.getElementById('visits').value !== '') {
	document.getElementById('add-visits').classList.remove('d-none');
    } else {
	document.getElementById('add-visits').classList.add('d-none');
    }
}

function checkDateBlank() {
    if(document.getElementById('end-date').value !== ''&& document.getElementById('start-date').value !== '') {
	document.getElementById('add-date').classList.remove('d-none');
    } else {
	document.getElementById('add-date').classList.add('d-none');
    }
}

function loadStoresAccordingToQuery() { //---------------------------------------------------------selectedCity/Area/Category/SubCategory should be taken from form-field instead of state
    var storeText = document.getElementById('enter-store').value, selectedCity, selectedArea, selectedCategory, selectedSubCategory;
    selectedCity = (window.instanceOfThis.state.currentCity !== '' ? (window.instanceOfThis.state.cityItems.find((city) => parseInt(city.props.value, 10) === window.instanceOfThis.state.currentCity).props.children) : '');
    selectedArea = (window.instanceOfThis.state.currentArea !== '' ? (window.instanceOfThis.state.areaItems[window.instanceOfThis.state.currentArea].split('_')[2]) : '');
    selectedCategory = (window.instanceOfThis.state.currentCategory !== '' ? (window.instanceOfThis.state.onlyCategoryList[window.instanceOfThis.state.currentCategory]) : '');
    selectedSubCategory = (window.instanceOfThis.state.currentSubCategory !== '' ? (window.instanceOfThis.state.subCategoryItems[window.instanceOfThis.state.currentSubCategory].split('_')[0]) : '');
    
    var query = "SELECT MasterRecordSet.*, StoreUuidMap.Store_ID FROM MasterRecordSet, StoreUuidMap where MasterRecordSet.UUID = StoreUuidMap.Store_Uuid AND ", params = '', storeParameter, request = false;
    var cityParameter = "city LIKE '" + (selectedCity !== '' ? selectedCity : '%') + "'";
    var areaParameter = " AND pincode LIKE '" + (selectedArea !== '' ? selectedArea : '%') + "'";
    var categoryParameter = " AND cat LIKE '" + (selectedCategory !== '' ? selectedCategory : '%') + "'";
    var subCategoryParameter = " AND subcat LIKE '" + (selectedSubCategory !== '' ? selectedSubCategory : '%') + "'";
    
    if(storeText.length > 1) {
	storeParameter = " AND sname LIKE '%" + storeText + "%'";
	params = query + cityParameter + areaParameter + categoryParameter + subCategoryParameter + storeParameter;
	request = true;
    } else if(storeText.length === 1) {
	var limit = " LIMIT 200";
	storeParameter = " AND sname LIKE '" + storeText + "%'";
	params = query + cityParameter + areaParameter + categoryParameter + subCategoryParameter + storeParameter + limit;
	request = true;
    } else {
	selectStores(storeText);
	loader(false);
    }

    if(request) {
	console.log(params);
	loader(true);
	makeAjaxGetRequest(("https://canvas.trapyz.com/getStores.php?params="+encodeURIComponent(params)), function(response) {
            var responseJson, storeDetails, pin, keys = [], storeList = [], index, storeHash = {}, areasList = window.instanceOfThis.state.areaItems, areaMapList = {}, hash = {}, areaArray = [], noPinCode = [], absentPins = [], categories = window.instanceOfThis.state.onlyCategories, subCategories = window.instanceOfThis.state.onlySubCategories, responseStr, tempStoreText = storeText;
	  console.log('stores loaded');
	  keys = Object.keys(areasList);
          for (index = 0; index < keys.length; index++) {
	      areaArray = areasList[keys[index]].split('_');
	      pin = areaArray[2].toString();
	      hash = {id: keys[index], name: areaArray[0]};
	      if (!areaMapList[pin]) {  areaMapList[pin] = {'areas':[], 'city':''}; }
	      areaMapList[pin]['areas'].push(hash);
	      areaMapList[pin]['city'] = areaArray[1];
          }

	if (response.responseText === "\n") {
	  responseJson = "";
	} else {
	    if(response.responseText.trim().substr(-1) !== '}') {
		responseStr = response.responseText + '}';
	    } else if(response.responseText.trim().substr(-1) === '}') {
		responseStr = response.responseText;
	    }
	    responseJson = JSON.parse(responseStr);
	}
	  
          keys = Object.keys(responseJson);
          for (index = 0; index < keys.length; index++) {
	      storeDetails = responseJson[keys[index]].split('_');
	      if(areaMapList[storeDetails[4]]) {
		  hash = {'sname': storeDetails[0], 'UUID':keys[index], 'storeId': storeDetails[5], 'city': storeDetails[1], 'category': storeDetails[2], 'categoryId': categories[storeDetails[2]], 'subcategory': storeDetails[3], 'subCategoryId': subCategories[storeDetails[3]],'pincode': storeDetails[4], 'areas': areaMapList[storeDetails[4]]['areas'], 'cityId': areaMapList[storeDetails[4]]['city'], pos: storeDetails[0].toLowerCase().indexOf(storeText.toLowerCase())};
		  storeList.push(hash);
	      } else {
		  //hash = {'sname': storeDetails[0], 'UUID':keys[index], 'storeId': storeDetails[5], 'city': storeDetails[1], 'category': storeDetails[2], 'subcategory': storeDetails[3], 'pincode': storeDetails[4], 'areas': [], 'cityId': ''};
		  hash = {'sname': storeDetails[0], 'city': storeDetails[1], 'pincode': storeDetails[4], 'UUID':keys[index], 'storeId': storeDetails[5]};
		  if(storeDetails[4] === '0') {
		      noPinCode.push(hash);
		  } else {
		      absentPins.push(hash);
		  }
	      }
          }
	    storeList.sort((a,b) => (a.pos - b.pos));
	    
          window.instanceOfThis.setState({selectedStores: storeList});
	  Object.values(storeList).forEach((i) => (storeHash[i.storeId] = i));
	  window.instanceOfThis.setState({storeHash: storeHash});
	    
	    if(document.getElementById('enter-store').value !== '' && document.getElementById('enter-store').value === tempStoreText) {
		selectStores(storeText);
		loader(false);
	    }

	    if(document.getElementById('enter-store').value === '') {
		loader(false);
	    }
	});
    }
}

function checkDayOfWeekQuery() {//---------------------------------------------After refactor not needed
    var query = window.instanceOfThis.state.query, dayTag = window.instanceOfThis.state.dayTag, a = [], h, hash, queryList = [], newQuery = [];

    if(dayTag.length > 0) {
	query.forEach(function(q) {
	    a = Object.entries(q).filter(e => e[0] !== 'day');
	    h = {};
	    a.forEach(b => h[b[0]] = b[1])
	    newQuery.push(h);
	});
	query = filterUniqueQuery(newQuery);
        
	dayTag.forEach(function(day) {
	    query.forEach(function(q) {
		hash = JSON.parse(JSON.stringify(q));
		hash.day = {'value': day.props.value};
		queryList.push(hash);
	    });
	});
    
	window.instanceOfThis.setState({query: queryList});
	window.instanceOfThis.state.query = queryList;
	document.getElementById('query-string').innerText = JSON.stringify(queryList);
    }
}

function checkTimeSlotQuery() {//---------------------------------------------After refactor not needed
    var query = window.instanceOfThis.state.query, a = [], h, hash, queryList = [], newQuery = [], timeSlotTag = window.instanceOfThis.state.timeSlotTag, slots = {1:{min: 5, max: 8}, 2:{min: 8, max: 12}, 3:{min: 12, max: 18}, 4:{min: 18, max: 21}, 5:{min:21, max:24}, 6:{min:0, max:5}};

    if(timeSlotTag.length > 0) {
	query.forEach(function(q) {
	    a = Object.entries(q).filter(e => e[0] !== 'slot');
	    h = {};
	    a.forEach(b => h[b[0]] = b[1])
	    newQuery.push(h);
	});
	
	query = filterUniqueQuery(newQuery);
        
	timeSlotTag.forEach(function(slot) {
	    query.forEach(function(q) {
		hash = JSON.parse(JSON.stringify(q));
		hash.slot = slots[slot.props.value];
		queryList.push(hash);
	    });
	});
    
	window.instanceOfThis.setState({query: queryList});
	window.instanceOfThis.state.query = queryList;
	document.getElementById('query-string').innerText = JSON.stringify(queryList);
    }
}

function selectAND() {
    window.instanceOfThis.setState({AND: true});
    window.instanceOfThis.setState({OR: false});
}

function selectOR() {
    window.instanceOfThis.setState({AND: false});
    window.instanceOfThis.setState({OR: true});
}

class TrapyzAdvancedPage extends Component {
  constructor (props) {
    super(props);
      this.citiesDone = false;
      this.areasDone = false;
      this.categoriesDone = false;
      this.state = {
	  uniqueCount: 0,
	  queryCount: 0,
	  currentQueryCount: 0,
	  cityItems: [],
	  categoriesList: [],	
	  areaItems: {},
	  subCategoryItems: {},
	  selectedSubCategories: [],	
	  selectedAreaItems: [],
	  query: [{'city': '','dimension': {'name': '', 'value':''},'distance':{'condition': '', 'value': ''},'duration':{'type': '', 'startDate': '', 'endDate': ''},'pin': '','visit':{'condition': '', 'value': ''}}],
	  onlyCategories: {},
	  onlySubCategories: {},
	  selectedStores: [],
	  selectedStoresList: [],
	  selectedCity: [],
	  selectedArea: [],
	  categoryTag: [],
	  subCategoryTag: [],
	  currentCategory: '',
	  currentSubCategory: '',
	  currentArea: '',
	  currentCity: '',
	  currentStore: '',
	  areaTag: [],
	  storeTag: [],
	  cityTag: [],
	  storeHash: {},
	  queryGroup: [],
	  queryResult: [],
	  startDate: null,
	  endDate: null,
	  distance: '',
	  visit: '',
	  onlyCategoryList: [],
	  dayTag: [],
	  timeSlotTag: [],
	  AND: false,
	  OR: true,
	  sname: []
    }
  }

    componentDidMount() {
	if(!this.citiesDone) { loadCities(); this.citiesDone = true; }
	if(!this.areasDone) { loadAreas(); this.areasDone = true; }
	if(!this.categoriesDone) { loadCategories(); loadSubCategories(); this.categoriesDone = true; }
    }

   render() {
	window.instanceOfThis = this;

    return (
	  <main className="container" onClick={hideList}>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossOrigin="anonymous"/>
	    <section className="d-block mt-4 table-responsive">
	      {this.state.queryResult}
	    </section>
	    
	    <div className="trapyz-user-image">
              <img className="trapyz-user-profile-image" src={trapyzLogo2} alt="" onClick={showHomePage} title="Home"/>
            </div>
	    <section className="alert alert-primary d-inline-block query-picker">
	      <div className="form-inline flex-column align-items-start">
	        <div className="form-control d-flex" id="city-box">
	          <input id="enter-city" placeholder="Enter City" className="pl-2" onKeyUp={() => selectCity()}/>
	          <button id="add-city" className="btn btn-sm btn-success ml-2 d-none" onClick={() => addCityQuery()}>Add</button>
	          <ul id="city-list" className="d-none city-list">
	            {this.state.selectedCity}
	          </ul>
	        </div>

	        <div className="form-control d-none" id="area-box">
	          <input id="enter-area" placeholder="Enter Area" className="pl-2" onKeyUp={() => selectArea()}/>
	          <button id="add-area" className="btn btn-sm btn-success ml-2 d-none" onClick={() => addAreaQuery()}>Add</button>
	          <ul id="area-list" className="d-none area-list">
	            {this.state.selectedArea}
	          </ul>
	        </div>
	    
	        <div className="form-control" id="category-box">
	          <input placeholder="Enter Category" id="enter-category" className="pl-2" onKeyUp={() => selectCategory()}/>
	          <button id="add-category" className="btn btn-sm btn-success ml-2 d-none" onClick={() => addCategoryQuery()}>Add</button>
	          <ul id="category-list" className="d-none category-list"  onClick={preventDefaultAction}>
	            {this.state.selectedCategory}
	          </ul>      
	        </div>
	        <div className="form-control d-none" id="sub-category-box">
	          <input id="enter-sub-category" placeholder="Enter Sub Category" className="pl-2" onKeyUp={() => selectSubCategories()}/>
                  <button id="add-sub-category" className="btn btn-sm btn-success ml-2 d-none" onClick={() => addSubCategoryQuery()}>Add</button>
	          <ul id="sub-category-list" className="d-none sub-category-list">
	            {this.state.selectedSubCategory}
	          </ul>      
	        </div>

                <div className="form-control d-none" id="store-box">
         	  <input id="enter-store" placeholder="Enter Store" className="pl-2" onKeyUp={() => loadStoresAccordingToQuery()}/>
                  <button id="add-store" className="btn btn-sm btn-success ml-2 d-none" onClick={() => addStoreQuery()}>Add</button>
	          <ul id="store-list" className="d-none store-list">
	            {this.state.selectedStoresList}
	          </ul>      
	        </div>

	        <div className="form-control" id="date-box">
	          <div className="d-flex justify-content-between">
	            <label className="mr-1">Start Date:</label>
	            <input id="start-date" placeholder="Start Date" type="date" onChange={() => checkDateBlank()}/>
	          </div>
	          <div className="d-flex justify-content-between mt-2">
	            <label className="mr-1">End Date:</label>
	            <input id="end-date" placeholder="End Date" type="date" onChange={() => checkDateBlank()}/>
	          </div>
	          <button id="add-date" className="btn btn-sm btn-success float-right mt-2 d-none" onClick={() => addDateToQuery()}>Add</button>
	        </div>
	        <select id="store-select" className="form-control d-none" >
	          <option value="">Select Store</option>        
	        </select>
	        <div className="form-control" id="day-box">
                  <div className="d-flex">
	            <label className="visits-label">Day:</label>
	            <select id="day-of-week" name="day" className="ml-2 pl-2"  onChange={() => checkDaysBlank()}>
	              <option value="">Select Day</option>
	              <option value="1">Monday</option>
	              <option value="2">Tuesday</option>
	              <option value="3">Wednesday</option>
	              <option value="4">Thursday</option>
	              <option value="5">Friday</option>
	              <option value="6">Saturday</option>
	              <option value="7">Sunday</option>
	            </select>
	            <button id="add-day-of-week" className="btn btn-sm btn-success ml-4 d-none" onClick={() => newAddDaysToQuery()}>Add</button>
                  </div>
	        </div>
	        <div className="form-control" id="time-slot-box">
                  <div className="d-flex">
	            <label className="visits-label">Time Slot:</label>
	            <select id="time-slot" name="time-slot" className="ml-2 pl-2"  onChange={() => checkTimeSlotBlank()}>
	              <option value="">Select Slot</option>
	              <option value="1" title="5AM-8AM">Dawn</option>
	              <option value="2" title="8AM-12PM">Morning</option>
	              <option value="3" title="12PM-6PM">Afternoon</option>
	              <option value="4" title="6PM-9PM">Evening</option>
	              <option value="5" title="9PM-12AM">Night</option>
	              <option value="6" title="12AM-5PM">Late Night</option>
	            </select>
	            <button id="add-time-slot" className="btn btn-sm btn-success ml-4 d-none" onClick={() => newAddTimeSlotToQuery()}>Add</button>
                  </div>
	        </div>
	        <div className="form-control" id="visits-box">
                  <div className="d-flex">
	            <label className="visit-label">Visit:</label>
	            <select id="visits-comparision" className="d-none">
	              <option value="eq">==</option>
	              <option value="gte">>=</option>
	              <option value="lte">{'<='}</option>
	              <option value="gt">></option>
	              <option value="lt">{'<'}</option>
	            </select>
	            <input id="visits" name="visits" className="ml-2 pl-2 mw-50" onKeyUp={() => checkVisitsBlank()}/>
	            <button id="add-visits" className="btn btn-sm btn-success ml-4 d-none" onClick={() => addVisitsToQuery()}>Add</button>
                  </div>
	        </div>
	        <div className="form-control" id="distance-box">
                  <div className="d-flex">
	            <label>Distance:</label>
	            <select id="distance-comparision" className="d-none">
	              <option value="eq">==</option>
	              <option value="gte">>=</option>
	              <option value="lte">{'<='}</option>
	              <option value="gt">></option>
	              <option value="lt">{'<'}</option>
	            </select>
	            <input id="distance" name="distance" className="pl-2 ml-2 mw-50"  onKeyUp={() => checkDistanceBlank()}/>
	            <button id="add-distance" className="btn btn-sm btn-success ml-4 d-none" onClick={() => addDistanceToQuery()}>Add</button>
	          </div>
                </div>
	        <div className="form-control d-flex justify-content-between" id="and-or-box">
                  <div className="d-flex align-items-center justify-content-center w-50">
                    <input type="radio" name="query-selection" value="OR" checked={this.state.OR} onChange={() => selectOR()}/>
	            <label className="ml-2">OR</label>
	          </div>
	          <div className="d-flex align-items-center justify-content-center w-50">
                    <input type="radio" name="query-selection" value="AND" checked={this.state.AND} onChange={() => selectAND()}/>
	            <label className="ml-2">AND</label>
	          </div>
	        </div>
                <div className="mt-2 d-flex justify-content-around w-100">
	          <button className="btn btn-sm btn-danger" onClick={resetQuery}>Reset Query</button>
                  <button id="show-count" className="btn btn-sm btn-primary" onClick={showCount}>Show Count</button>
	        </div>
	      </div>
	    </section>
	    <section className="d-inline-block current-query">
              <h4 className="text-center m-1 mb-2">Current Query State</h4>
	      <div className="current-query-segment">
	        <label className="mr-2">Cities:</label>
	        <div className="d-inline-block">{this.state.cityTag}</div>
	      </div>
	      <div className="current-query-segment">
	        <label className="mr-2">Areas:</label>
                <div className="d-inline-block">{this.state.areaTag}</div>
	      </div>
	      <div className="current-query-segment">
	        <label className="mr-2">Categories:</label>
                <div className="d-inline-block">{this.state.categoryTag}</div>
	      </div>
	      <div className="current-query-segment">
	        <label className="mr-2">Sub Categories:</label>
                <div className="d-inline-block">{this.state.subCategoryTag}</div>
	      </div>
	      <div className="current-query-segment">
	        <label className="mr-2">Stores:</label>
                <div className="d-inline-block">{this.state.storeTag}</div>
	      </div>
	      <div className="current-query-segment">
	        <div><label className="mr-2">Start Date:</label>
                <div className="d-inline-block">{(this.state.startDate ? (new Date(this.state.startDate)).toDateString() : '')}</div></div>
	        <div><label className="mr-2">End Date:</label>
                <div className="d-inline-block">{(this.state.endDate ? (new Date(this.state.endDate)).toDateString() : '')}</div></div>
	        <div className="d-flex justify-content-end"><button id="clear-date" className="btn btn-sm btn-danger d-none" onClick={() => clearDate()}>Clear</button></div>
	      </div>
	      <div className="current-query-segment">
	        <label className="mr-2">Day of Week:</label>
                <div className="d-inline-block">{this.state.dayTag}</div>
	      </div>
	      <div className="current-query-segment">
	        <label className="mr-2">Time Slot:</label>
                <div className="d-inline-block">{this.state.timeSlotTag}</div>
	      </div>
	      <div className="current-query-segment">
	        <label className="mr-2">Visit:</label>
                <div className="d-inline-block">{(this.state.visit ? (">= " + this.state.visit) : '')}</div>
	        <button id="clear-visits" className="btn btn-sm btn-danger d-none float-right" onClick={() => clearVisit()}>Clear</button>
	      </div>
	      <div className="current-query-segment">
	        <label className="mr-2">Distance:</label>
                <div className="d-inline-block">{(this.state.distance ? ("<= " + this.state.distance + "meters") : '')}</div>
	        <button id="clear-distance" className="btn btn-sm btn-danger d-none float-right" onClick={() => clearDistance()}>Clear</button>
	      </div>
	    </section>
	    <section className="align-self-center mlr-auto">
	      <div className="mb-2 text-center">Unique Gid</div>
	      <div className="segment-count alert alert-success"></div>
	    </section>
	    <section className="alert alert-info w-100">
	      <span>Input Query:</span>
	      <p id="query-string"></p>
	    </section>
	    
	    <aside id="loader" className="d-none loader">
	      <img src={loaderImage} className="loader-image" alt=""/>
	    </aside>
	</main>
    );
  }
}

export default TrapyzAdvancedPage;
