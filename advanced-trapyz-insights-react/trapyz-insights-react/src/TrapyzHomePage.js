import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import trapyzLogo from './trapyz-white-icon.png';
import dailyHuntLogo from './dailyhunt-logo.png';
import trapyzLogo2 from './trapyz-logo.png';
import trapyzMapLogo from './google-map-icon.png';
import trapyzAdvanceLogo from './advance_settings.png';
import segmentIcon from './segment-icon.png';
import foodImage from './foodie.jpg';
import geekImage from './geek.jpg';
import fashionImage from './fashion.jpg';
import autoImage from './automobile.jpg';
import homeImage from './homemaker.jpg';
import personalCareImage from './personal-care.jpg';
import sportImage from './sport.jpg';
import affluentImage from './affluent.jpg';
import travellerImage from './traveller.jpg';
import customImage from './custom.jpg';
import chevronDown from './logout-image.png';
import loaderImage from './loader.gif';
import hamburgerImage from './hamburger.png';
import TrapyzLoginPage from './TrapyzLoginPage';
import TrapyzMapPage from './TrapyzMapPage';
import TrapyzAdvancedPage from './TrapyzAdvancedPage';
//import TrapyzGeofences from './TrapyzGeofences';
import axios from 'axios';
import './TrapyzHomePage.css';
import TrapyzNewAdvancedPage from './TrapyzNewAdvancedPage';
import PieChart from 'react-minimal-pie-chart';
 
function makeAjaxGetRequest(requestUrl, callbackFunction, misc, synchType) {
  var xmlHttp = new XMLHttpRequest();
  
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };
  if (synchType === undefined) {
    synchType = true;
  }
  xmlHttp.open("GET", requestUrl, synchType);
  xmlHttp.send(null);
}

function makeAjaxPostRequest(str, requestUrl, callbackFunction, misc, synchType) {
    var xmlHttp = new XMLHttpRequest();
    var formData = new FormData();

  formData.append('segment', str);
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callbackFunction(this, misc);
    }
  };
  if (synchType === undefined) {
    synchType = true;
  }
  xmlHttp.open("POST", requestUrl, synchType);
  xmlHttp.send(formData);
}

function preventDefaultAction(event) {
  event.stopPropagation();
}

function logout() {
  var items = ["uname", "name", "key"], item;

  for(item in items) {
    sessionStorage.removeItem(items[item]);  
  }
  ReactDOM.render(<TrapyzLoginPage />, document.getElementById('root'));  
}

function showMapPage() {
    ReactDOM.render(<TrapyzMapPage google={window.google} data={window.instanceOfThis.state.latLngList}/>, document.getElementById('root'));  
}

function showAdvancedPage() {
    //ReactDOM.render(<TrapyzNewAdvancedPage/>, document.getElementById('root'));
    window.addEventListener("keyup",escapeEvent);
    ReactDOM.render(<TrapyzNewAdvancedPage/>, document.getElementById('trapyz-new-advanced-page'))
    document.getElementById("trapyz-new-advanced-page").style.display = "block";
}

function showNewAdvancedPage() {
    ReactDOM.render(<TrapyzNewAdvancedPage/>, document.getElementById('trapyz-new-advanced-page'));  
}

function showHomePage() {
    ReactDOM.render(<TrapyzHomePage />, document.getElementById('root'));  
}

function showGeofences() {
    //ReactDOM.render(<TrapyzGeofences />, document.getElementById('root'));  
}

function toggleLoader(flag) {
  if (flag) {
    document.getElementById("trapyz-main-tint-div").style.display = "block";
  } else {
    document.getElementById("trapyz-main-tint-div").style.display = "none";
  }
}

function hideAdvancedCreateSegment() {
    document.getElementById("trapyz-new-advanced-page").style.display = "none";
    showHomePage();
}

function escapeEvent(event){
    event.preventDefault();
    if(event.keyCode === 27){
	hideAdvancedCreateSegment();
      if(document.getElementById("createSegmentPanel").style.display === "block")
        hideCreateSegment();
      else
        hideEditSegment();
    }
}

function escapeHamburgerEvent(event){
    event.preventDefault();
    if(event.keyCode === 27){
	closeHamburgerMenu(event);
    }
}

function showCreateSegment() {
    document.getElementById("createSegmentPanel").style.display = "block";
    document.getElementsByClassName("trapyz-segment-create-button")[1].disabled = true;
    window.addEventListener("keyup",escapeEvent);
    document.getElementById('createSegmentPanel').scrollIntoView();
    document.getElementsByClassName("select-all")[0].checked = false;
    document.getElementById("trapyz-month-select").checked = true;
    showHomePage();
}

function showEditSegment(event) {
  var editPanelElement, segmentElement, segmentCardElement, trapyz = window.instanceOfThis,
  segmentQueryJson = {
    "segmentName": "",
    "segmentDesc": "",
    "api": "",
    "segmentCount": "",
    "queryList": [],
    "existingSegmentName": "",
    "segmentVisit": ""
  };
    window.instanceOfThis.state.paddingAngle = 3;
  editPanelElement = document.getElementById("editSegmentPanel");
  editPanelElement.style.display = "block";
  document.getElementsByClassName("select-all-edit")[0].checked = false;  
  document.getElementsByClassName("trapyz-segment-create-button")[0].disabled = true;  
  editPanelElement.scrollIntoView();
  window.addEventListener("keyup",escapeEvent);
  segmentQueryJson.api = trapyz.api;
  segmentQueryJson.existingSegmentName = event.target.getAttribute("val");
  segmentElement = document.getElementById(segmentQueryJson.existingSegmentName);
  trapyz.existingSegmentName = segmentQueryJson.existingSegmentName;
  trapyz.segmentQueryJson.existingSegmentName = segmentQueryJson.existingSegmentName;
  segmentCardElement = segmentElement.parentNode.parentNode.parentNode.parentNode;
  segmentCardElement.getElementsByClassName("trapyz-hamburger-menu")[0].style.display = "none";
  trapyz.setState({uniqueCount: segmentCardElement.getElementsByClassName("trapyz-card-count")[0].innerHTML});
  makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentDetails.php?api=" + segmentQueryJson.api + "&seg=" + encodeURIComponent(segmentQueryJson.existingSegmentName), function(response) {
      var responseJson, queryJson, citySelectElement, optionElements, areaSelectElement, areasJson, keys = [], areasList = [], areaParts, areaName, cityId, selectCity, index, index2, index3, index4, inputElement;
    responseJson = JSON.parse(response.responseText);
    queryJson = JSON.parse(responseJson["attributeJson"]);
      
    // document.getElementById("segment-edit-name-txt").value = responseJson.name;
    // document.getElementById("segment-edit-desc-txt").value = responseJson.description;

    document.getElementById("segment-name").value = responseJson.name;
    document.getElementById("segment-description").value = responseJson.description;
      
    /*citySelectElement = document.getElementById("trapyz-edit-city-select");
    optionElements = citySelectElement.getElementsByTagName("option");
    for (index = 0; index < optionElements.length; index++) {
      if (optionElements[index].value === queryJson.queryList[0].city) {
          optionElements[index].setAttribute("selected", true);
	  document.getElementById("trapyz-edit-city-select").value = queryJson.queryList[0].city;
        break;
      }
    }
       
    trapyz.setState({selectedEditAreaItems: []});
    selectCity = parseInt(document.getElementById("trapyz-edit-city-select").value);
    areasJson = trapyz.state.areaItems;
    keys = Object.keys(areasJson);
    for (index = 0; index < keys.length; index++) {
      areaParts = areasJson[keys[index]].split("_");
      areaName = areaParts[0];
      cityId = areaParts[1];
      if (parseInt(cityId) === selectCity) {
        areasList.push(
          <option value={keys[index]}>
            {areaName}
          </option>
        );
      }
    }
    trapyz.setState({selectedEditAreaItems: areasList});
 
    areaSelectElement = document.getElementById("trapyz-edit-area-select");
    optionElements = areaSelectElement.getElementsByTagName("option");
    for (index = 0; index < optionElements.length; index++) {
      if (optionElements[index].value === queryJson.queryList[0].pin) {
        optionElements[index].setAttribute("selected", true);
        break;
      }
    }*/

      var catIds = queryJson.queryList.map(q => q.dimension.value), intCatArr =  Object.entries(window.instanceOfThis.interestCategoryMap), ind, arr = [], flag;

	intCatArr.forEach(function(intCat) {
            ind = 0, flag = true;
          intCat[1].forEach(function(cat) {
	      if (!catIds.find(id => id == cat)) {
		  flag = false;
	      }
            ind = ind + 1;
            if(ind == intCat[1].length && flag) {
              arr.push(intCat[0]);
            }
	  });
        });

      optionElements = document.getElementById("trapyz-edit-interest-elements").getElementsByClassName("trapyz-interest-checkbox");
      for (index4 = 0; index4 < optionElements.length; index4++) {
          inputElement = optionElements[index4].getElementsByTagName("input")[0];
	  if(arr.find(val => inputElement.value == val)) {
	      inputElement.checked = true;
	  }
      }
/*
      
    keys = Object.keys(trapyz.interestCategoryMap);
  
    for (index = 0; index < keys.length; index++) {
      for (index2 = 0; index2 < trapyz.interestCategoryMap[keys[index]].length; index2++) {
        for (index3 = 0; index3 < queryJson.queryList.length; index3++) {
          if (queryJson.queryList[index3].dimension.value === trapyz.interestCategoryMap[keys[index]][index2]) {
            optionElements = document.getElementById("trapyz-edit-interest-elements").getElementsByClassName("trapyz-interest-checkbox");
            for (index4 = 0; index4 < optionElements.length; index4++) {
              inputElement = optionElements[index4].getElementsByTagName("input")[0];
              if (inputElement.value === keys[index]) {
                while (inputElement.checked !== true) {
                  inputElement.click();  
                }
              } 
            }
          }
        }
      }
    }    
*/
      
      
    if (queryJson.queryList[0].visit.value !== "") {
      document.getElementById("trapyz-edit-visit-txt").value = queryJson.queryList[0].visit.value;
    } 

      parseDateOption();
      document.getElementById('trapyz-edit-start-date').value = 'yyyy-mm-dd';
      document.getElementById('trapyz-edit-end-date').value = 'yyyy-mm-dd';
      
    if (queryJson.queryList[0].duration.type !== "") {
      if(queryJson.queryList[0].duration.type === "month3") {
        document.getElementById("trapyz-edit-month-3-select").checked = true;
      } else if(queryJson.queryList[0].duration.type === "month2") {
        document.getElementById("trapyz-edit-month-2-select").checked = true;
      } else if(queryJson.queryList[0].duration.type === "month") {
        document.getElementById("trapyz-edit-month-select").checked = true;
      } else if(queryJson.queryList[0].duration.type === "custom") {
          document.getElementById("trapyz-edit-custom-select").checked = true;
	  parseDateOption();
	  var customStartDate = new Date(queryJson.queryList[0].duration.startDate);
	  var customEndDate = new Date(queryJson.queryList[0].duration.endDate);
	  document.getElementById('trapyz-edit-start-date').value = customStartDate.getFullYear() + '-' + (customStartDate.getMonth() < 9 ? '0' : '') + (customStartDate.getMonth()+1) + '-' + (customStartDate.getDate() < 10 ? '0' : '') + customStartDate.getDate();
	  document.getElementById('trapyz-edit-end-date').value = customEndDate.getFullYear() + '-' + (customEndDate.getMonth() < 9 ? '0' : '') + (customEndDate.getMonth()+1) + '-' + (customEndDate.getDate() < 10 ? '0' : '') + customEndDate.getDate();

      }
    }
  }, segmentQueryJson.segmentName);   
}

function hideCreateSegment() {
  window.removeEventListener("keyup",escapeEvent);
  cleanupQueryParameters();
  cleanupUIElements();
    document.getElementById("createSegmentPanel").style.display = "none";
    window.instanceOfThis.state.paddingAngle = 0;
}

function hideEditSegment() {
  window.removeEventListener("keyup",escapeEvent);
  cleanupQueryParameters();
  cleanupUIElements();
  document.getElementById("editSegmentPanel").style.display = "none";
  window.instanceOfThis.state.paddingAngle = 0;
}

function closeHamburgerMenu(event){
  var menuItems;
  menuItems = event.target.parentNode.getElementsByClassName("trapyz-hamburger-menu");
  for(var i=0; i<menuItems.length; i++) {
    menuItems[i].style.display = "none";
    window.removeEventListener("keyup",escapeHamburgerEvent);
  }
}

function toggleHamburgerMenu(event){
  var menuItems;
  menuItems = event.target.parentNode.getElementsByClassName("trapyz-hamburger-menu");
  if(menuItems[0].style.display === "inline-block") {
    menuItems[0].style.display = "none";
  } else {
    menuItems[0].style.display = "inline-block";
    window.addEventListener("keyup",escapeHamburgerEvent);
  }
}

function setsedate(mode){
    var  now, weekInMilli, monthInMilli, temp, trapyz = window.instanceOfThis, weekElement, monthElement, customDateElement,month2Element,month3Element,
     startDateElement, endDateElement;
  // var trapyz=window.instanceOfThis;
  if (mode === "create") {
    //weekElement = document.getElementById("trapyz-week-select");
    month2Element = document.getElementById("trapyz-month-2-select");
    month3Element = document.getElementById("trapyz-month-3-select");  
    monthElement = document.getElementById("trapyz-month-select");
    customDateElement = document.getElementById("trapyz-custom-select")
    startDateElement = document.getElementById("trapyz-start-date");
    endDateElement = document.getElementById("trapyz-end-date");
  } else if (mode === "edit") {
      //weekElement = document.getElementById("trapyz-edit-week-select");
    month2Element = document.getElementById("trapyz-edit-month-2-select");
    month3Element = document.getElementById("trapyz-edit-month-3-select");
    monthElement = document.getElementById("trapyz-edit-month-select");
    customDateElement = document.getElementById("trapyz-edit-custom-select");
    startDateElement = document.getElementById("trapyz-edit-start-date");
    endDateElement = document.getElementById("trapyz-edit-end-date");
  }
  if (month2Element.checked) {
    /*now = new Date().getTime();
    weekInMilli = 604800000;
    trapyz.startDate = now - weekInMilli;
    trapyz.endDate = now;
    trapyz.dateType = "week";*/
      now = new Date().getTime();
    monthInMilli = 2592000000*2;
    trapyz.startDate = now - monthInMilli;
    trapyz.endDate = now;
    trapyz.dateType = "month2";
  }  else if (month3Element.checked) {
    now = new Date().getTime();
    monthInMilli = 2592000000*3;
    trapyz.startDate = now - monthInMilli;
    trapyz.endDate = now;
    trapyz.dateType = "month3";
  }  else if (monthElement.checked) {
    now = new Date().getTime();
    monthInMilli = 2592000000;
    trapyz.startDate = now - monthInMilli;
    trapyz.endDate = now;
    trapyz.dateType = "month";
  } else if (customDateElement.checked) {
    trapyz.startDate = new Date(startDateElement.value).getTime();
    trapyz.endDate = new Date(endDateElement.value).getTime();
    if (trapyz.startDate > window.instanceOfThis.endDate) {
      temp = trapyz.startDate;
      trapyz.startDate = trapyz.endDate;
      trapyz.endDate = temp;
    }
    trapyz.dateType = "custom";
  }
}
function getCategorylist(mode){
  var trapyz=window.instanceOfThis,checkboxElements=[],categoryList = [],index,index2;
  if(mode==="create"){
    checkboxElements = document.getElementById("trapyz-interest-elements").getElementsByClassName("trapyz-segment-checkbox");
  }
  else{
    checkboxElements = document.getElementById("trapyz-edit-interest-elements").getElementsByClassName("trapyz-segment-checkbox");
  }
   for (index = 0; index < checkboxElements.length; index++) {
    if (checkboxElements[index].checked) {
      for (index2 = 0; index2 < trapyz.interestCategoryMap[checkboxElements[index].value].length; index2++) {
        categoryList.push(trapyz.interestCategoryMap[checkboxElements[index].value][index2]);
      }
    }
  }
  return categoryList;
}
function showCount(mode) {
  var checkboxElements, checked = false, index, errorElement, segmentNameElement, segmentDescElement, trapyz = window.instanceOfThis;

  cleanupQueryParameters();
  if (mode === "create") {
    errorElement = document.getElementById("trapyz-error-message-div");
    checkboxElements = document.getElementById("trapyz-interest-elements").getElementsByClassName("trapyz-segment-checkbox");
  } else if (mode === "edit") {
    errorElement = document.getElementById("trapyz-edit-error-message-div");
    checkboxElements = document.getElementById("trapyz-edit-interest-elements").getElementsByClassName("trapyz-segment-checkbox");
    //segmentNameElement = document.getElementById("segment-edit-name-txt");
    //segmentDescElement = document.getElementById("segment-edit-desc-txt");
  }
  
  errorElement.style.display = "none";
  trapyz.setState({uniqueCount: 0});

  for (index = 0; index < checkboxElements.length; index++) {
    if (checkboxElements[index].checked) {
      checked = true;
      break;
    }
  }

  if (checked)  {
    queryModified(false);
    toggleLoader(true);  
    trapyz.segmentName = "test segemnt";
    trapyz.segmentDesc = "test segment";

    constructQueryJSON_UI(mode);
  } else {
    errorElement.style.display = "block";
  }
  return true;
}
function populateUlist(index){
  var trapyz = window.instanceOfThis;
  var keys = Object.keys(trapyz.uniqueGids);
  var visitCondition = trapyz.segmentQueryJson.segmentVisit;
  if(index >= keys.length){
    var count = trapyz.state.uniqueCount;
    trapyz.segmentQueryJson.segmentCount = count;
    toggleLoader(false);
    if(trapyz.export === true){
      downloadCSV();
      trapyz.export = false;
    }
    return;
  }
  else{
    if (keys[index].length > 15 && (trapyz.uniqueGids[keys[index]] >= visitCondition)) {
      trapyz.ulist.push(keys[index]);
    }
    populateUlist(index + 1);
  }
}
function executeQuery(baseRequestUrl, categoryList, filterString, index) {
    var trapyz = window.instanceOfThis, existingSegment; // oldCount = document.getElementsByClassName("trapyz-segment-count")[0].innerText;
    trapyz.setState({currentQueryCount: index});
    existingSegment = trapyz.existingSegmentName;
   var new_hash = {'description': trapyz.segmentDesc ,'name': trapyz.segmentName, 'attributeJson': JSON.stringify({ 'queryList': trapyz.segmentQueryJson.queryList }), 'apiKey': trapyz.api};
   var str = JSON.stringify(new_hash);

    var baseRequestUrl2 = "https://canvas.trapyz.com/new-insights-cat";
    var encoded_uri = encodeURIComponent(str);

    if(trapyz.export) {
	var baseRequestUrl3 = "https://canvas.trapyz.com/new-export";
	makeAjaxPostRequest(str, (baseRequestUrl3), function(response1) {
	     var a = JSON.parse("[" + response1.responseText + "]");
	     trapyz.uniqueGids = a[0];
	     toggleLoader(false);
	     populateUlist(a[0].length);
	     return;
	 });
	
    } else {
	window.instanceOfThis.state.paddingAngle = 0;
	
	makeAjaxPostRequest(str, (baseRequestUrl2), function(response) {
	var trapyz = window.instanceOfThis, countQueryJson = JSON.parse(response.responseText), countQuery = 0;
            countQuery = countQueryJson[trapyz.segmentName]["value"];
	    if(countQuery > 0) {
		window.instanceOfThis.state.paddingAngle = 3;
	    }
        trapyz.setState({currentQueryCount: countQuery});
      trapyz.segmentQueryJson.segmentName = trapyz.segmentName;
      trapyz.segmentQueryJson.segmentDesc = trapyz.segmentDesc;
         trapyz.segmentQueryJson.api = trapyz.api;
         trapyz.segmentQueryJson.existingSegmentName = existingSegment;
         trapyz.setState({uniqueCount: countQuery});
         if (countQuery > 0) {
             document.getElementsByClassName("trapyz-segment-create-button")[1].disabled = false;
             document.getElementsByClassName("trapyz-segment-create-button")[0].disabled = false;
         }

         populateUlist(0);
      return;

    });

    }
}

function constructQueryJSON_UI(mode) {
  var index, baseRequestUrl = "https://canvas.trapyz.com/insights-cat?cat=", filterString = "", categoryList = [], trapyz = window.instanceOfThis, queryJson = trapyz.queryJson;
  trapyz.visit="1";
  if (mode === "create") {
    trapyz.segmentQueryJson.segmentVisit= document.getElementById("trapyz-visit-txt").value;
  } else if (mode === "edit") {
    trapyz.segmentQueryJson.segmentVisit= document.getElementById("trapyz-edit-visit-txt").value;
  }

    trapyz.city = "";
    trapyz.pin = "";
  
  categoryList=getCategorylist(mode);
  setsedate(mode);
  if (trapyz.startDate !== null && trapyz.endDate !== null) {
    filterString += "&start=" + trapyz.startDate + "&end=" + trapyz.endDate;  
  }
  if (trapyz.visit !== "") {
    filterString += "&visit=" + trapyz.visit + "&vcon=" + trapyz.defaultVisitCondition;  
  }
  if (trapyz.pin !== "") {
    filterString += "&pin=" + trapyz.pin;  
  } else if (trapyz.city !== "") {
    filterString += "&city=" + trapyz.city;  
  }
  filterString += "&dist=" + trapyz.defaultDistance + "&dcon=" + trapyz.defaultDistanceCondition;  
  for (index = 0; index < categoryList.length; index++) {
    queryJson.dimension.name = trapyz.defaultDimensionName;
    queryJson.dimension.value = categoryList[index];
    queryJson.visit.condition = trapyz.defaultVisitCondition;
    queryJson.visit.value = trapyz.segmentQueryJson.segmentVisit;
    queryJson.distance.condition = trapyz.defaultDistanceCondition;
    queryJson.distance.value = trapyz.defaultDistance;
    queryJson.duration.type = trapyz.dateType;
    queryJson.duration.startDate = trapyz.startDate;
    queryJson.duration.endDate = trapyz.endDate;
    queryJson.city = trapyz.city;
    queryJson.pin = trapyz.pin;
    trapyz.segmentQueryJson.queryList.push(JSON.parse(JSON.stringify(queryJson)));
  }
  trapyz.setState({queryCount: categoryList.length});
  executeQuery(baseRequestUrl, categoryList,filterString, 0);
}
function constructQueryJSON(){
  var baseRequestUrl = "https://canvas.trapyz.com/insights-cat?cat=", filterString = "", responseJson,queryJson,trapyz = window.instanceOfThis;
  var api = trapyz.api, segmentName=trapyz.segmentName, categoryList=[],i;
  makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentDetails.php?api=" +api + "&seg=" + encodeURIComponent(segmentName), function(response) {
  responseJson = JSON.parse(response.responseText);
  queryJson = JSON.parse(responseJson["attributeJson"]);
  trapyz.segmentQueryJson.segmentVisit=queryJson.segmentVisit;
  trapyz.setState({queryCount:queryJson.queryList.length});
  trapyz.segmentQueryJson=queryJson;
  if(queryJson.queryList[0].duration.startDate === null || queryJson.queryList[0].duration.endDate === null){
      filterString += "&start=" + "&end=";
  }
  else{
    filterString += "&start=" + queryJson.queryList[0].duration.startDate + "&end=" + queryJson.queryList[0].duration.endDate;
  }
  if (queryJson.queryList[0].visit !== "") {
    filterString += "&visit=" + queryJson.queryList[0].visit.value + "&vcon=" + queryJson.queryList[0].visit.condition ;  
  }
  if (queryJson.queryList[0].pin !== "") {
    filterString += "&pin=" + queryJson.queryList[0].pin;  
  } else if (queryJson.queryList[0].city !== "") {
    filterString += "&city=" + queryJson.queryList[0].city;  
  }
  filterString += "&dist=" + queryJson.queryList[0].distance.value + "&dcon=" + queryJson.queryList[0].distance.condition;  
  for(i = 0;i < (queryJson.queryList.length);i++){
    categoryList.push(queryJson.queryList[i].dimension.value);
  }
  executeQuery(baseRequestUrl, categoryList,filterString, 0);
  });
}
function editSegment() {
    toggleLoader(true);
    var segmentJson, existingSegment, trapyz = window.instanceOfThis;
    existingSegment = trapyz.segmentQueryJson.existingSegmentName;
  if (trapyz.isQueryModified) {
    showCount('edit');  
  }

    if (trapyz.segmentQueryJson.segmentName !== "" && trapyz.segmentQueryJson.segmentDesc !== "" && trapyz.segmentQueryJson.api !== "" && trapyz.segmentQueryJson.queryList.length > 0) {
	  trapyz.segmentQueryJson.existingSegmentName = existingSegment;
      segmentJson = JSON.stringify(trapyz.segmentQueryJson);
	makeAjaxPostRequest(segmentJson, "https://canvas.trapyz.com/edit-segment", function(response) {
	    hideEditSegment();
	    toggleLoader(false);
	    window.location.reload();
	});  
    } else {
      document.getElementById("trapyz-error-message-div").style.display = "block";
      document.getElementsByClassName("trapyz-segment-create-button")[0].disabled = true;
    } 
}

function createSegment() {
  var segmentJson, trapyz = window.instanceOfThis;
    
    if (trapyz.isQueryModified) {
	showCount('create');
    }
    
    if ( trapyz.segmentQueryJson.segmentName !== "" && trapyz.segmentQueryJson.segmentDesc !== "" && trapyz.segmentQueryJson.api !== "" && trapyz.segmentQueryJson.queryList.length > 0) {
	toggleLoader(true);
      segmentJson = JSON.stringify(trapyz.segmentQueryJson);
	makeAjaxPostRequest(segmentJson, "https://canvas.trapyz.com/save-segment", function(response) {
	    console.log(response.responseText);
	    trapyz.isQueryModified = false;
	    hideCreateSegment();
	    toggleLoader(false);
	    window.location.reload();
	    return;
	});  
    } else {
      document.getElementById("trapyz-error-message-div").style.display = "block";
      document.getElementsByClassName("trapyz-segment-create-button")[1].disabled = true;	
    }		    
}

function parseDateOption() {
  var index, radioElements, dateContainer, displayFlag = false;

  queryModified(true);
  dateContainer = document.getElementsByClassName("trapyz-date-input-container");
  radioElements = document.getElementsByClassName("trapyz-segment-radio");
  for (index = 0; index < radioElements.length; index++) {
    if (radioElements[index].checked && radioElements[index].value === "custom") {
      displayFlag = true;
      break;
    }
  }
  if (displayFlag) {
    for (index = 0; index < dateContainer.length; index++) {
      dateContainer[index].style.opacity = "1";  
    }
  } else {
    for (index = 0; index < dateContainer.length; index++) {
      dateContainer[index].style.opacity = "0";  
    }
  }
}

function selectAllCheckBoxes() {
    var i, allCheckBoxes = document.getElementsByClassName("trapyz-segment-checkbox"), check=document.getElementsByClassName("select-all")[0];
    if(check.checked) {
      for(i=0; i<allCheckBoxes.length; i++) {
	allCheckBoxes[i].checked = true;
      }
    } else {
	for(i=0; i<allCheckBoxes.length; i++) {
	    allCheckBoxes[i].checked = false;
	}
    }
}

function checkCheckBoxes() {
    var i, createCheckBoxes, editCheckBoxes, createFlag = true, editFlag = true;
    createCheckBoxes = document.getElementById("trapyz-interest-elements").getElementsByClassName('trapyz-interest-checkbox');
    editCheckBoxes = document.getElementById("trapyz-edit-interest-elements").getElementsByClassName('trapyz-interest-checkbox');

    for(i=0; i<createCheckBoxes.length; i++) {
	if(createCheckBoxes[i].children[0].checked == false) {
	    createFlag = false;
	    break;
	}
    }

    if(createFlag) {
	document.getElementsByClassName("select-all")[0].checked = true;
    } else {
	document.getElementsByClassName("select-all")[0].checked = false;
    }

    for(i=0; i<editCheckBoxes.length; i++) {
	if(editCheckBoxes[i].children[0].checked == false) {
	    editFlag = false;
	    break;
	}
    }

    if(editFlag) {
	document.getElementsByClassName("select-all-edit")[0].checked = true;
    } else {
	document.getElementsByClassName("select-all-edit")[0].checked = false;
    }
}

function selectAllCheckBoxesEdit() {
    var i, allCheckBoxes = document.getElementsByClassName("trapyz-segment-checkbox"), check=document.getElementsByClassName("select-all-edit")[0];
    if(check.checked) {
      for(i=0; i<allCheckBoxes.length; i++) {
	allCheckBoxes[i].checked = true;
      }
    } else {
	for(i=0; i<allCheckBoxes.length; i++) {
	    allCheckBoxes[i].checked = false;
	}
    }
}

function hideConfirmation() {
  document.getElementsByClassName("trapyz-confirmation-outer-box")[0].style.display = "none";
  window.instanceOfThis.segmentToDelete = "";
}

function hideConfirmationLimit() {
  document.getElementsByClassName("trapyz-confirmation-limit-checkbox-outer-box")[0].style.display = "none";
  document.getElementsByClassName("trapyz-menu-panel")[0].style.display = "block";
}

function hideConfirmationUrl() {
  document.getElementsByClassName("trapyz-confirmation-url-outer-box")[0].style.display = "none";
  document.getElementsByClassName("trapyz-menu-panel")[0].style.display = "block";
}

function hideConfirmationExport() {
  document.getElementsByClassName("trapyz-confirmation-export-outer-box")[0].style.display = "none";
  var menuItems, i;
  menuItems = document.getElementsByClassName("trapyz-hamburger-menu");
  for(i=0; i<menuItems.length; i++) {
    if(menuItems[i].style.display === "inline-block") {
      menuItems[i].style.display = "none";
    }
  }
}

function showConfirmation(event) {
  document.getElementsByClassName("trapyz-confirmation-outer-box")[0].style.display = "block";
  window.instanceOfThis.segmentToDelete = event.target.getAttribute("val");
  event.target.parentNode.style.display = "none";
}

function showConfirmationExport() {
  document.getElementsByClassName("trapyz-confirmation-export-outer-box")[0].style.display = "block";
}

function deleteSegment() {
  var api, segmentName;

  if (window.instanceOfThis.segmentToDelete !== "") {
    api = sessionStorage.getItem("key");
    segmentName = window.instanceOfThis.segmentToDelete;
    makeAjaxGetRequest("https://canvas.trapyz.com/delete-segment.php?api=" + api + "&seg=" + segmentName, function() {
      window.location.reload(); 
    });  
  }
}

function refreshSegment(event) {
  var api, segmentName, targetElement, parentElement, tintElement;

  api = sessionStorage.getItem("key");
  segmentName = event.target.getAttribute("val");
  parentElement = document.getElementById(segmentName);
  parentElement.parentNode.getElementsByClassName("trapyz-hamburger-menu")[0].style.display = "none";
  tintElement = parentElement.parentNode.parentNode.style.background = "rgba(0,0,0,0.8)";
  parentElement.parentNode.parentNode.getElementsByClassName("trapyz-card-loader")[0].style.display = "inline-block";
  makeAjaxGetRequest("https://canvas.trapyz.com/new-refresh-segment.php?api=" + api + "&seg=" + segmentName, function(response) {
    var countQueryJson = JSON.parse(response.responseText), countQuery = 0;
    countQuery = countQueryJson[segmentName]["value"];
    parentElement = document.getElementById(segmentName);
    parentElement.parentNode.parentNode.getElementsByClassName("trapyz-card-loader")[0].style.display = "none";
    tintElement = parentElement.parentNode.parentNode.style.background = "rgba(0,0,0,0.6)";
    targetElement = parentElement.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("trapyz-card-count")[0];
    targetElement.innerHTML = countQuery;
  }, segmentName);
}

function queryModified(flag) {
  var trapyz = window.instanceOfThis;

  trapyz.isQueryModified = flag;
    checkCheckBoxes();  
}
function convertArrayOfObjectsToCSV(args) {
        var result,lineDelimiter, data;
        data = args.data || null;
        if (data == null || !data.length) {
            return null;
        }
        lineDelimiter = args.lineDelimiter || '\n';
        result = 'GIDS'+lineDelimiter;
        data.forEach(function(item) {
            result += item+lineDelimiter;
        });
        return result;
    }

function downloadCSV() {
  var trapyz = window.instanceOfThis;
  trapyz.ulist = trapyz.uniqueGids;  
  var ulist = trapyz.ulist;
  var data, filename, link;
  var parentElement = document.getElementById(trapyz.segmentName);
   
  var csv = convertArrayOfObjectsToCSV({
      data: ulist
  });
  while(true){
    if (csv === null && ulist.length === 0){
      cleanupQueryParameters();
      parentElement.parentNode.parentNode.getElementsByClassName("trapyz-card-loader")[0].style.display = "none";
      return;
    } 
    else{
      filename = ((trapyz.segmentName)+".csv" )|| 'export.csv';
      //if (!csv.match(/^data:text\/csv/i)) {
      //    csv = 'data:text/csv;charset=utf-8,' + csv;
      //}
	//data = encodeURI(csv);
	var blobdata = new Blob([csv],{type : 'text/csv'});

      link = document.createElement('a');
      //link.setAttribute('href', data);
      link.setAttribute("href", window.URL.createObjectURL(blobdata));

	link.setAttribute('download', filename); document.body.appendChild(link);
      link.click();
      cleanupQueryParameters();
      parentElement.parentNode.parentNode.getElementsByClassName("trapyz-card-loader")[0].style.display = "none";
      break;
    }
  }
}
function exportSegment(event){
  var trapyz = window.instanceOfThis;
  trapyz.export = true;
  toggleLoader(true);  
  trapyz.segmentName = event.target.getAttribute("val");
  var parentElement = document.getElementById(trapyz.segmentName);
  parentElement.parentNode.getElementsByClassName("trapyz-hamburger-menu")[0].style.display = "none";
  parentElement.parentNode.parentNode.getElementsByClassName("trapyz-card-loader")[0].style.display = "inline-block";
    constructQueryJSON();
}

function revertSegment(event) {
  var trapyz = window.instanceOfThis, parentElement, segmentName, segJson = {
    "segmentName" : "",
    "api" : "" 
  };

  segmentName = event.target.getAttribute("val");
  parentElement = document.getElementById(segmentName);
  parentElement.parentNode.getElementsByClassName("trapyz-hamburger-menu")[0].style.display = "none";
  parentElement.parentNode.parentNode.getElementsByClassName("trapyz-card-loader")[0].style.display = "inline-block";
  segJson.segmentName = segmentName
  segJson.api = trapyz.api;
  
  makeAjaxGetRequest("https://canvas.trapyz.com/revert-segment.php?seg=" + encodeURIComponent(JSON.stringify(segJson)), function(response) {
    makeAjaxGetRequest("https://canvas.trapyz.com/new-refresh-segment.php?api=" + window.instanceOfThis.api + "&seg=" + segmentName, function(response) {
      window.location.reload();  
    });
  });
}

function loadLatLng() {
    if(!window.instanceOfThis.props.data) {
	makeAjaxGetRequest("https://canvas.trapyz.com/getLatLngNew.php", function(response) {
            var responseJson, keys = [], latLng = [], index, latLngParts = [];
	    window.instanceOfThis.setState({latLngList: []});

	    if (response.responseText === "\n") {
		responseJson = "";
	    } else {
		responseJson = JSON.parse(response.responseText);
	    }
	    
            keys = Object.keys(responseJson);
            for (index = 0; index < keys.length; index++) {
		latLngParts = responseJson[keys[index]].split("_");
		latLng.push({ "latitude":parseFloat(latLngParts[0]), "longitude":parseFloat(latLngParts[1]),"sname":latLngParts[2]});

            }
            window.instanceOfThis.setState({latLngList: latLng});
	});  
    } else {
	window.instanceOfThis.setState({latLngList: window.instanceOfThis.props.data});
    }
}

function loadAreas(type) {
  var areasJson, keys = [], areasList = [], areaParts, areaName, cityId, selectCity, index;

  window.instanceOfThis.setState({selectedAreaItems: []});
  window.instanceOfThis.setState({selectedEditAreaItems: []});
  queryModified(true);
  if (type === "create") {
    selectCity = parseInt(document.getElementById("trapyz-city-select").value);
  } else {
    selectCity = parseInt(document.getElementById("trapyz-edit-city-select").value);
  }
  
  areasJson = window.instanceOfThis.state.areaItems;
  keys = Object.keys(areasJson);
  for (index = 0; index < keys.length; index++) {
    areaParts = areasJson[keys[index]].split("_");
    areaName = areaParts[0];
    cityId = areaParts[1];
      if (parseInt(cityId) === selectCity) {
      areasList.push(
        <option value={keys[index]}>
          {areaName}
        </option>
      );
    }
  }
  if (type === "create") {
    window.instanceOfThis.setState({selectedAreaItems: areasList});
  } else {
    window.instanceOfThis.setState({selectedEditAreaItems: areasList});
  }
}

function setArea() {
  var selectedArea;

  queryModified(true);
  selectedArea = parseInt(document.getElementById("trapyz-area-select").value);
  window.instanceOfThis.pin = selectedArea;
}

function cleanupUIElements() {
  var index, checkboxElements, optionElements;
  document.getElementById("trapyz-visit-txt").value = "";
    window.instanceOfThis.setState({uniqueCount: 0});
    window.instanceOfThis.state.paddingAngle = 0;

  checkboxElements = document.getElementsByClassName("trapyz-segment-checkbox");
  for(index = 0; index < checkboxElements.length; index++) {
    checkboxElements[index].checked = false;
  }
  optionElements = document.getElementsByClassName("trapyz-segment-radio");
  for(index = 0; index < optionElements.length; index++) {
    optionElements[index].checked = false; 
  }

  document.getElementById("trapyz-error-message-div").style.display = "none";
  document.getElementById("trapyz-edit-visit-txt").value = "";
  
    document.getElementById("trapyz-edit-error-message-div").style.display = "none";
    document.getElementsByClassName("select-all")[0].checked = false;
}

function showAdvanceEditSegment(event) {
    var trapyz = window.instanceOfThis, segmentElement,
	segmentQueryJson = {
	    "segmentName": "",
	    "segmentDesc": "",
	    "api": "",
	    "segmentCount": "",
	    "queryList": [],
	    "existingSegmentName": "",
	    "segmentVisit": ""
	};

    segmentQueryJson.api = trapyz.api;
    segmentQueryJson.existingSegmentName = event.target.getAttribute("val");
    segmentElement = document.getElementById(segmentQueryJson.existingSegmentName);
    trapyz.existingSegmentName = segmentQueryJson.existingSegmentName;
    trapyz.segmentQueryJson.existingSegmentName = segmentQueryJson.existingSegmentName;

    var segmentCardElement = segmentElement.parentNode.parentNode.parentNode.parentNode;
    
    makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentDetailsAdvance.php?api=" + segmentQueryJson.api + "&seg=" + encodeURIComponent(segmentQueryJson.existingSegmentName), function(response) {
	var responseJson, queryJson;
	responseJson = JSON.parse(response.responseText);
	queryJson = JSON.parse(responseJson["attributeJson"]);
	segmentQueryJson.queryList = queryJson.queryList;
	segmentQueryJson.segmentDesc = responseJson.description;
	segmentQueryJson.segmentName = responseJson.name;
	segmentQueryJson.segmentVisit = queryJson.segmentVisit;
	segmentQueryJson.segmentCount = segmentCardElement.getElementsByClassName("trapyz-card-count")[0].innerHTML;
	window.addEventListener("keyup",escapeEvent);
	document.getElementById("trapyz-new-advanced-page").style.display = "block";
	ReactDOM.render(<TrapyzNewAdvancedPage data={segmentQueryJson}/>, document.getElementById('trapyz-new-advanced-page'));
	window.scrollTo(0, 0);
    }, segmentQueryJson.segmentName);
}

function cleanupQueryParameters() {
  var trapyz=window.instanceOfThis;
  trapyz.visit = "";
  trapyz.city = "";
  trapyz.pin = "";
  trapyz.startDate = null;
  trapyz.endDate = null;
  trapyz.uniqueGids = {};
  trapyz.segmentName = "";
  trapyz.segmentDesc = "";
  trapyz.ulist=[];
  trapyz.export=false;
  trapyz.setState({uniqueCount: 0});
  trapyz.segmentQueryJson = {
    "segmentName": "",
    "segmentDesc": "",
    "api": "",
    "segmentCount": "",
    "queryList": [],
    "existingSegmentName": "",
    "segmentVisit": ""
  }
}

function showSaveSegmentBoxCustom(flag) {
    if(flag) {
	//if(document.getElementById('save-segment-box')) {
	    document.getElementById('save-segment-box-custom').classList.remove('hidden');
	//}
	//if(window.instanceOfThis.state.queryList.length > 0) {
	
	    
	    /*if(window.instanceOfThis.props.data) {
		document.getElementById('segment-name').value = window.instanceOfThis.props.data.segmentName;
		document.getElementById('segment-description').value = window.instanceOfThis.props.data.segmentDesc;
	    }
	} else {
	    alert('Build Query');
	} */
    } else {
	document.getElementById('save-segment-box-custom').classList.add('hidden');
	document.getElementById('segment-name').value = '';
	document.getElementById('segment-description').value ='';
    }
}

function saveSegmentCustom() {
    var queryList = window.instanceOfThis.state.queryList, name, desc, url;
    name = document.getElementById('segment-name').value;
    desc = document.getElementById('segment-description').value;
    window.instanceOfThis.segmentQueryJson.segmentName = name;
    window.instanceOfThis.segmentQueryJson.segmentDesc = desc;

    if(document.getElementById('createSegmentPanel').style.display == "block") {
	createSegment();
    } else if(document.getElementById('editSegmentPanel').style.display == "block") {
	editSegment();
    }
}

/*-----------------------------------------------------------------------------------------------Trapyz Advanced Page Methods----- Start*/


/*-----------------------------------------------------------------------------------------------Trapyz Advanced Page Methods----- End*/

class TrapyzHomePage extends Component {
  constructor (props) {
    super(props);
    this.api = sessionStorage.getItem("key");
    this.topCategoriesDone = false;
    this.segmentCardsDone = false;
    this.defaultDistanceCondition = "lt";
    this.defaultVisitCondition = "ge";
    this.defaultDistance = "100";
    this.defaultDimensionName = "cat";
    this.interestDone = false;
    this.descDone = false;
    this.export=false;
    this.citiesDone = false;
    this.isQueryModified = false;
    this.areasDone = false;
    this.customSegmentsDone = false;
    this.visit = "1";
    this.city = "";
    this.pin = "";
    this.dateType="";
    this.startDate = null;
    this.endDate = null;
    this.segmentName = "";
    this.segmentDesc = "";
    this.existingSegmentName = "";
    this.uniqueGids = {};
    this.ulist=[];
    this.queryJson = {
    "dimension": {
      "name": "",
      "value": ""
    },
    "visit": {
      "condition": "gt",
      "value": "1"  
    },
    "distance": {
      "condition": "",
      "value": ""
    },
    "duration": {
      "type": "",
      "startDate": "",
      "endDate": ""
    },
    "city": "",
    "pin": ""
  };
    this.customCardsList = [];
    this.customSegmentJson = {};
    this.segmentToDelete = "";
    this.segmentQueryJson = {
      "segmentName": "",
      "segmentDesc": "",
      "api": "",
      "segmentCount": "",
      "queryList": [],
      "existingSegmentName": "",
      "segmentVisit":""
    };
    this.username = sessionStorage.getItem("name");
    this.interestCategoryMap = {
	"Automotive":["20","8"],
	"Fashion": ["3","1","33","16","2","4","5","6","7","12","17","18","19"],
	"Beauty": ["27"],
	"Finance":["9"],
	"Education": ["34"],
	"Entertainment": ["13","40","41"],
	"F&B": ["14","15","24"],
	"Fitness": ["29","21","28","29"],
	"Home & Lifestyle": ["12","22"],
	"Leisure & Hobbies": ["31","26","35"],
	"Groceries": ["25","10"],
	"Travel": ["15","32","23","24","37"],
	"Electronics":["11"],
	"Business": ["30","36","38","39"]
    };
    this.state = {
	totalActive: 0,
	weeklyActive: 0,
	monthlyActive: 0,
	uniqueCount: 0,
	selectedCity: 0,
	queryCount: 0,
	currentQueryCount: 0,
	cityItems: [],
	cityEditItems: [],
	areaItems: {},
	selectedAreaItems: [],
	selectedEditAreaItems: [],
	top5Categories: [],
	segmentCards: [],
	interestItems: [],
	interestEditItems: [],
	customSegmentCards: [],
	descMap: {},
	latLngList: [],
	paddingAngle: 0
    }
  }
    
    componentDidMount () {
	if(!document.getElementById('googleMapApi')) {
            const script = document.createElement("script");
	    script.src = "https://maps.googleapis.com/maps/api/js?libraries=visualization&key=AIzaSyCAcSnS4ALTfkqEhlINgSfCAU3QVBUvleU";
	    script.async = true;
	    script.id = "googleMapApi";
            document.body.appendChild(script);
	}
    }
    
  render() {

    window.instanceOfThis = this;

    if (!this.descDone) {
      this.descDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentDesc.php?api=" + this.api, function(response) {
        window.instanceOfThis.setState({descMap: JSON.parse(response.responseText)});
      });
	loadLatLng();
    }

    if (!this.citiesDone) {
      this.citiesDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap?mapType=city", function(response) {
        var responseJson, keys = [], citiesList = [], index;

	if (response.responseText === "\n") {
	  responseJson = "";
	} else {
          responseJson = JSON.parse(response.responseText);
	}
	  
        keys = Object.keys(responseJson);
        for (index = 0; index < keys.length; index++) {
          citiesList.push(
            <option value={keys[index]}>
              {responseJson[keys[index]]}
            </option>
          );
        }
        window.instanceOfThis.setState({cityItems: citiesList});
        window.instanceOfThis.setState({cityEditItems: citiesList});
      });  
    }

    if (!this.areasDone) {
      this.areasDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/new-insights-getmap-city.php", function(response) {
        var responseJson, keys = [], areasList = {}, index;

	if (response.responseText === "\n") {
	  responseJson = "";
	} else {
          responseJson = JSON.parse(response.responseText);
	}
	  
        keys = Object.keys(responseJson);
        for (index = 0; index < keys.length; index++) {
          areasList[keys[index]] = responseJson[keys[index]];
        }
        window.instanceOfThis.setState({areaItems: areasList});
      });  
    }

    if (this.state.totalActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=total&api=" + this.api, function(response) {
        window.instanceOfThis.setState({totalActive: response.responseText});
      });  
    } 

    if (this.state.weeklyActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=week&api=" + this.api, function(response) {
        window.instanceOfThis.setState({weeklyActive: response.responseText});
      });  
    }

    if (this.state.monthlyActive === 0) {
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=month&api=" + this.api, function(response) {
        window.instanceOfThis.setState({monthlyActive: response.responseText});
      });  
    } 

    if (!this.interestDone) {
      var interestList = [], keys, index;
      this.interestDone = true;
      keys = Object.keys(this.interestCategoryMap);
      for (index = 0; index < keys.length; index++) {
        interestList.push(
	  <div className="trapyz-interest-checkbox" onClick={() => queryModified(true)}>
		<input type="checkbox" value={keys[index]} className={"trapyz-segment-checkbox id-" + index} name="trapyz-segment-checkbox" />
            {keys[index]}
          </div>);
      }
      this.setState({interestItems: interestList});
      this.setState({interestEditItems: interestList});
    }

    if (!this.topCategoriesDone) {
      var responseJson, sortedCategoriesResult = [], top5CategoriesList = [];
      
      this.topCategoriesDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/getPredefinedInsights.php?case=catgid&api=" + this.api, function(response) {
        var index, key;
        
        responseJson = JSON.parse(response.responseText);
        for (key in responseJson) {
          sortedCategoriesResult.push([key, responseJson[key]]);
        }
        sortedCategoriesResult.sort(function(a, b) {
          return b[1] - a[1];
        });
        for (index = 0; index < 5 && index < sortedCategoriesResult.length; index++) {
          var progressWidth = 0, styleString = "", sortedCategoryNumber = "";

          progressWidth = (sortedCategoriesResult[index][1] / sortedCategoriesResult[0][1]) * 100;

	  if(sortedCategoriesResult[index][1] >= 10000 && sortedCategoriesResult[index][1] < 1000000) {
	      sortedCategoryNumber = (Math.floor(sortedCategoriesResult[index][1]/1000)) + 'K';
	  } else if(sortedCategoriesResult[index][1] >= 1000000) {
	      sortedCategoryNumber = (Math.floor(sortedCategoriesResult[index][1]/1000000)) + 'M';
	  } else {
	    sortedCategoryNumber = sortedCategoriesResult[index][1];
	  }

          styleString = {
            width: progressWidth + "px"
          }
	    
          top5CategoriesList.push(
            <div className="trapyz-top-5-progress-content">
              <div className="trapyz-top-5-normal-heading">
                {sortedCategoriesResult[index][0]}
              </div>
              <div id="top-5-store-progress-0" className="trapyz-top-5-progress-bar" style={styleString}>
              </div>
              <div id="top-5-store-count-0" className="trapyz-top-5-normal-text">
                {sortedCategoryNumber}
              </div>
            </div>
          );
        }
        window.instanceOfThis.setState({top5Categories: top5CategoriesList});
      }); 
    }

    if (!this.segmentCardsDone) {
      var sortedSegmentsResult = [],
      segmentNameMap = {
        "Traveller": {
          "image": travellerImage
        },
        "Sports Enthusiast": {
          "image": sportImage
        },
        "Foodie": {
          "image": foodImage
        },
        "Personal Care": {
          "image": personalCareImage
        },
        "Geek": {
          "image": geekImage
        }, 
        "Auto Enthusiast": {
          "image": autoImage
        },
        "Fashionista": {
          "image": fashionImage
        },
        "Homemaker": {
          "image": homeImage
        },
        "Affluent": {
          "image": affluentImage
        },
	"Custom": {
	  "image": customImage
	}
      };

      this.segmentCardsDone = true;
      makeAjaxGetRequest("https://canvas.trapyz.com/getSegmentCardsData.php?api=" + this.api, function(response) {
        var idString = "", presetCardsList = [], customCardsList = [], key, index, imgSrc = "", cardsList = [], hamburgerMenu = [];
        responseJson = JSON.parse(response.responseText);
        for (key in responseJson) {
          sortedSegmentsResult.push([key, responseJson[key]["value"]]);
        }
        sortedSegmentsResult.sort(function(a, b) {
          return b[1] - a[1];
        });
        for (index = 0; index < sortedSegmentsResult.length; index++) {
          idString = "trapyz-card-" + index;
	    hamburgerMenu = [];
	  if (Object.keys(segmentNameMap).some(x => x === sortedSegmentsResult[index][0])) {
	      imgSrc = segmentNameMap[sortedSegmentsResult[index][0]]["image"];
	      hamburgerMenu.push(
		      <div className="trapyz-hamburger-menu">
		        <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={refreshSegment}>Refresh</div>
                        <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={showEditSegment}>Edit</div>
                        <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={revertSegment}>Revert</div>
		        <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={exportSegment}>Export to csv</div>
		        <div className="trapyz-hamburger-menu-item inactive" val={sortedSegmentsResult[index][0]}>Export to csv</div>
		        <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={showConfirmationExport}>Export to csv</div>
		      </div>
	      );    
	  } else if (responseJson[sortedSegmentsResult[index][0]]["isAdvance"] == 1) {
	      imgSrc = segmentNameMap["Custom"]["image"];
	      hamburgerMenu.push(
		      <div className="trapyz-hamburger-menu advance">
		        <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={refreshSegment}>Refresh</div>
                        <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={showAdvanceEditSegment}>Edit</div>
                        <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={showConfirmation}>Delete</div>
                        <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={exportSegment}>Export to csv</div>
		        <div className="trapyz-hamburger-menu-item inactive" val={sortedSegmentsResult[index][0]}>Export to csv</div>
		        <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={showConfirmationExport}>Export to csv</div>
		      </div>
	      );
	  } else {
	      imgSrc = segmentNameMap["Custom"]["image"];
	      hamburgerMenu.push(
		      <div className="trapyz-hamburger-menu">
		        <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={refreshSegment}>Refresh</div>
                        <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={showEditSegment}>Edit</div>
                        <div className="trapyz-hamburger-menu-item" val={sortedSegmentsResult[index][0]} onClick={showConfirmation}>Delete</div>
                        <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={exportSegment}>Export to csv</div>
		        <div className="trapyz-hamburger-menu-item inactive" val={sortedSegmentsResult[index][0]}>Export to csv</div>
		        <div className="trapyz-hamburger-menu-item hidden" val={sortedSegmentsResult[index][0]} onClick={showConfirmationExport}>Export to csv</div>
		      </div>
	      );
	  }

          if (responseJson[sortedSegmentsResult[index][0]]["type"] == "preset") {
            presetCardsList.push(
              <div className="trapyz-cards" id={idString}>
                <div className="trapyz-card-image-container">
                  <img className="trapyz-card-image" alt="" src={imgSrc}/>  
                  <div className="trapyz-card-image-tint">
                    <div className="trapyz-card-button-holder">
                      <img id={sortedSegmentsResult[index][0]} src={hamburgerImage} className="trapyz-card-button-img"  onClick={toggleHamburgerMenu}/>    
		      {hamburgerMenu}
                    </div>
                    <img className="trapyz-card-loader" alt="" src={loaderImage}/>
                  </div>
                </div>
                <div className="trapyz-card-title">
                  {sortedSegmentsResult[index][0]}
                </div>
                <div className="trapyz-card-count">
                  {sortedSegmentsResult[index][1]}
                </div>
                <div className="trapyz-card-text">
                  {window.instanceOfThis.state.descMap[sortedSegmentsResult[index][0]]}
                </div>
              </div>
            );
          } else {
            customCardsList.push(
              <div className="trapyz-cards height-auto" id={idString}>
                <div className="trapyz-card-image-container">
                  <img className="trapyz-card-image" alt="" src={imgSrc} />
                  <div className="trapyz-card-image-tint">
                    <div className="trapyz-card-button-holder">
                      <img id={sortedSegmentsResult[index][0]} src={hamburgerImage} className="trapyz-card-button-img" onClick={toggleHamburgerMenu}/>  
                      {hamburgerMenu}
                    </div>
                    <img className="trapyz-card-loader" alt="" src={loaderImage}/>
                  </div>
                </div>
                <div className="trapyz-card-title">
                  {sortedSegmentsResult[index][0]}
                </div>
                <div className="trapyz-card-count">
                  {sortedSegmentsResult[index][1]}
                </div>
                <div className="trapyz-card-text">
                  {window.instanceOfThis.state.descMap[sortedSegmentsResult[index][0]]}
                </div>
              </div>
            );
          }
        }
	    
        window.instanceOfThis.customCardsList = customCardsList;
        window.instanceOfThis.setState({segmentCards: presetCardsList});
        window.instanceOfThis.setState({customSegmentCards: customCardsList});
      });
    }

    return (
      <div className="trapyz-main-container">
        <div id="trapyz-main-tint-div" className="trapyz-main-tint">
          <img src={loaderImage} className="trapyz-loader-image" alt=""/>
        </div>
	
        <div className="trapyz-menu-panel">
          <div className="trapyz-main-logo">
            <img className="trapyz-logo-img" alt="" src={trapyzLogo}/>
          </div>
          <div className="trapyz-client-logo">
            <img className="trapyz-client-logo-img" alt="" src={dailyHuntLogo}/>
          </div>
          <div className="trapyz-unique-users-container">
            <div className="trapyz-total-users">
              <div id="total-active" className="trapyz-user-number">{this.state.totalActive}</div>
              Total<br/>Installed
            </div>
            <div className="trapyz-weekly-users">
              <div id="weekly-active" className="trapyz-user-number">{this.state.weeklyActive}</div>
              Weekly<br/>Active
            </div>
            <div className="trapyz-monthly-users">
              <div id="monthly-active" className="trapyz-user-number">{this.state.monthlyActive}</div>
              Monthly<br/>Active
            </div>
          </div>
          <div className="trapyz-segments-header">
            <img className="trapyz-segments-header-icon" alt="" src={segmentIcon}/>
            <div className="trapyz-segments-heading-container">
              <div className="trapyz-bold-heading">
                Segments
              </div>
              <div className="trapyz-normal-heading">
                Curated In-market segments
              </div>
            </div>
          </div>
          
          <div id="top-5-categories" className="trapyz-top-5-box">
            <div className="trapyz-top-5-bold-heading">
              Top 5 Visited Categories
            </div>
            {this.state.top5Categories}
          </div>
        </div>
        <div className="trapyz-canvas-panel">
	  <div className="trapyz-confirmation-outer-box">
            <div className="trapyz-confirmation-inner-box">
              Are you sure you want to delete this segment &#63;
              <div className="trapyz-confirmation-button-holder">
                <div className="trapyz-confirmation-ok-button" onClick={deleteSegment}>
                  Yes
                </div>
                <div className="trapyz-confirmation-cancel-button" onClick={hideConfirmation}>
                  No
                </div>
              </div>
            </div>
          </div>

	  <div className="trapyz-confirmation-export-outer-box">
            <div className="trapyz-confirmation-inner-box">
              <p>Export is disabled for this account.</p>
              <div className="trapyz-confirmation-button-holder">
                <div className="trapyz-confirmation-cancel-button" onClick={hideConfirmationExport}>
                  OK
                </div>
              </div>
            </div>
          </div>
	    
          <div className="trapyz-user-panel">
            <div className="trapyz-user-detail-container">
              <div className="trapyz-map-logo">
                <img className="trapyz-map-logo-image" src={trapyzMapLogo} alt="" onClick={showMapPage} title="Store Map"/>
              </div>
	      <div className="trapyz-user-image">
                <img className="trapyz-user-profile-image" src={trapyzLogo2} alt="" onClick={showHomePage} title="Home"/>
              </div>
              <div className="trapyz-user-text">
                {this.username}
              </div>
              <div className="trapyz-logout-image-container" onClick={logout}>
                <img className="trapyz-logout-image" src={chevronDown} alt="" title="Log Out"/>
              </div>
            </div>
          </div>
          <div className="trapyz-filter-panel">
            <div className="trapyz-filter-heading">
              Segments
            </div>
	    <div className="trapyz-create-segment-button" onClick={showAdvancedPage}>
              CUSTOM SEGMENTS
            </div>
            <div className="trapyz-create-segment-button" onClick={showCreateSegment}>
              INTEREST SEGMENTS
            </div>
          </div>
          <hr className="trapyz-hor-rule"></hr>
          <div className="trapyz-cards-panel">
            {this.state.segmentCards}
	    {this.state.customSegmentCards}
          </div>
        </div>
        <div id="editSegmentPanel" className="trapyz-edit-segment-panel-container" onClick={hideEditSegment}>
          <div className="trapyz-edit-segment-panel" onClick={preventDefaultAction}>
            <div className="trapyz-edit-segment-heading">
              Edit Segment
            </div>
            <hr className="trapyz-edit-segment-hor-rule"></hr>
            <div className="trapyz-segment-filter-div">
              <div className="trapyz-interest-div">
                <div className="trapyz-segment-filter-heading">
                  Interest Attributes
                </div>
                <div className="trapyz-segment-filter-desc">
                  Choose from one or many interests categories that describe your audience 
                </div>
                <div id="trapyz-edit-interest-elements" className="trapyz-segment-interest-elements">
	          <div className="select-all-checkboxes">
                    <input type="checkbox" className="select-all-edit" onClick={selectAllCheckBoxesEdit}/>
                    <span>Select All</span>
	          </div>
                  {this.state.interestEditItems}
                </div>
              </div>
              <div className="trapyz-period-div">
                <div className="trapyz-segment-filter-heading">
                  Visit Attributes
                </div>
                <div className="trapyz-segment-filter-desc">
                  Select and configure parameters that define visits by your audience
                </div>
                <div id="trapyz-edit-segment-date-elements" className="trapyz-segment-date-elements">
                  <div className="trapyz-visit-frequency-container">
                    <div className="trapyz-visit-label">
                      Minimum visits
                    </div>
                    <input id="trapyz-edit-visit-txt" type="text" placeholder="visits" className="trapyz-visit-txt" onKeyUp={() => queryModified(true)}/>
                  </div>
                  <div className="trapyz-date-last-month">
                    <input id="trapyz-edit-month-select" className="trapyz-segment-radio" type="radio" name="date" value="month" onChange={parseDateOption} /> Last Month
                  </div>
	          <div className="trapyz-date-last-week">
                    <input id="trapyz-edit-month-2-select" className="trapyz-segment-radio" type="radio" name="date" value="month2" onChange={parseDateOption}/> Last 60 days
                  </div>
	          <div className="trapyz-date-last-week">
                    <input id="trapyz-edit-month-3-select" className="trapyz-segment-radio" type="radio" name="date" value="month3" onChange={parseDateOption}/> Last 90 days
                  </div>
                  <div className="trapyz-date-range">
                    <input id="trapyz-edit-custom-select" className="trapyz-segment-radio" type="radio" name="date" value="custom" onChange={parseDateOption}/> Custom Range<br/>
                    <div className="trapyz-date-input-container">
                      <span className="trapyz-date-label">From:&nbsp;</span><input id="trapyz-edit-start-date" className="trapyz-date-input" type="date" placeholder="Start Date" onChange = {() => queryModified(true)}/>
                    </div>
                    <div id="trapyz-date-input-container" className="trapyz-date-input-container">
                      <span className="trapyz-date-label">To:&nbsp;</span><input id="trapyz-edit-end-date" className="trapyz-date-input" type="date" placeholder="End Date" onChange = {() => queryModified(true)}/>
                    </div>
                  </div>
                </div>
              </div>
	      <div className="trapyz-pie-chart-div">
                <div className="trapyz-segment-filter-heading">Segment Size</div>
	        <section>
	          <div className="total-count">
	            <span className="color"></span>
	            <div className="content">
	              <span className="count">{window.instanceOfThis.state.totalActive}</span>
	              <span>Total Installed</span>
	            </div>
	          </div>
	          <div className="unique-gids">
	            <span className="color"></span>
	            <div className="content">
	              <span className="count">{window.instanceOfThis.state.uniqueCount}</span>
	              <span>Segment Count</span>
	            </div>
	          </div>
	        </section>
	        <section className="pieChart">
	          <PieChart
	            data={[
		      { title: 'Unique Gids', value: parseInt(window.instanceOfThis.state.uniqueCount), color: '#F5A523' },
                      { title: 'Total Count', value: (parseInt(window.instanceOfThis.state.totalActive) - parseInt(window.instanceOfThis.state.uniqueCount)), color: '#498FE1' },
                    ]}
	            paddingAngle={window.instanceOfThis.state.paddingAngle}
	            animate={true}
	            animationDuration={1500}
                  />
	        </section>
	      </div>
            </div>
            <hr className="trapyz-create-segment-hor-rule"></hr>
	    <section className="trapyz-segment-operations-buttons">
              <div className="trapyz-segment-show-count-button" onClick={() => showCount("edit")}>
                SHOW COUNT
              </div>
              <div className="trapyz-segment-button-container">
                <button className="trapyz-segment-create-button" onClick={() => showSaveSegmentBoxCustom(true)}>
                  SAVE SEGMENT
                </button>
	        <div className="trapyz-segment-cancel-button" onClick={hideEditSegment}>
                  CANCEL
                </div>
                <div id="trapyz-edit-error-message-div" className="trapyz-error-message">
                  Required atleast one filter!
                </div>
              </div>
            </section>
	  </div>
        </div>
	    
        <div id="createSegmentPanel" className="trapyz-create-segment-panel-container" onClick={hideCreateSegment}>
          <div className="trapyz-create-segment-panel" onClick={preventDefaultAction}>
            <div className="trapyz-create-segment-heading">
              Create Segment
            </div>
            <hr className="trapyz-create-segment-hor-rule"></hr>
            <div className="trapyz-segment-filter-div">
	      <div className="trapyz-interest-div">
                <div className="trapyz-segment-filter-heading">
                  Interest Attributes
                </div>
                <div className="trapyz-segment-filter-desc">
                  Choose from one or many interests categories that describe your audience 
                </div>
                <div id="trapyz-interest-elements" className="trapyz-segment-interest-elements">
                  <div className="select-all-checkboxes">
                    <input type="checkbox" className="select-all" onClick={selectAllCheckBoxes}/>
                    <span>Select All</span>
	          </div>
	          {this.state.interestItems}
                </div>
              </div>
              <div className="trapyz-period-div">
                <div className="trapyz-segment-filter-heading">
                  Visit Attributes
                </div>
                <div className="trapyz-segment-filter-desc">
                  Select and configure parameters that define visits by your audience
                </div>
                <div className="trapyz-segment-date-elements">
                  <div className="trapyz-visit-frequency-container">
                    <div className="trapyz-visit-label">
                      Minimum visits
                    </div>
                    <input id="trapyz-visit-txt" type="text" placeholder="visits" className="trapyz-visit-txt" onChange = {() => queryModified(true)}/>
                  </div>
                  <div className="trapyz-date-last-month">
                    <input id="trapyz-month-select" className="trapyz-segment-radio" type="radio" name="date" value="month" onChange={parseDateOption}/> Last Month
                  </div>
                  <div className="trapyz-date-last-week">
                    <input id="trapyz-month-2-select" className="trapyz-segment-radio" type="radio" name="date" value="month2" onChange={parseDateOption}/> Last 60 days
                  </div>  
	          <div className="trapyz-date-last-week">
                    <input id="trapyz-month-3-select" className="trapyz-segment-radio" type="radio" name="date" value="month3" onChange={parseDateOption}/> Last 90 days
                  </div>
                  <div className="trapyz-date-range">
                    <input id="trapyz-custom-select" className="trapyz-segment-radio" type="radio" name="date" value="custom" onChange={parseDateOption}/> Custom Range<br/>
                    <div className="trapyz-date-input-container">
                      <span className="trapyz-date-label">From:&nbsp;</span><input id="trapyz-start-date" className="trapyz-date-input" type="date" placeholder="Start Date" onChange = {() => queryModified(true)}/>
                    </div>
                    <div id="trapyz-date-input-container" className="trapyz-date-input-container">
                      <span className="trapyz-date-label">To:&nbsp;</span><input id="trapyz-end-date" className="trapyz-date-input" type="date" placeholder="End Date" onChange = {() => queryModified(true)}/>
                    </div>
                  </div>
                </div>
              </div>
	      <div className="trapyz-pie-chart-div">
                <div className="trapyz-segment-filter-heading">Segment Size</div>
	        <section>
	          <div className="total-count">
	            <span className="color"></span>
	            <div className="content">
	              <span className="count">{window.instanceOfThis.state.totalActive}</span>
	              <span>Total Installed</span>
	            </div>
	          </div>
	          <div className="unique-gids">
	            <span className="color"></span>
	            <div className="content">
	              <span className="count">{window.instanceOfThis.state.uniqueCount}</span>
	              <span>Segment Count</span>
	            </div>
	          </div>
	        </section>
	        <section className="pieChart">
	          <PieChart
	              data={[
			{ title: 'Unique Gids', value: parseInt(window.instanceOfThis.state.uniqueCount), color: '#F5A523' },
                        { title: 'Total Count', value: (parseInt(window.instanceOfThis.state.totalActive) - parseInt(window.instanceOfThis.state.uniqueCount)), color: '#498FE1' },
                      ]}
	              paddingAngle={window.instanceOfThis.state.paddingAngle}
	              animate={true}
	              animationDuration={1500}
                  />
	        </section>
	      </div>
            </div>
            <hr className="trapyz-create-segment-hor-rule"></hr>
            <section className="trapyz-segment-operations-buttons">
	      <div className="trapyz-segment-show-count-button" onClick={() => showCount("create")}>
                SHOW COUNT
              </div>
              <div className="trapyz-segment-button-container">
                <button className="trapyz-segment-create-button" onClick={() => showSaveSegmentBoxCustom(true)}>
                  SAVE SEGMENT
                </button>
	        <div className="trapyz-segment-cancel-button" onClick={() => cleanupUIElements()}>
                  CLEAR
                </div>
              </div>
	    </section>
	    <div id="trapyz-error-message-div" className="trapyz-error-message">
              Required atleast one filter!
            </div>
          </div>
        </div>
	<section id="save-segment-box-custom" className="trapyz-save-segment-box hidden">
	  <header><span>Save Segment</span></header>
	  <section>
	    <div>
	      <label>Segment Name</label>
	      <input type="text" id="segment-name"/>
	    </div>
	    <div>
	      <label>Description</label>
	      <textarea id="segment-description"/>
	    </div>
	  </section>
	  <section className="trapyz-save-button-box">
	    <button id="save-segment" className="select" onClick={() => saveSegmentCustom()}>SAVE</button>
	    <button className="clear" onClick={() => showSaveSegmentBoxCustom(false)}>CANCEL</button>
	  </section>
	</section>

	<div id="trapyz-new-advanced-page" className="trapyz-create-segment-panel-container">
	    
	</div>
      </div>
    );
  }
}

export default TrapyzHomePage;
