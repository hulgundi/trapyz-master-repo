from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, MapAttribute
)
import json
import shutil
import pymysql
import concurrent.futures
import threading
lock = threading.Lock()

option = "mcount"
cat = "27"
subcat = "127"
apikey = "18_"
visits = -1
visitCondition = "eq"
startDate = 1516924806000
endDate = 1522029606000

db = pymysql.connect(host='172.31.0.193', user='pwx', passwd='ciscoplanet', db='trapyz_beta')
table = "MasterRecordSet"

def filterRecords(resultJson):
    visitFlag = False
    dateFlag = False

    if visits == -1:
        visitFlag = True
    else:
        if visitCondition == "lt":
            if resultJson["info"]["visits"] < visits:
                visitFlag = True
        elif visitCondition == "gt":
            if resultJson["info"]["visits"] > visits:
                visitFlag = True
        elif visitCondition == "eq":
            if resultJson["info"]["visits"] == visits:
                visitFlag = True
        elif visitCondition == "le":
            if resultJson["info"]["visits"] <= visits:
                visitFlag = True
        elif visitCondition == "ge":
            if resultJson["info"]["visits"] >= visits:
                visitFlag = True
        else:
            print("Invalid Filter Condition")

    if startDate == -1 or endDate == -1:
        dateFlag = True
    else:
        if int(resultJson["info"]["created"]) >= startDate and int(resultJson["info"]["created"]) <= endDate:
            dateFlag = True
            
    if visitFlag == True and dateFlag == True:
        print(json.dumps(resultJson))


def processResponse(responseObject):
    resultJson = {}
    resultJson["gid"] = responseObject.gid
    resultJson["cat"] = responseObject.categoryId
    resultJson["subcat"] = responseObject.subcategoryId
    resultJson["store"] = responseObject.storeUuid
    resultJson["api"] = responseObject.apiKey_date_hour_min 
    resultJson["info"] = {}
    resultJson["info"]["visits"] = responseObject.info.visits
    resultJson["info"]["distance"] = responseObject.info.distance
    resultJson["info"]["created"] = responseObject.info.created_at
    resultJson["info"]["updated"] = responseObject.info.updated_at
    filterRecords(resultJson)

def getRecordsByCategory(catid):
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        for data in ProcessedUserData.rate_limited_scan(filter_condition = (ProcessedUserData.categoryId == catid)):
            processResponse(data)

def getRecordsBySubCategory(subcatid):
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        for data in ProcessedUserData.rate_limited_scan(filter_condition = (ProcessedUserData.subcategoryId == subcatid)):
            processResponse(data)

def getRecordsByApikey(apikey):
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        for data in ProcessedUserData.rate_limited_scan(filter_condition = (ProcessedUserData.apiKey_date_hour_min.startswith(apikey))):
            processResponse(data)

class ProcessedUserData(Model):
    
    class Meta:
        table_name = "Tempdb"
    gid = UnicodeAttribute(null=True)
    date_hour_min_store_api = UnicodeAttribute(null=False)
    apiKey_date_hour_min = UnicodeAttribute(null=False)
    categoryId = UnicodeAttribute(null=False)
    info = MapAttribute(null=False)
    storeUuid = UnicodeAttribute(null=False)
    subcategoryId = UnicodeAttribute(null=False)


if option == "count":
    print(ProcessedUserData.count(hash_key=None, range_key_condition=None, filter_condition = (ProcessedUserData.categoryId == cat) & (ProcessedUserData.subcategoryId == subcat)))
elif option == "mcount":
    # field = "city"
    # fieldList = {}
    # cursor = db.cursor()
    # sql = "select distinct(" + field + ") from " + table
    # try:
    #     cursor.execute(sql)
    #     for row in cursor.fetchall():
    #         fieldList[row[0]] = 0
    #     print(fieldList)
    # except:
    #     print("Exception under mcount")
    #     db.rollback()
    #getRecordsByCategory(cat)
    getRecordsByApikey(apikey)
elif option == "ulist":
    print("ulist")
elif option == "slist":
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        for data in ProcessedUserData.rate_limited_scan(filter_condition = (ProcessedUserData.categoryId == cat) & (ProcessedUserData.subcategoryId == subcat)):
            resultJson = {}
            resultJson["gid"] = data.gid
            resultJson["cat"] = data.categoryId
            resultJson["subcat"] = data.subcategoryId
            resultJson["store"] = data.storeUuid
            resultJson["info"] = {}
            resultJson["info"]["visits"] = data.info.visits
            resultJson["info"]["distance"] = data.info.distance
            resultJson["info"]["created"] = data.info.created_at
            resultJson["info"]["updated"] = data.info.updated_at
            print(json.dumps(resultJson))
    executor.shutdown()
else:
    print("Invalid Option")    
